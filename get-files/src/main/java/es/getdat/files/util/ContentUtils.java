package es.getdat.files.util;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import javax.ws.rs.core.MultivaluedMap;

import org.jboss.logging.Logger;

public abstract class ContentUtils
{

   static Logger logger = Logger.getLogger(ContentUtils.class);

   public static String extract(MultivaluedMap<String, String> headers, String headerName, String headerValue)
   {
      String[] contentDisposition = headers.getFirst(headerName).split(";");

      for (String part : contentDisposition)
      {
         if ((part.trim().startsWith(headerValue)))
         {

            String[] name = part.split("=");

            String finalFileName = sanitizeFilename(name[1]);
            return finalFileName;
         }
      }
      return null;
   }

   public static String sanitizeFilename(String s)
   {
      return s.trim().replaceAll("\"", "");
   }

   public static byte[] getBytes(InputStream inputStream)
   {
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      try
      {
         byte[] buffer = new byte[1024];
         int read = 0;
         while ((read = inputStream.read(buffer, 0, buffer.length)) != -1)
         {
            baos.write(buffer, 0, read);
         }
         baos.flush();
         return baos.toByteArray();
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return null;
      }
      finally
      {
         try
         {
            baos.close();
         }
         catch (Exception e)
         {
         }
      }
   }
}
