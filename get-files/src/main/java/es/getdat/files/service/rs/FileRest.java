package es.getdat.files.service.rs;

import java.io.InputStream;

import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.jboss.logging.Logger;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import es.getdat.api.model.ResourceUpload;
import es.getdat.files.management.AppConstants;
import es.getdat.files.util.ContentUtils;

@Path(es.getdat.core.management.AppConstants.FILES_PATH)
@Stateless
@LocalBean
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.MULTIPART_FORM_DATA)
public class FileRest implements Serializable {

	protected final Logger logger = Logger.getLogger(getClass().getName());

	private static final long serialVersionUID = 1L;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addFile(ResourceUpload resourceUpload) {
		return null;
	}

	@POST
	@Path("/{accountId}")
	@Consumes("multipart/form-data")
	public Response addFile(@PathParam("accountId") String accoutId,
			MultipartFormDataInput input, @Context UriInfo uriInfo,
			@Context ServletContext servletContext) {
		String filename = null;
		try {

			Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
			List<InputPart> inputParts = uploadForm
					.get(AppConstants.FILES_UPLOADED_FILE_PARAMETER_NAME);

			InputPart inputPart = inputParts.get(0);
			MultivaluedMap<String, String> headers = inputPart.getHeaders();
			filename = ContentUtils.extract(headers,
					AppConstants.FILES_CONTENT_DISPOSITION_HEADER,
					AppConstants.FILES_UPLOADED_FILE_PARAMETER_NAME);
			if (filename == null) {
				filename = ContentUtils.extract(headers,
						AppConstants.FILES_CONTENT_DISPOSITION_HEADER, "name");
			}

			InputStream inputStream = inputPart
					.getBody(InputStream.class, null);

			byte[] bytes = ContentUtils.getBytes(inputStream);

			String repositoryPath = servletContext.getRealPath("files");
			String extension = filename.substring(filename.indexOf("."));
			String uid = UUID.randomUUID().toString();
			String finalName = uid + extension;
			logger.info("finalName:" + finalName);
			if (!Files.exists(Paths.get(repositoryPath, accoutId),
					LinkOption.NOFOLLOW_LINKS)) {
				Files.createDirectory(Paths.get(repositoryPath, accoutId));
			}
			Files.write(Paths.get(repositoryPath, accoutId, finalName), bytes);
			String publicUrl = uriInfo.getAbsolutePath().getScheme() + "://"
					+ uriInfo.getAbsolutePath().getHost() + ":"
					+ uriInfo.getAbsolutePath().getPort() + "/files/"
					+ accoutId + "/" + finalName;
			return Response.status(Response.Status.OK).entity(publicUrl)
					.build();

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return Response
					.status(Response.Status.INTERNAL_SERVER_ERROR)
					.entity("Error storing resource '" + filename
							+ "' for accoutId: " + accoutId).build();
		}
	}

	public static void main(String[] args) {
		String uid = UUID.randomUUID().toString();
		String filename = "prova.txt";
		String extension = filename.substring(filename.indexOf("."));
		String finalName = uid + extension;

		System.out.println(finalName);
	}
}
