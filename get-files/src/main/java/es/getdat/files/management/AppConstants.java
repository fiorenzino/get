package es.getdat.files.management;

public class AppConstants
{

   public static final String FILES_UPLOADED_FILE_PARAMETER_NAME = "filename";
   public static final String FILES_CONTENT_DISPOSITION_HEADER = "Content-Disposition";

}
