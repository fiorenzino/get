package es.getdat.templates.model.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ProcessTemplateSearch implements Serializable
{

   private static final long serialVersionUID = 1L;

   private List<String> allowedChannelTypes;
   private List<String> allowedPluginDefinitionIds;

   public List<String> getAllowedChannelTypes()
   {
      if (allowedChannelTypes == null)
      {
         allowedChannelTypes = new ArrayList<>();
      }
      return allowedChannelTypes;
   }

   public void setAllowedChannelTypes(List<String> allowedChannelTypes)
   {
      this.allowedChannelTypes = allowedChannelTypes;
   }

   public List<String> getAllowedPluginDefinitionIds()
   {
      if (allowedPluginDefinitionIds == null)
      {
         allowedPluginDefinitionIds = new ArrayList<>();
      }
      return allowedPluginDefinitionIds;
   }

   public void setAllowedPluginDefinitionIds(List<String> allowedPluginDefinitionIds)
   {
      this.allowedPluginDefinitionIds = allowedPluginDefinitionIds;
   }

}
