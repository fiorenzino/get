package es.getdat.templates.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.GenericGenerator;

@Entity
@XmlRootElement
public class StepTemplate implements Serializable
{

   public static final long serialVersionUID = 1L;

   private String id;

   private String name;

   /**
    * step identifier to be replaced by an actual step id
    */
   private String stepId;

   /**
    * optional, to further restrict available implementing plugins
    */
   private String pluginDefinitionId;

   private String channelType;

   private int order;

   /**
    * may we split this into pre- and post-conditions?
    */
   private String condition;

   /**
    * legacy steps must be invoked synchronously to ensure results are written to its corresponding content step out
    * values
    */
   private boolean legacy;

   /**
    * among a set of concurrent steps, we avoid checking that an async step has been executed right before we advance to
    * the next level steps
    * 
    * the chosen 'async' name is infamous , anyway, in order to identify this type of behaviour
    */
   private boolean async;

   private String referencedStepIds;

   private ProcessTemplate processTemplate;

   public StepTemplate()
   {
      super();
   }

   public StepTemplate(String name, String stepId, String pluginDefinitionId, String channelType)
   {
      super();
      this.name = name;
      this.stepId = stepId;
      this.pluginDefinitionId = pluginDefinitionId;
      this.channelType = channelType;
   }

   @Id
   @GeneratedValue(generator = "uuid")
   @GenericGenerator(name = "uuid", strategy = "uuid2")
   @Column(name = "uuid", unique = true)
   public String getId()
   {
      return id;
   }

   public void setId(String id)
   {
      this.id = id;
   }

   public String getStepId()
   {
      return stepId;
   }

   public void setStepId(String stepId)
   {
      this.stepId = stepId;
   }

   public String getPluginDefinitionId()
   {
      return pluginDefinitionId;
   }

   public void setPluginDefinitionId(String pluginDefinitionId)
   {
      this.pluginDefinitionId = pluginDefinitionId;
   }

   public String getChannelType()
   {
      return channelType;
   }

   public void setChannelType(String channelType)
   {
      this.channelType = channelType;
   }

   @Column(name = "stepOrder")
   public int getOrder()
   {
      return order;
   }

   public void setOrder(int order)
   {
      this.order = order;
   }

   @Column(name = "stepCondition", length = 10000)
   public String getCondition()
   {
      return condition;
   }

   public void setCondition(String condition)
   {
      this.condition = condition;
   }

   public boolean isLegacy()
   {
      return legacy;
   }

   public void setLegacy(boolean legacy)
   {
      this.legacy = legacy;
   }

   public boolean isAsync()
   {
      return async;
   }

   public void setAsync(boolean async)
   {
      this.async = async;
   }

   @Override
   public String toString()
   {
      return "StepTemplate [stepId=" + stepId + ", pluginDefinitionId=" + pluginDefinitionId
               + ", channelType=" + channelType + ", order=" + order + ", condition=" + condition + ", legacy="
               + legacy + ", async=" + async + "]";
   }

   public String getReferencedStepIds()
   {
      return referencedStepIds;
   }

   public void setReferencedStepIds(String referencedStepIds)
   {
      this.referencedStepIds = referencedStepIds;
   }

   public StepTemplate addReferencedStepId(String stepId)
   {
      if (this.referencedStepIds == null)
      {
         referencedStepIds = stepId;
      }
      else
      {
         referencedStepIds += ";" + stepId;
      }
      return this;
   }

   @Transient
   @XmlTransient
   public List<String> getReferencedStepIdsAsList()
   {
      return referencedStepIds == null ? new ArrayList<String>() : Arrays.asList(referencedStepIds.split(";"));
   }

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   @ManyToOne
   @XmlTransient
   public ProcessTemplate getProcessTemplate()
   {
      return processTemplate;
   }

   public void setProcessTemplate(ProcessTemplate processTemplate)
   {
      this.processTemplate = processTemplate;
   }

}
