package es.getdat.templates.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.GenericGenerator;

@Entity
@XmlRootElement
public class ProcessTemplate implements Serializable
{

   private static final long serialVersionUID = 1L;

   private String id;
   private String name;
   public List<StepTemplate> steps;

   @Id
   @GeneratedValue(generator = "uuid")
   @GenericGenerator(name = "uuid", strategy = "uuid2")
   @Column(unique = true)
   public String getId()
   {
      return id;
   }

   public void setId(String id)
   {
      this.id = id;
   }

   @OneToMany(cascade = CascadeType.ALL, mappedBy = "processTemplate", fetch = FetchType.EAGER)
   public List<StepTemplate> getSteps()
   {
      if (steps == null)
      {
         steps = new ArrayList<>();
      }
      return steps;
   }

   public void setSteps(List<StepTemplate> steps)
   {
      this.steps = steps;
   }

   @XmlTransient
   @Transient
   public int getLevels()
   {
      int levels = 0;
      for (StepTemplate step : getSteps())
      {
         if (step.getOrder() > levels)
         {
            levels = step.getOrder();
         }
      }
      return levels;
   }

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public ProcessTemplate add(StepTemplate stepTemplate)
   {
      int levels = getLevels() + 1;
      stepTemplate.setOrder(levels);
      getSteps().add(stepTemplate);
      stepTemplate.setProcessTemplate(this);
      return this;
   }

   public ProcessTemplate add(StepTemplate... stepTemplates)
   {
      int levels = getLevels() + 1;
      for (StepTemplate stepTemplate : stepTemplates)
      {
         stepTemplate.setOrder(levels);
         getSteps().add(stepTemplate);
         stepTemplate.setProcessTemplate(this);
      }
      return this;
   }

}
