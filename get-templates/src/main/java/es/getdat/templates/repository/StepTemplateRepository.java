package es.getdat.templates.repository;

import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.getdat.api.repository.AbstractRepository;
import es.getdat.api.repository.Search;
import es.getdat.templates.model.StepTemplate;

@Stateless
@LocalBean
public class StepTemplateRepository extends AbstractRepository<StepTemplate>
{

   private static final long serialVersionUID = 1L;

   @PersistenceContext
   EntityManager em;

   @Override
   protected EntityManager getEm()
   {
      return em;
   }

   @Override
   public void setEm(EntityManager em)
   {
      this.em = em;
   }

   @Override
   protected String getDefaultOrderBy()
   {
      return "id";
   }

   @Override
   protected void applyRestrictions(Search<StepTemplate> search, String alias,
            String separator, StringBuffer sb, Map<String, Object> params)
   {
      // id
      if (search.getObj().getId() != null && !search.getObj().getId().trim().isEmpty())
      {
         sb.append(separator).append(alias)
                  .append(".id = :id ");
         params.put("id", search.getObj().getId());
         separator = " and ";
      }

      // name
      if (search.getObj().getName() != null && !search.getObj().getName().trim().isEmpty())
      {
         sb.append(separator).append(" upper ( ").append(alias)
                  .append(".name ) like :name ");
         params.put("name", likeParam(search.getObj().getName().trim().toUpperCase()));
         separator = " and ";
      }
   }

}
