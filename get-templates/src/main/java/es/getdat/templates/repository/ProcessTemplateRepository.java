package es.getdat.templates.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import es.getdat.api.repository.AbstractRepository;
import es.getdat.api.repository.Search;
import es.getdat.templates.model.ProcessTemplate;

@Stateless
@LocalBean
public class ProcessTemplateRepository extends AbstractRepository<ProcessTemplate>
{

   private static final long serialVersionUID = 1L;

   @PersistenceContext
   EntityManager em;

   @Override
   protected EntityManager getEm()
   {
      return em;
   }

   @Override
   public void setEm(EntityManager em)
   {
      this.em = em;
   }

   @Override
   protected String getDefaultOrderBy()
   {
      return "id asc";
   }

   @Override
   protected void applyRestrictions(Search<ProcessTemplate> search, String alias,
            String separator, StringBuffer sb, Map<String, Object> params)
   {
      // id
      if (search.getObj().getId() != null && !search.getObj().getId().trim().isEmpty())
      {
         sb.append(separator).append(alias)
                  .append(".id = :id ");
         params.put("id", search.getObj().getId());
         separator = " and ";
      }

      // name
      if (search.getObj().getName() != null && !search.getObj().getName().trim().isEmpty())
      {
         sb.append(separator).append(" upper ( ").append(alias)
                  .append(".name ) like :name ");
         params.put("name", likeParam(search.getObj().getName().trim().toUpperCase()));
         separator = " and ";
      }
   }

   @SuppressWarnings("unchecked")
   public List<ProcessTemplate> allowed(List<String> allowedChannelTypes, List<String> allowedPluginDefinitionIds)
   {
      try
      {
         String query = "select p from " + ProcessTemplate.class.getSimpleName() + " p where p.id not in ( ";
         query += " select pp.id from " + ProcessTemplate.class.getSimpleName() + " pp left join pp.steps s ";
         String separator = " where ";
         Map<String, Object> params = new HashMap<String, Object>();
         if (allowedChannelTypes != null && allowedChannelTypes.size() > 0)
         {
            query += separator + " s.channelType not in ( :allowedChannelTypes ) ";
            params.put("allowedChannelTypes", allowedChannelTypes);
            separator = " or ";
         }
         if (allowedPluginDefinitionIds != null && allowedPluginDefinitionIds.size() > 0)
         {
            query += separator + " s.pluginDefinitionId not in ( :allowedPluginDefinitionIds ) ";
            params.put("allowedPluginDefinitionIds", allowedPluginDefinitionIds);
            separator = " or ";
         }
         query += " ) ";
//         logger.info(query);
         Query q = getEm().createQuery(query);
         for (String key : params.keySet())
         {
            q.setParameter(key, params.get(key));
         }
         return (List<ProcessTemplate>) q.getResultList();
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return new ArrayList<>();
      }
   }
}
