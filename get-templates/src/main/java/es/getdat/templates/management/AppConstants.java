package es.getdat.templates.management;

public class AppConstants extends es.getdat.core.management.AppConstants
{

   public static final String TEMPLATES_PATH = "/v1/templates";
   public static final String STEPS_PATH = "/v1/steps";

}
