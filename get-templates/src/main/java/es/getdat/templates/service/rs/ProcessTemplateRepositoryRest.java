package es.getdat.templates.service.rs;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import es.getdat.api.model.PaginatedListWrapper;
import es.getdat.api.service.GetRepositoryService;
import es.getdat.templates.management.AppConstants;
import es.getdat.templates.model.ProcessTemplate;
import es.getdat.templates.model.StepTemplate;
import es.getdat.templates.model.pojo.ProcessTemplateSearch;
import es.getdat.templates.repository.ProcessTemplateRepository;

@Path(AppConstants.TEMPLATES_PATH)
@Stateless
@LocalBean
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProcessTemplateRepositoryRest extends GetRepositoryService<ProcessTemplate>
{

   private static final long serialVersionUID = 1L;

   public ProcessTemplateRepositoryRest()
   {
      super();
   }

   @Inject
   public ProcessTemplateRepositoryRest(ProcessTemplateRepository repository)
   {
      super(repository);
   }

   @POST
   @Path("/allowed")
   public Response allowed(ProcessTemplateSearch search) throws Exception
   {
      logger.info("@POST /allowed");
      try
      {
         List<ProcessTemplate> allowed = ((ProcessTemplateRepository) getRepository()).allowed(
                  search.getAllowedChannelTypes(), search.getAllowedPluginDefinitionIds());
         PaginatedListWrapper<ProcessTemplate> wrapper = new PaginatedListWrapper<>();
         wrapper.setList(allowed);
         wrapper.setListSize(allowed.size());
         wrapper.setStartRow(0);
         return Response.status(Status.OK).entity(wrapper).build();
      }
      catch (Exception e)
      {
         return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                  .entity("Error when querying for: " + search).build();
      }
   }

   @Override
   protected void prePersist(ProcessTemplate processTemplate) throws Exception
   {
      for ( StepTemplate stepTemplate : processTemplate.getSteps() ) {
         stepTemplate.setProcessTemplate(processTemplate);
      }
      super.prePersist(processTemplate);
   }

}
