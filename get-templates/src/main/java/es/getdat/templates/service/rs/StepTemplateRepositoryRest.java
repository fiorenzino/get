package es.getdat.templates.service.rs;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import es.getdat.api.service.GetRepositoryService;
import es.getdat.templates.management.AppConstants;
import es.getdat.templates.model.StepTemplate;
import es.getdat.templates.repository.StepTemplateRepository;

@Path(AppConstants.STEPS_PATH)
@Stateless
@LocalBean
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class StepTemplateRepositoryRest extends GetRepositoryService<StepTemplate>
{

   private static final long serialVersionUID = 1L;

   public StepTemplateRepositoryRest()
   {
      super();
   }

   @Inject
   public StepTemplateRepositoryRest(StepTemplateRepository repository)
   {
      super(repository);
   }

}
