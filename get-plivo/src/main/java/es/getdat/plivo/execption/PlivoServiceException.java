package es.getdat.plivo.execption;

public class PlivoServiceException extends Exception
{

   private static final long serialVersionUID = 1L;

   public PlivoServiceException(String message)
   {
      super(message);
   }

}
