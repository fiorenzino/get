package es.getdat.plivo.management;

public class AppConstants extends es.getdat.core.management.AppConstants
{

   public static final String PLIVO_ACCOUNTNAME = "plivoAccountName";
   public static final String PLIVO_AUTHID = "plivoAuthId";
   public static final String PLIVO_AUTHTOKEN = "plivoAuthToken";
   public static final String PLIVO_NUMBER = "plivoNumber";

   public static final String PLIVO_SMS_DST = "plivoSmsDst";
   public static final String PLIVO_SMS_TEXT = "plivoSmsText";
   public static final String PLIVO_SMS_URL = "plivoSmsUrl";

   public static final String PLIVO_BUZZ_TO = "plivoBuzzTo";

   public static final String PLIVO_CALL_TO = "plivoCallTo";
   public static final String PLIVO_CALL_MSG = "plivoCallMsg";
   public static final String PLIVO_CALL_FILE = "plivoCallFile";

   public static final String PLIVO_CALL_ANSWER_URL = "plivoCallAnswerUrl";
   public static final String PLIVO_CALL_FALLBACK_URL = "plivoCallFallbackUrl";
   public static final String PLIVO_CALL_HANGUP_URL = "plivoCallHangupUrl";

   public static final String PLIVO_CALL_EXTERNAL_UID = "RequestUUID";
   public static final String PLIVO_SMS_EXTERNAL_UID = "MessageUUID";

   public static final String PLIVO_SMS_STATUS = "Status";
   public static final String PLIVO_SMS_STATUS_SENT = "delivered";
   public static final String PLIVO_SMS_STATUS_FAILED = "failed";

   // Indicates the status of the call.
   // `ringing`, `in-progress`,or `completed`. In case the
   // call is hung up, the CallStatus is set to `completed` for inbound calls and to `completed`, `busy`, `failed`,
   // `timeout` or `no-answer` for outbound calls.
   public static final String PLIVO_BUZZ_STATUS = "CallStatus";
   public static final String PLIVO_BUZZ_STATUS_SENT = "completed;busy;no-answer;timeout";
   public static final String PLIVO_BUZZ_STATUS_FAILED = "failed";

   public static final String PLIVO_CALL_STATUS = "CallStatus";
   public static final String PLIVO_CALL_STATUS_SENT = "completed";
   public static final String PLIVO_CALL_STATUS_FAILED = "busy;no-answer;timeout;failed";

}
