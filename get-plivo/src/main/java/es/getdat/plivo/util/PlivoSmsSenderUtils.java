package es.getdat.plivo.util;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.jboss.logging.Logger;
import org.plivo.ee.helper.api.MessageApi;
import org.plivo.ee.helper.api.configuration.GlobalConstant;
import org.plivo.ee.helper.api.pojo.ApiResponse;
import org.plivo.ee.helper.api.util.Authenticator;

import es.getdat.api.model.Content;
import es.getdat.core.management.AppProperties;
import es.getdat.plivo.execption.PlivoServiceException;
import es.getdat.plivo.management.AppConstants;
import es.getdat.util.CallAutocloseableUtils;
import es.getdat.util.ContentUtils;
import es.getdat.util.DateUtils;

public class PlivoSmsSenderUtils
{

   static String SERVICE_NAME = "PLIVO_SMS";

   private static final long serialVersionUID = 1L;

   static Logger logger = Logger.getLogger(PlivoSmsSenderUtils.class.getName());

   public static void executeAndUpdateEventDetails(Content content) throws Exception
   {
      String eventId = content.get(AppConstants.EVENT_ID);
      String stepId = content.get(AppConstants.STEP_ID);
      String when = DateUtils.getPrecisionStringDate();
      Map<String, String> logs = new HashMap<String, String>();
      try
      {
         // logging
         logger.info(content);

         // check arguments
         String dst = content.getStepValuesIn(stepId).get(AppConstants.PLIVO_SMS_DST);
         String text = content.getStepValuesIn(stepId).get(AppConstants.PLIVO_SMS_TEXT);
         String number = content.getStepValuesIn(stepId).get(AppConstants.PLIVO_NUMBER);
         String authId = content.getStepValuesIn(stepId).get(AppConstants.PLIVO_AUTHID);
         String authToken = content.getStepValuesIn(stepId).get(AppConstants.PLIVO_AUTHTOKEN);
         String accountName = content.getStepValuesIn(stepId).get(AppConstants.PLIVO_ACCOUNTNAME);
         String url = content.getStepValuesIn(stepId).get(AppConstants.PLIVO_SMS_URL);
         logs.put(AppConstants.PLIVO_AUTHID, authId);
         logs.put(AppConstants.PLIVO_AUTHTOKEN, authToken);
         logs.put(AppConstants.PLIVO_ACCOUNTNAME, accountName);
         logs.put(AppConstants.PLIVO_NUMBER, number);
         logs.put(AppConstants.PLIVO_SMS_DST, dst);
         logs.put(AppConstants.PLIVO_SMS_TEXT, text);
         logs.put(AppConstants.PLIVO_SMS_URL, url);
         if (dst == null || dst.trim().isEmpty())
         {
            throw new PlivoServiceException("failed to send sms to: " + dst);
         }

         // execution infos
         String externalId = null;
         Boolean success = null;
         String info = null;

         // biz logic
         if (AppProperties.test.value(false) != null && !AppProperties.test.cast(Boolean.class))
         {
            Client client = ClientBuilder.newClient().register(new Authenticator(authId, authToken));
            MessageApi messageApi = new MessageApi(client);
            messageApi.put(GlobalConstant.AUTH_ID, authId)
                     .put(GlobalConstant.SRC, number)
                     .put(GlobalConstant.DST, dst)
                     .put(GlobalConstant.TEXT, text)
                     .put(GlobalConstant.TYPE, "sms")
                     .put(GlobalConstant.URL, url + "?eventId=" + eventId + "&stepId=" + stepId)
                     .put(GlobalConstant.METHOD, "POST")
                     .put(GlobalConstant.LOG, "true");
            ApiResponse result = messageApi.sendMessage();
            if (result != null && result.message_uuid != null && result.message_uuid.size() > 0)
            {
               externalId = result.message_uuid.get(0);
               // we wait for the callback
               info = "waiting for callback";
               success = null;
               logs.put("result", result.toString());
               logger.info("msgId: " + externalId + " using account: " + accountName);
            }
            else
            {
               success = false;
               info = "request failed";
            }
         }
         else
         {
            externalId = UUID.randomUUID().toString();
            success = true;
            info = "test: immediate success, no callback";
         }

         // handle intemrediate result
         Map<String, String> outs = new HashMap<>();
         if (success != null && success.equals(Boolean.TRUE))
         {
            outs.put(AppConstants.EXECUTED, success.toString());
         }
         else
         {
            outs.put(AppConstants.PREPARED, "true");
         }
         Content delta = ContentUtils.details(eventId, stepId, when, SERVICE_NAME, "executeAndUpdateEventDetails",
                  externalId, info,
                  content, outs, logs);
         CallAutocloseableUtils.call(null, AppConstants.API_PATH + AppConstants.ROUTER_PATH, delta);

      }
      catch (Exception e)
      {
         logs.put("exception", e.getClass().getCanonicalName());
         logs.put("message", e.getMessage());
         throw e;
      }
      finally
      {
         CallAutocloseableUtils.logs(null, AppConstants.API_PATH
                  + es.getdat.core.management.AppConstants.OPERATIONS_PATH + "/parameters",
                  eventId,
                  stepId, when, SERVICE_NAME, logs);
      }
   }

   public static void logAndReturnToRouter(Map<String, String> logs) throws Exception
   {
      String when = DateUtils.getPrecisionStringDate();
      String eventId = null;
      String stepId = null;
      try
      {
         // check callback infos
         eventId = logs.get(AppConstants.EVENT_ID);
         stepId = logs.get(AppConstants.STEP_ID);
         if (eventId == null || eventId.trim().isEmpty())
         {
            throw new PlivoServiceException("failed to extract eventId from sms callback");
         }
         if (stepId == null || stepId.trim().isEmpty())
         {
            throw new PlivoServiceException("failed to extract stepId from sms callback");
         }

         // execution infos
         String externalId = logs.get(AppConstants.PLIVO_SMS_EXTERNAL_UID);
         Boolean success = null;
         String info = null;

         // biz logic
         String status = logs.get(AppConstants.PLIVO_SMS_STATUS);
         if (AppConstants.PLIVO_SMS_STATUS_FAILED.equals(status))
         {
            success = false;
         }
         else if (AppConstants.PLIVO_SMS_STATUS_SENT.equals(status))
         {
            success = true;
         }

         // handle result to return to router
         Content content = new Content();
         content.put(AppConstants.EVENT_ID, eventId);
         content.put(AppConstants.STEP_ID, stepId);
         Map<String, String> outs = new HashMap<>();
         ContentUtils.result(eventId, stepId, when, SERVICE_NAME, "logAndReturnToRouter", externalId, info, success,
                  content, outs, logs);

         // back to router
         if (success != null)
         {
            CallAutocloseableUtils.call(null, AppConstants.API_PATH + AppConstants.ROUTER_PATH, content);
         }
      }
      catch (Exception e)
      {
         logs.put("exception", e.getClass().getCanonicalName());
         logs.put("message", e.getMessage());
      }
      finally
      {
         CallAutocloseableUtils.logs(null, AppConstants.API_PATH
                  + es.getdat.core.management.AppConstants.OPERATIONS_PATH + "/parameters",
                  eventId,
                  stepId, when, SERVICE_NAME, logs);
      }
   }
}
