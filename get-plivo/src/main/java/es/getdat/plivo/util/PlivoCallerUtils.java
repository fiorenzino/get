package es.getdat.plivo.util;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.jboss.logging.Logger;
import org.plivo.ee.helper.api.CallApi;
import org.plivo.ee.helper.api.configuration.GlobalConstant;
import org.plivo.ee.helper.api.pojo.ApiResponse;
import org.plivo.ee.helper.api.util.Authenticator;

import es.getdat.api.model.Content;
import es.getdat.core.management.AppProperties;
import es.getdat.plivo.execption.PlivoServiceException;
import es.getdat.plivo.management.AppConstants;
import es.getdat.util.CallAutocloseableUtils;
import es.getdat.util.ContentUtils;
import es.getdat.util.DateUtils;

public class PlivoCallerUtils
{

   static String SERVICE_NAME = "PLIVO_CALL";

   static Logger logger = Logger.getLogger(PlivoCallerUtils.class.getName());

   public static void executeAndUpdateEventDetails(Content content) throws Exception
   {
      String eventId = content.get(AppConstants.EVENT_ID);
      String stepId = content.get(AppConstants.STEP_ID);
      String when = DateUtils.getPrecisionStringDate();
      Map<String, String> logs = new HashMap<String, String>();

      // execution infos
      String externalId = null;
      Boolean success = false;
      String info = null;

      try
      {
         // logging
         logger.info(content);

         // check arguments
         String plivoCallTo = content.getStepValuesIn(stepId).get(AppConstants.PLIVO_CALL_TO);
         String plivoCallAnswerUrl = content.getStepValuesIn(stepId)
                  .get(AppConstants.PLIVO_CALL_ANSWER_URL);
         String plivoCallFallbackUrl = content.getStepValuesIn(stepId)
                  .get(AppConstants.PLIVO_CALL_FALLBACK_URL);
         String plivoCallHangupUrl = content.getStepValuesIn(stepId)
                  .get(AppConstants.PLIVO_CALL_HANGUP_URL);
         String plivoCallFile = content.getStepValuesIn(stepId).get(AppConstants.PLIVO_CALL_FILE);
         String plivoCallMsg = content.getStepValuesIn(stepId).get(AppConstants.PLIVO_CALL_MSG);
         String number = content.getStepValuesIn(stepId).get(AppConstants.PLIVO_NUMBER);
         String authId = content.getStepValuesIn(stepId).get(AppConstants.PLIVO_AUTHID);
         String authToken = content.getStepValuesIn(stepId).get(AppConstants.PLIVO_AUTHTOKEN);
         String accountName = content.getStepValuesIn(stepId).get(AppConstants.PLIVO_ACCOUNTNAME);
         logs
                  .put(AppConstants.PLIVO_CALL_TO, plivoCallTo);
         logs.put(AppConstants.PLIVO_AUTHID, authId);
         logs.put(AppConstants.PLIVO_AUTHTOKEN, authToken);
         logs.put(AppConstants.PLIVO_ACCOUNTNAME, accountName);
         logs.put(AppConstants.PLIVO_NUMBER, number);
         logs.put(AppConstants.PLIVO_CALL_ANSWER_URL,
                  plivoCallAnswerUrl);
         logs.put(AppConstants.PLIVO_CALL_FALLBACK_URL,
                  plivoCallFallbackUrl);
         logs.put(AppConstants.PLIVO_CALL_HANGUP_URL,
                  plivoCallHangupUrl);
         logs.put(AppConstants.WHEN, DateUtils.getPrecisionStringDate());
         logs.put(AppConstants.SERVICE_NAME, SERVICE_NAME);
         if (plivoCallMsg != null && !plivoCallMsg.isEmpty())
         {
            logs.put(AppConstants.PLIVO_CALL_MSG,
                     plivoCallMsg);
         }
         if (plivoCallFile != null && !plivoCallFile.isEmpty())
         {
            logs.put(AppConstants.PLIVO_CALL_FILE,
                     plivoCallFile);
         }
         if (plivoCallTo == null || plivoCallTo.trim().isEmpty())
         {
            // TODO - test here if we have all needed params
            throw new PlivoServiceException("failed to perform buzz to: " + plivoCallTo);
         }

         // biz logic
         if (AppProperties.test.value(false) != null && !AppProperties.test.cast(Boolean.class))
         {
            Client client = ClientBuilder.newClient().register(new Authenticator(authId, authToken));
            CallApi callApi = new CallApi(client);
            callApi.put(GlobalConstant.AUTH_ID, authId)
                     .put(GlobalConstant.FROM, number)
                     .put(GlobalConstant.TO, plivoCallTo)
                     .put(GlobalConstant.ANSWER_URL, plivoCallAnswerUrl + "?eventId=" + eventId + "&stepId=" + stepId)
                     .put(GlobalConstant.ANSWER_METHOD, "POST")
                     .put(GlobalConstant.HANGUP_URL, plivoCallHangupUrl + "?eventId=" + eventId + "&stepId=" + stepId)
                     .put(GlobalConstant.HANGUP_METHOD, "POST")
                     .put(GlobalConstant.FALLBACK_URL,
                              plivoCallFallbackUrl + "?eventId=" + eventId + "&stepId=" + stepId)
                     .put(GlobalConstant.FALLBACK_METHOD, "POST");
            ApiResponse result = callApi.makeOutboundCall();
            if (result != null && result.request_uuid != null && result.request_uuid.length() > 0)
            {
               externalId = result.request_uuid;
               // we wait for the callback
               info = "waiting for callback";
               success = null;
               logs.put("result", result.toString());
               logger.info("callId: " + externalId + " using accountName:"
                        + accountName);
            }
            else
            {
               success = false;
               info = "request failed";
            }
         }
         else
         {
            externalId = UUID.randomUUID().toString();
            success = true;
            info = "test: immediate success, no callback";
         }

         // handle intemrediate result
         Map<String, String> outs = new HashMap<>();
         if (success != null && success.equals(Boolean.TRUE))
         {
            outs.put(AppConstants.EXECUTED, success.toString());
         }
         else
         {
            outs.put(AppConstants.PREPARED, "true");
         }
         Content delta = ContentUtils.details(eventId, stepId, when, SERVICE_NAME, "executeAndUpdateEventDetails()",
                  externalId, info, content, outs, logs);
         CallAutocloseableUtils.call(null, AppConstants.API_PATH + AppConstants.ROUTER_PATH, delta);
      }
      catch (Exception e)
      {
         logs.put("exception", e.getClass().getCanonicalName());
         logs.put("message", e.getMessage());
         throw e;
      }
      finally
      {
         CallAutocloseableUtils.logs(null, AppConstants.API_PATH
                  + es.getdat.core.management.AppConstants.OPERATIONS_PATH + "/parameters",
                  eventId,
                  stepId, when, SERVICE_NAME, logs);
      }
   }

   public static void logAndReturnToRouter(Map<String, String> logs, String callbackMethod) throws Exception
   {
      String when = DateUtils.getPrecisionStringDate();
      String eventId = null;
      String stepId = null;
      try
      {
         // check callback infos
         eventId = logs.get(AppConstants.EVENT_ID);
         stepId = logs.get(AppConstants.STEP_ID);
         if (eventId == null || eventId.trim().isEmpty())
         {
            throw new PlivoServiceException("failed to extract eventId from sms callback");
         }
         if (stepId == null || stepId.trim().isEmpty())
         {
            throw new PlivoServiceException("failed to extract stepId from sms callback");
         }

         // execution infos
         Boolean success = null;
         String info = null;

         // biz logic
         String externalId = logs.get(AppConstants.PLIVO_CALL_EXTERNAL_UID);
         String status = logs.get(AppConstants.PLIVO_CALL_STATUS);
         if (AppConstants.PLIVO_CALL_STATUS_FAILED.contains(status))
         {
            success = false;
         }
         else if (AppConstants.PLIVO_CALL_STATUS_SENT.contains(status))
         {
            success = true;
         }

         // handle result to return to router
         Content content = new Content();
         content.put(AppConstants.EVENT_ID, eventId);
         content.put(AppConstants.STEP_ID, stepId);
         Map<String, String> outs = new HashMap<>();
         ContentUtils.result(eventId, stepId, when, SERVICE_NAME, "logAndReturnToRouter()", externalId,
                  info, success, content, outs, logs);

         // back to router
         if (success != null)
         {
            CallAutocloseableUtils.call(null, AppConstants.API_PATH + AppConstants.ROUTER_PATH, content);
         }
      }
      catch (Exception e)
      {
         logs.put("exception", e.getClass().getCanonicalName());
         logs.put("message", e.getMessage());
      }
      finally
      {
         CallAutocloseableUtils.logs(null, AppConstants.API_PATH
                  + es.getdat.core.management.AppConstants.OPERATIONS_PATH + "/parameters",
                  eventId,
                  stepId, when, SERVICE_NAME, logs);
      }
   }
}
