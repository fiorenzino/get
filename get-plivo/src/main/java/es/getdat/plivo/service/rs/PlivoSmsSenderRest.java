package es.getdat.plivo.service.rs;

import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.getdat.api.model.Content;
import es.getdat.api.service.GetService;
import es.getdat.plivo.util.LoggerCallUtils;
import es.getdat.plivo.util.PlivoSmsSenderUtils;

@Path("/v1/plivo/sms")
@Stateless
@LocalBean
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PlivoSmsSenderRest extends GetService
{

   private static final long serialVersionUID = 1L;

   @POST
   @Asynchronous
   public void execute(Content content)
   {
      try
      {
         PlivoSmsSenderUtils.executeAndUpdateEventDetails(content);
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
   }

   @POST
   @GET
   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
   @Produces(MediaType.APPLICATION_XML)
   @Path("/url")
   public Response url(@Context HttpServletRequest httpServletRequest) throws Exception
   {
      logger.info("plivo sms url executing");
      PlivoSmsSenderUtils.logAndReturnToRouter(LoggerCallUtils.log(httpServletRequest));
      org.plivo.ee.helper.xml.Response responseXml = new org.plivo.ee.helper.xml.Response();
      return Response
               .ok()
               .entity(responseXml).build();
   }

}
