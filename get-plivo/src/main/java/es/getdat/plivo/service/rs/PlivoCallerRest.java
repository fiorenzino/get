package es.getdat.plivo.service.rs;

import java.util.Map;

import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.plivo.ee.helper.api.util.XmlUtils;
import org.plivo.ee.helper.xml.Play;
import org.plivo.ee.helper.xml.Speak;
import org.plivo.ee.helper.xml.type.Language;
import org.plivo.ee.helper.xml.type.Voice;

import es.getdat.api.model.Content;
import es.getdat.api.service.GetService;
import es.getdat.plivo.management.AppConstants;
import es.getdat.plivo.util.LoggerCallUtils;
import es.getdat.plivo.util.PlivoCallerUtils;
import es.getdat.util.CallAutocloseableUtils;

@Path("/v1/plivo/call")
@Stateless
@LocalBean
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PlivoCallerRest extends GetService
{

   private static final long serialVersionUID = 1L;

   @Inject
   PlivoCallerUtils plivoCallerService;

   @POST
   @Asynchronous
   public void execute(Content content)
   {
      try
      {
         PlivoCallerUtils.executeAndUpdateEventDetails(content);
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
   }

   @POST
   @GET
   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
   @Produces(MediaType.APPLICATION_XML)
   @Path("/answer")
   public Response answer(@Context HttpServletRequest httpServletRequest) throws Exception
   {
      logger.info("plivo call answer executing");
      Map<String, String> logs = LoggerCallUtils.log(httpServletRequest);
      String eventId = logs.get(AppConstants.EVENT_ID);
      String stepId = logs.get(AppConstants.STEP_ID);
      logger.info("eventId: " + eventId + ",stepId:" + stepId);
      String msg = null;
      String file = null;
      try
      {
         Content content = CallAutocloseableUtils.content(null, AppConstants.API_PATH + AppConstants.EVENTDETAILS_PATH
                  + "/events/{eventId}/steps/{stepId}/content", eventId, stepId);
         logger.info(content);
         msg = content.getStepValuesIn(stepId).get(AppConstants.PLIVO_CALL_MSG);
         file = content.getStepValuesIn(stepId).get(AppConstants.PLIVO_CALL_FILE);
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
      }
      org.plivo.ee.helper.xml.Response responseXml = new org.plivo.ee.helper.xml.Response();
      if (msg != null && !msg.trim().isEmpty())
      {
         responseXml.speak = new Speak(XmlUtils.escape(msg));
         responseXml.speak.voice = Voice.WOMAN;
         responseXml.speak.language = Language.Italian;
      }
      else if (file != null && !file.trim().isEmpty())
      {
         responseXml.play = new Play(file);
      }
      else
      {
         responseXml.speak = new Speak("Ciao, non riesco a leggere il msg memorizzato.");
         responseXml.speak.voice = Voice.WOMAN;
         responseXml.speak.language = Language.Italian;
      }

      PlivoCallerUtils.logAndReturnToRouter(logs, "answer");
      logger.info("---------------------------");
      return Response
               .ok()
               .entity(responseXml).build();
   }

   @POST
   @GET
   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
   @Produces(MediaType.APPLICATION_XML)
   @Path("/fallback")
   public Response fallback(@Context HttpServletRequest httpServletRequest)
   {
      logger.info("plivo call fallback executing");
      try
      {
         PlivoCallerUtils.logAndReturnToRouter(LoggerCallUtils.log(httpServletRequest), "fallback");
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      org.plivo.ee.helper.xml.Response responseXml = new org.plivo.ee.helper.xml.Response();
      responseXml.hangUp();
      return Response
               .ok()
               .entity(responseXml).build();
   }

   @POST
   @GET
   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
   @Produces(MediaType.APPLICATION_XML)
   @Path("/hangup")
   public Response hangup(@Context HttpServletRequest httpServletRequest)
   {
      logger.info("plivo call hangup executing");
      try
      {
         PlivoCallerUtils.logAndReturnToRouter(LoggerCallUtils.log(httpServletRequest), "hangup");
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      org.plivo.ee.helper.xml.Response responseXml = new org.plivo.ee.helper.xml.Response();
      responseXml.hangUp();
      return Response
               .ok()
               .entity(responseXml).build();
   }
}