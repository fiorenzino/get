package es.getdat.plivo.service.rs;

import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.getdat.api.model.Content;
import es.getdat.api.service.GetService;
import es.getdat.plivo.util.LoggerCallUtils;
import es.getdat.plivo.util.PlivoBuzzUtils;

@Path("/v1/plivo/buzz")
@Stateless
@LocalBean
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PlivoBuzzRest extends GetService
{

   private static final long serialVersionUID = 1L;

   @POST
   @Asynchronous
   public void execute(Content content)
   {
      try
      {
         PlivoBuzzUtils.executeAndUpdateEventDetails(content);
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
   }

   @POST
   @GET
   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
   @Produces(MediaType.APPLICATION_XML)
   @Path("/answer")
   public Response answer(@Context HttpServletRequest httpServletRequest)
   {
      logger.info("plivo buzz answer executing");
      try
      {
         PlivoBuzzUtils.logAndReturnToRouter(LoggerCallUtils.log(httpServletRequest), "answer");
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      org.plivo.ee.helper.xml.Response responseXml = new org.plivo.ee.helper.xml.Response();
      responseXml.hangUp();
      return Response
               .ok()
               .entity(responseXml).build();
   }

   @POST
   @GET
   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
   @Produces(MediaType.APPLICATION_XML)
   @Path("/fallback")
   public Response fallback(@Context HttpServletRequest httpServletRequest)
   {
      logger.info("plivo buzz fallback executing");
      try
      {
         PlivoBuzzUtils.logAndReturnToRouter(LoggerCallUtils.log(httpServletRequest), "fallback");
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      org.plivo.ee.helper.xml.Response responseXml = new org.plivo.ee.helper.xml.Response();
      return Response
               .ok()
               .entity(responseXml).build();
   }

   @POST
   @GET
   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
   @Produces(MediaType.APPLICATION_XML)
   @Path("/hangup")
   public Response hangup(@Context HttpServletRequest httpServletRequest)
   {
      logger.info("plivo buzz hangup executing");
      try
      {
         PlivoBuzzUtils.logAndReturnToRouter(LoggerCallUtils.log(httpServletRequest), "hangup");
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      org.plivo.ee.helper.xml.Response responseXml = new org.plivo.ee.helper.xml.Response();
      return Response
               .ok()
               .entity(responseXml).build();
   }

}
