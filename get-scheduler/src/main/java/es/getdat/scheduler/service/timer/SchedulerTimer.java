package es.getdat.scheduler.service.timer;

import java.io.Serializable;
import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.getdat.api.model.Content;
import es.getdat.core.management.AppConstants;
import es.getdat.newrouter.service.rs.RouterRest;

@Stateless
@LocalBean
public class SchedulerTimer implements Serializable
{

   private static final long serialVersionUID = 1L;

   Logger logger = Logger.getLogger(getClass().getName());

   @Resource
   TimerService timerService;

   @Inject
   RouterRest routerRest;

   public void createTimer(Content content, Date when)
   {
      try
      {
         timerService.createSingleActionTimer(when, new TimerConfig(content,
                  true));
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
      }
   }

   @Timeout
   public void timeout(Timer timer)
   {
      try
      {
         Content content = (Content) timer.getInfo();
         logger.info("timeout execution: " + new Date() + " " + content.get(AppConstants.EVENT_ID));
         boolean direct = false;
         executeNow(content, direct);
      }
      catch (Throwable e)
      {
         logger.error(e.getMessage(), e);
      }
   }

   public void executeNow(Content content, boolean direct) throws Exception
   {
      if (direct)
      {
         logger.warn("direct execution: " + new Date() + " " + content);
      }
      routerRest.executeAsync(content);
      // CallUtils.call(AppProperties.baseUrl.value()
      // + AppConstants.API_PATH + AppConstants.ROUTER_PATH, content);
   }

}
