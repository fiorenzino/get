package es.getdat.scheduler.service.rs;

import java.util.Date;
import java.util.TimeZone;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;

import es.getdat.api.model.Content;
import es.getdat.core.management.AppConstants;
import es.getdat.events.model.Event;
import es.getdat.events.service.EventService;
import es.getdat.scheduler.service.timer.SchedulerTimer;
import es.getdat.util.DateUtils;

@Path(AppConstants.SCHEDULER_PATH)
@Stateless
@LocalBean
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class SchedulerRest
{

   protected final Logger logger = Logger.getLogger(getClass().getName());

   @Inject
   SchedulerTimer schedulerTimer;

   @Inject
   EventService eventService;

   @GET
   @Path("/up")
   public Response up()
   {
      return Response.status(Response.Status.OK).entity(true).build();
   }

   @POST
   public boolean executeSync(Content content) throws Exception
   {
      try
      {
         logger.info(content);

         // when gets normalized to local date and timezone, but we track the original timezone and dst to show the
         // users's
         // "when" if needed (e.g.: gui masks)
         String _timezone = content.get(AppConstants.TIMEZONE);
         String _dst = content.get(AppConstants.DST);
         String _when = content.get(AppConstants.WHEN);
         if (_timezone != null)
         {
            _when = DateUtils.toLocalDateAsString(_when, _timezone, _dst);
            content.put(AppConstants.WHEN, _when);
         }
         else
         {
            content.put(AppConstants.TIMEZONE, TimeZone.getDefault().getDisplayName());
         }

         String accountId = content.get(AppConstants.ACCOUNT_ID);
         if (accountId == null || accountId.trim().isEmpty())
         {
            logger.info(" the account id is empty - i can't execute");
            return false;
         }

         Date when = DateUtils.getDateFromString(content.get(AppConstants.WHEN));
         if (when.before(new Date()))
         {
            logger.info(" the event is passed - i can't execute");
            return false;
         }
         else
         {
            // I MUST CREATE THE EVENT TO CONTROLL
            Event event = eventService.persistEventFromContent(content);
            if (event != null)
            {
               content.put(AppConstants.EVENT_ID, event.getId());
            }
            // CallUtils.newEvent(content, eventTarget);
            // IF THE TIME IS ESPIRED (WHEN) IN CREATING THE EVENT
            if (when.before(new Date()))
            {
               boolean direct = true;
               schedulerTimer.executeNow(content, direct);
            }
            else
            {
               schedulerTimer.createTimer(content, when);
            }
            return true;
         }
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         // return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
         // .entity("Error executing with content: " + content).build();
         return false;
      }
   }

   @POST
   @Path("/immediate")
   public Response executeImmediate(Content content)
   {
      try
      {
         logger.info(content);

         // when gets normalized to local date and timezone, but we track the original timezone and dst to show the
         // users's
         // "when" if needed (e.g.: gui masks)
         String _timezone = content.get(AppConstants.TIMEZONE);
         String _dst = content.get(AppConstants.DST);
         String _when = content.get(AppConstants.WHEN);
         if (_timezone != null)
         {
            _when = DateUtils.toLocalDateAsString(_when, _timezone, _dst);
            content.put(AppConstants.WHEN, _when);
         }
         else
         {
            content.put(AppConstants.TIMEZONE, TimeZone.getDefault().getDisplayName());
         }

         String accountId = content.get(AppConstants.ACCOUNT_ID);
         if (accountId == null || accountId.trim().isEmpty())
         {
            logger.info(" the account id is empty - i can't execute");
            return Response
                     .status(Response.Status.INTERNAL_SERVER_ERROR)
                     .entity(" the account id is empty - i can't execute: ")
                     .build();
         }

         Date when = DateUtils.getDateFromString(content.get(AppConstants.WHEN));
         if (when.before(new Date()))
         {
            logger.info(" the event is passed - i can't execute");
            return Response
                     .status(Response.Status.INTERNAL_SERVER_ERROR)
                     .entity("the event is passed - i can't execute: "
                              + content.get(AppConstants.WHEN))
                     .build();
         }
         else
         {
            // I MUST CREATE THE EVENT TO CONTROLL
            Event event = eventService.persistEventFromContent(content);
            if (event != null)
            {
               content.put(AppConstants.EVENT_ID, event.getId());
            }
            // CallUtils.newEvent(content, eventTarget);
            // IF THE TIME IS ESPIRED (WHEN) IN CREATING THE EVENT
            if (when.before(new Date()))
            {
               boolean direct = true;
               schedulerTimer.executeNow(content, direct);
            }
            else
            {
               schedulerTimer.createTimer(content, when);
            }
            return Response.status(Response.Status.NO_CONTENT)
                     .entity("Reponse: " + content.toString()).build();
         }

      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                  .entity("Error executing with content: " + content).build();
      }

   }

   @GET
   @Path("/test")
   public Response test(@QueryParam("when") String when, @QueryParam("timezone") String timezone)
   {
      try
      {
         String localDateAsString = DateUtils.getStringFromDate(new Date(), TimeZone.getDefault().getID());
         String localTimezone = TimeZone.getDefault().getID();

         String clientDateAsString = when;
         String clientTimezone = timezone;

         // NO
         // DateUtils.getStringFromDate(
         // DateUtils.getDateFromStringAndTimezome(when, clientTimezone), localTimezone)
         //
         // NO
         // DateUtils.getStringFromDate(
         // DateUtils.getDateFromStringAndTimezome(when, localTimezone), localTimezone)
         //
         // NO
         // DateUtils.getStringFromDate(
         // DateUtils.getDateFromStringAndTimezome(when, clientTimezone), clientTimezone)
         //
         // SI
         // String clientDateHere = DateUtils.getStringFromDate(
         // DateUtils.getDateFromStringAndTimezome(when, clientTimezone), localTimezone);

         String clientDateHere = DateUtils.toLocalDateAsString(clientDateAsString, clientTimezone, null);

         StringBuffer sb = new StringBuffer();
         sb.append("local time: ").append(localDateAsString).append(" (").append(localTimezone)
                  .append(") | ");
         sb.append("client time: ").append(clientDateAsString).append(" (").append(clientTimezone).append(" ) | ");
         sb.append("client time (here): ").append(clientDateHere).append(" (")
                  .append(localTimezone).append(") ");
         return Response.status(Response.Status.OK).entity(sb.toString()).build();
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                  .entity("un recognized pattern yyyy-MM-dd'T'HH:mm:ss for " + when + " or timezone for " + timezone)
                  .build();
      }
   }

}
