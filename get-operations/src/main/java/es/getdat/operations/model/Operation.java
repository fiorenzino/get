package es.getdat.operations.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Operation implements Serializable
{
   // id, idEvent, idStep, name, value (tra cui serviceName, externalUuid?)
   private static final long serialVersionUID = 1L;

   private Long id;
   private String idEvent;
   private String idStep;
   private String name;
   private String value;
   private Date executionDate;
   private String timezone;
   private String dst;

   public Operation()
   {
   }

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   public Long getId()
   {
      return id;
   }

   public void setId(Long id)
   {
      this.id = id;
   }

   public String getIdEvent()
   {
      return idEvent;
   }

   public void setIdEvent(String idEvent)
   {
      this.idEvent = idEvent;
   }

   public String getIdStep()
   {
      return idStep;
   }

   public void setIdStep(String idStep)
   {
      this.idStep = idStep;
   }

   @Column(length = 2048)
   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   @Column(length = 2048)
   public String getValue()
   {
      return value;
   }

   public void setValue(String value)
   {
      this.value = value;
   }

   @Temporal(TemporalType.TIMESTAMP)
   public Date getExecutionDate()
   {
      return executionDate;
   }

   public void setExecutionDate(Date executionDate)
   {
      this.executionDate = executionDate;
   }

   public String getTimezone()
   {
      return timezone;
   }

   public void setTimezone(String timezone)
   {
      this.timezone = timezone;
   }

   public String getDst()
   {
      return dst;
   }

   public void setDst(String dst)
   {
      this.dst = dst;
   }

   @Override
   public String toString()
   {
      return "Operation [id=" + id + ", idEvent=" + idEvent + ", idStep=" + idStep + ", name=" + name + ", value="
               + value + ", executionDate=" + executionDate + ", timezone=" + timezone + ", dst=" + dst + "]";
   }

}
