package es.getdat.operations.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jboss.logging.Logger;

import es.getdat.api.model.Content;
import es.getdat.core.management.AppConstants;
import es.getdat.operations.model.Operation;

public class OperationUtils
{

   static final Logger logger = Logger.getLogger(OperationUtils.class.getName());

   public static List<Operation> fromContent(Content content) throws Exception
   {
      List<Operation> operations = new ArrayList<>();
      String stepId = content.get(AppConstants.STEP_ID);
      String eventId = content.get(AppConstants.EVENT_ID);
      String timezone = content.get(AppConstants.TIMEZONE);
      String dst = content.get(AppConstants.DST);

      Map<String, String> parameters = content.getStepValuesIn(stepId);
      if (stepId.equals(eventId))
      {
         logger.info("ROUTER EVENT");
      }
      for (String key : parameters.keySet())
      {
         Operation operation = new Operation();
         operation.setIdEvent(eventId);
         operation.setIdStep(stepId);
         operation.setName(key);
         operation.setValue(parameters.get(key));
         operation.setTimezone(timezone);
         operation.setDst(dst);
         operation.setExecutionDate(new Date());
         operations.add(operation);
      }
      return operations;
   }
}
