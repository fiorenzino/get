package es.getdat.operations.repository;

import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.getdat.api.repository.AbstractRepository;
import es.getdat.api.repository.Search;
import es.getdat.operations.model.Operation;

@Stateless
@LocalBean
public class OperationRepository extends AbstractRepository<Operation> {

	private static final long serialVersionUID = 1L;

	@PersistenceContext
	EntityManager em;

	@Override
	protected EntityManager getEm() {
		return em;
	}

	@Override
	public void setEm(EntityManager em) {
		this.em = em;
	}

	@Override
	protected void applyRestrictions(Search<Operation> search, String alias,
			String separator, StringBuffer sb, Map<String, Object> params) {
      // eventId
      if (search.getObj().getIdEvent() != null && !search.getObj().getIdEvent().trim().isEmpty())
      {
         sb.append(separator).append(alias)
                  .append(".idEvent = :idEvent ");
         params.put("idEvent", search.getObj().getIdEvent());
         separator = " and ";
      }

      // stepId
      if (search.getObj().getIdStep() != null && !search.getObj().getIdStep().trim().isEmpty())
      {
         sb.append(separator).append(alias)
                  .append(".idStep = :idStep ");
         params.put("idStep", search.getObj().getIdStep());
         separator = " and ";
      }
      // from
      if (search.getFrom() != null && search.getFrom().getExecutionDate() != null)
      {
         sb.append(separator).append(alias).append(".executionDate >= :fromExecutionDate ");
         params.put("fromExecutionDate", search.getFrom().getExecutionDate());
         separator = " and ";
      }
      // to
      if (search.getTo() != null && search.getTo().getExecutionDate() != null)
      {
         sb.append(separator).append(alias).append(".executionDate <= :fromExecutionDate ");
         params.put("fromExecutionDate", search.getTo().getExecutionDate());
         separator = " and ";
      }
      // name
      if (search.getObj().getName() != null && !search.getObj().getName().trim().isEmpty())
      {
         sb.append(separator).append(" upper ( ").append(alias)
                  .append(".name ) like :name ");
         params.put("name", likeParam(search.getObj().getName().trim().toUpperCase()));
         separator = " and ";
      }
      // value
      if (search.getObj().getValue() != null && !search.getObj().getValue().trim().isEmpty())
      {
         sb.append(separator).append(" upper ( ").append(alias)
                  .append(".value ) like :value ");
         params.put("value", likeParam(search.getObj().getValue().trim().toUpperCase()));
         separator = " and ";
      }
	}

	@Override
	protected String getDefaultOrderBy() {
		return "id";
	}

	

}
