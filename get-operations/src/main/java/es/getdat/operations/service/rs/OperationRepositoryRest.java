package es.getdat.operations.service.rs;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.getdat.api.model.Content;
import es.getdat.api.service.GetRepositoryService;
import es.getdat.core.management.AppConstants;
import es.getdat.operations.model.Operation;
import es.getdat.operations.repository.OperationRepository;
import es.getdat.operations.util.OperationUtils;

@Path(AppConstants.OPERATIONS_PATH)
@Stateless
@LocalBean
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class OperationRepositoryRest extends GetRepositoryService<Operation>
{

   private static final long serialVersionUID = 1L;

   @Inject
   OperationRepository operationRepository;

   public OperationRepositoryRest()
   {
   }

   @Inject
   public OperationRepositoryRest(OperationRepository operationRepository)
   {
      super(operationRepository);
   }

   @Override
   protected void prePersist(Operation operation) throws Exception
   {
      if (operation == null)
      {
         throw new Exception("operation is null");
      }
      if (operation.getExecutionDate() == null)
      {
         throw new Exception("operation timestamp is null");
      }
   }

   @POST
   @Path("/parameters")
   public Response createFromContent(Content content) throws Exception
   {
//      logger.info("@@POST /parameters => createFromContent");
      String idEvent = content.get(AppConstants.EVENT_ID);
      if (idEvent == null || idEvent.isEmpty())
      {
         return Response
                  .status(Response.Status.NOT_FOUND)
                  .entity("Failed to update resource: event id not present "
                           + content).build();
      }
      String idStep = content.get(AppConstants.EVENT_ID);
      if (idStep == null || idStep.isEmpty())
      {
         return Response
                  .status(Response.Status.NOT_FOUND)
                  .entity("Failed to update resource: step id not present "
                           + content).build();
      }
      List<Operation> operations = null;
      try
      {
         operations = OperationUtils.fromContent(content);
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                  .entity("Error extracting content : " + content).build();
      }

      List<Operation> badRequests = new ArrayList<>();
      List<Operation> internalErrors = new ArrayList<>();
      List<Object> uuids = new ArrayList<>();
      for (Operation operation : operations)
      {
         try
         {
            prePersist(operation);
         }
         catch (Exception e)
         {
            badRequests.add(operation);
            continue;
         }
         Operation persisted = null;
         try
         {
            persisted = getRepository().persist(operation);
         }
         catch (Exception e)
         {
            logger.error(e.getMessage(), e);
            continue;
         }
         finally
         {
            if (persisted == null || getId(persisted) == null)
            {
               internalErrors.add(operation);
            }
            else
            {
               uuids.add(getId(persisted));
               try
               {
                  postPersist(persisted);
               }
               catch (Exception e)
               {
                  logger.error(e.getMessage(), e);
               }
            }
         }
      }
      if (badRequests.size() > 0)
      {
         return Response.status(Response.Status.BAD_REQUEST)
                  .entity("Error before creating one or more operations: " + badRequests.toString()).build();
      }
      if (internalErrors.size() > 0)
      {
         return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                  .entity("Failed to create one or more operations: " + internalErrors.toString()).build();
      }
      return Response.status(Response.Status.OK)
               .entity("Created resources: " + uuids.toString()).build();

   }

}
