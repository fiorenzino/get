package es.getdat.mail.exception;

public class MailServiceException extends Exception
{

   private static final long serialVersionUID = 1L;

   public MailServiceException(String message)
   {
      super(message);
   }

}
