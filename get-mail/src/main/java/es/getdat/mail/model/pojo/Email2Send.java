package es.getdat.mail.model.pojo;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Email2Send implements Serializable {

	private static final long serialVersionUID = 1L;
	private String subject;
	private String body;
	private String from;
	private List<File> attachments;
	private List<String> to;
	private List<String> bcc;
	private List<String> cc;

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public List<File> getAttachments() {
		if (attachments == null)
			this.attachments = new ArrayList<>();
		return attachments;
	}

	public void setAttachments(List<File> attachments) {
		this.attachments = attachments;
	}

	public List<String> getTo() {
		if (to == null)
			this.to = new ArrayList<>();
		return to;
	}

	public void setTo(List<String> to) {
		this.to = to;
	}

	public List<String> getBcc() {
		if (bcc == null)
			this.bcc = new ArrayList<>();
		return bcc;
	}

	public void setBcc(List<String> bcc) {
		this.bcc = bcc;
	}

	public List<String> getCc() {
		if (cc == null)
			this.cc = new ArrayList<>();
		return cc;
	}

	public void setCc(List<String> cc) {
		this.cc = cc;
	}

	public void add(String[] toAdd, String field) {
		if (toAdd != null && toAdd.length > 0 && field != null
				&& !field.trim().isEmpty()) {
			switch (field) {
			case "cc":
				getCc().addAll(Arrays.asList(toAdd));
				break;
			case "bcc":
				getBcc().addAll(Arrays.asList(toAdd));
				break;
			case "tos":
				getTo().addAll(Arrays.asList(toAdd));
				break;
			}
		}
	}

	public String toString(String field) {
		StringBuffer to = new StringBuffer();
		switch (field) {
		case "cc":
			for (String mail : getCc()) {
				if (to.length() > 0)
					to.append(", ");
				to.append(mail);
			}
			break;
		case "bcc":
			for (String mail : getBcc()) {
				if (to.length() > 0)
					to.append(", ");
				to.append(mail);
			}
			break;
		case "tos":
			for (String mail : getTo()) {
				if (to.length() > 0)
					to.append(", ");
				to.append(mail);
			}
			break;
		}

		return to.toString();
	}

}
