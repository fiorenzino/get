package es.getdat.mail.management;

public class AppConstants extends es.getdat.core.management.AppConstants
{

   public static final String MAIL_ADDRESS = "mailAddress";
   public static final String MAIL_USER = "mailUser";
   public static final String MAIL_PWD = "mailPwd";
   public static final String MAIL_AUTH = "mailAuth";
   public static final String MAIL_SSL = "mailSsl";
   public static final String MAIL_PORT = "mailPort";
   public static final String MAIL_DEBUG = "mailDebug";
   public static final String MAIL_TLS = "mailTls";

   public static final String MAIL_FROM = "mailFrom";
   public static final String MAIL_TOS = "mailTos";
   public static final String MAIL_BCC = "mailBcc";
   public static final String MAIL_CC = "mailCc";
   public static final String MAIL_SUBJECT = "mailSubject";
   public static final String MAIL_BODY = "mailBody";

}
