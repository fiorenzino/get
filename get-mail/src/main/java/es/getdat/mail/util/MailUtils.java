package es.getdat.mail.util;

import static es.getdat.mail.management.AppConstants.MAIL_ADDRESS;
import static es.getdat.mail.management.AppConstants.MAIL_AUTH;
import static es.getdat.mail.management.AppConstants.MAIL_BCC;
import static es.getdat.mail.management.AppConstants.MAIL_BODY;
import static es.getdat.mail.management.AppConstants.MAIL_CC;
import static es.getdat.mail.management.AppConstants.MAIL_DEBUG;
import static es.getdat.mail.management.AppConstants.MAIL_FROM;
import static es.getdat.mail.management.AppConstants.MAIL_PORT;
import static es.getdat.mail.management.AppConstants.MAIL_PWD;
import static es.getdat.mail.management.AppConstants.MAIL_SSL;
import static es.getdat.mail.management.AppConstants.MAIL_SUBJECT;
import static es.getdat.mail.management.AppConstants.MAIL_TLS;
import static es.getdat.mail.management.AppConstants.MAIL_TOS;
import static es.getdat.mail.management.AppConstants.MAIL_USER;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import es.getdat.api.model.Content;
import es.getdat.api.util.ContentUtils;
import es.getdat.core.management.AppConstants;
import es.getdat.mail.model.pojo.Email2Send;
import es.getdat.mail.model.pojo.MailServer;

public class MailUtils
{

   public static Email2Send getEmail2Send(Content content)
   {
      if (content != null)
      {
         String stepId = content.get(AppConstants.STEP_ID);
         Email2Send email = new Email2Send();
         email.setFrom(content.getIn(stepId, MAIL_FROM));
         email.setBody(content.getIn(stepId, MAIL_BODY));
         email.setSubject(content.getIn(stepId, MAIL_SUBJECT));
         email.add(ContentUtils.getArray(content.getStepValuesIn(stepId), MAIL_TOS), "tos");
         email.add(ContentUtils.getArray(content.getStepValuesIn(stepId), MAIL_CC), "cc");
         email.add(ContentUtils.getArray(content.getStepValuesIn(stepId), MAIL_BCC), "bcc");
         // HOW TO MAKE THE ATTACHMENTS??
         return email;
      }
      return null;
   }

   public static MailServer getMailServer(Content content)
   {
      if (content != null)
      {
         String stepId = content.get(AppConstants.STEP_ID);
         MailServer server = new MailServer();
         server.setAddress(content.getIn(stepId, MAIL_ADDRESS));
         server.setUser(content.getIn(stepId, MAIL_USER));
         server.setPwd(content.getIn(stepId, MAIL_PWD));
         server.setPort(content.getIn(stepId, MAIL_PORT));
         server.setAuth(ContentUtils.getBoolean(content.getStepValuesIn(stepId), MAIL_AUTH));
         server.setSsl(ContentUtils.getBoolean(content.getStepValuesIn(stepId), MAIL_SSL));
         server.setTls(ContentUtils.getBoolean(content.getStepValuesIn(stepId), MAIL_TLS));
         server.setDebug(ContentUtils.getBoolean(content.getStepValuesIn(stepId), MAIL_DEBUG));
         return server;
      }
      return null;
   }

   public static boolean isValidEmailAddress(String emailAddress)
   {
      String expression = "[\\w\\.-]*[a-zA-Z0-9_]@[\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]";
      CharSequence inputStr = emailAddress;
      Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
      Matcher matcher = pattern.matcher(inputStr);
      return matcher.matches();
   }
}
