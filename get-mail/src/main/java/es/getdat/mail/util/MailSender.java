package es.getdat.mail.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.jboss.logging.Logger;

import es.getdat.api.model.Content;
import es.getdat.core.management.AppProperties;
import es.getdat.mail.exception.MailServiceException;
import es.getdat.mail.management.AppConstants;
import es.getdat.mail.model.pojo.Email2Send;
import es.getdat.mail.model.pojo.MailServer;
import es.getdat.util.CallAutocloseableUtils;
import es.getdat.util.ContentUtils;
import es.getdat.util.DateUtils;

public class MailSender implements Serializable
{

   static String SERVICE_NAME = "MAIL";

   private static final long serialVersionUID = 1L;

   static Logger logger = Logger.getLogger(MailSender.class.getName());

   public static void executeAndReturnToRouter(Content content) throws Exception
   {
      String eventId = content.get(AppConstants.EVENT_ID);
      String stepId = content.get(AppConstants.STEP_ID);
      String when = DateUtils.getPrecisionStringDate();
      Map<String, String> logs = new HashMap<String, String>();
      try
      {
         // logging
         logger.info(content);

         // check arguments
         Email2Send email2Send = MailUtils.getEmail2Send(content);
         MailServer mailServer = MailUtils.getMailServer(content);
         logs.put("from", email2Send.getFrom());
         logs.put("to", email2Send.getTo() == null ? "" : email2Send.getTo().toString());
         if (email2Send.getFrom() == null || email2Send.getFrom().trim().isEmpty() || email2Send.getTo() == null
                  || email2Send.getTo().isEmpty())
         {
            throw new MailServiceException("impossible to send an email without from or to field");
         }

         // execution infos
         String externalId = null;
         Boolean success = null;
         String info = null;

         // biz logic
         if (AppProperties.test.value(false) != null && !AppProperties.test.cast(Boolean.class))
         {
            externalId = Sender.send(email2Send, mailServer);
            success = externalId != null && externalId.trim().length() > 0;
            info = "real";
            logs.put("from", email2Send.getFrom());
            logs.put("to", email2Send.getTo() == null ? null : email2Send.getTo().toString());
         }
         else
         {
            externalId = UUID.randomUUID().toString();
            success = true;
            info = "test";
         }

         // handle result to return to router
         Map<String, String> outs = new HashMap<>();
         ContentUtils.result(eventId, stepId, when, SERVICE_NAME, "executeAndReturnToRouter()", externalId, info,
                  success,
                  content, outs, logs);

         // back to router
         if (success != null)
         {
            CallAutocloseableUtils.call(null, AppConstants.API_PATH + AppConstants.ROUTER_PATH, content);
         }

      }
      catch (Exception e)
      {
         logs.put("exception", e.getClass().getCanonicalName());
         logs.put("message", e.getMessage());
         logger.error(e.getMessage(), e);
      }
      finally
      {
         CallAutocloseableUtils.logs(null, AppConstants.API_PATH
                  + es.getdat.core.management.AppConstants.OPERATIONS_PATH + "/parameters",
                  eventId,
                  stepId, when, SERVICE_NAME, logs);
      }

   }

}
