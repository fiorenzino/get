package es.getdat.mail.util;

import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

import org.jboss.logging.Logger;

import es.getdat.mail.model.pojo.Email2Send;
import es.getdat.mail.model.pojo.MailServer;
import es.getdat.mail.model.pojo.MyMessage;

public class Sender implements Serializable {

	private static final long serialVersionUID = 1L;

	static Logger logger = Logger.getLogger(Sender.class.getName());

	public static String send(Email2Send email, MailServer server)
			throws Exception {
		Session session;

		if (server.isAuth()) {
			Authenticator auth = new PopAuthenticator(server.getUser(),
					server.getPwd());
			session = Session.getInstance(getProperties(server), auth);
		} else {
			session = Session.getInstance(getProperties(server), null);
		}
		MyMessage mailMsg = new MyMessage(session);// a new email message
		InternetAddress[] addresses = null;
		try {
			if (email.getTo() != null && email.getTo().size() > 0) {
				addresses = InternetAddress.parse(email.toString("tos"), false);
				mailMsg.setRecipients(Message.RecipientType.TO, addresses);
			} else {
				throw new MessagingException(
						"The mail message requires a 'To' address.");
			}
			if (email.getCc() != null && email.getCc().size() > 0) {
				addresses = InternetAddress.parse(email.toString("cc"), false);
				mailMsg.setRecipients(Message.RecipientType.CC, addresses);
			}
			if (email.getBcc() != null && email.getBcc().size() > 0) {
				addresses = InternetAddress.parse(email.toString("bcc"), false);
				mailMsg.setRecipients(Message.RecipientType.BCC, addresses);
			}
			if (email.getFrom() != null) {
				mailMsg.setFrom(new InternetAddress(email.getFrom()));
			} else {
				throw new MessagingException(
						"The mail message requires a valid 'From' address.");
			}
			//
			if (email.getSubject() != null)
				mailMsg.setSubject(email.getSubject());
			// corpo e attachment
			if (email.getAttachments() != null
					&& email.getAttachments().size() > 0) {
				BodyPart messageBodyPart = new MimeBodyPart();
				if (email.getBody() != null)
					messageBodyPart.setText(email.getBody());
				Multipart multipart = new MimeMultipart();
				multipart.addBodyPart(messageBodyPart);
				// attachment
				for (File allegato : email.getAttachments()) {
					messageBodyPart = new MimeBodyPart();
					DataSource source = new FileDataSource(
							allegato.getCanonicalPath());
					DataHandler dataH = new DataHandler(source);
					messageBodyPart.setDataHandler(dataH);
					messageBodyPart.setFileName(allegato.getName());
					multipart.addBodyPart(messageBodyPart);
				}
				mailMsg.setContent(multipart);
			} else {
				mailMsg.setContent(email.getBody(),
						"text/plain;charset=\"UTF-8\"");
			}

			mailMsg.setId("<CN" + System.currentTimeMillis() + "@getdat.es>");

			mailMsg.setSentDate(new Date());
			mailMsg.saveChanges();
			// Finally, send the mail message; throws a 'SendFailedException'
			// if any of the message's recipients have an invalid address
			Transport.send(mailMsg);
		} catch (MessagingException e) {
			logger.error(e.toString());
			e.printStackTrace();
			return "";
		} catch (Exception exc) {
			logger.error(exc.toString());
			exc.printStackTrace();
			return "";
		}
		return mailMsg.getMessageID();
	}

	private static Properties getProperties(MailServer server) {
		// Properties properties = System.getProperties();
		Properties properties = new Properties();
		properties.put("mail.smtp.host", server.getAddress());
		if (server.isAuth()) {
			properties.put("mail.smtp.auth", "true");
		}
		if (server.isTls()) {
			properties.put("mail.smtp.starttls.enable", "true");
		}
		if (server.isSsl()) {
			properties.put("mail.smtp.socketFactory.class",
					"javax.net.ssl.SSLSocketFactory");
			properties.put("mail.transport.protocol", "smtps");
		}
		if (server.isDebug()) {
			properties.put("mail.debug", "true");
		} else {
			properties.put("mail.debug", "false");
		}
		if (server.getPort() != null && server.getPort().compareTo("") != 0) {
			properties.put("mail.smtp.port", server.getPort());
			properties.put("mail.smtp.socketFactory.port", server.getPort());
			properties.put("mail.smtp.socketFactory.fallback", "false");
		}
		// properties.setProperty("mail.smtp.quitwait", "false");
		return properties;
	}

}
