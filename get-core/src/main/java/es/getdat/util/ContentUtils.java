package es.getdat.util;

import java.util.Map;

import org.jboss.logging.Logger;

import es.getdat.api.model.Content;
import es.getdat.api.model.enums.EventDetailsType;
import es.getdat.core.management.AppConstants;

/**
 * utilities to - manage full content updates
 * 
 * and at the same time - compute the delta content needed to update remote event details store(s) and singleton(s)
 * process cache
 * 
 * @author pisi79
 * 
 */
public class ContentUtils extends es.getdat.api.util.ContentUtils
{
   static Logger logger = Logger.getLogger(ContentUtils.class);

   public static Content forward(String eventId, String stepId, Content fullContent)
            throws Exception
   {
      Content deltaContent = new Content();
      deltaContent.put(AppConstants.EVENT_ID, eventId);
      deltaContent.put(AppConstants.STEP_ID, eventId);
      deltaContent.put(AppConstants.WHEN, DateUtils.getPrecisionStringDate());
      deltaContent.put(AppConstants.EVENT_DETAILS_TYPE, EventDetailsType.FORWARD.name());

      Map<String, String> delta = deltaContent.getStepValuesOut(eventId);
      Map<String, String> full = fullContent.getStepValuesOut(eventId);

      if (stepId == null || stepId.trim().isEmpty())
      {
         put(delta, full, AppConstants.OPERATION, "starting");
      }
      else
      {
         put(delta, full, AppConstants.OPERATION, "forward from step " + stepId);
      }
      put(delta, full, AppConstants.SERVICE_NAME, "router");
      put(delta, full, AppConstants.HOST,
               System.getProperty(AppConstants.JBOSS_QUALIFIED_HOST_NAME_PROPERTY));
      put(delta, full, AppConstants.HOST,
               System.getProperty(AppConstants.JBOSS_QUALIFIED_HOST_NAME_PROPERTY));

      return deltaContent;
   }

   public static Content block(String eventId, String stepId, String info, Content fullContent)
            throws Exception
   {
      Content deltaContent = new Content();
      deltaContent.put(AppConstants.EVENT_ID, eventId);
      deltaContent.put(AppConstants.STEP_ID, eventId);
      deltaContent.put(AppConstants.WHEN, DateUtils.getPrecisionStringDate());
      deltaContent.put(AppConstants.EVENT_DETAILS_TYPE, EventDetailsType.FORWARD.name());

      Map<String, String> delta = deltaContent.getStepValuesOut(eventId);
      Map<String, String> full = fullContent.getStepValuesOut(eventId);

      put(delta, full, AppConstants.SERVICE_NAME, "router");
      put(delta, full, AppConstants.OPERATION, "blocked step " + stepId);
      put(delta, full, AppConstants.HOST,
               System.getProperty(AppConstants.JBOSS_QUALIFIED_HOST_NAME_PROPERTY));
      if (info != null)
      {
         put(delta, full, AppConstants.INFO, info);
      }

      return deltaContent;
   }

   public static Content wait(String eventId, String stepId, String info, Content fullContent)
            throws Exception
   {
      Content deltaContent = new Content();
      deltaContent.put(AppConstants.EVENT_ID, eventId);
      deltaContent.put(AppConstants.STEP_ID, eventId);
      deltaContent.put(AppConstants.WHEN, DateUtils.getPrecisionStringDate());
      deltaContent.put(AppConstants.EVENT_DETAILS_TYPE, EventDetailsType.WAIT.name());

      Map<String, String> delta = deltaContent.getStepValuesOut(eventId);
      Map<String, String> full = fullContent.getStepValuesOut(eventId);

      put(delta, full, AppConstants.SERVICE_NAME, "router");
      put(delta, full, AppConstants.OPERATION, "paused step " + stepId);
      put(delta, full, AppConstants.HOST,
               System.getProperty(AppConstants.JBOSS_QUALIFIED_HOST_NAME_PROPERTY));
      if (info != null)
      {
         put(delta, full, AppConstants.INFO, info);
      }

      return deltaContent;
   }

   public static Content out(Content full)
   {
      Content delta = new Content();
      String eventId = full.get(AppConstants.EVENT_ID);
      String stepId = full.get(AppConstants.STEP_ID);
      delta.put(AppConstants.EVENT_ID, eventId);
      delta.put(AppConstants.STEP_ID, stepId);
      for (String key : full.getStepValuesOut(stepId).keySet())
      {
         delta.getStepValuesOut(stepId).put(key, full.getStepValuesOut(stepId).get(key));
      }
      return delta;
   }

   public static Map<String, String> getOutMap(String eventId, String stepId, Content full)
   {
      return full.getStepValuesOut(stepId);
   }

   public static Content end(String eventId, String stepId, Content fullContent)
            throws Exception
   {
      Content deltaContent = new Content();
      deltaContent.put(AppConstants.EVENT_ID, eventId);
      deltaContent.put(AppConstants.STEP_ID, eventId);
      deltaContent.put(AppConstants.WHEN, DateUtils.getPrecisionStringDate());
      deltaContent.put(AppConstants.EVENT_DETAILS_TYPE, EventDetailsType.END.name());

      // in or out ? it was in but to me shoud be out
      // Map<String, String> delta = forwardContent.getStepValuesIn(eventId);
      // Map<String, String> full = content.getStepValuesIn(eventId);
      Map<String, String> delta = deltaContent.getStepValuesOut(eventId);
      Map<String, String> full = fullContent.getStepValuesOut(eventId);

      if (stepId == null || stepId.trim().isEmpty())
      {
         put(delta, full, AppConstants.OPERATION, "ending");
         for (String key : fullContent.getGlobal().keySet())
         {
            put(delta, full, key, fullContent.getGlobal().get(key));
         }
      }
      else
      {
         put(delta, full, AppConstants.OPERATION, "ended with step " + stepId);
      }
      put(delta, full, AppConstants.SERVICE_NAME, "router");
      put(delta, full, AppConstants.HOST,
               System.getProperty(AppConstants.JBOSS_QUALIFIED_HOST_NAME_PROPERTY));
      put(delta, full, AppConstants.HOST,
               System.getProperty(AppConstants.JBOSS_QUALIFIED_HOST_NAME_PROPERTY));

      return deltaContent;
   }

   public static Content details(String eventId,
            String stepId, String when, String serviceName, String operationDesc, String externalId, String info,
            Content fullContent, Map<String, String> outs, Map<String, String> logs)
   {
      return result(eventId, stepId, when, serviceName, operationDesc, externalId, info, null, fullContent, outs,
               logs);
   }

   public static Content result(String eventId,
            String stepId, String when, String serviceName, String operationDesc,
            String externalId, String info, Boolean success,
            Content fullContent, Map<String, String> outs, Map<String, String> logs)
   {
      put(logs, AppConstants.HOST,
               System.getProperty(AppConstants.JBOSS_QUALIFIED_HOST_NAME_PROPERTY));
      put(logs, AppConstants.STEP_ID, stepId);
      put(logs, AppConstants.SERVICE_NAME, serviceName);
      put(logs, AppConstants.OPERATION, operationDesc);
      put(logs, AppConstants.EXTERNAL_ID, "" + externalId);
      if (info != null)
      {
         put(logs, AppConstants.INFO, info);
      }

      Content deltaContent = new Content();
      deltaContent.put(AppConstants.EVENT_ID, eventId);
      deltaContent.put(AppConstants.STEP_ID, stepId);
      deltaContent.put(AppConstants.WHEN, when);
      deltaContent.put(AppConstants.EVENT_DETAILS_TYPE, EventDetailsType.OUT.name());

      Map<String, String> delta = deltaContent.getStepValuesOut(stepId);
      Map<String, String> full = fullContent.getStepValuesOut(stepId);
      if (success != null)
      {
         put(delta, full, AppConstants.EXECUTED, "" + true);
         put(delta, full, AppConstants.SUCCESS, "" + success);
      }
      if (outs != null && !outs.isEmpty())
      {
         for (String key : outs.keySet())
         {
            put(full, delta, key, outs.get(key));
         }
      }

      return deltaContent;
   }

   public static Content fail(Content content, Throwable t)
   {
      Content deltaContent = new Content();
      deltaContent.put(AppConstants.EVENT_ID, content.get(AppConstants.EVENT_ID));
      deltaContent.put(AppConstants.STEP_ID, content.get(AppConstants.STEP_ID));
      deltaContent.put(AppConstants.WHEN, DateUtils.getPrecisionStringDate());
      deltaContent.put(AppConstants.EVENT_DETAILS_TYPE, EventDetailsType.OUT.name());
      deltaContent.put(AppConstants.EXECUTED, Boolean.FALSE.toString());
      deltaContent.put(AppConstants.INFO, t.getClass().getCanonicalName() + ": " + t.getMessage());
      return deltaContent;
   }

   private static void put(Map<String, String> delta, Map<String, String> full, String key, String value)
   {
      if (delta != null)
      {
         delta.put(key, value);
      }
      if (full != null)
      {
         full.put(key, value);
      }
   }

   private static void put(Map<String, String> logs, String key, String value)
   {
      if (logs != null)
      {
         logs.put(key, value);
      }
   }
}
