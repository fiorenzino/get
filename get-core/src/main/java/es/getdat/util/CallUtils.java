package es.getdat.util;

import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.InvocationCallback;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.logging.Logger;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;

import es.getdat.api.model.Content;
import es.getdat.api.model.enums.EventDetailsType;
import es.getdat.core.management.AppConstants;

public class CallUtils
{
   static Logger logger = Logger.getLogger(CallUtils.class);

   public static boolean call(String serviceUrl, Content content) throws Exception
   {
      Response response = null;
      try
      {
         WebTarget target = getTarget(serviceUrl, "");

         response = target
                  .request()
                  .buildPost(
                           Entity.entity(content, MediaType.APPLICATION_JSON))

                  .invoke();
         if (response != null
                  && (response.getStatus() == Status.OK.getStatusCode() || response
                           .getStatus() == Status.NO_CONTENT.getStatusCode()))
         {
            return true;
         }
         else
         {
            logger.error("code: " + response.getStatus() + " - status: "
                     + Status.fromStatusCode(response.getStatus())
                     + " - family: "
                     + Status.Family.familyOf(response.getStatus()));
         }
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return false;
      }
      finally
      {
         response.close();
      }
      return false;
   }

   public static Content callWithResponse(String serviceUrl, Content content) throws Exception
   {
      Response response = null;
      try
      {
         WebTarget target = getTarget(serviceUrl, "");

         response = target
                  .request()
                  .buildPost(
                           Entity.entity(content, MediaType.APPLICATION_JSON))

                  .invoke();
         if (response.getStatus() == Status.OK.getStatusCode())
         {
            Content responseContent = response.readEntity(Content.class);
            return responseContent;
         }
         else if (response.getStatus() == Status.NO_CONTENT.getStatusCode())
         {
            Content responseContent = new Content();
            return responseContent;
         }

      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
      }
      finally
      {
         response.close();
      }
      return null;
   }

   public static boolean asyncCall(String serviceUrl, Content content) throws Exception
   {
      try
      {
         WebTarget target = getTarget(serviceUrl, "");
         target
                  .request()
                  .buildPost(
                           Entity.entity(content, MediaType.APPLICATION_JSON))
                  .submit(new InvocationCallback<Content>()
                  {
                     @Override
                     public void failed(Throwable throwable)
                     {
                        logger.error(throwable.getMessage(), throwable);
                     }

                     @Override
                     public void completed(Content content)
                     {
                        logger.info(content);

                     }
                  });
         return true;

      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return false;
      }
   }

   public static WebTarget getTarget(String targetHost, String targetPath)
   {
      try
      {
         Client client = new ResteasyClientBuilder().disableTrustManager()
                  .build();
         WebTarget target = client.target(targetHost + targetPath);
         return target;
      }
      catch (Exception e)
      {
         logger.error(e.getMessage());
      }
      return null;
   }

   public static Form getForm(Map<String, String> params)
   {
      Form form = new Form();
      for (String queryParam : params.keySet())
      {
         String paramValue = params.get(queryParam);
         if (paramValue != null)
         {
            form.param(queryParam, paramValue.toString());
         }
      }
      return form;
   }

   public static Content content(WebTarget target, String eventId,
            String stepId) throws Exception
   {
      Response res = null;
      try
      {
         target = target.resolveTemplate("eventId", eventId).resolveTemplate("idEvent", eventId);
         if (stepId != null)
         {
            target = target.resolveTemplate("stepId", stepId).resolveTemplate("idStep", stepId);
         }
         res = target.request().get();
         if (res.getStatus() == Status.OK.getStatusCode())
         {
            Content content = res.readEntity(Content.class);
            return content;
         }
         return null;
      }
      finally
      {
         if (res != null)
            res.close();
      }
   }

   public static void logs(WebTarget target, String eventId,
            String stepId, String when, String serviceName, Map<String, String> logs)
   {
      Content content = new Content();
      content.put(AppConstants.EVENT_ID, eventId);
      content.put(AppConstants.STEP_ID, stepId);
      content.put(AppConstants.WHEN, when);

      for (String key : logs.keySet())
      {
         content.getStepValuesIn(stepId).put(key, logs.get(key));
      }
      content.getStepValuesIn(stepId).put(AppConstants.HOST,
               System.getProperty(AppConstants.JBOSS_QUALIFIED_HOST_NAME_PROPERTY));
      content.getStepValuesIn(stepId).put(AppConstants.SERVICE_NAME, serviceName);
      Response res = null;
      try
      {
         res = target.request()
                  .buildPost(Entity.entity(content, MediaType.APPLICATION_JSON))
                  .invoke();
         if (res.getStatus() != Status.OK.getStatusCode())
         {
            logger.error("log operations: " + res.getStatus() + " --> " + res.getStatusInfo());
         }
      }
      finally
      {
         if (res != null)
            res.close();
      }

   }

   public static void details(WebTarget target, Content delta)
            throws Exception
   {
      Response res = null;
      try
      {
         res = target.request()
                  .buildPost(Entity.entity(delta, MediaType.APPLICATION_JSON))
                  .invoke();
         if (res.getStatus() != Status.OK.getStatusCode())
         {
            logger.error("failed to update event details!");
         }
      }
      finally
      {
         res.close();
      }

   }

   public static void start(WebTarget target, String eventId)
            throws Exception
   {
      // TODO: SIAMO ASINCRONI ORA
      Response res = null;
      try
      {
         res = target.resolveTemplate("idEvent", eventId).request()
                  .put(Entity.entity(eventId, MediaType.TEXT_PLAIN));
         // if (res.getStatus() != Status.NO_CONTENT.getStatusCode())
         // {
         // logger.error("failed to set event start time! eventId: " + eventId + ", status:" + res.getStatus());
         // }
      }
      finally
      {
         res.close();
      }

   }

   public static void end(WebTarget target, String eventId)
            throws Exception
   {
      Response res = target.resolveTemplate("idEvent", eventId).request()
               .put(Entity.entity(eventId, MediaType.TEXT_PLAIN));
      // TODO: SIAMO ASINCRONI ORA
      // if (res.getStatus() != Status.NO_CONTENT.getStatusCode())
      // {
      // logger.error("failed to set event end time! eventId: " + eventId + ", status:" + res.getStatus());
      // }
   }

   public static void call(WebTarget target, Content content) throws Exception
   {
      Response res = target.request().post(Entity.entity(content, MediaType.APPLICATION_JSON));
      // logger.info(target.getUri().toString() + " --> " + content + " --> " + res.getStatus());
   }

}
