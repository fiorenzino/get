package es.getdat.core.rest;

import javax.ws.rs.ApplicationPath;

import es.getdat.core.management.AppConstants;

@ApplicationPath(AppConstants.API_PATH)
public class JaxRsActivator extends javax.ws.rs.core.Application
{

}
