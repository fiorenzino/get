package es.getdat.core.management;

public class AppConstants extends es.getdat.api.management.AppConstants
{

   public static final String API_PATH = "/api";

   public static final String PLUGIN_DEFINITIONS_PATH = "/v1/plugins/definitions";
   public static final String PLUGIN_CONFIGURATIONS_PATH = "/v1/plugins/configurations";
   public static final String ROUTER_PATH = "/v1/router";
   public static final String SCHEDULER_PATH = "/v1/scheduler";
   public static final String EVENTS_PATH = "/v1/events";
   public static final String EVENTDETAILS_PATH = "/v1/eventdetails";
   public static final String OPERATIONS_PATH = "/v1/operations";
   public static final String REGISTRATION_PATH = "/v1/registrations";
   public static final String CACHE_PATH = "/v1/cache";

   public static final String ACCOUNTING_PATH = "/v1/accounting";
   public static final String ACCOUNTS_PATH = "/v1/accounting/accounts";
   public static final String ACTIVATIONS_PATH = "/v1/accounting/activations";
   public static final String DATES_PATH = "/v1/dates";
   public static final String FILES_PATH = "/v1/files";

   public static final String USERS_PATH = "/v1/users";

   public static final String JBOSS_QUALIFIED_HOST_NAME_PROPERTY = "jboss.qualified.host.name";

}
