package es.getdat.core.producer;

import java.io.Serializable;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Named;
import javax.ws.rs.client.WebTarget;

import es.getdat.core.annotation.RsClientTarget;
import es.getdat.core.management.AppConstants;
import es.getdat.core.management.AppProperties;
import es.getdat.util.CallUtils;

@Named
public class RsClientProducer implements Serializable
{

   private static final long serialVersionUID = 1L;

   public RsClientProducer()
   {
   }

   @Produces
   @RsClientTarget
   public WebTarget createTarget(InjectionPoint injectionPoint)
   {
      String targetHost = AppProperties.baseUrl.value();
      String targetPath = AppConstants.API_PATH
               + injectionPoint.getAnnotated()
                        .getAnnotation(RsClientTarget.class).value();
      return CallUtils.getTarget(targetHost, targetPath);
   }

}
