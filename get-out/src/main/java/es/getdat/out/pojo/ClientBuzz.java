package es.getdat.out.pojo;

public class ClientBuzz
{
   public String date;
   public String number;

   @Override
   public String toString()
   {
      return "ClientBuzz [" + (date != null ? "date=" + date + ", " : "") + (number != null ? "number=" + number : "")
               + "]";
   }

   public String getDate()
   {
      return date;
   }

   public void setDate(String date)
   {
      this.date = date;
   }

   public String getNumber()
   {
      return number;
   }

   public void setNumber(String number)
   {
      this.number = number;
   }

}
