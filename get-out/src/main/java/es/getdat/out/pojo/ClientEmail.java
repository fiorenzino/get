package es.getdat.out.pojo;

public class ClientEmail
{
   public String date;
   public String mailFrom;
   public String mailTo;
   public String mailSubject;
   public String mailBody;

   @Override
   public String toString()
   {
      return "ClientEmail [" + (date != null ? "date=" + date + ", " : "")
               + (mailFrom != null ? "mailFrom=" + mailFrom + ", " : "")
               + (mailTo != null ? "mailTo=" + mailTo + ", " : "")
               + (mailSubject != null ? "mailSubject=" + mailSubject + ", " : "")
               + (mailBody != null ? "mailBody=" + mailBody : "") + "]";
   }

   public String getDate()
   {
      return date;
   }

   public void setDate(String date)
   {
      this.date = date;
   }

   public String getMailFrom()
   {
      return mailFrom;
   }

   public void setMailFrom(String mailFrom)
   {
      this.mailFrom = mailFrom;
   }

   public String getMailTo()
   {
      return mailTo;
   }

   public void setMailTo(String mailTo)
   {
      this.mailTo = mailTo;
   }

   public String getMailSubject()
   {
      return mailSubject;
   }

   public void setMailSubject(String mailSubject)
   {
      this.mailSubject = mailSubject;
   }

   public String getMailBody()
   {
      return mailBody;
   }

   public void setMailBody(String mailBody)
   {
      this.mailBody = mailBody;
   }

}
