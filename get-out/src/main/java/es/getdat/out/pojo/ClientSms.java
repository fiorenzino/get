package es.getdat.out.pojo;

public class ClientSms
{
   public String date;
   public String number;
   public String msg;

   @Override
   public String toString()
   {
      return "ClientSms [" + (date != null ? "date=" + date + ", " : "")
               + (number != null ? "number=" + number + ", " : "") + (msg != null ? "msg=" + msg : "") + "]";
   }

   public String getDate()
   {
      return date;
   }

   public void setDate(String date)
   {
      this.date = date;
   }

   public String getNumber()
   {
      return number;
   }

   public void setNumber(String number)
   {
      this.number = number;
   }

   public String getMsg()
   {
      return msg;
   }

   public void setMsg(String msg)
   {
      this.msg = msg;
   }

}
