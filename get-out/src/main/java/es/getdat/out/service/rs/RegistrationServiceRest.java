package es.getdat.out.service.rs;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.logging.Logger;

import es.getdat.accounting.model.Account;
import es.getdat.accounting.model.Activation;
import es.getdat.accounting.pojo.UserToken;
import es.getdat.accounting.repository.AccountRepository;
import es.getdat.accounting.repository.ActivationRepository;
import es.getdat.out.management.AppConstants;
import es.getdat.out.util.AccountUtils;
import es.getdat.out.util.fb.FbClient;
import es.getdat.out.util.fb.pojo.FbIdentity;
import es.getdat.out.util.fb.shorttoken.DebugShortTokenResponse;
import es.getdat.plugin.model.PluginConfiguration;
import es.getdat.plugin.repository.PluginConfigurationRepository;

@Path(AppConstants.REGISTRATION_PATH)
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class RegistrationServiceRest
{

   protected final Logger logger = Logger.getLogger(getClass());

   @Inject
   AccountRepository accountRepository;

   @Inject
   PluginConfigurationRepository pluginConfigurationRepository;

   @Inject
   ActivationRepository activationRepository;

   @POST
   public Response register(UserToken userToken) throws Exception
   {
      try
      {
         // verifico che contenga userid e short token
         verifyUserToken(userToken);
         // verifico che non esista un utente con user-id
         if (accountRepository.countByUserId(userToken.userid) > 0)
         {
            logger.info("user id already used");
            throw new Exception("user id already used");
         }
         // genero il long-token da short-token
         DebugShortTokenResponse debugShortTokenResponse = FbClient.verifyShortToken(userToken.shortToken);
         logger.info(debugShortTokenResponse);
         UserToken userTokenCreated = FbClient.getLongToken(userToken.shortToken, userToken.userid);
         // getME (informazioni utente)
         FbIdentity fbIdentity = FbClient.getFbIdentity(userTokenCreated.longToken);
         Account account = AccountUtils.fromFbIdentity(fbIdentity, userTokenCreated);

         accountRepository.persist(account);
         List<PluginConfiguration> pluginList = pluginConfigurationRepository.getSystemDefaultPluginConfigurationList();
         for (PluginConfiguration pluginConfiguration : pluginList)
         {
            Activation newActivation = new Activation();
            newActivation.setAccountId(account.getId());
            newActivation.setPluginConfigurationId(pluginConfiguration.getId());
            newActivation.setChannelType(pluginConfiguration.getChannelType());
            newActivation.setInit(new Date());
            newActivation.setEnd(null);
            newActivation.setUserDefault(true);
            activationRepository.persist(newActivation);
         }
         // ritorno user con dentro fbIdentity e long-token con expiration-date
         return Response.status(Status.OK).entity(account).build();
      }
      catch (Exception e)
      {
         logger.error(e.getMessage());
         return Response
                  .status(Response.Status.BAD_REQUEST)
                  .entity("Error before creating resource: " + e.getMessage())
                  .build();
      }
   }

   private void verifyUserToken(UserToken userToken) throws Exception
   {
      if (userToken.userid == null || userToken.userid.trim().isEmpty())
      {
         logger.info("user-id null or empty");
         throw new Exception("user-id null or empty");
      }

      if (userToken.shortToken == null || userToken.shortToken.trim().isEmpty())
      {
         logger.info("shortToken null or empty");
         throw new Exception("shortToken null or empty");
      }
   }

   @PUT
   public Response update(UserToken userToken) throws Exception
   {
      try
      {
         // verifico che contenga userid e short token
         verifyUserToken(userToken);

         // verifico che esista un utente con user-id
         Account user = accountRepository.findByUserid(userToken.userid);
         if (user == null)
         {
            logger.info("user id not exist");
            throw new Exception("user id not exist");
         }
         // ricreo il long-token
         UserToken userTokenCreated = FbClient.getLongToken(userToken.shortToken, userToken.userid);
         accountRepository.updateLongTokenAndExpirationDate(user.getId(), userTokenCreated.longToken,
                  userTokenCreated.expirationDate);
         user.setExpirationDate(userTokenCreated.expirationDate);
         user.setLongToken(userTokenCreated.longToken);
         // ritorno il long-token con expiration-date
         return Response.status(Status.OK).entity(user).build();

      }
      catch (Exception e)
      {
         logger.error(e.getMessage());
         return Response
                  .status(Response.Status.BAD_REQUEST)
                  .entity("Error before creating resource: " + e.getMessage())
                  .build();
      }
   }
}
