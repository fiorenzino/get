package es.getdat.out.service.rs;

import static es.getdat.api.management.AppConstants.WHEN;
import static es.getdat.api.management.AppConstants.ACCOUNT_ID;

import java.util.UUID;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.jboss.logging.Logger;

import es.getdat.accounting.model.Account;
import es.getdat.accounting.repository.AccountRepository;
import es.getdat.accounting.repository.ActivationRepository;
import es.getdat.api.model.Content;
import es.getdat.api.model.Process;
import es.getdat.api.model.Step;
import es.getdat.api.model.enums.ChannelType;
import es.getdat.core.annotation.AccountTokenVerification;
import es.getdat.out.management.AppConstants;
import es.getdat.out.pojo.ClientBuzz;
import es.getdat.out.pojo.ClientCall;
import es.getdat.out.pojo.ClientEmail;
import es.getdat.out.pojo.ClientSms;
import es.getdat.scheduler.service.rs.SchedulerRest;

@Path(AppConstants.USERS_PATH + "/{userid}")
@Stateless
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProcessLauncherService
{
   protected final Logger logger = Logger.getLogger(getClass());

   @Context
   UriInfo uriInfo;

   @Inject
   AccountRepository accountRepository;

   @Inject
   ActivationRepository activationRepository;

   @Inject
   SchedulerRest schedulerRest;

   @AccountTokenVerification
   @Path("/email/schedule")
   @POST
   public Response scheduleEmail(ClientEmail clientEmail)
   {
      logger.info(clientEmail);
      if (clientEmail == null || clientEmail.date == null || clientEmail.date.trim().isEmpty()
               || clientEmail.mailBody.isEmpty() || clientEmail.mailFrom.isEmpty() || clientEmail.mailSubject.isEmpty()
               || clientEmail.mailTo.isEmpty())
      {
         logger.error("Error schedule email: clientEmail not regular");
         return Response
                  .status(Response.Status.BAD_REQUEST)
                  .entity("Error schedule email: clientEmail not regular")
                  .build();
      }
      try
      {
         String userid = uriInfo.getPathParameters().getFirst("userid");
         Account account = accountRepository.findByUserid(userid);
         if (account == null)
         {
            logger.error("Error schedule email: account null");
            return Response
                     .status(Response.Status.BAD_REQUEST)
                     .entity("Error schedule email: account null")
                     .build();
         }
         String pluginconfigurationId = activationRepository
                  .getPluginConfigurationId(account.getId(), ChannelType.EMAIL, null);
         if (pluginconfigurationId == null || pluginconfigurationId.isEmpty())
         {
            logger.error("Error schedule email: pluginconfigurationId null: accountid" + account.getId()
                     + ", channel: " + ChannelType.EMAIL);
            return Response
                     .status(Response.Status.BAD_REQUEST)
                     .entity("Error schedule email: pluginconfigurationId null")
                     .build();
         }
         boolean result = schedule(makeEmailContent(clientEmail.date, pluginconfigurationId,
                  account.getId(),
                  clientEmail.mailFrom, clientEmail.mailTo, clientEmail.mailSubject,
                  clientEmail.mailBody));
         if (result)
         {
            logger.info("Schedule email: OK");
            return Response.status(Status.OK).entity(true).build();
         }
      }
      catch (Exception e)
      {
         logger.error(e.getMessage());
         return Response
                  .status(Response.Status.BAD_REQUEST)
                  .entity("Error schedule email: " + e.getMessage())
                  .build();
      }
      logger.info("Schedule email: KO");
      return Response
               .status(Response.Status.INTERNAL_SERVER_ERROR)
               .entity("Error schedule email")
               .build();
   }

   @AccountTokenVerification
   @Path("/call/schedule")
   @POST
   public Response scheduleCall(ClientCall clientCall)
   {
      logger.info(clientCall);
      if (clientCall == null || clientCall.date == null || clientCall.date.trim().isEmpty()
               || clientCall.msg.isEmpty() || clientCall.number.isEmpty())
      {
         logger.error("Error schedule call: clientCall not regular");
         return Response
                  .status(Response.Status.BAD_REQUEST)
                  .entity("Error schedule call: clientCall not regular")
                  .build();
      }
      try
      {
         String userid = uriInfo.getPathParameters().getFirst("userid");
         Account account = accountRepository.findByUserid(userid);
         if (account == null)
         {
            logger.error("Error schedule call: account null");
            return Response
                     .status(Response.Status.BAD_REQUEST)
                     .entity("Error schedule call: account null")
                     .build();
         }
         String pluginconfigurationId = activationRepository
                  .getPluginConfigurationId(account.getId(), ChannelType.CALL, null);
         if (pluginconfigurationId == null || pluginconfigurationId.isEmpty())
         {
            logger.error("Error schedule call: pluginconfigurationId null");
            return Response
                     .status(Response.Status.BAD_REQUEST)
                     .entity("Error schedule call: pluginconfigurationId null")
                     .build();
         }

         boolean result = schedule(makeCallContent(clientCall.date, pluginconfigurationId,
                  account.getId(),
                  clientCall.number, clientCall.msg));
         if (result)
         {
            logger.info("Schedule call: OK");
            return Response.status(Status.OK).entity(true).build();
         }

      }
      catch (Exception e)
      {
         logger.error(e.getMessage());
         return Response
                  .status(Response.Status.BAD_REQUEST)
                  .entity("Error schedule call: " + e.getMessage())
                  .build();
      }
      logger.info("Error schedule call: KO");
      return Response
               .status(Response.Status.INTERNAL_SERVER_ERROR)
               .entity("Error schedule call")
               .build();
   }

   @AccountTokenVerification
   @Path("/sms/schedule")
   @POST
   public Response scheduleSms(ClientSms clientSms)
   {
      logger.info(clientSms);
      if (clientSms == null || clientSms.date == null || clientSms.date.trim().isEmpty()
               || clientSms.msg.isEmpty() || clientSms.number.isEmpty())
      {
         logger.error("Error schedule sms: clientSms not regular");
         return Response
                  .status(Response.Status.BAD_REQUEST)
                  .entity("Error schedule sms: clientSms not regular")
                  .build();
      }
      try
      {
         String userid = uriInfo.getPathParameters().getFirst("userid");
         Account account = accountRepository.findByUserid(userid);
         if (account == null)
         {
            logger.error("Error schedule sms: account null");
            return Response
                     .status(Response.Status.BAD_REQUEST)
                     .entity("Error schedule sms: account null")
                     .build();
         }
         String pluginconfigurationId = activationRepository
                  .getPluginConfigurationId(account.getId(), ChannelType.SMS, null);
         if (pluginconfigurationId == null || pluginconfigurationId.isEmpty())
         {
            logger.error("Error schedule sms: pluginconfigurationId null");
            return Response
                     .status(Response.Status.BAD_REQUEST)
                     .entity("Error schedule call: pluginconfigurationId null")
                     .build();
         }

         boolean result = schedule(makeSmsContent(clientSms.date, pluginconfigurationId, account.getId(),
                  clientSms.number, clientSms.msg));
         if (result)
         {
            logger.info("Schedule sms: OK");
            return Response.status(Status.OK).entity(true).build();
         }
      }
      catch (Exception e)
      {
         logger.error(e.getMessage());
         return Response
                  .status(Response.Status.BAD_REQUEST)
                  .entity("Error schedule sms: " + e.getMessage())
                  .build();
      }
      logger.info("Error schedule sms: KO");
      return Response
               .status(Response.Status.INTERNAL_SERVER_ERROR)
               .entity("Error schedule email")
               .build();
   }

   @AccountTokenVerification
   @Path("/buzz/schedule")
   @POST
   public Response scheduleBuzz(ClientBuzz clientBuzz)
   {
      logger.info(clientBuzz);
      if (clientBuzz == null || clientBuzz.date == null || clientBuzz.date.trim().isEmpty()
               || clientBuzz.number.isEmpty())
      {
         logger.error("Error schedule buzz: clientBuzz not regular");
         return Response
                  .status(Response.Status.BAD_REQUEST)
                  .entity("Error schedule buzz: clientBuzz not regular")
                  .build();
      }
      try
      {
         String userid = uriInfo.getPathParameters().getFirst("userid");
         Account account = accountRepository.findByUserid(userid);
         if (account == null)
         {
            logger.error("Error schedule buzz: account null");
            return Response
                     .status(Response.Status.BAD_REQUEST)
                     .entity("Error schedule buzz: account null")
                     .build();
         }
         String pluginconfigurationId = activationRepository
                  .getPluginConfigurationId(account.getId(), ChannelType.BUZZ, null);
         if (pluginconfigurationId == null || pluginconfigurationId.isEmpty())
         {
            logger.error("Error schedule buzz: pluginconfigurationId null");
            return Response
                     .status(Response.Status.BAD_REQUEST)
                     .entity("Error schedule buzz: pluginconfigurationId null")
                     .build();
         }

         boolean result = schedule(makeBuzzContent(clientBuzz.date, pluginconfigurationId,
                  account.getId(),
                  clientBuzz.number));
         if (result)
         {
            logger.info("Schedule buzz: OK");
            return Response.status(Status.OK).entity(true).build();
         }

      }
      catch (Exception e)
      {
         logger.error(e.getMessage());
         return Response
                  .status(Response.Status.BAD_REQUEST)
                  .entity("Error schedule buzz: " + e.getMessage())
                  .build();
      }
      logger.info("Error schedule buzz: KO");
      return Response
               .status(Response.Status.INTERNAL_SERVER_ERROR)
               .entity("Error schedule email")
               .build();
   }

   private boolean schedule(Content content) throws Exception
   {
      try
      {
         return schedulerRest.executeSync(content);
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
      }
      return false;
   }

   private Content makeEmailContent(String date, String pluginconfigurationId, String accountId,
            String mailFrom, String mailTo, String mailSubject, String mailBody) throws Exception
   {

      Process process = new Process();
      Step step = new Step(UUID.randomUUID().toString(), pluginconfigurationId,
               ChannelType.EMAIL.name(), accountId);
      // step.condition = "process.get('" + step.stepId + "').routed && process.get('" + step.stepId + "').executed";
      process.add(step);
      process.successCondition = "process.get('" + step.stepId + "').success";

      Content content = new Content();
      content.getGlobal().put(WHEN, date);
      content.getGlobal().put(ACCOUNT_ID, accountId);
      content.getGlobal().put(AppConstants.PROCESS, process.toJsonString());
      content.getStepValuesIn(step.stepId).put(es.getdat.mail.management.AppConstants.MAIL_FROM,
               mailFrom);
      content.getStepValuesIn(step.stepId).put(es.getdat.mail.management.AppConstants.MAIL_TOS,
               mailTo);
      content.getStepValuesIn(step.stepId).put(es.getdat.mail.management.AppConstants.MAIL_SUBJECT,
               mailSubject);
      content.getStepValuesIn(step.stepId).put(es.getdat.mail.management.AppConstants.MAIL_BODY,
               mailBody);

      return content;
   }

   private Content makeSmsContent(String date, String pluginconfigurationId, String accountId, String number, String msg)
            throws Exception
   {

      Process process = new Process();
      Step step = new Step(UUID.randomUUID().toString(), pluginconfigurationId,
               ChannelType.SMS.name(), accountId);
      // step.condition = "process.get('" + step.stepId + "').routed && process.get('" + step.stepId + "').executed";
      process.add(step);
      process.successCondition = "process.get('" + step.stepId + "').success";

      Content content = new Content();
      content.getGlobal().put(WHEN, date);
      content.getGlobal().put(ACCOUNT_ID, accountId);
      content.getGlobal().put(AppConstants.PROCESS, process.toJsonString());
      content.getStepValuesIn(step.stepId).put(es.getdat.plivo.management.AppConstants.PLIVO_SMS_DST, number);
      content.getStepValuesIn(step.stepId).put(es.getdat.plivo.management.AppConstants.PLIVO_SMS_TEXT, msg);

      return content;
   }

   private Content makeCallContent(String date, String pluginconfigurationId, String accountId, String number,
            String msg)
            throws Exception
   {
      Process process = new Process();
      Step step = new Step(UUID.randomUUID().toString(), pluginconfigurationId,
               ChannelType.CALL.name(), accountId);
      // step.condition = "process.get('" + step.stepId + "').routed && process.get('" + step.stepId + "').executed";
      process.add(step);
      process.successCondition = "process.get('" + step.stepId + "').success";

      Content content = new Content();
      content.getGlobal().put(WHEN, date);
      content.getGlobal().put(ACCOUNT_ID, accountId);
      content.getGlobal().put(AppConstants.PROCESS, process.toJsonString());
      content.getStepValuesIn(step.stepId).put(es.getdat.plivo.management.AppConstants.PLIVO_CALL_TO, number);
      content.getStepValuesIn(step.stepId).put(es.getdat.plivo.management.AppConstants.PLIVO_CALL_MSG, msg);

      return content;
   }

   private Content makeBuzzContent(String date, String pluginconfigurationId, String accountId, String number)
            throws Exception
   {
      Process process = new Process();
      Step step = new Step(UUID.randomUUID().toString(), pluginconfigurationId,
               ChannelType.BUZZ.name(), accountId);
      // step.condition = "process.get('" + step.stepId + "').routed && process.get('" + step.stepId + "').executed";
      process.add(step);
      process.successCondition = "process.get('" + step.stepId + "').success";

      Content content = new Content();
      content.getGlobal().put(WHEN, date);
      content.getGlobal().put(ACCOUNT_ID, accountId);
      content.getGlobal().put(AppConstants.PROCESS, process.toJsonString());
      content.getStepValuesIn(step.stepId).put(es.getdat.plivo.management.AppConstants.PLIVO_BUZZ_TO, number);

      return content;
   }
}
