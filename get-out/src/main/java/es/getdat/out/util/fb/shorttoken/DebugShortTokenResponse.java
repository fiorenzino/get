package es.getdat.out.util.fb.shorttoken;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import es.getdat.accounting.util.fb.shorttoken.DebugShortTokenResponseData;

@XmlRootElement
public class DebugShortTokenResponse
{

   // {
   // "data": {
   // "app_id": "752381511504980",
   // "application": "forgetters.club - Test1",
   // "error": {
   // "code": 190,
   // "message":
   // "Error validating access token: Session has expired on Dec 26, 2014 5:00am. The current time is Dec 26, 2014 5:56am.",
   // "subcode": 463
   // },
   // "expires_at": 1419598800,
   // "is_valid": false,
   // "scopes": [
   // "public_profile",
   // "email",
   // "publish_actions"
   // ],
   // "user_id": "10205237953655726"
   // }
   // }
   @XmlElement(name = "data")
   public DebugShortTokenResponseData debugTokenResponseData;

   @Override
   public String toString()
   {
      return "{"
               + (debugTokenResponseData != null ? "data=" + debugTokenResponseData : "") + "}";
   }

}
