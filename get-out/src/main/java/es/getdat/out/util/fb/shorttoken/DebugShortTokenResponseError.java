package es.getdat.out.util.fb.shorttoken;

public class DebugShortTokenResponseError
{
   // "error": {
   // "code": 190,
   // "message":
   // "Error validating access token: Session has expired on Dec 26, 2014 5:00am. The current time is Dec 26, 2014 5:56am.",
   // "subcode": 463
   // },
   public String code;
   public String message;
   public String subcode;

   @Override
   public String toString()
   {
      return (code != null ? "code=" + code + ", " : "")
               + (message != null ? "message=" + message + ", " : "") + (subcode != null ? "subcode=" + subcode : "");
   }

}
