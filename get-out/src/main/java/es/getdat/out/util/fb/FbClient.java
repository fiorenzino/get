package es.getdat.out.util.fb;

import java.util.Calendar;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;

import es.getdat.accounting.pojo.UserToken;
import es.getdat.core.management.AppProperties;
import es.getdat.out.management.AppConstants;
import es.getdat.out.util.fb.filter.client.JsonContentTypeResponseFilter;
import es.getdat.out.util.fb.longtoken.DebugLongTokenResponse;
import es.getdat.out.util.fb.pojo.FbIdentity;
import es.getdat.out.util.fb.shorttoken.DebugShortTokenResponse;

public class FbClient
{
   public static DebugShortTokenResponse verifyShortToken(String shortToken) throws Exception
   {
      WebTarget target = getTarget(AppConstants.FB_targetHost, AppConstants.FB_targetPathDebugToken).queryParam(
               "input_token", shortToken).queryParam("access_token",
               getAuthToken(AppProperties.FB_client_id.value(), AppProperties.FB_client_secret.value()));
      DebugShortTokenResponse result = target.request(MediaType.TEXT_HTML).get(
               DebugShortTokenResponse.class);
      return result;
   }

   public static UserToken getLongToken(String shortToken, String user_id) throws Exception
   {
      Calendar calendar = Calendar.getInstance();
      WebTarget target = getTarget(AppConstants.FB_targetHost, AppConstants.FB_targetPathLongToken)
               .queryParam("grant_type", "fb_exchange_token")
               .queryParam("client_id", AppProperties.FB_client_id.value())
               .queryParam("client_secret", AppProperties.FB_client_secret.value())
               .queryParam("fb_exchange_token", shortToken);
      String result = target.request(MediaType.TEXT_HTML).get(
               String.class);
      System.out.println(result);
      if (result != null)
      {
         UserToken userToken = new UserToken();
         userToken.userid = user_id;
         String[] pieces = result.split("&");
         if (pieces != null && pieces.length == 2)
         {
            String[] accTok = pieces[0].split("=");
            userToken.shortToken = shortToken;
            userToken.longToken = accTok[1];
            String[] expires = pieces[1].split("=");
            calendar.add(Calendar.SECOND, Integer.valueOf(expires[1]));
            userToken.expirationDate = calendar.getTime();
            return userToken;
         }
      }

      return new UserToken();
   }

   public static DebugLongTokenResponse verifyLongToken(String longToken) throws Exception
   {
      WebTarget target = getTarget(AppConstants.FB_targetHost, AppConstants.FB_targetPathDebugToken).queryParam(
               "input_token", longToken).queryParam("access_token",
               getAuthToken(AppProperties.FB_client_id.value(), AppProperties.FB_client_secret.value()));
      DebugLongTokenResponse result = target.request(MediaType.TEXT_HTML).get(
               DebugLongTokenResponse.class);
      return result;
   }

   public static FbIdentity getFbIdentity(String longToken) throws Exception
   {
      WebTarget target = getTarget(AppConstants.FB_targetHost, AppConstants.FB_targetPathMe)
               .queryParam("access_token", longToken);
      FbIdentity result = target.request(MediaType.TEXT_HTML).get(
               FbIdentity.class);
      return result;
   }

   public static WebTarget getTarget(String targetHost, String targetPath) throws Exception
   {
      Client client = new ResteasyClientBuilder().disableTrustManager()
               .register(JsonContentTypeResponseFilter.class)
               .build();
      WebTarget target = client.target(targetHost + targetPath);
      return target;
   }

   public static String getAuthToken(String client_id, String client_secret) throws Exception
   {
      WebTarget target = getTarget(AppConstants.FB_targetHost, AppConstants.FB_targetPathAuth).queryParam(
               "client_id", client_id).queryParam("client_secret", client_secret)
               .queryParam("grant_type", AppConstants.FB_grant_type);
      String result = target.request(MediaType.TEXT_HTML).get(
               String.class);
      System.out.println(result);
      String[] accTok = result.split("=");
      return accTok[1];
   }
}
