package es.getdat.out.util;

import es.getdat.accounting.model.Account;
import es.getdat.accounting.model.enums.AccountType;
import es.getdat.accounting.pojo.UserToken;
import es.getdat.out.util.fb.pojo.FbIdentity;

public class AccountUtils
{
   public static Account fromFbIdentity(FbIdentity fbIdentity, UserToken userToken)
   {
      Account account = new Account();
      account.setEmail(fbIdentity.email);
      account.setExpirationDate(userToken.expirationDate);
      account.setFirstName(fbIdentity.first_name);
      account.setLastName(fbIdentity.last_name);
      account.setImageUrl("http://graph.facebook.com/" + fbIdentity.id + "/picture?width=270&height=270");
      account.setLongToken(userToken.longToken);
      account.setUsername(fbIdentity.name);
      account.setUserid("" + fbIdentity.id);
      account.setAccountType(AccountType.FACEBOOK);
      return account;
   }
}
