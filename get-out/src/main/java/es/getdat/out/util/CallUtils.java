package es.getdat.out.util;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.logging.Logger;

import es.getdat.api.model.Content;
import es.getdat.api.model.enums.ChannelType;

public class CallUtils
{

   static Logger logger = Logger.getLogger(CallUtils.class);

   public static String getConfigurationId(String accountId,
            ChannelType channelType, WebTarget target)
   {
      try
      {

         Response res = target
                  .path("/plugins/{accountId}/channelType/{channelType}")
                  .resolveTemplate("accountId",
                           "01d68957-c500-4ee6-b21c-98329e180a12")
                  .resolveTemplate("channelType", channelType).request()
                  .get();
         if (res.getStatus() == Status.OK.getStatusCode())
         {
            String pluginConfigurationId = res.readEntity(String.class);
            logger.info(pluginConfigurationId);
            return pluginConfigurationId;
         }

      }
      catch (Exception e)
      {
         logger.info(e.getMessage());
      }
      return null;
   }

   public static Content getPluginContentProperties(
            String pluginConfigurationId, WebTarget target) throws Exception
   {
      Response res = null;
      try
      {
         res = target
                  .path("/{pluginConfigurationId}/content")
                  .resolveTemplate("pluginConfigurationId",
                           pluginConfigurationId).request().get();
         if (res.getStatus() == Status.OK.getStatusCode())
         {
            Content content = res.readEntity(Content.class);
            return content;
         }
         else
         {
            return null;
         }
      }
      finally
      {
         if (res != null)
         {
            res.close();
         }
      }
   }

   public static boolean existPluginConfiguration(
            String pluginConfigurationId, WebTarget target) throws Exception
   {
      Response res = null;
      try
      {
         res = target
                  .path("/{pluginConfigurationId}/exist")
                  .resolveTemplate("pluginConfigurationId",
                           pluginConfigurationId).request().get();
         if (res.getStatus() == Status.OK.getStatusCode())
         {
            return true;
         }
         else
         {
            return false;
         }
      }
      finally
      {
         if (res != null)
         {
            res.close();
         }
      }
   }
}
