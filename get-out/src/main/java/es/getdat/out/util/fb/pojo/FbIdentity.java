package es.getdat.out.util.fb.pojo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FbIdentity implements Serializable
{
   private static final long serialVersionUID = 1L;
   // {"id":"10205237953655726",
   // "email":"fiorenzino\u0040gmail.com",
   // "first_name":"Fiorenzino",
   // "gender":"male",
   // "last_name":"Flowers",
   // "link":"https:\/\/www.facebook.com\/app_scoped_user_id\/10205237953655726\/"
   // ,"locale":"it_IT",
   // "name":"Fiorenzino Flowers",
   // "timezone":1,
   // "updated_time":"2014-12-27T08:15:25+0000",
   // "verified":true}
   public Long id;
   public String email;
   public String first_name;
   public String gender;
   public String last_name;
   public String link;
   public String locale;
   public String name;
   public String timezone;
   public String updated_time;
   public boolean verified;

   @Override
   public String toString()
   {
      return "FbIdentity [" + (id != null ? "id=" + id + ", " : "") + (email != null ? "email=" + email + ", " : "")
               + (first_name != null ? "first_name=" + first_name + ", " : "")
               + (gender != null ? "gender=" + gender + ", " : "")
               + (last_name != null ? "last_name=" + last_name + ", " : "")
               + (link != null ? "link=" + link + ", " : "") + (locale != null ? "locale=" + locale + ", " : "")
               + (name != null ? "name=" + name + ", " : "") + (timezone != null ? "timezone=" + timezone + ", " : "")
               + (updated_time != null ? "updated_time=" + updated_time + ", " : "") + "verified=" + verified + "]";
   }

}
