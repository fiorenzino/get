package es.getdat.accounting.management;

public class AppConstants extends es.getdat.core.management.AppConstants
{

   public static String FB_targetHost = "https://graph.facebook.com";
   public static String FB_targetHost22 = "https://graph.facebook.com/v2.2";
   public static String FB_targetPathAuth = "/oauth/access_token";
   public static String FB_targetPathDebugToken = "/debug_token";
   public static String FB_targetPathLongToken = "/oauth/access_token";

   public static String FB_targetPathMe = "/me";
   // public static String FB_client_id = "752381511504980";
   // public static String FB_client_secret = "95aba24ea120ab97821fa71833b309bc";
   public static String FB_grant_type = "client_credentials";

}
