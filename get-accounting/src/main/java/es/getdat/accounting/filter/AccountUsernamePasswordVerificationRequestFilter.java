package es.getdat.accounting.filter;

import java.io.IOException;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.jboss.logging.Logger;

import es.getdat.accounting.repository.AccountRepository;
import es.getdat.core.annotation.AccountUsernamePasswordVerification;
import es.getdat.util.HttpUtils;

@Provider
@AccountUsernamePasswordVerification
public class AccountUsernamePasswordVerificationRequestFilter implements ContainerRequestFilter
{

   @Inject
   AccountRepository accountRepository;

   private final static Logger logger = Logger.getLogger(AccountUsernamePasswordVerificationRequestFilter.class
            .getName());

   @Override
   public void filter(ContainerRequestContext requestCtx) throws IOException
   {
      logger.info("Executing REST request filter");
      try
      {
         String[] pars = HttpUtils.getUsernamePassword(requestCtx.getHeaders());
         String username = pars[0];
         String password = pars[1];
         if (username == null || username.trim().isEmpty())
         {
            logger.error("username empty");
            requestCtx.abortWith(Response.status(Response.Status.BAD_REQUEST).build());
         }
         if (password == null || password.trim().isEmpty())
         {
            logger.error("password empty");
            requestCtx.abortWith(Response.status(Response.Status.BAD_REQUEST).build());
         }

         int result = accountRepository.countByUsernameAndPassword(username, password);
         if (result != 1)
         {
            logger.error("count by username and password failed");
            requestCtx.abortWith(Response.status(Response.Status.BAD_REQUEST).build());
         }
      }
      catch (Exception e)
      {
         requestCtx.abortWith(Response.status(Response.Status.BAD_REQUEST).build());
      }
   }
}
