package es.getdat.accounting.filter;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.jboss.logging.Logger;

import es.getdat.accounting.model.Account;
import es.getdat.accounting.repository.AccountRepository;
import es.getdat.core.annotation.AccountTokenVerification;
import es.getdat.util.HttpUtils;

@Provider
@AccountTokenVerification
public class AccountTokenVerificationRequestFilter implements ContainerRequestFilter
{

   @Inject
   AccountRepository accountRepository;

   private final static Logger logger = Logger.getLogger(AccountTokenVerificationRequestFilter.class.getName());

   @Override
   public void filter(ContainerRequestContext requestCtx) throws IOException
   {
      logger.info("Executing REST request filter");
      try
      {
         String token = HttpUtils.getBearerToken(requestCtx.getHeaders());
         if (token == null || token.trim().isEmpty())
         {
            logger.error("token empty");
            requestCtx.abortWith(Response.status(Response.Status.BAD_REQUEST).build());
         }
         Account account = accountRepository.findByToken(token);
         if (account == null)
         {
            logger.error("no user found by token");
            requestCtx.abortWith(Response.status(Response.Status.BAD_REQUEST).build());
         }
         String userid = null;
         for (String key : requestCtx.getUriInfo().getPathParameters().keySet())
         {
            if (key.equals("userid"))
            {
               List<String> value = requestCtx.getUriInfo().getPathParameters().get(key);
               userid = value.get(0);
               break;
            }
         }
         if (userid == null || userid.isEmpty())
         {
            logger.error("userid path parameter null or empty");
            requestCtx.abortWith(Response.status(Response.Status.BAD_REQUEST).build());
         }

         if (!account.getUserid().equals(userid))
         {
            logger.error("userid params does not match token's userid");
            requestCtx.abortWith(Response.status(Response.Status.BAD_REQUEST).build());
         }
      }
      catch (Exception e)
      {
         requestCtx.abortWith(Response.status(Response.Status.BAD_REQUEST).build());
      }
   }
}
