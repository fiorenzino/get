package es.getdat.accounting.service.rs;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import es.getdat.accounting.model.Account;
import es.getdat.accounting.repository.AccountRepository;
import es.getdat.api.service.GetRepositoryService;
import es.getdat.core.management.AppConstants;

@Path(AppConstants.ACCOUNTS_PATH)
@Stateless
@LocalBean
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AccountRepositoryRest extends GetRepositoryService<Account> {

	private static final long serialVersionUID = 1L;

	@Inject
	AccountRepository accountRepository;

	public AccountRepositoryRest() {
	}

	@Inject
	public AccountRepositoryRest(AccountRepository accountRepository) {
		super(accountRepository);
	}

	@Override
	protected void prePersist(Account account) throws Exception {
		if (account == null) {
			logger.info("account null");
			throw new Exception("account null");
		}
		if (account.getUsername() == null
				|| account.getUsername().trim().isEmpty()) {
			logger.info("account username null or empty");
			throw new Exception("account username null or empty");
		}
		if (accountRepository.countByUsername(account.getUsername(),
				account.getId()) > 0) {
			logger.info("account username already used");
			throw new Exception("account username already used");
		}
	}

	@Override
	protected void preUpdate(Account account) throws Exception {
		prePersist(account);
	}

}
