package es.getdat.accounting.service.rs;

import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.getdat.accounting.repository.ActivationRepository;
import es.getdat.api.model.Content;
import es.getdat.api.model.enums.ChannelType;
import es.getdat.api.service.GetService;
import es.getdat.core.management.AppConstants;
import es.getdat.plugin.model.PluginConfiguration;
import es.getdat.plugin.repository.PluginConfigurationRepository;

@Path(AppConstants.ACCOUNTING_PATH)
@Stateless
@LocalBean
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AccountingRest extends GetService
{

   private static final long serialVersionUID = 1L;

   @Inject
   ActivationRepository activationRepository;

   @Inject
   PluginConfigurationRepository pluginConfigurationRepository;

   @POST
   @Asynchronous
   public void execute(Content content)
   {
      logger.info(content);
   }

   @GET
   @Path("/plugins/{accountId}/pluginconfigurations")
   public Response getPluginConfigurationId(
            @PathParam("accountId") String accountId,
            @QueryParam("channeltype") ChannelType channelType)
   {
      try
      {
         String pluginConfigurationId = activationRepository
                  .getPluginConfigurationId(accountId, channelType, null);
         if (pluginConfigurationId == null
                  || pluginConfigurationId.trim().isEmpty())
         {
            return Response
                     .status(Response.Status.NOT_FOUND)
                     .entity("Resource not found for accountId: "
                              + accountId + " and channelType: "
                              + channelType).build();
         }
         return Response.status(Response.Status.OK)
                  .entity(pluginConfigurationId).build();
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return Response
                  .status(Response.Status.INTERNAL_SERVER_ERROR)
                  .entity("Error reading resource for accountId: "
                           + accountId + " and channelType: " + channelType)
                  .build();
      }
   }

   @GET
   @Path("/plugins/{accountId}/pluginconfigurations/{pluginConfigurationId}")
   public Response getPluginConfiguration(
            @PathParam("accountId") String accountId,
            @PathParam("pluginConfigurationId") String pluginConfigurationId,
            @QueryParam("channeltype") ChannelType channelType)
   {
      try
      {
         String pluginConfigurationIdVerified = activationRepository
                  .getPluginConfigurationId(accountId, channelType,
                           pluginConfigurationId);
         if (pluginConfigurationIdVerified == null
                  || pluginConfigurationIdVerified.trim().isEmpty())
         {
            return Response
                     .status(Response.Status.NOT_FOUND)
                     .entity("Resource not found for accountId: "
                              + accountId + " and channelType: "
                              + channelType).build();
         }
         return Response.status(Response.Status.OK)
                  .entity(pluginConfigurationId).build();
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return Response
                  .status(Response.Status.INTERNAL_SERVER_ERROR)
                  .entity("Error reading resource for accountId: "
                           + accountId + " and channelType: " + channelType)
                  .build();
      }
   }

   /*
    * in the future this service should be come back more values (amount/etc/etc)
    */
   @GET
   @Path("/plugins/{accountId}/pluginconfigurations/content")
   public Response getPluginConfigurationContent(
            @PathParam("accountId") String accountId,
            @QueryParam("channeltype") ChannelType channelType)
   {
      try
      {
         String pluginConfigurationId = activationRepository
                  .getPluginConfigurationId(accountId, channelType, null);
         if (pluginConfigurationId == null
                  || pluginConfigurationId.trim().isEmpty())
         {
            return Response
                     .status(Response.Status.NOT_FOUND)
                     .entity("Plugin Configuration Id not found for accountId: "
                              + accountId
                              + " and channelType: "
                              + channelType).build();
         }

         PluginConfiguration pluginConfiguration = pluginConfigurationRepository
                  .find(pluginConfigurationId);
         if (pluginConfiguration != null)
         {
            return Response.status(Response.Status.OK).entity(new Content(pluginConfiguration.getProperties())).build();
         }
         else
         {
            return Response
                     .status(Response.Status.NOT_FOUND)
                     .entity("Plugin Configuration not found for accountId: "
                              + accountId
                              + " and channelType: "
                              + channelType).build();
         }
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return Response
                  .status(Response.Status.INTERNAL_SERVER_ERROR)
                  .entity("Error reading resource for accountId: "
                           + accountId + " and channelType: " + channelType)
                  .build();
      }

   }

   @GET
   @Path("/up")
   public Response up()
   {
      return Response.status(Response.Status.OK).entity(true).build();
   }
}
