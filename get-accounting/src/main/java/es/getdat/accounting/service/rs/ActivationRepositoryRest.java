package es.getdat.accounting.service.rs;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import es.getdat.accounting.model.Activation;
import es.getdat.accounting.repository.AccountRepository;
import es.getdat.accounting.repository.ActivationRepository;
import es.getdat.api.service.GetRepositoryService;
import es.getdat.core.management.AppConstants;
import es.getdat.plugin.repository.PluginConfigurationRepository;

@Path(AppConstants.ACTIVATIONS_PATH)
@Stateless
@LocalBean
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ActivationRepositoryRest extends GetRepositoryService<Activation>
{

   private static final long serialVersionUID = 1L;

   @Inject
   ActivationRepository activationRepository;

   @Inject
   AccountRepository accountRepository;

   @Inject
   PluginConfigurationRepository pluginConfigurationRepository;

   public ActivationRepositoryRest()
   {
   }

   @Inject
   public ActivationRepositoryRest(ActivationRepository activationRepository)
   {
      super(activationRepository);
   }

   @Override
   protected void prePersist(Activation activation) throws Exception
   {
      if (activation == null)
      {
         throw new Exception("activation null");
      }
      if (activation.getAccountId() == null
               || activation.getAccountId().trim().isEmpty())
      {
         throw new Exception("account id null or empty");
      }
      if (accountRepository.find(activation.getAccountId()) == null)
      {
         throw new Exception("account id doesn't exist");
      }
      if (activation.getPluginConfigurationId() == null
               || activation.getPluginConfigurationId().trim().isEmpty())
      {
         throw new Exception("pluginConfiguration Id null or empty");
      }

      if (!pluginConfigurationRepository.exist(activation.getPluginConfigurationId()))
      {
         throw new Exception("plugin configuration doesn't exist");
      }

      if (activationRepository.countByAccountIdAndPluginConfigurationId(
               activation.getAccountId(),
               activation.getPluginConfigurationId(), activation.getId()) > 0)
      {
         throw new Exception(
                  "the account have already this plugin configuration");
      }
   }

   @Override
   protected void preUpdate(Activation activation) throws Exception
   {
      prePersist(activation);
   }

}
