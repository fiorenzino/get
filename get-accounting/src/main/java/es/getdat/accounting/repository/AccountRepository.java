package es.getdat.accounting.repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import es.getdat.accounting.model.Account;
import es.getdat.accounting.model.enums.AccountType;
import es.getdat.api.repository.AbstractRepository;
import es.getdat.api.repository.Search;

@Stateless
@LocalBean
public class AccountRepository extends AbstractRepository<Account>
{

   private static final long serialVersionUID = 1L;

   @PersistenceContext
   EntityManager em;

   @Override
   protected EntityManager getEm()
   {
      return em;
   }

   @Override
   public void setEm(EntityManager em)
   {
      this.em = em;
   }

   @Override
   protected void applyRestrictions(Search<Account> search, String alias,
            String separator, StringBuffer sb, Map<String, Object> params)
   {
      // id
      if (search.getObj().getId() != null && search.getObj().getId().trim().length() > 0)
      {
         sb.append(separator).append(alias).append(".id = :id ");
         params.put("id", search.getObj().getId());
         separator = " and ";
      }

      // username
      if (search.getObj().getUsername() != null
               && !search.getObj().getUsername().trim().isEmpty())
      {
         sb.append(separator).append(alias).append(".username = :username ");
         params.put("username", search.getObj().getUsername().trim());
         separator = " and ";
      }

      // userid
      if (search.getObj().getUserid() != null
               && !search.getObj().getUserid().trim().isEmpty())
      {
         sb.append(separator).append(alias).append(".userid = :userid ");
         params.put("userid", search.getObj().getUserid().trim());
         separator = " and ";
      }

      // long token
      if (search.getObj().getLongToken() != null
               && !search.getObj().getLongToken().trim().isEmpty())
      {
         sb.append(separator).append(alias).append(".longToken = :longToken ");
         params.put("longToken", search.getObj().getLongToken().trim());
         separator = " and ";
         // sb.append(separator).append(alias)
         // .append(".expirationDate > :now ");
         // params.put("now", new Date());
         // separator = " and ";
      }

      // account type
      if (search.getObj().getAccountType() != null)
      {
         sb.append(separator).append(alias).append(".accountType = :accountType ");
         params.put("accountType", search.getObj().getAccountType());
         separator = " and ";
      }

   }

   @Override
   protected String getDefaultOrderBy()
   {
      return "id";
   }

   public Long countByUsername(String username, String id)
   {
      StringBuffer query = new StringBuffer(
               "select count(a) from Account a where a.username = :username ");
      if (id != null && !id.trim().isEmpty())
      {
         query.append(" and id != :id");
      }
      Query queryObj = getEm().createQuery(query.toString());
      queryObj.setParameter("username", username);
      if (id != null && !id.trim().isEmpty())
      {
         queryObj.setParameter("id", id);
      }
      Long result = (Long) queryObj.getSingleResult();
      logger.info("count: " + result);
      return result;
   }

   public int countByUserId(String userid) throws Exception
   {
      Search<Account> search = new Search<Account>(Account.class);
      search.getObj().setUserid(userid);
      return getListSize(search);
   }

   public Account findByToken(String longToken)
   {
      return findByToken(AccountType.FACEBOOK, longToken);
   }

   private Account findByToken(AccountType accountType, String longToken)
   {
      Date now = new Date();
      try
      {
         return getEm()
                  .createQuery(
                           "select a from "
                                    + Account.class.getSimpleName()
                                    + " a where a.accountType = :ACCOUNTTYPE and a.longToken = :LONGTOKEN "
                           // + "and a.expirationDate > :NOW "
                           ,
                           Account.class).setParameter("ACCOUNTTYPE", accountType).setParameter("LONGTOKEN", longToken)
                  // .setParameter("NOW", now)
                  .getSingleResult();
      }
      catch (NoResultException nre)
      {
         logger.error(nre.getClass().getCanonicalName() + " for " + accountType + ": " + longToken + " @ " + now);
         return null;
      }
      catch (NonUniqueResultException nure)
      {
         logger.error(nure.getClass().getCanonicalName() + " for " + accountType + ": " + longToken + " @ " + now);
         return null;
      }
   }

   public void updateLongTokenAndExpirationDate(String id, String longToken, Date expirationDate) throws Exception
   {
      getEm().createNativeQuery(
               "UPDATE "
                        + Account.TABLE_NAME
                        + "SET longToken = :longToken, expirationDate = :expirationDate  WHERE id = :id ")
               .setParameter("id", id)
               .setParameter("expirationDate", expirationDate)
               .setParameter("longToken", longToken).executeUpdate();
   }

   public Account findByUserid(String userid) throws Exception
   {
      try
      {
         return getEm()
                  .createQuery(
                           "select a from " + Account.class.getSimpleName()
                                    + " a where a.userid = :userid", Account.class)
                  .setParameter("userid", userid).getSingleResult();
      }
      catch (NoResultException nre)
      {
         logger.error(nre.getClass().getCanonicalName() + " for userid: " + userid);
         return null;
      }
      catch (NonUniqueResultException nure)
      {
         logger.error(nure.getClass().getCanonicalName() + " for userid: " + userid);
         return null;
      }
   }

   public int countByUsernameAndPassword(String username, String password)
   {
      try
      {
         Search<Account> search = new Search<Account>(Account.class);
         search.getObj().setUsername(username);
         search.getObj().setPassword(password);
         search.getObj().setAccountType(AccountType.LOCAL);
         return getListSize(search);
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return 0;
      }
   }

   public boolean checkAvailable(String username, String userid, String uuid)
   {
      try
      {
         if (username != null)
         {
            List<Account> accounts = getEm()
                     .createQuery(
                              "select a from " + Account.class.getSimpleName() + " a where a.username = :username ",
                              Account.class).setParameter("username", username).getResultList();
            if (accounts.size() > 0)
            {
               if (uuid == null)
               {
                  return false;
               }
               for (Account account : accounts)
               {
                  if (!uuid.equals(account.getId()))
                  {
                     return false;
                  }
               }
            }
         }
         if (userid != null)
         {
            List<Account> accounts = getEm()
                     .createQuery(
                              "select a from " + Account.class.getSimpleName() + " a where a.userid = :userid ",
                              Account.class).setParameter("userid", userid).getResultList();
            if (accounts.size() > 0)
            {
               if (uuid == null)
               {
                  return false;
               }
               for (Account account : accounts)
               {
                  if (!uuid.equals(account.getId()))
                  {
                     return false;
                  }
               }
            }
         }
         return true;
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return false;
      }
   }
}
