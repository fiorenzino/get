package es.getdat.accounting.repository;

import java.util.List;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import es.getdat.accounting.model.Activation;
import es.getdat.api.model.enums.ChannelType;
import es.getdat.api.repository.AbstractRepository;
import es.getdat.api.repository.Search;

@Stateless
@LocalBean
public class ActivationRepository extends AbstractRepository<Activation>
{

   private static final long serialVersionUID = 1L;

   @PersistenceContext
   EntityManager em;

   @Override
   protected EntityManager getEm()
   {
      return em;
   }

   @Override
   public void setEm(EntityManager em)
   {
      this.em = em;
   }

   @Override
   protected void applyRestrictions(Search<Activation> search, String alias,
            String separator, StringBuffer sb, Map<String, Object> params)
   {
      // accountId;
      if (search.getObj().getAccountId() != null
               && !search.getObj().getAccountId().trim().isEmpty())
      {
         sb.append(separator).append(alias)
                  .append(".accountId = :accountId ");
         params.put("accountId", search.getObj().getAccountId());
         separator = " and ";
      }
      // channelType;
      if (search.getObj().getChannelType() != null)
      {
         sb.append(separator).append(alias)
                  .append(".channelType = :channelType ");
         params.put("channelType", search.getObj().getChannelType());
         separator = " and ";
      }
      // userDefault
      if (search.getObj().isUserDefault())
      {
         sb.append(separator).append(alias)
                  .append(".userDefault = :userDefault ");
         params.put("userDefault", search.getObj().isUserDefault());
         separator = " and ";
      }
      // pluginConfigurationId
      if (search.getObj().getPluginConfigurationId() != null
               && !search.getObj().getPluginConfigurationId().trim().isEmpty())
      {
         sb.append(separator).append(alias)
                  .append(".pluginConfigurationId = :pluginConfigurationId ");
         params.put("pluginConfigurationId", search.getObj().getPluginConfigurationId());
         separator = " and ";
      }
   }

   @Override
   protected String getDefaultOrderBy()
   {
      return "id";
   }

   public String getPluginConfigurationId(String accountId,
            ChannelType channelType, String pluginConfigurationId) throws Exception
   {
      Search<Activation> search = new Search<Activation>(Activation.class);
      search.getObj().setAccountId(accountId);
      search.getObj().setChannelType(channelType);
      search.getObj().setPluginConfigurationId(pluginConfigurationId);
      search.getObj().setUserDefault(true);
      List<Activation> list = getList(search, 0, 1);
      if (list != null && list.size() > 0)
      {
         return list.get(0).getPluginConfigurationId();
      }
      return null;
   }

   public Long countByAccountIdAndPluginConfigurationId(String accountId,
            String pluginConfigurationId, String id)
   {

      StringBuffer query = new StringBuffer(
               "select count(a) from Activation a "
                        + " where a.accountId = :accountId "
                        + " and a.pluginConfigurationId = :pluginConfigurationId ");
      if (id != null && !id.trim().isEmpty())
      {
         query.append(" and id != :id");
      }
      Query queryObj = getEm().createQuery(query.toString());
      queryObj.setParameter("accountId", accountId).setParameter(
               "pluginConfigurationId", pluginConfigurationId);
      if (id != null && !id.trim().isEmpty())
      {
         queryObj.setParameter("id", id);
      }
      Long result = (Long) queryObj.getSingleResult();
      logger.info("count: " + result);
      return result;
   }

   public void deleteByPluginConfigurationId(String id) throws Exception
   {
      int deleted = getEm().createNativeQuery(
               "delete from " + Activation.class.getSimpleName() + " where pluginConfigurationId = :id ")
               .setParameter("id", id).executeUpdate();
      logger.info(deleted + " " + Activation.class.getSimpleName() + "(s) depending on pluginConfigurationId " + id);
   }

   public void deleteByAccountId(String id) throws Exception
   {
      int deleted = getEm().createNativeQuery(
               "delete from " + Activation.class.getSimpleName() + " where accountId = :id ")
               .setParameter("id", id).executeUpdate();
      logger.info(deleted + " " + Activation.class.getSimpleName() + "(s) depending on accountId " + id);
   }
}
