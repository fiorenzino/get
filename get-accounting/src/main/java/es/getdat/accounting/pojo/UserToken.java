package es.getdat.accounting.pojo;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserToken
{
   public String userid;
   public String shortToken;
   public String longToken;
   public Date expirationDate;

   public UserToken()
   {
      // TODO Auto-generated constructor stub
   }

   public UserToken(String userid,
            String shortToken,
            String longToken,
            Date expirationDate)
   {
      this.userid = userid;
      this.shortToken = shortToken;
      this.longToken = longToken;
      this.expirationDate = expirationDate;
   }

   @Override
   public String toString()
   {
      return "UserToken [" + (userid != null ? "userid=" + userid + ", " : "")
               + (shortToken != null ? "shortToken=" + shortToken + ", " : "")
               + (longToken != null ? "longToken=" + longToken + ", " : "")
               + (expirationDate != null ? "expirationDate=" + expirationDate : "") + "]";
   }
   
   
}
