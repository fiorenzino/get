package es.getdat.accounting.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.GenericGenerator;

import es.getdat.accounting.model.enums.AccountType;

@Entity
@XmlRootElement
public class Account implements Serializable
{

   private static final long serialVersionUID = 1L;
   public static final String TABLE_NAME = "account";

   private String id;
   private String username;
   private String password;

   private AccountType accountType;
   private String userid;
   private String longToken;
   private Date expirationDate;

   private String email;
   private String firstName;
   private String lastName;
   private String imageUrl;
   private String phoneNumber;

   @Id
   @GeneratedValue(generator = "uuid")
   @GenericGenerator(name = "uuid", strategy = "uuid2")
   @Column(name = "uuid", unique = true)
   public String getId()
   {
      return id;
   }

   public void setId(String id)
   {
      this.id = id;
   }

   public String getUsername()
   {
      return username;
   }

   public void setUsername(String username)
   {
      this.username = username;
   }

   public String getPassword()
   {
      return password;
   }

   public void setPassword(String password)
   {
      this.password = password;
   }

   @Enumerated(EnumType.STRING)
   @XmlTransient
   public AccountType getAccountType()
   {
      return accountType;
   }

   public void setAccountType(AccountType accountType)
   {
      this.accountType = accountType;
   }

   public String getUserid()
   {
      return userid;
   }

   public void setUserid(String userid)
   {
      this.userid = userid;
   }

   @Column(length = 1024)
   public String getLongToken()
   {
      return longToken;
   }

   public void setLongToken(String longToken)
   {
      this.longToken = longToken;
   }

   @Temporal(TemporalType.TIMESTAMP)
   public Date getExpirationDate()
   {
      return expirationDate;
   }

   public void setExpirationDate(Date expirationDate)
   {
      this.expirationDate = expirationDate;
   }

   public String getEmail()
   {
      return email;
   }

   public void setEmail(String email)
   {
      this.email = email;
   }

   public String getFirstName()
   {
      return firstName;
   }

   public void setFirstName(String firstName)
   {
      this.firstName = firstName;
   }

   public String getLastName()
   {
      return lastName;
   }

   public void setLastName(String lastName)
   {
      this.lastName = lastName;
   }

   public String getImageUrl()
   {
      return imageUrl;
   }

   public void setImageUrl(String imageUrl)
   {
      this.imageUrl = imageUrl;
   }

   public String getPhoneNumber()
   {
      return phoneNumber;
   }

   public void setPhoneNumber(String phoneNumber)
   {
      this.phoneNumber = phoneNumber;
   }

   @Override
   public String toString()
   {
      return "Account [" + (id != null ? "id=" + id + ", " : "")
               + (username != null ? "username=" + username + ", " : "")
               + (password != null ? "password=" + password + ", " : "")
               + (accountType != null ? "accountType=" + accountType + ", " : "")
               + (userid != null ? "userid=" + userid + ", " : "")
               + (longToken != null ? "longToken=" + longToken + ", " : "")
               + (expirationDate != null ? "expirationDate=" + expirationDate + ", " : "")
               + (email != null ? "email=" + email + ", " : "")
               + (firstName != null ? "firstName=" + firstName + ", " : "")
               + (lastName != null ? "lastName=" + lastName + ", " : "")
               + (imageUrl != null ? "imageUrl=" + imageUrl + ", " : "")
               + (phoneNumber != null ? "phoneNumber=" + phoneNumber : "") + "]";
   }

}
