package es.getdat.accounting.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.GenericGenerator;

import es.getdat.api.model.enums.ChannelType;

@Entity
public class Activation implements Serializable
{

   private static final long serialVersionUID = 1L;

   private String id;
   private String accountId;
   private String pluginConfigurationId;
   private ChannelType channelType;
   private boolean userDefault;
   private Date init;
   private Date end;
   private String pluginConfigurationName;
   private String pluginConfigurationDescription;

   @Id
   @GeneratedValue(generator = "uuid")
   @GenericGenerator(name = "uuid", strategy = "uuid2")
   @Column(name = "uuid", unique = true)
   public String getId()
   {
      return id;
   }

   public void setId(String id)
   {
      this.id = id;
   }

   public String getAccountId()
   {
      return accountId;
   }

   public void setAccountId(String accountId)
   {
      this.accountId = accountId;
   }

   public String getPluginConfigurationId()
   {
      return pluginConfigurationId;
   }

   public void setPluginConfigurationId(String pluginConfigurationId)
   {
      this.pluginConfigurationId = pluginConfigurationId;
   }

   @Enumerated(EnumType.STRING)
   public ChannelType getChannelType()
   {
      return channelType;
   }

   public void setChannelType(ChannelType channelType)
   {
      this.channelType = channelType;
   }

   public boolean isUserDefault()
   {
      return userDefault;
   }

   public void setUserDefault(boolean userDefault)
   {
      this.userDefault = userDefault;
   }

   public Date getInit()
   {
      return init;
   }

   public void setInit(Date init)
   {
      this.init = init;
   }

   public Date getEnd()
   {
      return end;
   }

   public void setEnd(Date end)
   {
      this.end = end;
   }

   @Override
   public String toString()
   {
      return "Activation ["
               + (id != null ? "id=" + id + ", " : "")
               + (accountId != null ? "accountId=" + accountId + ", " : "")
               + (pluginConfigurationId != null ? "pluginConfigurationId="
                        + pluginConfigurationId + ", " : "")
               + (channelType != null ? "channelType=" + channelType + ", "
                        : "") + "userDefault=" + userDefault + ", "
               + (init != null ? "init=" + init + ", " : "")
               + (end != null ? "end=" + end : "") + "]";
   }

   @Transient
   @XmlTransient
   public String getPluginConfigurationName()
   {
      return pluginConfigurationName;
   }

   @Transient
   @XmlTransient
   public String getPluginConfigurationDescription()
   {
      return pluginConfigurationDescription;
   }

   public void setPluginConfigurationDescription(String pluginConfigurationDescription)
   {
      this.pluginConfigurationDescription = pluginConfigurationDescription;
   }

   public void setPluginConfigurationName(String pluginConfigurationName)
   {
      this.pluginConfigurationName = pluginConfigurationName;
   }

}
