package es.getdat.accounting.util.fb.longtoken;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlElement;

public class DebugLongTokenResponseData
{
   // "app_id":"752381511504980",
   // "application":"forgetters.club - Test1",
   // "expires_at":1424945986,
   // "is_valid":true,
   // "issued_at":1419761986,
   // "scopes":["public_profile","email","publish_actions"],
   // "user_id":"10205237953655726"
   public String app_id;
   public String application;
   @XmlElement(name = "error")
   public DebugLongTokenResponseError debugLongTokenResponseError;
   public Long expires_at;
   public String is_valid;
   public String user_id;
   public Long issued_at;
   public String[] scopes;

   @Override
   public String toString()
   {
      return (app_id != null ? "app_id=" + app_id + ", " : "")
               + (application != null ? "application=" + application + ", " : "")
               + (debugLongTokenResponseError != null ? "error=" + debugLongTokenResponseError
                        + ", " : "") + (expires_at != null ? "expires_at=" + expires_at + ", " : "")
               + (is_valid != null ? "is_valid=" + is_valid + ", " : "")
               + (user_id != null ? "user_id=" + user_id + ", " : "")
               + (issued_at != null ? "issued_at=" + issued_at + ", " : "")
               + (scopes != null ? "scopes=" + Arrays.toString(scopes) : "");
   }

}
