package es.getdat.accounting.util.fb.shorttoken;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlElement;

public class DebugShortTokenResponseData
{
   // "data": {
   // "app_id": "752381511504980",
   // "application": "forgetters.club - Test1",
   // "error": {
   // "code": 190,
   // "message":
   // "Error validating access token: Session has expired on Dec 26, 2014 5:00am. The current time is Dec 26, 2014 5:56am.",
   // "subcode": 463
   // },
   public String app_id;
   public String application;
   @XmlElement(name = "error")
   public DebugShortTokenResponseError debugTokenResponseError;
   public Long expires_at;
   public String is_valid;
   public String user_id;
   public String[] scopes;

   @Override
   public String toString()
   {
      return (app_id != null ? "app_id=" + app_id + ", " : "")
               + (application != null ? "application=" + application + ", " : "")
               + (debugTokenResponseError != null ? "error=" + debugTokenResponseError + ", " : "")
               + (expires_at != null ? "expires_at=" + expires_at + ", " : "")
               + (is_valid != null ? "is_valid=" + is_valid + ", " : "")
               + (user_id != null ? "user_id=" + user_id + ", " : "")
               + (scopes != null ? "scopes=" + Arrays.toString(scopes) : "");
   }

}
