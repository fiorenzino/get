package es.getdat.accounting.util.fb.longtoken;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DebugLongTokenResponse
{

   // {"data":{
   // "app_id":"752381511504980",
   // "application":"forgetters.club - Test1",
   // "expires_at":1424945986,
   // "is_valid":true,
   // "issued_at":1419761986,
   // "scopes":["public_profile","email","publish_actions"],
   // "user_id":"10205237953655726"
   // }
   // }

   @XmlElement(name = "data")
   public DebugLongTokenResponseData debugLongTokenResponseData;

   @Override
   public String toString()
   {
      return "{"
               + (debugLongTokenResponseData != null ? "data=" + debugLongTokenResponseData : "") + "}";
   }

}
