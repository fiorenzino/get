package es.getdat.router.service.rs;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.Singleton;
import javax.ws.rs.core.Response;

import es.getdat.api.model.Content;
import es.getdat.core.management.AppConstants;

@Singleton
public class ProcessServiceRs
{

   Map<String, Content> events = new ConcurrentHashMap<>();

   public Response updateStep(Content stepContent)
   {

      String eventId = stepContent.get(AppConstants.EVENT_ID);
      String stepId = stepContent.get(AppConstants.STEP_ID);
      if (events.containsKey(eventId))
      {
         Content eventContent = events.get(eventId);
         if (eventContent != null && eventContent.getStepValuesIn().containsKey(stepId))
         {
            eventContent.getStepValuesOut(stepId).clear();
            eventContent.getStepValuesOut(stepId).putAll(stepContent.getStepValuesOut(stepId));

            // QUI DEVO VALUTARE SE POSSO ANDARE AVANTI O DEVO ASPETTARE QUALCHE STEP

            // A QUESTO PUNTO VALUTO LA CONDIZIONE

            return Response.ok().build();
         }
         else
         {
            return Response.serverError().build();
         }

      }
      else
      {
         return Response.serverError().build();
      }
   }
}
