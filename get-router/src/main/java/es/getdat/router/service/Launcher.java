package es.getdat.router.service;

import java.util.List;

import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.getdat.api.model.Content;
import es.getdat.api.model.Process;
import es.getdat.api.model.Step;
import es.getdat.core.management.AppConstants;
import es.getdat.core.management.AppProperties;
import es.getdat.router.util.StepValorizer;
import es.getdat.util.CallUtils;

@Stateless
@LocalBean
public class Launcher
{

   static Logger logger = Logger.getLogger(Launcher.class);

   @Inject
   Cache cache;

   public boolean synchronousCall(String serviceUrl, Content content, Process process, String eventId, String stepId)
   {
      boolean routed = false;
      try
      {
         routed = CallUtils.call(serviceUrl, content);
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
      }
      if (!routed)
      {
         cache.routed(eventId, stepId, content, process, false);
      }
      return routed;
   }

   @Asynchronous
   public void asynchronousCall(String serviceUrl, Content content, Process process, String eventId, String stepId)
   {
      boolean routed = false;
      try
      {
         routed = CallUtils.call(serviceUrl, content);
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
      }
      if (!routed)
      {
         logger.info("NOT ROUTED: " + content);
         cache.routed(eventId, stepId, content, process, false);
      }
   }

   @Asynchronous
   public void launch(String serviceUrl, Content content)
   {
      try
      {
         boolean routed = CallUtils.call(serviceUrl, content);
         if (!routed)
         {
            logger.info("NOT ROUTED: " + content);
         }
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
      }

   }
   @Asynchronous
   public void launch(List<Step> steps, Content content, Step condition, Process process)
   {
      try
      {
         for (Step step : steps)
         {
            Content resp = CallUtils.callWithResponse(content.getIn(step.stepId, AppConstants.SERVICE_URL), content);
            content.addStepValuesOut(step.stepId, resp.getStepValuesOut(step.stepId));
         }
         boolean result = StepValorizer.evaluateCondition(content, condition.condition);
         if (result)
         {
            process.route(condition.stepId);
            content.put(AppConstants.PROCESS, process.toJsonString());
            content.put(AppConstants.STEP_ID, condition.stepId);
            CallUtils.call(AppProperties.baseUrl.value()
                     + AppConstants.API_PATH + AppConstants.ROUTER_PATH, content);
         }

      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
      }
   }

}
