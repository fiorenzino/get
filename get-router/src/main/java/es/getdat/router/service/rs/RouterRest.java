package es.getdat.router.service.rs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.getdat.api.model.Content;
import es.getdat.api.model.Process;
import es.getdat.api.model.Step;
import es.getdat.api.service.GetService;
import es.getdat.core.annotation.RsClientTarget;
import es.getdat.core.management.AppConstants;
import es.getdat.router.service.Cache;
import es.getdat.router.service.Launcher;
import es.getdat.router.util.CallUtils;
import es.getdat.router.util.StepChecker;
import es.getdat.router.util.StepValorizer;
import es.getdat.util.ContentUtils;
import es.getdat.util.DateUtils;

@Path(AppConstants.ROUTER_PATH
// + "_workInProgress"
)
@Stateless
@LocalBean
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class RouterRest extends GetService
{

   private static final long serialVersionUID = 1L;

   @Inject
   Launcher launcher;

   @Inject
   Cache cache;

   @Inject
   @RsClientTarget(AppConstants.ACCOUNTING_PATH)
   Instance<WebTarget> accountingTarget;

   @Inject
   @RsClientTarget(AppConstants.PLUGIN_CONFIGURATIONS_PATH)
   Instance<WebTarget> pluginConfigurationTarget;

   @Inject
   @RsClientTarget(AppConstants.EVENTS_PATH + "/{eventId}/start")
   Instance<WebTarget> eventStartTarget;
   @Inject
   @RsClientTarget(AppConstants.EVENTS_PATH + "/{eventId}/end")
   Instance<WebTarget> eventEndTarget;

   @Context
   HttpServletRequest httpServletRequest;

   @POST
   @Asynchronous
   public void execute(Content content)
   {
      String eventId = null;
      String stepId = null;
      try
      {
         // development information
         logger.info(content);

         // recognize event
         eventId = content.get(AppConstants.EVENT_ID);
         if (eventId == null || eventId.trim().isEmpty())
         {
            String errorMessage = "No eventId for content: " + content;
            logger.error(errorMessage);
            Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                     .entity(errorMessage).build();
         }

         // recognize step
         stepId = content.get(AppConstants.STEP_ID);
         if (stepId == null)
         {
            // we begin a brand new process
            CallUtils.start(eventStartTarget.get(), eventId);
            // first saving to cache must be synchronous to check for uniqueness
            if (!cache.create(content))
            {
               block("Cache creation error", eventId, stepId, content);
            }
         }
         else
         {
            // we are resuming an already started process
            boolean ended = false;

            // cache update can be asynchronous
            Content out = ContentUtils.out(content);
            cache.update(out, ended);
            Content delta = ContentUtils.forward(eventId, stepId, content);
            cache.update(delta, ended);

         }

         // get process definition
         Process process = null;
         try
         {
            String processAsString = content.get(AppConstants.PROCESS);
            if (processAsString != null && processAsString.trim().length() > 0)
            {
               process = new Process(content.get(AppConstants.PROCESS));
            }
            else
            {
               process = cache.getProcess(eventId);
            }
         }
         catch (Exception e)
         {
            logger.error(e.getMessage(), e);
         }
         if (process == null)
         {
            block("Failed to get process definition", eventId, stepId, content);
         }

         // get current step
         Step currentStep = null;
         if (stepId != null)
         {
            currentStep = process.get(stepId);
            // no step means a fatal error
            if (currentStep == null)
            {
               block("Step " + stepId + " not found", eventId, stepId, content);
            }

            // merge process info of invoker with info about other steps, from cache
            if (!cache.merge(eventId, content, process))
            {
               block("Cache merge error", eventId, stepId, content);
            }

            // step not routed means we are here too soon? thus, not going to block. only a warning
            if (!currentStep.routed)
            {
               String warningMessage = "Step " + stepId + " not routed";
               logger.warn(warningMessage);
               // Response.status(Response.Status.PRECONDITION_FAILED)
               // .entity(warningMessage).build();
            }

            List<Step> concurrentSteps = process.concurrent(currentStep.stepId, currentStep.order);
            boolean allSynchronusStepsInParallelHaveEnded = true;
            for (Step concurrentStep : concurrentSteps)
            {
               if (concurrentStep.async)
               {
                  continue;
               }
               String executed = content.getStepValuesOut(concurrentStep.stepId).get(AppConstants.EXECUTED);
               if (executed == null)
               {
                  allSynchronusStepsInParallelHaveEnded = false;
               }
               try
               {
                  if (!Boolean.parseBoolean(executed))
                  {
                     allSynchronusStepsInParallelHaveEnded = false;
                  }
               }
               catch (Exception e)
               {
                  allSynchronusStepsInParallelHaveEnded = false;
               }
            }
            if (!allSynchronusStepsInParallelHaveEnded)
            {
               Object warningMessage = "Synchronous step(s) running in parallel to step " + stepId
                        + " have not ended yet";
               Response.status(Response.Status.PRECONDITION_FAILED)
                        .entity(warningMessage).build();
            }
            if (!StepChecker.checkSatisfied(currentStep.condition, process, content))
            {
               end(eventId, stepId, content);
            }
         }

         // what steps are next?
         List<Step> nextSteps = (currentStep == null) ? process.begin() : process.next(currentStep);

         // check if this is the last process stage
         if (nextSteps.isEmpty())
         {
            end(eventId, stepId, content);
         }

         // check if all steps can be routed
         List<String> blockedStepIds = new ArrayList<String>();

         for (Step step : nextSteps)
         {
            boolean canBeRouted = StepValorizer
                     .valorizeSingle(content, step, accountingTarget, pluginConfigurationTarget);

            // let's abort everything in this case
            if (!canBeRouted)
            {
               blockedStepIds.add(stepId);
            }
         }
         if (blockedStepIds.size() > 0)
         {
            block("no plugin configuration available for step(s): " + blockedStepIds, eventId, stepId, content);
         }

         for (Step step : nextSteps)
         {

            if (StepChecker.checkSatisfied(step.condition, process, content))
            {
               if (step.legacy)
               {
                  fireAndUpdate(step, content, process, eventId);
               }
               else
               {
                  fire(step, content, process, eventId);
               }
            }
            else
            {
               wait("Condition to run step " + step.stepId + "is not yet satisfied", eventId, stepId, content);
            }
         }

         Response.status(Response.Status.NO_CONTENT)
                  .entity("Reponse: " + content.toString()).build();

      }
      catch (Exception e)
      {
         String errorMessage = "Exception = " + e.getClass().getCanonicalName() + ", message = " + e.getMessage()
                  + "\nwhile executing with content: " + content;
         logger.error(e.getMessage(), e);
         try
         {
            block(errorMessage, eventId, stepId, content);
         }
         catch (Exception be)
         {
            logger.error(be.getMessage(), be);
            Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                     .entity(errorMessage).build();
         }
      }
   }

   private Response block(String errorMessage, String eventId, String stepId, Content content) throws Exception
   {
      logger.error(errorMessage);
      Content delta = ContentUtils.block(eventId, stepId, errorMessage, content);
      boolean ended = true;
      cache.update(delta, ended);
      // CallUtils.end(eventEndTarget.get(), eventId);
      return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
               .entity(errorMessage).build();
   }

   private void wait(String warnMessage, String eventId, String stepId, Content content) throws Exception
   {
      logger.warn(warnMessage);
      Content delta = ContentUtils.wait(eventId, stepId, warnMessage, content);
      boolean ended = false;
      cache.update(delta, ended);
   }

   private Response end(String eventId, String stepId, Content content) throws Exception
   {
      Content delta = ContentUtils.end(eventId, stepId, content);
      boolean ended = true;
      cache.update(delta, ended);
      // CallUtils.end(eventEndTarget.get(), eventId);
      return Response.status(Response.Status.NO_CONTENT)
               .entity("Reponse: " + content.toString()).build();
   }

   private void fireAndUpdate(Step step, Content content, Process process, String eventId)
   {
      String when = DateUtils.getPrecisionStringDate();
      String serviceName = "launcher";
      String operationDesc = "fireAndUpdate";
      String externalUuid = "" + System.currentTimeMillis();
      String info = "";
      Map<String, String> parameters = new HashMap<String, String>();
      boolean success = false;
      boolean ended = false;
      try
      {
         content.put(AppConstants.STEP_ID, step.stepId);
         content.put(AppConstants.PROCESS, process.toJsonString());
         String serviceUrl = content.getIn(step.stepId, AppConstants.SERVICE_URL);

         // this has to be done before, otherwise process may continue to the router again before we have finished
         // processing this step
         cache.routed(eventId, step.stepId, content, process, true);

         success = launcher.synchronousCall(serviceUrl, content, process, eventId, step.stepId);
         if (success)
         {
            info = serviceUrl;
         }
         else
         {
            info = "Failed to route";
            ended = true;
         }
      }
      catch (Exception e)
      {
         info = "Exception = " + e.getClass().getCanonicalName() + ", message = " + e.getMessage()
                  + "\nwhile executing with content: " + content;
         logger.error(info, e);
         ended = true;
      }
      finally
      {
         Content delta = ContentUtils.result(eventId, step.stepId, when, serviceName, operationDesc, externalUuid,
                  info, success, content, parameters, null);
         cache.update(delta, ended);
      }
   }

   public void fire(Step step, Content content, Process process, String eventId) throws Exception
   {
      String info = "";
      boolean ended = false;
      try
      {
         content.put(AppConstants.STEP_ID, step.stepId);
         content.put(AppConstants.PROCESS, process.toJsonString());
         String serviceUrl = content.getIn(step.stepId, AppConstants.SERVICE_URL);

         // this has to be done before, otherwise process may continue to the router again before we have finished
         // processing this step
         cache.routed(eventId, step.stepId, content, process, true);

         launcher.asynchronousCall(serviceUrl, content, process, eventId, step.stepId);

      }
      catch (Exception e)
      {
         info = "Exception = " + e.getClass().getCanonicalName() + ", message = " + e.getMessage()
                  + "\nwhile executing with content: " + content;
         logger.error(info, e);
         ended = true;
      }
      finally
      {
         if (ended)
         {
            end(eventId, step.stepId, content);
         }
      }
   }

   // @GET
   // @Path("/up")
   // public Response up()
   // {
   // return Response.status(Response.Status.OK).entity(true).build();
   // }
}
