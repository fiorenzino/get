package es.getdat.router.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.Asynchronous;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.ws.rs.client.WebTarget;

import org.jboss.logging.Logger;

import es.getdat.api.model.Content;
import es.getdat.api.model.Process;
import es.getdat.api.model.Step;
import es.getdat.core.annotation.RsClientTarget;
import es.getdat.core.management.AppConstants;
import es.getdat.util.CallUtils;

@Singleton
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class Cache
{

   Logger logger = Logger.getLogger(getClass());

   @Inject
   @RsClientTarget(AppConstants.EVENTDETAILS_PATH
            + "/events/{eventId}/steps/{stepId}/content")
   Instance<WebTarget> readStepContentTarget;

   @Inject
   @RsClientTarget(AppConstants.EVENTDETAILS_PATH
            + "/events/{eventId}/content")
   Instance<WebTarget> readEventContentTarget;

   @Inject
   @RsClientTarget(AppConstants.EVENTDETAILS_PATH + "/parameters")
   Instance<WebTarget> writeStepContentTarget;

   // @Inject
   // @RsClientTarget(AppConstants.EVENTS_PATH + "/parameters")
   // Instance<WebTarget> writeEventContentTarget;

   @Inject
   @RsClientTarget(AppConstants.EVENTS_PATH + "/{idEvent}/start")
   Instance<WebTarget> eventStartedTarget;

   @Inject
   @RsClientTarget(AppConstants.EVENTS_PATH + "/{idEvent}/end")
   Instance<WebTarget> eventEndedTarget;

   private Map<String, Content> events = new ConcurrentHashMap<>();

   /**
    * a private method to add logging if needed, synchronization and more...
    * 
    * @param id
    * @param content
    */
   private/* synchronized */void put(String id, Content content)
   {
      events.put(id, content);
   }

   public boolean create(Content full)
   {
      try
      {
         String eventId = full.get(AppConstants.EVENT_ID);

         // retrieve from cache / restore from db
         String stepId = null;
         boolean failIfMissing = false;
         Content alreadyIn = retrieve(eventId, stepId, failIfMissing);
         if (alreadyIn != null)
         {
            // should not happen!
            return false;
         }

         // this should be thread safe!!
         put(eventId, full);

         // update remote event repo -
         // TODO - make these asynchronous
         CallUtils.start(eventStartedTarget.get(), eventId);
         CallUtils.details(writeStepContentTarget.get(), full);
         return true;
      }
      catch (Throwable t)
      {
         logger.error(t.getMessage(), t);
         return false;
      }

   }

   @Asynchronous
   public void update(Content delta, boolean ended)
   {

      try
      {
         String eventId = delta.get(AppConstants.EVENT_ID);
         String stepId = delta.get(AppConstants.STEP_ID);

         // retrieve from cache
         boolean failIfMissing = true;
         Content full = retrieve(eventId, stepId, failIfMissing);

         if (ended)
         {
            logger.info("TODO - mark as ended " + ended + " somehow in cache!!!");
            full.getGlobal().put(AppConstants.ENDED, Boolean.TRUE.toString());
            CallUtils.end(eventEndedTarget.get(), eventId);
         }

         // global update...
         for (String k : delta.getGlobal().keySet())
         {
            full.getGlobal().put(k, delta.getGlobal().get(k));
         }
         // step update
         Map<String, String> deltaStepValuesIn = delta.getStepValuesIn(stepId);
         Map<String, String> fullStepValuesIn = full.getStepValuesIn(stepId);
         for (String k : deltaStepValuesIn.keySet())
         {
            fullStepValuesIn.put(k, deltaStepValuesIn.get(k));
         }
         Map<String, String> deltaStepValuesOut = delta.getStepValuesOut(stepId);
         Map<String, String> fullStepValuesOut = full.getStepValuesOut(stepId);
         for (String k : deltaStepValuesOut.keySet())
         {
            fullStepValuesOut.put(k, deltaStepValuesOut.get(k));
         }

         CallUtils.details(writeStepContentTarget.get(), full);
      }
      catch (Throwable t)
      {
         logger.error(t.getMessage(), t);
      }
   }

   public boolean merge(String eventId, Content content, Process process)
   {
      try
      {
         String stepId = null;
         boolean failIfMissing = true;
         Content full = retrieve(eventId, stepId, failIfMissing);

         for (Step step : process.steps)
         {
            String routed = full.getStepValuesIn(step.stepId).get(AppConstants.ROUTED);
            if (routed != null)
            {
               try
               {
                  step.routed = Boolean.parseBoolean(routed);
                  for (String outKey : full.getStepValuesOut(step.stepId).keySet())
                  {
                     String outValue = full.getStepValuesOut(step.stepId).get(outKey);
                     content.getStepValuesOut(step.stepId).put(outKey, outValue);
                  }
               }
               catch (Exception e)
               {
                  logger.error(e.getMessage());
               }
            }
         }
         return true;
      }
      catch (Throwable t)
      {
         logger.error(t.getMessage(), t);
         return false;
      }

   }

   public boolean routed(String eventId, String stepId, Content content, Process process, boolean routed)
   {
      try
      {
         boolean failIfMissing = true;
         Content fullContent = retrieve(eventId, stepId, failIfMissing);
         Process fullProcess = new Process(fullContent.get(AppConstants.PROCESS));
         fullProcess.get(stepId).routed = routed;
         fullContent.put(AppConstants.PROCESS, fullProcess.toJsonString());
         fullContent.getStepValuesIn(stepId).put(AppConstants.ROUTED, "" + routed);

         if (process == null)
         {
            if (content != null)
            {
               process = new Process(fullContent.get(AppConstants.PROCESS));
            }
            process.get(stepId).routed = routed;
            if (content != null)
            {
               content.put(AppConstants.PROCESS, process.toJsonString());
               content.getStepValuesIn(stepId).put(AppConstants.ROUTED, "" + routed);
            }
         }
         return true;
      }
      catch (Throwable t)
      {
         logger.error(t.getMessage(), t);
         return false;
      }
   }

   private Content retrieve(String eventId, String stepId, boolean failIfMissing) throws Exception
   {
      // retrieve from cache
      Content full = events.get(eventId);
      if (full != null)
      {
         return full;
      }

      // if not found we try to restore it from the db
      if (stepId == null)
      {
         full = CallUtils.content(readEventContentTarget.get(), eventId, stepId);
      }
      else
      {
         full = CallUtils.content(readStepContentTarget.get(), eventId, stepId);
      }

      if (full == null)
      {
         if (failIfMissing)
         {
            throw new Exception("Could not restore content for eventId " + eventId + ", stepId " + stepId);
         }
      }
      else
      {
         put(eventId, full);
      }

      return full;
   }

   public Process getProcess(String eventId)
   {
      try
      {
         return new Process(events.get(eventId).get(AppConstants.PROCESS));
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return null;
      }
   }
}
