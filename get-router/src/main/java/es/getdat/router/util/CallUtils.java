package es.getdat.router.util;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.logging.Logger;

import es.getdat.api.model.Content;
import es.getdat.api.model.enums.ChannelType;

public class CallUtils extends es.getdat.util.CallUtils
{

   static Logger logger = Logger.getLogger(CallUtils.class);

   public static String getPluginConfigurationId(String accountId,
            ChannelType channelType, WebTarget target)
   {
      try
      {

         Response res = target
                  .path("/plugins/{accountId}/pluginconfigurations")
                  .resolveTemplate("accountId", accountId)
                  .queryParam("channeltype", channelType).request().get();
         if (res.getStatus() == Status.OK.getStatusCode())
         {
            String pluginConfigurationId = res.readEntity(String.class);
            logger.info(pluginConfigurationId);
            return pluginConfigurationId;
         }

      }
      catch (Exception e)
      {
         logger.info(e.getMessage());
      }
      return null;
   }

   public static Boolean existPluginConfigurationId(String accountId,
            String pluginConfigurationId, ChannelType channelType,
            WebTarget target)
   {
      try
      {

         Response res = target
                  .path("/plugins/{accountId}/pluginconfigurations/{pluginConfigurationId}")
                  .resolveTemplate("accountId", accountId)
                  .resolveTemplate("pluginConfigurationId",
                           pluginConfigurationId)
                  .queryParam("channeltype", channelType).request().get();
         if (res.getStatus() == Status.OK.getStatusCode())
         {
            return true;
         }
         else if (res.getStatus() == Status.NOT_FOUND.getStatusCode())
         {
            return false;
         }
      }
      catch (Exception e)
      {
         logger.info(e.getMessage());
      }
      return null;
   }

   public static Content getPluginContentProperties(
            String pluginConfigurationId, ChannelType channelType,
            WebTarget target)
   {
      try
      {

         Response res = target.path("/{id}/content")
                  .resolveTemplate("id", pluginConfigurationId)
                  .resolveTemplate("channelType", channelType).request()
                  .get();
         if (res.getStatus() == Status.OK.getStatusCode())
         {
            Content content = res.readEntity(Content.class);
            return content;
         }

      }
      catch (Exception e)
      {
         logger.info(e.getMessage());
      }
      return null;
   }

   public static void start(WebTarget webTarget, String eventId)
   {
      Response res = webTarget.resolveTemplate("eventId", eventId).request()
               .buildPut(Entity.entity(eventId, MediaType.APPLICATION_JSON))
               .invoke();
      if (res.getStatus() != Status.NO_CONTENT.getStatusCode())
      {
         logger.error("failed to update event start time!");
      }
   }

   public static void end(WebTarget webTarget, String eventId)
   {
      Response res = webTarget.resolveTemplate("eventId", eventId).request()
               .buildPut(Entity.entity(eventId, MediaType.APPLICATION_JSON))
               .invoke();
      if (res.getStatus() != Status.NO_CONTENT.getStatusCode())
      {
         logger.error("failed to update event start time!");
      }
   }

}
