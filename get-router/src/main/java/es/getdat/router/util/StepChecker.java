package es.getdat.router.util;

import java.util.HashMap;
import java.util.Map;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.SimpleBindings;

import org.jboss.logging.Logger;

import es.getdat.api.model.Content;
import es.getdat.api.model.Process;

public class StepChecker
{
   static Logger logger = Logger.getLogger(StepChecker.class);

   public static boolean checkSatisfied(String condition, Process process, Content content)
   {
      if (condition == null || condition.trim().isEmpty())
      {
         return true;
      }
      // logger.info(condition);
      try
      {
         ScriptEngineManager sem = new ScriptEngineManager();
         Bindings bindings = new SimpleBindings();
         bindings.put("content", content);
         bindings.put("process", process);
         ScriptEngine se = sem.getEngineByName("JavaScript");
         Object result = se.eval(condition, bindings);
         // logger.info(result);
         if (result == null)
         {
            return false;
         }
         else
         {
            return Boolean.parseBoolean("" + result);
         }
      }
      catch (Exception e)
      {
         logger.error(e.getClass().getCanonicalName()
                  + " - " + e.getMessage());
         return false;
      }
   }

   public static void main(String[] args) throws Exception
   {
      ScriptEngineManager sem = new ScriptEngineManager();
      Bindings bindings = new SimpleBindings();
      Map<String, String> map = new HashMap<String, String>();
      bindings.put("map", map);
      ScriptEngine se = sem.getEngineByName("JavaScript");
      String condition = "map.put('test','prova'); map.put('test','esperimento');";
      Object result = se.eval(condition, bindings);
      logger.info(result);
      logger.info(map);

   }
}
