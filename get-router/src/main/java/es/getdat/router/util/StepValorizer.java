package es.getdat.router.util;

import javax.enterprise.inject.Instance;
import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.SimpleBindings;
import javax.ws.rs.client.WebTarget;

import org.jboss.logging.Logger;

import es.getdat.api.model.Content;
import es.getdat.api.model.Process;
import es.getdat.api.model.Step;
import es.getdat.api.model.enums.ChannelType;
import es.getdat.core.management.AppConstants;

public class StepValorizer
{
   static Logger logger = Logger.getLogger(StepValorizer.class);

   public static boolean valorizeSingle(Content content, Step step, Instance<WebTarget> accountingTarget,
            Instance<WebTarget> pluginConfigurationTarget) throws
            Exception
   {

      // checking required plugin configuration for this step
      if (step.pluginConfigurationId != null && !step.pluginConfigurationId.isEmpty())
      {
         // verify against account id and channel type
         if (step.accountId != null && !step.accountId.isEmpty()
                  && step.channelType != null && !step.channelType.isEmpty())
         {
            boolean exists = CallUtils.existPluginConfigurationId(
                     step.accountId, step.pluginConfigurationId, ChannelType.valueOf(step.channelType),
                     accountingTarget.get());
            if (!exists)
            {
               logger.error("CallUtils.existPluginConfigurationId DOES NOT EXIST!" + step.pluginConfigurationId);
               // TODO - loggare come event questo fatto?
               return false;
            }
            else
            {
               logger.debug("CallUtils.existPluginConfigurationId VERIFIED!" + step.pluginConfigurationId);
            }
         }
         // no suitabile parameters to verify against
         else
         {
            logger.error("CallUtils.existPluginConfigurationId CANNOT BE VERIFIED!" + step.pluginConfigurationId);
            // TODO - loggare come event questo fatto?
            return false;
         }
      }

      // choosing plugin configuration for this step
      else if (step.accountId != null && !step.accountId.isEmpty()
               && step.channelType != null && !step.channelType.isEmpty())

      {
         String pluginConfigurationId = CallUtils.getPluginConfigurationId(
                  step.accountId, ChannelType.valueOf(step.channelType), accountingTarget.get());
         if (pluginConfigurationId == null || pluginConfigurationId.isEmpty())
         {
            logger.error("I CAN'T RESOLVE pluginConfigurationId: "
                     + step.pluginConfigurationId + " USING accountId:"
                     + step.accountId + " - channel:" + step.channelType);
            // TODO - loggare come event questo fatto
            return false;
         }
         else
         {
            logger.debug("CallUtils.existPluginConfigurationId FOUND!" + step.pluginConfigurationId);
            step.pluginConfigurationId = pluginConfigurationId;
         }

      }

      // if we are here, we have a valid plugin configuration for sure
      Content pluginContent = CallUtils.getPluginContentProperties(
               step.pluginConfigurationId, ChannelType.valueOf(step.channelType),
               pluginConfigurationTarget.get());

      // TODO - DEVE SEMPRE ESSERCI?
      if (pluginContent == null)
      {
         logger.error("CallUtils.existPluginConfigurationId NO PLUGIN CONTENT! pluginConfigurationId: "
                  + step.pluginConfigurationId + " - step.channelType: "
                  + step.channelType);
         // TODO - loggare come event questo fatto
         return false;
      }

      content.put(AppConstants.STEP_ID, step.stepId);
      content.addStepValuesIn(step.stepId, pluginContent.getGlobal());
      return true;
   }

   public static boolean evaluateCondition(Content content, String condition) throws Exception
   {
      ScriptEngineManager sem = new ScriptEngineManager();
      Bindings bindings = new SimpleBindings();
      bindings.put("content", content);
      ScriptEngine se = sem.getEngineByName("JavaScript");
      Boolean result = (Boolean) se.eval(condition, bindings);
      logger.info(result);
      return result;
   }

}
