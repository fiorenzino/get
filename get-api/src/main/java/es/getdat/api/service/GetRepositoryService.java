package es.getdat.api.service;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.jboss.logging.Logger;

import es.getdat.api.model.PaginatedListWrapper;
import es.getdat.api.repository.Repository;
import es.getdat.api.repository.Search;
import es.getdat.api.util.RepositoryUtils;

public abstract class GetRepositoryService<T> implements Serializable
{

   private static final long serialVersionUID = 1L;

   protected final Logger logger = Logger.getLogger(getClass());

   private Repository<T> repository;

   public GetRepositoryService()
   {

   }

   public GetRepositoryService(Repository<T> repository)
   {
      this.repository = repository;
   }

   /*
    * C
    */

   protected void prePersist(T object) throws Exception
   {
   }

   @POST
   public Response persist(T object) throws Exception
   {
      try
      {
         prePersist(object);
      }
      catch (Exception e)
      {
         logger.error(e.getMessage());
         return Response
                  .status(Response.Status.BAD_REQUEST)
                  .entity("Error before creating resource: " + e.getMessage())
                  .build();
      }
      try
      {
         T persisted = repository.persist(object);
         if (persisted == null || getId(persisted) == null)
         {
            logger.error("Failed to create resource: " + object);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                     .entity("Failed to create resource: " + object).build();
         }
         else
         {
            return Response.status(Response.Status.OK).entity(persisted)
                     .build();
         }
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                  .entity("Error creating resource: " + object).build();
      }
      finally
      {
         try
         {
            postPersist(object);
         }
         catch (Exception e)
         {
            logger.error(e.getMessage(), e);
         }
      }
   }

   protected void postPersist(T object) throws Exception
   {
   }

   /*
    * R
    */

   @GET
   @Path("/{id}")
   public Response fetch(@PathParam("id") String id)
   {
      try
      {
         T t = repository.fetch(repository.castId(id));
         if (t == null)
         {
            return Response.status(Response.Status.NOT_FOUND)
                     .entity("Resource not found for ID: " + id).build();
         }
         else
         {
            return Response.status(Response.Status.OK).entity(t).build();
         }
      }
      catch (NoResultException e)
      {
         logger.error(e.getMessage());
         return Response.status(Response.Status.NOT_FOUND)
                  .entity("Resource not found for ID: " + id).build();
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                  .entity("Error reading resource for ID: " + id).build();
      }
   }

   /*
    * U
    */

   protected void preUpdate(T object) throws Exception
   {
   }

   @PUT
   public Response update(T object) throws Exception
   {
      try
      {
         preUpdate(object);
      }
      catch (Exception e)
      {
         return Response
                  .status(Response.Status.BAD_REQUEST)
                  .entity("Errore before updating resource: "
                           + e.getMessage()).build();
      }
      try
      {
         repository.update(object);
         return Response.status(Response.Status.OK).entity(object).build();
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                  .entity("Error updating resource: " + object).build();
      }
      finally
      {
         try
         {
            postUpdate(object);
         }
         catch (Exception e)
         {
            logger.error(e.getMessage(), e);
         }
      }
   }

   protected void postUpdate(T object) throws Exception
   {
   }

   /*
    * D
    */

   @DELETE
   @Path("/{id}")
   public Response delete(@PathParam("id") String id) throws Exception
   {
      try
      {
         repository.delete(id);
         return Response.status(Response.Status.NO_CONTENT)
                  .entity("Resource deleted for ID: " + id).build();
      }
      catch (NoResultException e)
      {
         logger.error(e.getMessage());
         return Response.status(Response.Status.NOT_FOUND)
                  .entity("Resource not found for ID: " + id).build();
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                  .entity("Error deleting resource for ID: " + id).build();
      }
   }

   /*
    * E
    */

   @GET
   @Path("/{id}/exist")
   public Response exist(@PathParam("id") String id)
   {
      try
      {
         boolean exist = repository.exist(repository.castId(id));
         if (!exist)
         {
            return Response.status(Response.Status.NOT_FOUND)
                     .entity("Resource not found for ID: " + id).build();
         }
         else
         {
            return Response.status(Response.Status.FOUND)
                     .entity("Resource exists for ID: " + id).build();
         }
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                  .entity("Error reading resource for ID: " + id).build();
      }
   }

   /*
    * Q
    */

   @GET
   public Response getList(
            @DefaultValue("0") @QueryParam("startRow") Integer startRow,
            @DefaultValue("10") @QueryParam("pageSize") Integer pageSize,
            @QueryParam("orderBy") String orderBy, @Context UriInfo ui)
   {
      try
      {
         Search<T> search = getSearch(ui, orderBy);
         int listSize = repository.getListSize(search);
         List<T> list = repository.getList(search, startRow, pageSize);
         PaginatedListWrapper<T> wrapper = new PaginatedListWrapper<>();
         wrapper.setList(list);
         wrapper.setListSize(listSize);
         wrapper.setStartRow(startRow);
         return Response.status(Status.OK).entity(wrapper).build();
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                  .entity("Error reading resource list").build();
      }
   }

   protected Search<T> getSearch(UriInfo ui, String orderBy)
   {
      Search<T> s = new Search<T>(getClassType());
      if (orderBy != null && !orderBy.trim().isEmpty())
      {
         s.setOrder(orderBy);
      }
      if (ui != null && ui.getQueryParameters() != null
               && !ui.getQueryParameters().isEmpty())
      {
         MultivaluedMap<String, String> queryParams = ui
                  .getQueryParameters();
         // TODO - DA TESTARE:
         // tutte le prop su search.getObj() sono in "chiaro" - senza prefissi
         // tutte le prop su oggetti search.getFrom() - search.getTo() sono con prefisso: es.from.id - from.dataInit

         for (String key : queryParams.keySet())
         {
            try
            {
               T instance;
               Field field;
               String value = queryParams.getFirst(key);
               if (key.contains("."))
               {
                  String obj = key.substring(0, key.indexOf("."));
                  switch (obj)
                  {
                  case "from":
                     instance = s.getFrom();
                     break;
                  case "to":
                     instance = s.getTo();
                     break;
                  default:
                     instance = s.getObj();
                     break;
                  }
                  String prop = key.substring(key.indexOf(".") + 1);
                  field = s.getObj().getClass().getDeclaredField(prop);
               }
               else
               {
                  instance = s.getObj();
                  field = s.getObj().getClass().getDeclaredField(key);
               }

               if (field != null)
               {
                  field.setAccessible(true);
                  if (field.getType().equals(Double.class))
                  {
                     field.set(instance, new Double(value));
                  }
                  else if (field.getType().equals(Integer.class))
                  {
                     field.set(instance, new Integer(value));
                  }
                  else if (field.getType().equals(Long.class))
                  {
                     field.set(instance, new Long(value));
                  }
                  else if (field.getType().equals(BigDecimal.class))
                  {
                     field.set(instance, new BigDecimal(value));
                  }
                  else if (field.getType().equals(Date.class))
                  {
                     DateFormat df = new SimpleDateFormat("yyyyMMddHHmm");
                     field.set(instance, df.parse(value));
                  }
                  else if (field.getType().isEnum())
                  {
                     for (Object enumConstant : field.getType().getEnumConstants())
                     {
                        if (enumConstant.toString().equals(value))
                        {
                           field.set(instance, enumConstant);
                        }
                     }
                  }
                  else
                  {
                     field.set(instance, value);
                  }

               }
            }
            catch (ParseException | NoSuchFieldException | SecurityException
                     | IllegalArgumentException | IllegalAccessException e)
            {
               logger.error(e.getMessage());
            }
         }

      }
      return s;
   }

   protected Repository<T> getRepository()
   {
      return repository;
   }

   @SuppressWarnings({ "rawtypes", "unchecked" })
   private Class<T> getClassType()
   {
      Class clazz = getClass();
      while (!(clazz.getGenericSuperclass() instanceof ParameterizedType))
      {
         clazz = clazz.getSuperclass();
      }
      ParameterizedType parameterizedType = (ParameterizedType) clazz
               .getGenericSuperclass();
      return (Class<T>) parameterizedType.getActualTypeArguments()[0];
   }

   /**
    * Override this is needed
    * 
    * @param t
    * @return
    */
   protected Object getId(T t)
   {
      return RepositoryUtils.getId(t);
   }
}