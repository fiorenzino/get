package es.getdat.api.management;

public class AppConstants
{

   public static final String APP_NAME = "get";

   public static final String WHEN = "when";
   public static final String TIMEZONE = "timezone";
   public static final String DST = "dst";

   public static final String PROCESS = "process";
   public static final String SUCCESS_CONDITION = "successCondition";
   public static final String CHANNEL_TYPE = "channelType";
   public static final String SERVICE_URL = "serviceUrl";
   public static final String SERVICE_NAME = "serviceName";
   public static final String EXTERNAL_ID = "externalId";

   public static final String ACCOUNT_ID = "accountId";
   public static final String PLUGINCONFIGURATION_ID = "pluginConfigurationId";
   public static final String ORDER = "order";

   public static final String EVENT_ID = "eventId";
   public static final String STEP_ID = "stepId";
   public static final String EVENT_DETAILS_TYPE = "eventDetailsType";

   public static final String OPERATION = "operation";
   public static final String EXECUTED = "executed";
   public static final String SUCCESS = "success";
   public static final String PREPARED = "prepared";
   public static final String ENDED = "ended";
   public static final String HOST = "host";
   public static final String INFO = "info";
   public static final String ROUTED = "routed";
   public static final String CONDITION = "condition";
   public static final String ASYNC = "async";

   @Deprecated
   public static final String EVENT_CLOSE = "eventClose";

   public static final String JBOSS_QUALIFIED_HOST_NAME_PROPERTY = "jboss.qualified.host.name";

}
