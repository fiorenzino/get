package es.getdat.api.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import es.getdat.api.model.enums.ContentValueType;

@XmlRootElement
public class ContentValue implements Serializable
{
   private static final long serialVersionUID = 1L;
   private String eventId;
   private String stepId;
   private String name;
   private String value;
   private ContentValueType contentValueType;

   public ContentValue()
   {
   }

   public ContentValue(String eventId, String stepId, String name, String value, ContentValueType contentValueType)
   {
      this.eventId = eventId;
      this.stepId = stepId;
      this.name = name;
      this.value = value;
      this.contentValueType = contentValueType;
   }

   public String getEventId()
   {
      return eventId;
   }

   public void setEventId(String eventId)
   {
      this.eventId = eventId;
   }

   public String getStepId()
   {
      return stepId;
   }

   public void setStepId(String stepId)
   {
      this.stepId = stepId;
   }

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public String getValue()
   {
      return value;
   }

   public void setValue(String value)
   {
      this.value = value;
   }

   public ContentValueType getContentValueType()
   {
      return contentValueType;
   }

   public void setContentValueType(ContentValueType contentValueType)
   {
      this.contentValueType = contentValueType;
   }
}
