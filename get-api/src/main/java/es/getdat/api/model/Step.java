package es.getdat.api.model;

import static es.getdat.api.management.AppConstants.ACCOUNT_ID;
import static es.getdat.api.management.AppConstants.ASYNC;
import static es.getdat.api.management.AppConstants.CHANNEL_TYPE;
import static es.getdat.api.management.AppConstants.CONDITION;
import static es.getdat.api.management.AppConstants.EXECUTED;
import static es.getdat.api.management.AppConstants.ORDER;
import static es.getdat.api.management.AppConstants.PLUGINCONFIGURATION_ID;
import static es.getdat.api.management.AppConstants.ROUTED;
import static es.getdat.api.management.AppConstants.STEP_ID;
import static es.getdat.api.management.AppConstants.SUCCESS;

import java.io.Serializable;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public class Step implements Serializable
{
   public static final long serialVersionUID = 1L;
   public String stepId;
   public String pluginConfigurationId;
   public String channelType;
   public String accountId;
   public int order;

   public boolean routed;

   /**
    * may we split this into pre- and post-conditions?
    */
   public String condition;

   /**
    * legacy steps must be invoked synchronously to ensure results are written to its corresponding content step out
    * values
    */
   public boolean legacy;

   /**
    * among a set of concurrent steps, we avoid checking that an async step has been executed right before we advance to
    * the next level steps
    * 
    * the chosen 'async' name is infamous , anyway, in order to identify this type of behaviour
    */
   public boolean async;

   public boolean executed;

   public boolean success;

   public Step()
   {

   }

   public Step(String stepId, String pluginConfigurationId, String channelType,
            String accountId)
   {
      this.stepId = stepId;
      this.pluginConfigurationId = pluginConfigurationId;
      this.channelType = channelType;
      this.accountId = accountId;
   }

   public Step(String stepId, String pluginConfigurationId, String channelType,
            String accountId, boolean sync)
   {
      this.stepId = stepId;
      this.pluginConfigurationId = pluginConfigurationId;
      this.channelType = channelType;
      this.accountId = accountId;
      this.async = sync;
   }

   public Step(String stepId, String pluginConfigurationId, String channelType,
            String accountId, int order)
   {
      this.stepId = stepId;
      this.pluginConfigurationId = pluginConfigurationId;
      this.channelType = channelType;
      this.accountId = accountId;
      this.order = order;
   }

   public Step(String stepId, String pluginConfigurationId, String channelType,
            String accountId, String condition)
   {
      this.stepId = stepId;
      this.pluginConfigurationId = pluginConfigurationId;
      this.channelType = channelType;
      this.accountId = accountId;
      this.condition = condition;
   }

   public Step(String stepId, String condition)
   {
      this.stepId = stepId;
      this.condition = condition;
   }

   public Step(JsonObject jsonObject)
   {
      if (jsonObject.containsKey(STEP_ID))
         this.stepId = jsonObject.getString(STEP_ID);
      if (jsonObject.containsKey(PLUGINCONFIGURATION_ID))
         this.pluginConfigurationId = jsonObject.getString(PLUGINCONFIGURATION_ID);
      if (jsonObject.containsKey(CHANNEL_TYPE))
         this.channelType = jsonObject.getString(CHANNEL_TYPE);
      if (jsonObject.containsKey(ACCOUNT_ID))
         this.accountId = jsonObject.getString(ACCOUNT_ID);
      if (jsonObject.containsKey(ROUTED))
         this.routed = jsonObject.getBoolean(ROUTED);
      if (jsonObject.containsKey(CONDITION))
         this.condition = jsonObject.getString(CONDITION);
      if (jsonObject.containsKey(ORDER))
         this.order = jsonObject.getInt(ORDER);
      if (jsonObject.containsKey(ASYNC))
         this.async = jsonObject.getBoolean(ASYNC);
      if (jsonObject.containsKey(EXECUTED))
         this.executed = jsonObject.getBoolean(EXECUTED);
      if (jsonObject.containsKey(SUCCESS))
         this.success = jsonObject.getBoolean(SUCCESS);
   }

   public JsonObject toJson()
   {
      JsonObjectBuilder builder = Json.createObjectBuilder();
      if (pluginConfigurationId != null && !pluginConfigurationId.isEmpty())
         builder.add(PLUGINCONFIGURATION_ID, pluginConfigurationId);
      if (channelType != null && !channelType.isEmpty())
         builder.add(CHANNEL_TYPE, channelType);
      if (accountId != null && !accountId.isEmpty())
         builder.add(ACCOUNT_ID, accountId);
      if (stepId != null && !stepId.isEmpty())
         builder.add(STEP_ID, stepId);
      if (condition != null && !condition.isEmpty())
         builder.add(CONDITION, condition);
      builder.add(ORDER, order);
      builder.add(ROUTED, routed);
      builder.add(ASYNC, async);
      builder.add(EXECUTED, executed);
      builder.add(SUCCESS, success);
      return builder.build();
   }

   public String toJsonString()
   {
      return toJson().toString();
   }

   @Override
   public String toString()
   {
      return "Step [" + (stepId != null ? "stepId=" + stepId + ", " : "")
               + (pluginConfigurationId != null ? "pluginConfigurationId=" + pluginConfigurationId + ", " : "")
               + (channelType != null ? "channelType=" + channelType + ", " : "")
               + (accountId != null ? "accountId=" + accountId + ", " : "") + "order=" + order + ", routed=" + routed
               + ", " + (condition != null ? "condition=" + condition + ", " : "") + "legacy=" + legacy + ", async="
               + async + ", executed=" + executed + ", success=" + success + "]";
   }

}
