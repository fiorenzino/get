package es.getdat.api.model;

import static es.getdat.api.management.AppConstants.PROCESS;
import static es.getdat.api.management.AppConstants.SUCCESS_CONDITION;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public class Process implements Serializable
{
   private static final long serialVersionUID = 1L;
   public List<Step> steps;
   private int levels;

   // DOVREI AGGIUNGERE UNA CONDIZIONE DI SUCCESSO
   public String successCondition;

   public Process()
   {
      this.steps = new ArrayList<>();
   }

   public Process add(Step step)
   {
      this.levels++;
      step.order = this.levels;
      this.steps.add(step);
      return this;
   }

   public Process add(Step... steps)
   {
      this.levels++;
      for (Step step : steps)
      {
         step.order = this.levels;
         this.steps.add(step);
      }
      return this;
   }

   public Process(Step... steps)
   {
      this.levels++;
      this.steps = new ArrayList<>();
      for (Step step : steps)
      {
         step.order = this.levels;
         this.steps.add(step);
      }
   }

   public Process(List<Step> steps)
   {
      for (Step step : steps)
      {
         add(step);
      }
   }

   public Process(String jsonString) throws Exception
   {

      this(Json.createReader(new ByteArrayInputStream(jsonString.getBytes("UTF-8"))).readObject());
   }

   public Process(JsonObject jsonObject)
   {
      this.steps = new ArrayList<>();
      JsonArray jsonArray = jsonObject.getJsonArray(PROCESS);
      for (int i = 0; i < jsonArray.size(); i++)
      {
         JsonObject jsonObject1 = (JsonObject) jsonArray.get(i);
         add(new Step(jsonObject1));
      }
      if (jsonObject.containsKey(SUCCESS_CONDITION))
         this.successCondition = jsonObject.getString(SUCCESS_CONDITION);
   }

   public JsonObject toJson()
   {
      JsonArrayBuilder processArrayBuilder =
               Json.createArrayBuilder();
      for (Step step : steps)
      {
         processArrayBuilder.add(step.toJson());
      }
      JsonObjectBuilder builder = Json.createObjectBuilder();
      if (successCondition != null && !successCondition.isEmpty())
         builder.add(SUCCESS_CONDITION, successCondition);
      return builder.add(PROCESS, processArrayBuilder.build()).build();
   }

   public String toJsonString()
   {
      return toJson().toString();
   }

   public Step get(String stepId)
   {
      for (Step step : steps)
      {
         if (step.stepId.equals(stepId))
         {
            return step;
         }
      }
      return null;
   }

   public void route(String stepId)
   {
      for (int i = 0; i < steps.size(); i++)
      {
         if (steps.get(i).stepId.equals(stepId))
         {
            steps.get(i).routed = true;
            return;
         }
      }
   }

   public void unroute(String stepId)
   {
      for (int i = 0; i < steps.size(); i++)
      {
         if (steps.get(i).stepId.equals(stepId))
         {
            steps.get(i).routed = false;
            return;
         }
      }
   }

   public void execute(String stepId)
   {
      for (int i = 0; i < steps.size(); i++)
      {
         if (steps.get(i).stepId.equals(stepId))
         {
            steps.get(i).executed = true;
            return;
         }
      }
   }

   public void success(String stepId)
   {
      for (int i = 0; i < steps.size(); i++)
      {
         if (steps.get(i).stepId.equals(stepId))
         {
            steps.get(i).success = true;
            return;
         }
      }
   }

   public List<Step> begin()
   {
      List<Step> nextSteps = new ArrayList<>();
      for (Step step : steps)
      {
         if (step.order == 1 && !step.routed)
         {
            nextSteps.add(step);
         }
      }
      return nextSteps;
   }

   public List<Step> next(Step currentStep)
   {
      if (currentStep == null || !currentStep.routed)
      {
         return new ArrayList<Step>();
      }
      List<Step> nextSteps = new ArrayList<>();
      for (Step step : steps)
      {
         if (step.order == currentStep.order + 1 && !step.routed)
         {
            nextSteps.add(step);
         }
      }
      return nextSteps;
   }

   public List<Step> get(int order)
   {
      return concurrent(null, order);
   }

   public List<Step> concurrent(String stepIdToExclude, int order)
   {
      List<Step> result = new ArrayList<>();
      for (int i = 0; i < steps.size(); i++)
      {
         if (steps.get(i).order == order && !steps.get(i).stepId.equals(stepIdToExclude))
         {
            result.add(steps.get(i));
         }
      }
      return result;
   }

   public boolean isLast(int order)
   {
      return order == levels;
   }

}
