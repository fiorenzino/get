package es.getdat.api.model.enums;

public enum ChannelType {
	WEBHOOK, BUZZ, CALL, SMS, EMAIL;
}
