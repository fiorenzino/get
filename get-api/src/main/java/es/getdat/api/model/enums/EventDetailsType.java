package es.getdat.api.model.enums;

public enum EventDetailsType
{
   IN, OUT, FORWARD, END, WAIT;
}
