package es.getdat.api.model;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PaginatedListWrapper<T>
{
   /**
    * Primo risultato.. aggiungrei anche l'order tra i campi del wrapper
    */
   private Integer startRow;

   /**
    * A che serve? non c'è anche list?
    */
   private Integer pageSize;

   /**
    * lo chiamiamo totalSize, così non si confonde con la size della list?
    */
   private Integer listSize;

   /**
    * la chiamiamo page, listPage o cmq qualcosa di diverso da list che sembra la lista completa?
    */
   private List<T> list;

   /**
    * lo aggiungiamo?
    */
   private String orderBy;

   public PaginatedListWrapper()
   {
      // TODO Auto-generated constructor stub
   }

   public PaginatedListWrapper(Integer startRow, Integer pageSize,
            Integer listSize, List<T> list)
   {
      this.startRow = startRow;
      this.pageSize = pageSize;
      this.listSize = listSize;
      this.list = list;
   }

   public Integer getStartRow()
   {
      return startRow;
   }

   public void setStartRow(Integer startRow)
   {
      this.startRow = startRow;
   }

   public Integer getPageSize()
   {
      return pageSize;
   }

   public void setPageSize(Integer pageSize)
   {
      this.pageSize = pageSize;
   }

   public Integer getListSize()
   {
      return listSize;
   }

   public void setListSize(Integer listSize)
   {
      this.listSize = listSize;
   }

   public List<T> getList()
   {
      return list;
   }

   public void setList(List<T> list)
   {
      this.list = list;
   }
}