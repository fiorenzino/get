package es.getdat.api.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ResourceUpload implements Serializable {

	private static final long serialVersionUID = 1L;
	private String accoutId;
	private String filename;
	private byte[] bytes;

	public ResourceUpload() {
		// TODO Auto-generated constructor stub
	}

	public ResourceUpload(String accoutId, String filename, byte[] bytes) {
		this.accoutId = accoutId;
		this.filename = filename;
		this.bytes = bytes;
	}

	public String getAccoutId() {
		return accoutId;
	}

	public void setAccoutId(String accoutId) {
		this.accoutId = accoutId;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}
}
