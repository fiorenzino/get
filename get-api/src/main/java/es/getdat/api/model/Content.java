package es.getdat.api.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement
public class Content implements Serializable
{

   private static final long serialVersionUID = 1L;
   private static final String SLA = "\"";
   private static final String O_GRAF = "{";
   private static final String C_GRAF = "}";
   private static final String O_QUA = "[";
   private static final String C_QUA = "]";
   private static final String DUE_P = ":";
   private static final String VIR = ",";
   private static final String A_CAP = "\n";

   // QUI CI VANNO VARIABILI COME: PROCESS, EVENT_ID, STEP_ID, WHEN, TIMEZONE
   private Map<String, String> global;
   private Map<String, Map<String, String>> stepValuesIn;
   private Map<String, Map<String, String>> stepValuesOut;

   public Content()
   {
   }

   public Content(Map<String, String> global)
   {
      this.global = global;
   }

   public Map<String, String> getGlobal()
   {
      if (global == null)
         this.global = new HashMap<String, String>();
      return global;
   }

   public void setGlobal(Map<String, String> global)
   {
      this.global = global;
   }

   public Map<String, Map<String, String>> getStepValuesIn()
   {
      if (stepValuesIn == null)
         stepValuesIn = new LinkedHashMap<>();
      return stepValuesIn;
   }

   public void setStepValuesIn(Map<String, Map<String, String>> stepValuesIn)
   {
      this.stepValuesIn = stepValuesIn;
   }

   public Map<String, Map<String, String>> getStepValuesOut()
   {
      if (stepValuesOut == null)
         stepValuesOut = new LinkedHashMap<>();
      return stepValuesOut;
   }

   public void setStepValuesOut(Map<String, Map<String, String>> stepValuesOut)
   {
      this.stepValuesOut = stepValuesOut;
   }

   @XmlTransient
   public String getIn(String stepId, String key)
   {
      Map<String, String> firstMap = getStepValuesIn().get(stepId);
      if (firstMap != null)
         return firstMap.get(key);
      return null;
   }

   public Content putIn(String stepId, String key, String value)
   {
      if (getStepValuesIn().containsKey(stepId))
      {
         Map<String, String> map = getStepValuesIn().get(stepId);
         map.put(key, value);
      }
      else
      {
         Map<String, String> map = new HashMap<>();
         map.put(key, value);
         getStepValuesIn().put(stepId, map);
      }
      return this;
   }

   @XmlTransient
   public List<String> getAllIn(String key)
   {
      List<String> all = new ArrayList<>();
      for (String stepId : getStepValuesIn().keySet())
      {
         Map<String, String> map = getStepValuesIn(stepId);
         if (map.containsKey(key))
         {
            all.add(map.get(key));
         }
      }
      return all;
   }

   @XmlTransient
   public String getOut(String stepId, String key)
   {
      Map<String, String> firstMap = getStepValuesOut().get(stepId);
      if (firstMap != null)
         return firstMap.get(key);
      return null;
   }

   public Content putOut(String stepId, String key, String value)
   {
      if (getStepValuesOut().containsKey(stepId))
      {
         Map<String, String> map = getStepValuesOut().get(stepId);
         map.put(key, value);
      }
      else
      {
         Map<String, String> map = new HashMap<>();
         map.put(key, value);
         getStepValuesOut().put(stepId, map);
      }
      return this;
   }

   @XmlTransient
   public List<String> getAllOut(String key)
   {
      List<String> all = new ArrayList<>();
      for (String stepId : getStepValuesOut().keySet())
      {
         Map<String, String> map = getStepValuesOut(stepId);
         if (map.containsKey(key))
         {
            all.add(map.get(key));
         }
      }
      return all;
   }

   public void addStepValuesIn(String stepId, Map<String, String> stepValues)
   {
      if (getStepValuesIn().containsKey(stepId))
      {
         getStepValuesIn().get(stepId).putAll(stepValues);
      }
      else
      {
         getStepValuesIn().put(stepId, stepValues);
      }
   }

   public void addStepValuesOut(String stepId, Map<String, String> stepValues)
   {
      if (getStepValuesOut().containsKey(stepId))
      {
         getStepValuesOut().get(stepId).putAll(stepValues);
      }
      else
      {
         getStepValuesIn().put(stepId, stepValues);
      }
   }

   @XmlTransient
   public Map<String, String> getStepValuesIn(String stepId)
   {
      if (getStepValuesIn().containsKey(stepId))
      {
         return getStepValuesIn().get(stepId);
      }
      Map<String, String> newMap = new HashMap<String, String>();
      getStepValuesIn().put(stepId, newMap);
      return newMap;
   }

   public String get(String key)
   {
      String value = getGlobal().get(key);
      if (value != null && !value.trim().isEmpty())
         return value.trim();
      return null;
   }

   public Content put(String key, String value)
   {
      getGlobal().put(key, value);
      return this;
   }

   @XmlTransient
   public Map<String, String> getStepValuesOut(String stepId)
   {
      if (getStepValuesOut().containsKey(stepId))
      {
         return getStepValuesOut().get(stepId);
      }
      Map<String, String> newMap = new HashMap<String, String>();
      getStepValuesOut().put(stepId, newMap);
      return newMap;
   }

   @Override
   public String toString()
   {

      StringBuffer stringBuffer = new StringBuffer("[ Content Values. ");

      if (global != null && !global.isEmpty())
      {
         stringBuffer.append(O_GRAF).append(SLA).append("global").append(SLA).append(DUE_P).append(O_GRAF);

         for (String key : getGlobal().keySet())
         {
            stringBuffer.append(SLA).append(key).append(SLA).append(DUE_P).append(SLA).append(global.get(key))
                     .append(SLA)
                     .append(VIR);
         }
         stringBuffer.append(C_GRAF + A_CAP + O_QUA);
      }
      Map<String, String> map = null;
      if (stepValuesIn != null && !stepValuesIn.isEmpty())
      {
         for (String stepId : getStepValuesIn().keySet())
         {
            stringBuffer.append(O_GRAF).append(SLA).append(stepId).append(SLA).append(DUE_P + O_GRAF);
            map = getStepValuesIn(stepId);
            for (String key : map.keySet())
            {
               stringBuffer.append(SLA).append(key).append(SLA).append(DUE_P).append(SLA).append(map.get(key))
                        .append(SLA)
                        .append(VIR);
            }
            stringBuffer.append(C_GRAF + A_CAP);
            if (stepValuesOut != null && !stepValuesOut.isEmpty() && stepValuesOut.get(stepId) != null
                     && !stepValuesOut.get(stepId).isEmpty())
            {
               stringBuffer.append(O_GRAF).append(SLA).append(stepId).append(SLA).append(DUE_P + O_GRAF);
               map = getStepValuesOut(stepId);
               for (String key : map.keySet())
               {
                  stringBuffer.append(SLA).append(key).append(SLA).append(DUE_P).append(SLA).append(map.get(key))
                           .append(SLA)
                           .append(VIR);
               }
               stringBuffer.append(C_GRAF + A_CAP);
            }
         }

      }
      return stringBuffer.append(C_GRAF + C_QUA).toString();
   }
}
