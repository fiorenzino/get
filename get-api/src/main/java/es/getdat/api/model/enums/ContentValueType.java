package es.getdat.api.model.enums;

public enum ContentValueType
{
   IN, OUT, GLOBAL, ALL;
}
