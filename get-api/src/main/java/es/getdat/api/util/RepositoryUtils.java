package es.getdat.api.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class RepositoryUtils
{

   private static final String LONG = "java.lang.Long";

   public static String getIdFieldName(@SuppressWarnings("rawtypes") Class clazz) throws Exception
   {
      for (Field field : clazz.getDeclaredFields())
      {
         Annotation[] annotations = field.getDeclaredAnnotations();
         for (Annotation annotation : annotations)
         {
            if (annotation.annotationType().equals(javax.persistence.Id.class))
            {
               field.setAccessible(true);
               return field.getName();
            }
         }
      }
      for (Method method : clazz.getDeclaredMethods())
      {
         Annotation[] annotations = method.getDeclaredAnnotations();
         for (Annotation annotation : annotations)
         {
            if (annotation.annotationType().equals(javax.persistence.Id.class))
            {
               method.setAccessible(true);
               int index = method.getName().indexOf("get") + 3;
               return method.getName().substring(index, index + 1).toLowerCase()
                        + method.getName().substring(index + 1);
            }
         }
      }
      throw new Exception("not found field/method with @Id annotation");
   }

   public static Class<?> getIdFieldClass(@SuppressWarnings("rawtypes") Class clazz) throws Exception
   {
      for (Field field : clazz.getDeclaredFields())
      {
         Annotation[] annotations = field.getDeclaredAnnotations();
         for (Annotation annotation : annotations)
         {
            if (annotation.annotationType().equals(javax.persistence.Id.class))
            {
               field.setAccessible(true);
               return field.getClass();
            }
         }
      }
      for (Method method : clazz.getDeclaredMethods())
      {
         Annotation[] annotations = method.getDeclaredAnnotations();
         for (Annotation annotation : annotations)
         {
            if (annotation.annotationType().equals(javax.persistence.Id.class))
            {
               method.setAccessible(true);
               return method.getClass();
            }
         }
      }
      throw new Exception("not found field/method with @Id annotation");
   }

   public static Object getId(Object t)
   {
      try
      {
         for (Field field : t.getClass().getDeclaredFields())
         {
            Annotation[] annotations = field.getDeclaredAnnotations();
            for (Annotation annotation : annotations)
            {
               if (annotation.annotationType().equals(javax.persistence.Id.class))
               {
                  field.setAccessible(true);
                  return field.get(t);
               }
            }
         }
         for (Method method : t.getClass().getDeclaredMethods())
         {
            Annotation[] annotations = method.getDeclaredAnnotations();
            for (Annotation annotation : annotations)
            {
               if (annotation.annotationType().equals(javax.persistence.Id.class))
               {
                  method.setAccessible(true);
                  return method.invoke(t);
               }
            }
         }
         // Field f = t.getClass().getDeclaredField("id");
         // f.setAccessible(true);
         // return f.get(t);
      }
      catch (Exception e)
      {
         e.printStackTrace();

      }
      return null;
   }

   public static Object castId(String key, @SuppressWarnings("rawtypes") Class clazz) throws Exception
   {
      @SuppressWarnings("rawtypes")
      Class idField = RepositoryUtils.getIdFieldClass(clazz);
      switch (idField.getCanonicalName())
      {
      case LONG:
         return Long.parseLong((String) key);
      default:
         return key;
      }
   }

}
