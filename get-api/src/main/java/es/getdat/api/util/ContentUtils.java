package es.getdat.api.util;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import es.getdat.api.management.AppConstants;
import es.getdat.api.model.Content;
import es.getdat.api.model.ContentValue;
import es.getdat.api.model.Process;
import es.getdat.api.model.Step;
import es.getdat.api.model.enums.ContentValueType;

public class ContentUtils
{
   public static Boolean getBoolean(Map<String, String> map, String key)
   {
      String val = map.get(key);
      if (val != null)
         return Boolean.valueOf(val);
      return null;
   }

   public static String[] getArray(Map<String, String> map, String key)
   {
      String val = map.get(key);
      if (val != null)
      {
         return val.split(",|;");
      }
      return null;
   }

   public static String findStepValueFromPreviuos(Content content, String stepId, String key)
   {
      Map<String, String> previous = null;
      for (String step : content.getStepValuesIn().keySet())
      {
         if (step.equals(stepId))
         {
            break;
         }
         else
         {
            previous = content.getStepValuesIn(step);
         }
      }
      if (previous != null && previous.containsKey(key))
      {
         return previous.get(key);
      }
      return null;
   }

   public static String findStepValue(Content content, String stepId, String key)
   {
      Map<String, String> actual = content.getStepValuesIn(stepId);
      if (actual != null && actual.containsKey(key))
      {
         return actual.get(key);
      }
      return null;
   }

   public static List<String> findAllIn(Content content, String stepId, String key) throws Exception
   {
      List<String> result = new ArrayList<>();
      // tutti i precedenti a me
      String jsonString = content.get(AppConstants.PROCESS);
      Process process = new Process(jsonString);
      for (Step step : process.steps)
      {
         if (!step.stepId.equals(stepId))
         {
            String value = content.getIn(step.stepId, key);
            if (value != null && !value.isEmpty())
               result.add(value);
         }
      }

      return result;
   }

   public List<String> findLatestIn(Content content, String stepId, String key) throws Exception
   {
      List<String> result = new ArrayList<>();
      // TODO

      return result;
   }

   public static List<ContentValue> toContentValue(Content content, String eventId, String stepId,
            ContentValueType contentValueType)
   {
      if (contentValueType == null)
         contentValueType = ContentValueType.ALL;
      List<ContentValue> values = new LinkedList<ContentValue>();
      Map<String, String> vals = new LinkedHashMap<String, String>();
      switch (contentValueType)
      {
      case GLOBAL:
         vals = content.getGlobal();
         for (String key : vals.keySet())
         {
            values.add(new ContentValue(eventId, stepId, key, vals.get(key), ContentValueType.GLOBAL));
         }
         break;
      case IN:
         if (stepId != null)
         {
            vals = content.getStepValuesIn(stepId);
            for (String key : vals.keySet())
            {
               values.add(new ContentValue(eventId, stepId, key, vals.get(key), ContentValueType.IN));
            }
         }
         else
         {
            for (String stepIdIn : content.getStepValuesIn().keySet())
            {
               vals = content.getStepValuesIn(stepIdIn);
               for (String key : vals.keySet())
               {
                  values.add(new ContentValue(eventId, stepId, key, vals.get(key), ContentValueType.IN));
               }
            }
         }
         break;
      case OUT:
         if (stepId != null)
         {
            vals = content.getStepValuesOut(stepId);
            for (String key : vals.keySet())
            {
               values.add(new ContentValue(eventId, stepId, key, vals.get(key), ContentValueType.OUT));
            }
         }
         else
         {
            for (String stepIdOut : content.getStepValuesIn().keySet())
            {
               vals = content.getStepValuesOut(stepIdOut);
               for (String key : vals.keySet())
               {
                  values.add(new ContentValue(eventId, stepId, key, vals.get(key), ContentValueType.OUT));
               }
            }
         }
         break;
      default:
         vals = content.getGlobal();
         for (String key : vals.keySet())
         {
            values.add(new ContentValue(eventId, stepId, key, vals.get(key), ContentValueType.GLOBAL));
         }
         for (String stepIdIn : content.getStepValuesIn().keySet())
         {
            vals = content.getStepValuesIn(stepIdIn);
            for (String key : vals.keySet())
            {
               values.add(new ContentValue(eventId, stepId, key, vals.get(key), ContentValueType.IN));
            }
         }
         for (String stepIdOut : content.getStepValuesIn().keySet())
         {
            vals = content.getStepValuesOut(stepIdOut);
            for (String key : vals.keySet())
            {
               values.add(new ContentValue(eventId, stepId, key, vals.get(key), ContentValueType.OUT));
            }
         }
         break;
      }

      return values;
   }
}
