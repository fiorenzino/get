https://github.com/rg3/youtube-dl
http://stackoverflow.com/questions/43890/crop-mp3-to-first-30-seconds



ffmpeg -t 30 -acodec copy -i inputfile.mp3 outputfile.mp3


# Trim from 00:02:54.583 to end of file
ffmpeg -i input.mp3 -ss 00:02:54.583 -acodec copy output.mp3
 
# Trim from 00:02:54.583 for 5 minutes
ffmpeg -i input.mp3 -ss 00:02:54.583 -t 300 -acodec copy output.mp3



youtube-dl
--cache-dir DIR                  Location in the filesystem where youtube-dl
                                 can store some downloaded information
                                 permanently. By default $XDG_CACHE_HOME
                                 /youtube-dl or ~/.cache/youtube-dl . At the
                                 moment, only YouTube player files (for
                                 videos with obfuscated signatures) are
                                 cached, but that may change.
--no-cache-dir                   Disable filesystem caching
--rm-cache-dir                   Delete all filesystem cache files


ALLA FINE PER CENTOS:
https://trac.ffmpeg.org/wiki/CompilationGuide/Centos




INSTALLATO:

unset TMOUT

mkdir $OPENSHIFT_DATA_DIR/data/bin
mkdir $OPENSHIFT_DATA_DIR/data/sources
mkdir $OPENSHIFT_DATA_DIR/data/build
mkdir $OPENSHIFT_DATA_DIR/data/mp3

cd $OPENSHIFT_DATA_DIR/data/sources
curl -L -O http://downloads.sourceforge.net/project/lame/lame/3.99/lame-3.99.5.tar.gz
tar xzvf lame-3.99.5.tar.gz
cd lame-3.99.5
./configure --prefix="$OPENSHIFT_DATA_DIR/build" --bindir="$OPENSHIFT_DATA_DIR/bin" --disable-shared --enable-nasm
make
make install
make distclean

----

cd $OPENSHIFT_DATA_DIR/data/sources
wget https://libav.org/releases/libav-0.5.10.tar.gz
tar xzvf libav-0.5.10.tar.gz
cd libav-0.5.10
./configure --prefix="$OPENSHIFT_DATA_DIR/build" --extra-cflags="-I$OPENSHIFT_DATA_DIR/build/include" --extra-ldflags="-L$OPENSHIFT_DATA_DIR/build/lib" --bindir="$OPENSHIFT_DATA_DIR/bin" --enable-libmp3lame
make
make install
make distclean

----

cd $OPENSHIFT_DATA_DIR/data/bin
https://yt-dl.org/downloads/latest/youtube-dl --no-check-certificate 
chmod 777 youtube-dl

cd $OPENSHIFT_DATA_DIR/data/mp3
./youtube-dl https://www.youtube.com/watch?v=sPe7VlqKtu4 --id --no-cache-dir --exec './ffmpeg -t 30 -i {} -acodec copy {}.mp3 && rm {}'