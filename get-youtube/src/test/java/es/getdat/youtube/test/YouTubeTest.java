package es.getdat.youtube.test;

import org.junit.Test;

import es.getdat.youtube.service.util.YoutubeUtils;

public class YouTubeTest
{

   @Test
   public void YouTubeCmdTest()
   {
      String cmd = "/usr/local/opt/youtube-dl/bin/youtube-dl https://www.youtube.com/watch?v=%s --id --cache-dir %s --exec '/usr/local/opt/ffmpeg/bin/ffmpeg -t %s -ss %s -i {} -acodec copy %s.mp3' & sleep 1 && ps aux | grep %s";
      String youtubeId = "sPe7VlqKtu4";
      String finalFile = "/Users/fiorenzo/prova";
      String chacheDir = "/Users/fiorenzo/youtube-cache";
      int from = 20;
      int sec = 30;
      YoutubeUtils.execute(cmd, youtubeId, chacheDir, finalFile, from, sec);
   }

   @Test
   public void CmdTest()
   {
      String youtubeId = "sPe7VlqKtu4";
      int from = 30;
      int sec = 56;
      String fileName = "/home/fiorenzo/prova";
      String cmd = "youtube-dl https://www.youtube.com/watch?v=%s --id --cache-dir --exec 'ffmpeg -t %s -ss %s -i {} -acodec copy %s.mp3'";
      String youtubeCmd = String.format(
               cmd,
               youtubeId,
               from,
               sec,
               fileName);
      System.out.println(youtubeCmd);
      org.junit.Assert
               .assertEquals(
                        "youtube-dl https://www.youtube.com/watch?v=sPe7VlqKtu4 --id --cache-dir --exec 'ffmpeg -t 30 -ss 56 -i {} -acodec copy /home/fiorenzo/prova.mp3'",
                        youtubeCmd);

   }
}
