package es.getdat.youtube.service.rs;

import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import es.getdat.api.model.Content;
import es.getdat.api.service.GetService;

@Path("/v1/youtube")
@Stateless
@LocalBean
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class YoutubeRest extends GetService
{

   private static final long serialVersionUID = 1L;

   static String SERVICE_NAME = "YOUTUBE";

   @POST
   @Asynchronous
   public void execute(Content content)
   {
      logger.info(content);
   }

}
