package es.getdat.youtube.service.util;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.jboss.logging.Logger;

public class YoutubeUtils
{

   static Logger logger = Logger.getLogger(YoutubeUtils.class);

   // FFMPEG + LAME
   // -t: duration
   // -ss position'
   // da 30 a 100 diventa -t 30 -ss70

   // ./youtube-dl https://www.youtube.com/watch?v=sPe7VlqKtu4 --id --cache-dir
   // --exec './ffmpeg -t 30 -i {} -acodec copy {}.mp3 && rm {}'

   public static boolean execute(String cmd, String youtubeId, String cacheDir, String finalFile, int from, int sec)
   {
      // System.out.println(cmd);
      Process p;
      if (new File(finalFile).exists())
      {
         logger.info("destination file exists: i will delete before create a new file!");
         new File(finalFile).delete();
      }
      try
      {
         // String cmd =
         // "youtube-dl https://www.youtube.com/watch?v=%s --id --cache-dir --exec 'ffmpeg -t %s -ss %s -i {} -acodec copy %s.mp3'";
         String youtubeCmd = String.format(
                  cmd,
                  youtubeId,
                  cacheDir,
                  from,
                  sec,
                  finalFile, youtubeId);
         System.out.println(youtubeCmd);
         // executeCmd(new String[] { youtubeCmd });
         // executeCmd(youtubeCmd.split(" "));
         executeCmd(new String[] { "/Users/fiorenzo/exec.sh",
         // "--exec '/usr/local/opt/ffmpeg/bin/ffmpeg -t 20 -ss 30 -i {} -acodec copy /Users/fiorenzo/prova.mp3'"
         });

         return true;
         // logger.info("executing command " + youtubeCmd);
         // p = Runtime.getRuntime().exec(youtubeCmd);
         // int exitVal = p.waitFor();
         // // System.out.println("Exited with error code " + exitVal);
         // if (exitVal == 0)
         // return true;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      return false;
   }

   public static String executeCmd(String[] cmd)
   {
      String outputString = "";
      String errorString = "";
      Runtime rt = Runtime.getRuntime();
      Process proc = null;
      try
      {
         proc = rt.exec(cmd);
         // Thread.sleep(60000);
         int exitVal = proc.waitFor();
         logger.debug("processo terminato: " + exitVal);
         InputStream stdin = proc.getInputStream();
         InputStream stder = proc.getErrorStream();
         outputString = stream2string(stdin);
         errorString = stream2string(stder);
         logger.info("Process exitValue: " + exitVal);
         if (outputString.compareTo("") == 0)
         {
            logger.info("errorString: " + errorString);
            return errorString;
         }
         logger.info("outputString: " + outputString);
         return outputString;
      }
      catch (Exception e)
      {
         // TODO Auto-generated catch block
         logger.info("terza ecc: esecuzione wait");
         e.printStackTrace();
      }
      finally
      {
         if (proc != null)
         {
            close(proc.getOutputStream());
            close(proc.getInputStream());
            close(proc.getErrorStream());
            proc.destroy();
         }
      }
      return "";

   }

   private static String stream2string(InputStream stream) throws IOException
   {
      String lines = "0";

      while (stream.available() != 0)
      {
         byte[] b = new byte[stream.available()];
         stream.read(b);
         // log.info("dimensione b: " + b.length + " " + b.toString());
         lines = new String(b);
      }
      return lines;
   }

   private static void close(Closeable c)
   {
      if (c != null)
      {
         try
         {
            c.close();
         }
         catch (IOException e)
         {
            // ignored
         }
      }
   }
}
