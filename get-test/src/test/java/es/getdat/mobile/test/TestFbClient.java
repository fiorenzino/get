package es.getdat.mobile.test;

import java.time.Instant;
import java.util.Date;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.junit.Assert;
import org.junit.Test;

import es.getdat.accounting.util.fb.filter.client.JsonContentTypeResponseFilter;
import es.getdat.accounting.util.fb.longtoken.DebugLongTokenResponse;
import es.getdat.accounting.util.fb.pojo.FbIdentity;
import es.getdat.accounting.util.fb.shorttoken.DebugShortTokenResponse;

public class TestFbClient
{

   static String targetHost = "https://graph.facebook.com";
   //
   static String targetHost22 = "https://graph.facebook.com/v2.2";
   static String targetPathAuth = "/oauth/access_token";
   static String targetPathDebugToken = "/debug_token";
   static String targetPathLongToken = "/oauth/access_token";
   //
   static String targetPathMe = "/me";
   //
   // static String targetPathMePhotos = "/{user-id}/permissions";
   //
   static String client_id = "752381511504980";
   static String client_secret = "95aba24ea120ab97821fa71833b309bc";

   static String grant_type = "client_credentials";

   //
   // https://graph.facebook.com/oauth/access_token?
   // client_id=752381511504980
   // &client_secret=95aba24ea120ab97821fa71833b309bc&grant_type=client_credentials

   @Test
   public void authTokenTest()
   {
      getAuthToken(client_id, client_secret);
   }

   //
   // https://graph.facebook.com/debug_token?
   // input_token=CAAKsSXi6lFQBAG8X8H81UYxKvzKkUdnGlkyUDjPmejfwZBkZAxUXI2ayVlC5E6LrRuvRxyLGXMp0hcFCSZBaZAPTU01EHrofUzDCej210GzI2IpYtEdZCjZCIvS834SnZBA3YKZCvM4N8hiBxYkIXOGuRdkzbU0jXNHPurQnt4ZC4Nt4EepHR3B6gZCtqqnzZAvOQFyOQ1fz9ySablzYA7RGoeH
   // &access_token=752381511504980|ZEL_KG3skWhJ7fleyiSjNtBlWyk
   //

   @Test
   public void verifyShortToken()
   {
      //   static String shortToken = "CAAKsSXi6lFQBACZCRnW8ZBzGyZCfJcZAKiHdQmqHJSbQBFZCM91KyzLX6El2KFCbgTBM4gw5Av4CMfc3OnKtnL8MkhwokNZCA7XGYMmnf30kDd6ff63LTe1xWfYXTi8sQ0R8ZCQbhFsGjyeq0V15fhBldWcbyQxNpbO7wCM5PBwa03SSB9VZB9rps0ZC2zJ7sQUGNxSv6jxp5v6OsXJzjJL6U";
//      static String userid = "10205237953655726";
      String input_token = "CAAKsSXi6lFQBACZCRnW8ZBzGyZCfJcZAKiHdQmqHJSbQBFZCM91KyzLX6El2KFCbgTBM4gw5Av4CMfc3OnKtnL8MkhwokNZCA7XGYMmnf30kDd6ff63LTe1xWfYXTi8sQ0R8ZCQbhFsGjyeq0V15fhBldWcbyQxNpbO7wCM5PBwa03SSB9VZB9rps0ZC2zJ7sQUGNxSv6jxp5v6OsXJzjJL6U";
      WebTarget target = getTarget(targetHost, targetPathDebugToken).queryParam(
               "input_token", input_token).queryParam("access_token",
               getAuthToken(client_id, client_secret));
      DebugShortTokenResponse result = target.request(MediaType.TEXT_HTML).get(
               DebugShortTokenResponse.class);
      System.out.println(result);
      Date date = Date.from(Instant.ofEpochSecond(result.debugTokenResponseData.expires_at));
      System.out.println(date);

   }

   // GET /oauth/access_token?
   // grant_type=fb_exchange_token&
   // client_id={app-id}&
   // client_secret={app-secret}&
   // fb_exchange_token={short-lived-token}
   @Test
   public void longToken()
   {
      String short_token = "CAAKsSXi6lFQBACZCRnW8ZBzGyZCfJcZAKiHdQmqHJSbQBFZCM91KyzLX6El2KFCbgTBM4gw5Av4CMfc3OnKtnL8MkhwokNZCA7XGYMmnf30kDd6ff63LTe1xWfYXTi8sQ0R8ZCQbhFsGjyeq0V15fhBldWcbyQxNpbO7wCM5PBwa03SSB9VZB9rps0ZC2zJ7sQUGNxSv6jxp5v6OsXJzjJL6U";
      WebTarget target = getTarget(targetHost, targetPathLongToken)
               .queryParam("grant_type", "fb_exchange_token")
               .queryParam("client_id", client_id)
               .queryParam("client_secret", client_secret)
               .queryParam("fb_exchange_token", short_token);
      String result = target.request(MediaType.TEXT_HTML).get(
               String.class);
      System.out.println(result);
      Assert.assertNotNull(result);
      String[] pieces = result.split("&");
      Assert.assertNotNull(pieces);
      String[] accTok = pieces[0].split("=");
      Assert.assertNotNull(accTok);
      System.out.println(accTok[0] + ":" + accTok[1]);
      String[] expires = pieces[1].split("=");
      Assert.assertNotNull(expires);
      System.out.println(expires[0] + ":" + expires[1]);
      Date date = Date.from(Instant.ofEpochSecond(Long.valueOf(expires[1])));
      System.out.println(date);
   }

   @Test
   public void verifyLongToken()
   {
      String input_token = "CAAKsSXi6lFQBAJSNZBAEQLWnTvIKjEkMpaeWOTEYAxWCzmzj2daDG9dNhAwcl7ne3qJcLWEF1Wrci2KX8PcwQxTOuvZBJBMKIfZA5tBthOFvVYnZC4YGZAxMjCoIJCoL74AwyfdjXcpMWHGHfCuJQ3ZBis7X9olyZBKrkJmPsCWsVADQAllHAZC18oNnGvtJkRgZD";
      WebTarget target = getTarget(targetHost, targetPathDebugToken).queryParam(
               "input_token", input_token).queryParam("access_token",
               getAuthToken(client_id, client_secret));
      DebugLongTokenResponse result = target.request(MediaType.TEXT_HTML).get(
               DebugLongTokenResponse.class);
      System.out.println(result);
      System.out.println("user_id:" + result.debugLongTokenResponseData.user_id);
      Date expires_at = Date.from(Instant.ofEpochSecond(result.debugLongTokenResponseData.expires_at));
      System.out.println("expires_at:" + expires_at);

      Date issued_at = Date.from(Instant.ofEpochSecond(result.debugLongTokenResponseData.issued_at));
      System.out.println("issued_at:" + issued_at);
   }

   @Test
   public void getMe()
   {
      String long_token = "CAAKsSXi6lFQBAJSNZBAEQLWnTvIKjEkMpaeWOTEYAxWCzmzj2daDG9dNhAwcl7ne3qJcLWEF1Wrci2KX8PcwQxTOuvZBJBMKIfZA5tBthOFvVYnZC4YGZAxMjCoIJCoL74AwyfdjXcpMWHGHfCuJQ3ZBis7X9olyZBKrkJmPsCWsVADQAllHAZC18oNnGvtJkRgZD";
      WebTarget target = getTarget(targetHost, targetPathMe)
               .queryParam("access_token", long_token);
      FbIdentity result = target.request(MediaType.TEXT_HTML).get(
               FbIdentity.class);
      System.out.println(result);
   }

   @Test
   public void getPhotos()
   {
      String long_token = "CAAKsSXi6lFQBAJSNZBAEQLWnTvIKjEkMpaeWOTEYAxWCzmzj2daDG9dNhAwcl7ne3qJcLWEF1Wrci2KX8PcwQxTOuvZBJBMKIfZA5tBthOFvVYnZC4YGZAxMjCoIJCoL74AwyfdjXcpMWHGHfCuJQ3ZBis7X9olyZBKrkJmPsCWsVADQAllHAZC18oNnGvtJkRgZD";
      WebTarget target = getTarget(targetHost22, "/{user-id}/photos")
               .resolveTemplate("user-id", "10205237953655726")
               .queryParam("access_token", long_token);
      String result = target.request(MediaType.TEXT_HTML).get(
               String.class);
      System.out.println(result);
   }

   public static WebTarget getTarget(String targetHost, String targetPath)
   {
      try
      {
         Client client = new ResteasyClientBuilder().disableTrustManager()
                  .register(JsonContentTypeResponseFilter.class)
                  .build();
         WebTarget target = client.target(targetHost + targetPath);
         return target;
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      return null;
   }

   public static String getAuthToken(String client_id, String client_secret)
   {
      WebTarget target = getTarget(targetHost, targetPathAuth).queryParam(
               "client_id", client_id).queryParam("client_secret", client_secret)
               .queryParam("grant_type", grant_type);
      String result = target.request(MediaType.TEXT_HTML).get(
               String.class);
      System.out.println(result);
      Assert.assertNotNull(result);
      String[] accTok = result.split("=");
      Assert.assertNotNull(accTok[1]);
      return accTok[1];
   }
}
