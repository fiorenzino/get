package es.getdat.mobile.test;

import static es.getdat.core.management.AppConstants.ACTIVATIONS_PATH;
import static es.getdat.core.management.AppConstants.API_PATH;
import static es.getdat.core.management.AppConstants.PLUGIN_CONFIGURATIONS_PATH;
import static es.getdat.core.management.AppConstants.PLUGIN_DEFINITIONS_PATH;

import java.util.Calendar;
import java.util.Date;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.getdat.accounting.model.Account;
import es.getdat.accounting.pojo.UserToken;
import es.getdat.all.helper.junit.Order;
import es.getdat.all.helper.junit.OrderedRunner;
import es.getdat.all.test.CrudTests;
import es.getdat.api.model.enums.ChannelType;
import es.getdat.core.management.AppConstants;
import es.getdat.out.pojo.ClientBuzz;
import es.getdat.out.pojo.ClientCall;
import es.getdat.out.pojo.ClientEmail;
import es.getdat.out.pojo.ClientSms;
import es.getdat.plugin.model.PluginConfiguration;
import es.getdat.plugin.model.PluginDefinition;
import es.getdat.plugins.test.TestUtils;
import es.getdat.util.DateUtils;

@RunWith(OrderedRunner.class)
public class MobileNoDeleteTest
{
   static Logger logger = Logger.getLogger(MobileNoDeleteTest.class);

   static String TARGET_HOST = "http://get-dates.rhcloud.com/";
   // static String TARGET_HOST = "http://localhost:8080";
   static String longToken = null;
   static String shortToken = "CAAKsSXi6lFQBAMwwC9s7MKpXFaEKkwTNFMHx111s9kP9t8ZBz83yh1oopskA0lejsgiVJZAn4IXN7ojMLHHZBox0f2CZBdkOZBDxexvrTUVS3jzGhyOhqlQUObSXWvXXqaFLGip8kOiQC53nnsvJDx6TEYvME2zSZAbLX6qrSH2WxS4n03pQbwoUEmPZArWuv1V3ubVVNsPYPWntLMmBOkJ";
   static String userid = "10205237953655726";
   static Date expirationDate = null;

   private static PluginDefinition mailPluginDefinition;
   private static PluginConfiguration mailPluginConfiguration;

   private static PluginDefinition buzzPluginDefinition;
   private static PluginConfiguration buzzPluginConfiguration;

   private static PluginDefinition callPluginDefinition;
   private static PluginConfiguration callPluginConfiguration;

   private static PluginDefinition smsPluginDefinition;
   private static PluginConfiguration smsPluginConfiguration;

   private static UserToken userToken;

   private static CrudTests crudTests;

   private static Account account;

   // @BeforeClass
   // @Test
   // @Order(order = 1)
   public void beforeTest()
   {
      crudTests = new CrudTests();
      // crudTests.create(
      // TestUtils.TARGET_HOST,
      // API_PATH + ACTIVATIONS_PATH,
      // TestUtils.newActivation("f15c2c54-5e7b-4b7f-b894-df1eb54b53c3",
      // "a84a7976-44df-4f8a-95dd-abff46b6473a", ChannelType.EMAIL, true));

      // MAIL
      mailPluginDefinition = crudTests
               .create(TestUtils.TARGET_HOST, API_PATH + PLUGIN_DEFINITIONS_PATH,
                        TestUtils.newPluginDefinitionMailGun());
      mailPluginConfiguration = crudTests
               .create(TestUtils.TARGET_HOST, API_PATH + PLUGIN_CONFIGURATIONS_PATH,
                        TestUtils.newPluginConfigurationMailGun(mailPluginDefinition
                                 .getId()));
      // BUZZ
      buzzPluginDefinition = crudTests.create(TestUtils.TARGET_HOST, API_PATH
               + PLUGIN_DEFINITIONS_PATH,
               TestUtils.newPluginDefinitionPlivoBuzz());
      buzzPluginConfiguration = crudTests.create(TestUtils.TARGET_HOST, API_PATH
               + PLUGIN_CONFIGURATIONS_PATH, TestUtils
               .newPluginConfigurationPlivoBuzz(buzzPluginDefinition.getId()));
      // CALL
      callPluginDefinition = crudTests.create(TestUtils.TARGET_HOST, API_PATH
               + PLUGIN_DEFINITIONS_PATH,
               TestUtils.newPluginDefinitionPlivoCall());
      callPluginConfiguration = crudTests.create(TestUtils.TARGET_HOST, API_PATH
               + PLUGIN_CONFIGURATIONS_PATH, TestUtils
               .newPluginConfigurationPlivoCall(callPluginDefinition.getId()));

      // SMS
      smsPluginDefinition = crudTests.create(TestUtils.TARGET_HOST, API_PATH
               + PLUGIN_DEFINITIONS_PATH,
               TestUtils.newPluginDefinitionPlivoSms());
      // create plugin configuration
      smsPluginConfiguration = crudTests.create(TestUtils.TARGET_HOST, API_PATH
               + PLUGIN_CONFIGURATIONS_PATH, TestUtils
               .newPluginConfigurationPlivoSms(smsPluginDefinition.getId()));

   }

   // @AfterClass
   public static void afterTest()
   {

      // MAIL
      crudTests.delete(TestUtils.TARGET_HOST, API_PATH + PLUGIN_CONFIGURATIONS_PATH,
               mailPluginConfiguration.getId());

      crudTests.delete(TestUtils.TARGET_HOST, API_PATH + PLUGIN_DEFINITIONS_PATH,
               mailPluginDefinition.getId());
      // // BUZZ
      // crudTests.delete(TestUtils.TARGET_HOST, API_PATH + PLUGIN_CONFIGURATIONS_PATH,
      // buzzPluginConfiguration.getId());
      //
      // crudTests.delete(TestUtils.TARGET_HOST, API_PATH + PLUGIN_DEFINITIONS_PATH,
      // buzzPluginDefinition.getId());
      //
      // // CALL
      // crudTests.delete(TestUtils.TARGET_HOST, API_PATH + PLUGIN_CONFIGURATIONS_PATH,
      // callPluginConfiguration.getId());
      //
      // crudTests.delete(TestUtils.TARGET_HOST, API_PATH + PLUGIN_DEFINITIONS_PATH,
      // callPluginDefinition.getId());
      //
      // // SMS
      // crudTests.delete(TestUtils.TARGET_HOST, API_PATH + PLUGIN_CONFIGURATIONS_PATH,
      // smsPluginConfiguration.getId());
      //
      // crudTests.delete(TestUtils.TARGET_HOST, API_PATH + PLUGIN_DEFINITIONS_PATH,
      // smsPluginDefinition.getId());
      //
      // // ACCOUNT
      // crudTests
      // .delete(TestUtils.TARGET_HOST, API_PATH + ACCOUNTS_PATH, account.getId());

   }

   @Test
   @Order(order = 1)
   public void registrationTest()
   {
      userToken = new UserToken(userid, shortToken, longToken, expirationDate);
      Client client = ClientBuilder.newClient();
      WebTarget target = client.target(TARGET_HOST + AppConstants.API_PATH + AppConstants.REGISTRATION_PATH);
      account = target.request(MediaType.APPLICATION_JSON).post(Entity.json(userToken), Account.class);
      Assert.assertNotNull(account);
      logger.info(account.toString());
   }

   @Test
   @Order(order = 2)
   public void scheduleEmailTest()
   {
      userid = "10205237953655726";
      longToken =
               "CAAKsSXi6lFQBAFxuAoCBbrnqAxBKM3aeXGxHTlpW8IqZBg25QduJoR9CBWZBPUTGZCFCh7vZCEOr00H2juXMEOqlqRONBmQEYeIc2RyjKVJ5AXc4XQc1z7hUigFAE3YH9CZCM9eCZBaTMFjMzfz9mwWyB6TJx2IUCURtKe0uvBhVIZCpr4ZAy9DI";
      // longToken =
      // "CAAKsSXi6lFQBAFHpLHgcVVXBnX2hffVpOnYZAvRXGjTqHAW72kSnboIIFs5RKCywuzZCta4ZBu9xoBP4TgrrKjX30byikamknO1vE1E7z8G4xrvWRHD4MSFb4u4wqoxY8pALOkN6XZCxijAIsjV4qAmw3UpKw2GCeZB90aSGDdzgThuytbzUO";
      try
      {
         for (int i = 0; i < 300; i++)
         {
            ClientEmail clientEmail = new ClientEmail();
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.SECOND, 5);
            clientEmail.date = DateUtils.getStringFromDate(calendar.getTime());
            clientEmail.mailBody = i + ") body: " + calendar.getTimeInMillis();
            clientEmail.mailFrom = "pisi79.spam@gmail.com";
            clientEmail.mailTo = "samuele.pasini@gmail.com";
            clientEmail.mailSubject = i + ") subject: " + calendar.getTimeInMillis();
            Client client = ClientBuilder.newClient();
            WebTarget target = client.target(
                     TARGET_HOST + AppConstants.API_PATH + AppConstants.USERS_PATH + "/{userid}"
                              + "/email/schedule").resolveTemplate("userid", userid);
            Invocation.Builder invocationBuilder =
                     target.request(MediaType.APPLICATION_JSON).header("Bearer", longToken);
            Response result = invocationBuilder
                     .buildPost(Entity.json(clientEmail)).invoke();
            logger.info(i + ")" + result.getStatus());
         }

      }
      catch (Exception e1)
      {
         e1.printStackTrace();
      }

   }

   @Test
   @Order(order = 3)
   public void scheduleSmsTest()
   {
      userid = "10205237953655726";
      longToken =
               "CAAKsSXi6lFQBAFxuAoCBbrnqAxBKM3aeXGxHTlpW8IqZBg25QduJoR9CBWZBPUTGZCFCh7vZCEOr00H2juXMEOqlqRONBmQEYeIc2RyjKVJ5AXc4XQc1z7hUigFAE3YH9CZCM9eCZBaTMFjMzfz9mwWyB6TJx2IUCURtKe0uvBhVIZCpr4ZAy9DI";

      try
      {
         for (int i = 0; i < 20; i++)
         {
            ClientSms clientSms = new ClientSms();
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.SECOND, 20);

            clientSms.date = DateUtils.getStringFromDate(calendar.getTime());
            clientSms.msg = "msg: " + i + ")" + calendar.getTimeInMillis();
            clientSms.number = "+393922274929";
            Client client = ClientBuilder.newClient();
            WebTarget target = client.target(
                     TARGET_HOST + AppConstants.API_PATH + AppConstants.USERS_PATH + "/{userid}"
                              + "/sms/schedule").resolveTemplate("userid", userid);
            Invocation.Builder invocationBuilder =
                     target.request(MediaType.APPLICATION_JSON).header("Bearer", longToken);

            Response result = invocationBuilder
                     .buildPost(Entity.json(clientSms)).invoke();
            Assert.assertNotNull(result);
            logger.info(result.getStatus());
            logger.info(result.getEntity());
         }

      }
      catch (Exception e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }

   @Test
   @Order(order = 4)
   public void scheduleBuzzTest()
   {
      userid = "10205237953655726";
      longToken =
               "CAAKsSXi6lFQBAFxuAoCBbrnqAxBKM3aeXGxHTlpW8IqZBg25QduJoR9CBWZBPUTGZCFCh7vZCEOr00H2juXMEOqlqRONBmQEYeIc2RyjKVJ5AXc4XQc1z7hUigFAE3YH9CZCM9eCZBaTMFjMzfz9mwWyB6TJx2IUCURtKe0uvBhVIZCpr4ZAy9DI";
      ClientBuzz clientBuzz = new ClientBuzz();
      try
      {
         Calendar calendar = Calendar.getInstance();
         calendar.add(Calendar.SECOND, 5);

         clientBuzz.date = DateUtils.getStringFromDate(calendar.getTime());
         clientBuzz.number = "+393922274929";
         Client client = ClientBuilder.newClient();
         WebTarget target = client.target(TARGET_HOST + AppConstants.API_PATH + AppConstants.USERS_PATH + "/{userid}"
                  + "/buzz/schedule").resolveTemplate("userid", userid);
         Invocation.Builder invocationBuilder =
                  target.request(MediaType.APPLICATION_JSON).header("Bearer",
                           longToken);
         Response result = invocationBuilder
                  .buildPost(Entity.json(clientBuzz)).invoke();
         Assert.assertNotNull(result);
         logger.info(result.getStatus());
         logger.info(result.getEntity());
      }
      catch (Exception e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }

   @Test
   @Order(order = 5)
   public void scheduleCallTest()
   {
      userid = "10205237953655726";
      longToken =
               "CAAKsSXi6lFQBAFxuAoCBbrnqAxBKM3aeXGxHTlpW8IqZBg25QduJoR9CBWZBPUTGZCFCh7vZCEOr00H2juXMEOqlqRONBmQEYeIc2RyjKVJ5AXc4XQc1z7hUigFAE3YH9CZCM9eCZBaTMFjMzfz9mwWyB6TJx2IUCURtKe0uvBhVIZCpr4ZAy9DI";
      ClientCall clientCall = new ClientCall();
      try
      {
         Calendar calendar = Calendar.getInstance();
         calendar.add(Calendar.SECOND, 5);

         clientCall.date = DateUtils.getStringFromDate(calendar.getTime());
         clientCall.number = "+393922274929";
         // clientCall.number = "+393927620081";
         clientCall.msg = "ciao ciao.";
         Client client = ClientBuilder.newClient();
         WebTarget target = client.target(TARGET_HOST + AppConstants.API_PATH + AppConstants.USERS_PATH + "/{userid}"
                  + "/call/schedule").resolveTemplate("userid", userid);
         Invocation.Builder invocationBuilder =
                  target.request(MediaType.APPLICATION_JSON).header("Bearer", longToken);
         Response result = invocationBuilder
                  .buildPost(Entity.json(clientCall)).invoke();
         Assert.assertNotNull(result);
         logger.info(result.getStatus());
         logger.info(result.getEntity());
      }
      catch (Exception e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }

   // @Test
   public void updateTokenTest()
   {
      UserToken userToken = new UserToken(userid, shortToken, longToken, expirationDate);
      Client client = ClientBuilder.newClient();
      WebTarget target = client.target(TARGET_HOST + AppConstants.API_PATH + AppConstants.REGISTRATION_PATH);
      Object result = target.request(MediaType.APPLICATION_JSON).put(Entity.json(userToken)).getEntity();
      Assert.assertNotNull(result);
   }
}
