package es.getdat.mobile.test;

import static es.getdat.core.management.AppConstants.ACCOUNTS_PATH;
import static es.getdat.core.management.AppConstants.API_PATH;
import static es.getdat.core.management.AppConstants.PLUGIN_CONFIGURATIONS_PATH;
import static es.getdat.core.management.AppConstants.PLUGIN_DEFINITIONS_PATH;

import java.util.Calendar;
import java.util.Date;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.getdat.accounting.model.Account;
import es.getdat.accounting.pojo.UserToken;
import es.getdat.all.helper.junit.Order;
import es.getdat.all.helper.junit.OrderedRunner;
import es.getdat.all.test.CrudTests;
import es.getdat.core.management.AppConstants;
import es.getdat.out.pojo.ClientBuzz;
import es.getdat.out.pojo.ClientCall;
import es.getdat.out.pojo.ClientEmail;
import es.getdat.out.pojo.ClientSms;
import es.getdat.plugin.model.PluginConfiguration;
import es.getdat.plugin.model.PluginDefinition;
import es.getdat.plugins.test.TestUtils;
import es.getdat.util.DateUtils;

@RunWith(OrderedRunner.class)
public class MobileTest
{
   static Logger logger = Logger.getLogger(MobileTest.class);

   static String TARGET_HOST = "http://get-dates.rhcloud.com/";
   static String longToken = null;
   static String shortToken = "CAAKsSXi6lFQBAHtmXiZBiMBAmiR9YqyusJV3ndcSS7Szw2YWczrGEEOEA9z5k6buk8v3wRBzxL6VZCpciSfUpbQzimJJi0rPJzVmvghWi7ydZAJqgIb42W14pHGR3v1b1B2wG3RkHZA7ula1UfP1u79b66qVLcXFzUHiMCZCyAhWLaK3sHZCkrqZCWZAD5xeKwrl6g6QsGVfNMl9njhNfrJx";
   static String userid = "10205237953655726";
   static Date expirationDate = null;

   private static PluginDefinition mailPluginDefinition;
   private static PluginConfiguration mailPluginConfiguration;

   private static PluginDefinition buzzPluginDefinition;
   private static PluginConfiguration buzzPluginConfiguration;

   private static PluginDefinition callPluginDefinition;
   private static PluginConfiguration callPluginConfiguration;

   private static PluginDefinition smsPluginDefinition;
   private static PluginConfiguration smsPluginConfiguration;

   private static UserToken userToken;

   private static CrudTests crudTests;

   private static Account account;

   @BeforeClass
   public static void beforeTest()
   {
      crudTests = new CrudTests();
      // MAIL
      mailPluginDefinition = crudTests
               .create(TestUtils.TARGET_HOST, API_PATH + PLUGIN_DEFINITIONS_PATH,
                        TestUtils.newPluginDefinitionMailGun());
      mailPluginConfiguration = crudTests
               .create(TestUtils.TARGET_HOST, API_PATH + PLUGIN_CONFIGURATIONS_PATH,
                        TestUtils.newPluginConfigurationMailGun(mailPluginDefinition
                                 .getId()));
      // BUZZ
      buzzPluginDefinition = crudTests.create(TestUtils.TARGET_HOST, API_PATH
               + PLUGIN_DEFINITIONS_PATH,
               TestUtils.newPluginDefinitionPlivoBuzz());
      buzzPluginConfiguration = crudTests.create(TestUtils.TARGET_HOST, API_PATH
               + PLUGIN_CONFIGURATIONS_PATH, TestUtils
               .newPluginConfigurationPlivoBuzz(buzzPluginDefinition.getId()));
      // CALL
      callPluginDefinition = crudTests.create(TestUtils.TARGET_HOST, API_PATH
               + PLUGIN_DEFINITIONS_PATH,
               TestUtils.newPluginDefinitionPlivoCall());
      callPluginConfiguration = crudTests.create(TestUtils.TARGET_HOST, API_PATH
               + PLUGIN_CONFIGURATIONS_PATH, TestUtils
               .newPluginConfigurationPlivoCall(callPluginDefinition.getId()));

      // SMS
      smsPluginDefinition = crudTests.create(TestUtils.TARGET_HOST, API_PATH
               + PLUGIN_DEFINITIONS_PATH,
               TestUtils.newPluginDefinitionPlivoSms());
      // create plugin configuration
      smsPluginConfiguration = crudTests.create(TestUtils.TARGET_HOST, API_PATH
               + PLUGIN_CONFIGURATIONS_PATH, TestUtils
               .newPluginConfigurationPlivoSms(smsPluginDefinition.getId()));

   }

   @AfterClass
   public static void afterTest()
   {

      try
      {
         Thread.sleep(60000);
      }
      catch (InterruptedException e)
      {
         e.printStackTrace();
      }

      // MAIL
      crudTests.delete(TestUtils.TARGET_HOST, API_PATH + PLUGIN_CONFIGURATIONS_PATH,
               mailPluginConfiguration.getId());

      crudTests.delete(TestUtils.TARGET_HOST, API_PATH + PLUGIN_DEFINITIONS_PATH,
               mailPluginDefinition.getId());
      // BUZZ
      crudTests.delete(TestUtils.TARGET_HOST, API_PATH + PLUGIN_CONFIGURATIONS_PATH,
               buzzPluginConfiguration.getId());

      crudTests.delete(TestUtils.TARGET_HOST, API_PATH + PLUGIN_DEFINITIONS_PATH,
               buzzPluginDefinition.getId());

      // CALL
      crudTests.delete(TestUtils.TARGET_HOST, API_PATH + PLUGIN_CONFIGURATIONS_PATH,
               callPluginConfiguration.getId());

      crudTests.delete(TestUtils.TARGET_HOST, API_PATH + PLUGIN_DEFINITIONS_PATH,
               callPluginDefinition.getId());

      // SMS
      crudTests.delete(TestUtils.TARGET_HOST, API_PATH + PLUGIN_CONFIGURATIONS_PATH,
               smsPluginConfiguration.getId());

      crudTests.delete(TestUtils.TARGET_HOST, API_PATH + PLUGIN_DEFINITIONS_PATH,
               smsPluginDefinition.getId());

      // ACCOUNT
      crudTests
               .delete(TestUtils.TARGET_HOST, API_PATH + ACCOUNTS_PATH, account.getId());

   }

   @Test
   @Order(order = 1)
   public void registrationTest()
   {
      userToken = new UserToken(userid, shortToken, longToken, expirationDate);
      Client client = ClientBuilder.newClient();
      WebTarget target = client.target(TARGET_HOST + AppConstants.API_PATH + AppConstants.REGISTRATION_PATH);
      account = target.request(MediaType.APPLICATION_JSON).post(Entity.json(userToken), Account.class);
      Assert.assertNotNull(account);
      logger.info(account.toString());
   }

   // @Test
   // @Order(order = 2)
   public void scheduleEmailTest()
   {

      ClientEmail clientEmail = new ClientEmail();
      try
      {
         Calendar calendar = Calendar.getInstance();
         calendar.add(Calendar.SECOND, 30);
         clientEmail.date = DateUtils.getStringFromDate(calendar.getTime());
         clientEmail.mailBody = "body: " + calendar.getTimeInMillis();
         clientEmail.mailFrom = "fiorenzino@gmail.com";
         clientEmail.mailTo = "fiorenzo.pizza@gmail.com";
         clientEmail.mailSubject = "subject: " + calendar.getTimeInMillis();
         Client client = ClientBuilder.newClient();
         WebTarget target = client.target(TARGET_HOST + AppConstants.API_PATH + AppConstants.USERS_PATH + "/{userid}"
                  + "/email/schedule").resolveTemplate("userid", userid);
         Invocation.Builder invocationBuilder =
                  target.request(MediaType.APPLICATION_JSON).header("Bearer", account.getLongToken());
         Response result = invocationBuilder
                  .buildPost(Entity.json(clientEmail)).invoke();
         logger.info(result.getStatus());
         logger.info(result.getEntity());
      }
      catch (Exception e1)
      {
         e1.printStackTrace();
      }

   }

   // @Test
   // @Order(order = 3)
   public void scheduleSmsTest()
   {
      ClientSms clientSms = new ClientSms();
      try
      {
         Calendar calendar = Calendar.getInstance();
         calendar.add(Calendar.SECOND, 5);

         clientSms.date = DateUtils.getStringFromDate(calendar.getTime());
         clientSms.msg = "msg: " + calendar.getTimeInMillis();
         clientSms.number = "+393922274929";
         Client client = ClientBuilder.newClient();
         WebTarget target = client.target(TARGET_HOST + AppConstants.API_PATH + AppConstants.USERS_PATH + "/{userid}"
                  + "/sms/schedule").resolveTemplate("userid", userid);
         Invocation.Builder invocationBuilder =
                  target.request(MediaType.APPLICATION_JSON).header("Bearer", account.getLongToken());

         Response result = invocationBuilder
                  .buildPost(Entity.json(clientSms)).invoke();
         Assert.assertNotNull(result);
         logger.info(result.getStatus());
         logger.info(result.getEntity());
      }
      catch (Exception e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }

   // @Test
   // @Order(order = 4)
   public void scheduleBuzzTest()
   {
      ClientBuzz clientBuzz = new ClientBuzz();
      try
      {
         Calendar calendar = Calendar.getInstance();
         calendar.add(Calendar.SECOND, 5);

         clientBuzz.date = DateUtils.getStringFromDate(calendar.getTime());
         clientBuzz.number = "+393922274929";
         Client client = ClientBuilder.newClient();
         WebTarget target = client.target(TARGET_HOST + AppConstants.API_PATH + AppConstants.USERS_PATH + "/{userid}"
                  + "/buzz/schedule").resolveTemplate("userid", userid);
         Invocation.Builder invocationBuilder =
                  target.request(MediaType.APPLICATION_JSON).header("Bearer",
                           account.getLongToken());
         Response result = invocationBuilder
                  .buildPost(Entity.json(clientBuzz)).invoke();
         Assert.assertNotNull(result);
         logger.info(result.getStatus());
         logger.info(result.getEntity());
      }
      catch (Exception e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }

    @Test
    @Order(order = 5)
   public void scheduleCallTest()
   {
      ClientCall clientCall = new ClientCall();
      try
      {
         Calendar calendar = Calendar.getInstance();
         calendar.add(Calendar.SECOND, 5);

         clientCall.date = DateUtils.getStringFromDate(calendar.getTime());
         clientCall.number = "+393922274929";
         clientCall.msg = "msg: " + calendar.getTimeInMillis();
         Client client = ClientBuilder.newClient();
         WebTarget target = client.target(TARGET_HOST + AppConstants.API_PATH + AppConstants.USERS_PATH + "/{userid}"
                  + "/call/schedule").resolveTemplate("userid", userid);
         Invocation.Builder invocationBuilder =
                  target.request(MediaType.APPLICATION_JSON).header("Bearer", account.getLongToken());
         Response result = invocationBuilder
                  .buildPost(Entity.json(clientCall)).invoke();
         Assert.assertNotNull(result);
         logger.info(result.getStatus());
         logger.info(result.getEntity());
      }
      catch (Exception e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }

   // @Test
   public void updateTokenTest()
   {
      UserToken userToken = new UserToken(userid, shortToken, longToken, expirationDate);
      Client client = ClientBuilder.newClient();
      WebTarget target = client.target(TARGET_HOST + AppConstants.API_PATH + AppConstants.REGISTRATION_PATH);
      Object result = target.request(MediaType.APPLICATION_JSON).put(Entity.json(userToken)).getEntity();
      Assert.assertNotNull(result);
   }
}
