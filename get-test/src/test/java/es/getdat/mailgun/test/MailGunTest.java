package es.getdat.mailgun.test;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.ResponseProcessingException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.junit.Test;

import es.getdat.mailgun.service.model.ApiResponse;
import es.getdat.mailgun.util.Authenticator;
import es.getdat.util.CallUtils;

public class MailGunTest
{
   @Test
   public void sendMail()
   {
      String mailUser = "xxx";
      String mailPwd = "xxxx";

      String from = "xxx@gmail.com";
      String tos = "xxx@gmail.com";

      String subject = "prova mailgun";
      String body = "semplice prova";

      Client client = ClientBuilder.newClient().register(new Authenticator(mailUser, mailPwd));
      WebTarget webTarget = client.target("https://api.mailgun.net/v2/we.getdat.es/messages");
      Map<String, String> map = new HashMap<String, String>();
      map.put("from", from);
      String[] to = tos.split(";");
      for (String singleTo : to)
      {
         map.put("to", singleTo);
      }
      map.put("subject", subject);
      map.put("text", body);
      map.put("v:my-custom-data", "{\"event_id\": 12345678}");

      Response response = webTarget.request()
               .post(Entity.form(CallUtils.getForm(map)));
      if (response == null)
      {
         throw new ResponseProcessingException(null, "no response");
      }
      if (response.getStatus() < 200 || response.getStatus() >= 300)
      {
         System.out.println(response.getStatusInfo() == null ? "no status info"
                  : response.getStatusInfo().getReasonPhrase());

      }

      ApiResponse apiResponse = response.readEntity(ApiResponse.class);
      System.out.println(apiResponse);
   }
}
