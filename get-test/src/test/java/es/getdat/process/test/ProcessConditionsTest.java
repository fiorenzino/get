package es.getdat.process.test;

import static es.getdat.api.management.AppConstants.WHEN;

import java.util.Calendar;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import es.getdat.api.model.Content;
import es.getdat.api.model.Process;
import es.getdat.api.model.Step;
import es.getdat.api.model.enums.ChannelType;
import es.getdat.core.management.AppConstants;
import es.getdat.router.util.StepChecker;
import es.getdat.util.DateUtils;

public class ProcessConditionsTest
{
   @Test
   public void testSimpleStepCondition() throws Exception
   {
      Calendar calendar = Calendar.getInstance();
      calendar.add(Calendar.SECOND, 2);

      Process process = new Process();
      String pluginconfigurationId = "123";
      String accountId = "123";
      Step step = new Step(UUID.randomUUID().toString(), pluginconfigurationId,
               ChannelType.SMS.name(), accountId);
      step.condition = "process.get('" + step.stepId + "').routed && process.get('" + step.stepId + "').executed";
      process.add(step);
      System.out.println(process.toJsonString());
      Content content = new Content();
      content.getGlobal().put(WHEN, DateUtils.getStringFromDate(calendar.getTime()));
      content.getGlobal().put(AppConstants.PROCESS, process.toJsonString());
      content.getStepValuesIn(step.stepId).put(es.getdat.plivo.management.AppConstants.PLIVO_SMS_DST, "1234W");
      content.getStepValuesIn(step.stepId).put(es.getdat.plivo.management.AppConstants.PLIVO_SMS_TEXT, " msg");

      boolean result = StepChecker.checkSatisfied(step.condition, process, content);
      System.out.println(result);
      Assert.assertFalse(result);
      process.route(step.stepId);
      process.execute(step.stepId);
      System.out.println(process.toJsonString());
      content.getGlobal().put(AppConstants.PROCESS, process.toJsonString());
      result = StepChecker.checkSatisfied(step.condition, process, content);
      System.out.println(result);
      Assert.assertTrue(result);
   }

   @Test
   public void testCombinedStepCondition() throws Exception
   {
      Calendar calendar = Calendar.getInstance();
      calendar.add(Calendar.SECOND, 2);

      Process process = new Process();
      String pluginconfigurationId = "123";
      String accountId = "123";
      Step step1 = new Step(UUID.randomUUID().toString(), pluginconfigurationId,
               ChannelType.CALL.name(), accountId, 1);
      step1.condition = "process.get('" + step1.stepId + "').routed && process.get('" + step1.stepId
               + "').executed && !process.get('" + step1.stepId + "').success";
      process.add(step1);

      Step step2 = new Step(UUID.randomUUID().toString(), pluginconfigurationId,
               ChannelType.SMS.name(), accountId, 2);
      step2.condition = "process.get('" + step2.stepId + "').routed && process.get('" + step2.stepId
               + "').executed && !process.get('" + step2.stepId + "').success";
      process.add(step2);

      Step step3 = new Step(UUID.randomUUID().toString(), pluginconfigurationId,
               ChannelType.EMAIL.name(), accountId, 3);
      process.add(step3);
      process.successCondition = "process.get('" + step1.stepId + "').success || process.get('" + step2.stepId
               + "').success || process.get('" + step3.stepId + "').success";

      System.out.println(process.toJsonString());
      Content content = new Content();
      content.getGlobal().put(WHEN, DateUtils.getStringFromDate(calendar.getTime()));
      content.getGlobal().put(AppConstants.PROCESS, process.toJsonString());
      content.getStepValuesIn(step1.stepId).put(es.getdat.plivo.management.AppConstants.PLIVO_SMS_DST, "1234W");
      content.getStepValuesIn(step1.stepId).put(es.getdat.plivo.management.AppConstants.PLIVO_SMS_TEXT, " msg");

      boolean result = StepChecker.checkSatisfied(step1.condition, process, content);
      result = StepChecker.checkSatisfied(process.successCondition, process, content);
      System.out.println(result);
      Assert.assertFalse(result);
      process.route(step1.stepId);
      process.execute(step1.stepId);
      System.out.println(process.toJsonString());
      content.getGlobal().put(AppConstants.PROCESS, process.toJsonString());
      result = StepChecker.checkSatisfied(step1.condition, process, content);
      System.out.println(result);
      Assert.assertTrue(result);
      result = StepChecker.checkSatisfied(step2.condition, process, content);
      System.out.println(result);
      Assert.assertFalse(result);
      process.route(step2.stepId);
      process.execute(step2.stepId);
      System.out.println(process.toJsonString());
      content.getGlobal().put(AppConstants.PROCESS, process.toJsonString());
      result = StepChecker.checkSatisfied(step2.condition, process, content);
      System.out.println(result);
      Assert.assertTrue(result);
      process.route(step3.stepId);
      process.execute(step3.stepId);
      process.success(step3.stepId);
      result = StepChecker.checkSatisfied(process.successCondition, process, content);
      Assert.assertTrue(result);
   }
}
