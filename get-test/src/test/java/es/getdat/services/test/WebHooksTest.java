package es.getdat.services.test;

import static es.getdat.api.management.AppConstants.ACCOUNT_ID;
import static es.getdat.api.management.AppConstants.WHEN;
import static es.getdat.core.management.AppConstants.ACCOUNTS_PATH;
import static es.getdat.core.management.AppConstants.ACTIVATIONS_PATH;
import static es.getdat.core.management.AppConstants.API_PATH;
import static es.getdat.core.management.AppConstants.PLUGIN_CONFIGURATIONS_PATH;
import static es.getdat.core.management.AppConstants.PLUGIN_DEFINITIONS_PATH;
import static es.getdat.webhook.management.AppConstants.WEBHOOK_MSG;
import static es.getdat.webhook.management.AppConstants.WEBHOOK_URL;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.UUID;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.logging.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import es.getdat.accounting.model.Account;
import es.getdat.accounting.model.Activation;
import es.getdat.all.test.CrudTests;
import es.getdat.api.model.Content;
import es.getdat.api.model.Process;
import es.getdat.api.model.Step;
import es.getdat.api.model.enums.ChannelType;
import es.getdat.core.management.AppConstants;
import es.getdat.plugin.model.PluginConfiguration;
import es.getdat.plugin.model.PluginDefinition;
import es.getdat.plugins.test.TestUtils;
import es.getdat.router.util.StepChecker;
import es.getdat.util.DateUtils;

/*
 * create account 
 * 
 * create plugin definition 
 * 
 * create plugin configuration 
 * 
 * create attivation  
 * 
 * schedule event with plivo channel types (buzz)
 */
public class WebHooksTest
{

   static Logger logger = Logger.getLogger(WebHooksTest.class);

   private static Account account;
   private static PluginDefinition pluginDefinition;
   private static PluginConfiguration pluginConfiguration;
   private static Activation activation;
   private static CrudTests crudTests;

   @BeforeClass
   public static void beforeTest()
   {
      crudTests = new CrudTests();
      // create account
      account = crudTests.create(TestUtils.TARGET_HOST, API_PATH + ACCOUNTS_PATH,
               TestUtils.newAccount());
      // create plugin definition
      pluginDefinition = crudTests.create(TestUtils.TARGET_HOST, API_PATH
               + PLUGIN_DEFINITIONS_PATH,
               TestUtils.newPluginDefinitionWebHook());
      // create plugin configuration
      pluginConfiguration = crudTests.create(TestUtils.TARGET_HOST, API_PATH
               + PLUGIN_CONFIGURATIONS_PATH, TestUtils
               .newPluginConfigurationWebhookNew(pluginDefinition.getId()));
      // create attivation
      activation = crudTests
               .create(TestUtils.TARGET_HOST, API_PATH + ACTIVATIONS_PATH, TestUtils
                        .newActivation(account.getId(),
                                 pluginConfiguration.getId(),
                                 ChannelType.WEBHOOK, true));
   }

   @AfterClass
   public static void afterTest()
   {
      // destroy attivation
      crudTests.delete(TestUtils.TARGET_HOST, API_PATH + ACTIVATIONS_PATH,
               activation.getId());
      // destroy plugin configuration
      crudTests.delete(TestUtils.TARGET_HOST, API_PATH + PLUGIN_CONFIGURATIONS_PATH,
               pluginConfiguration.getId());
      // destroy plugin definition
      crudTests.delete(TestUtils.TARGET_HOST, API_PATH + PLUGIN_DEFINITIONS_PATH,
               pluginDefinition.getId());
      // destroy account
      crudTests
               .delete(TestUtils.TARGET_HOST, API_PATH + ACCOUNTS_PATH, account.getId());
   }

   @Test
   public void webHooksTest() throws Exception
   {

      Content content = makeContent();

      WebTarget target = CrudTests
               .getTarget(TestUtils.TARGET_HOST, "/api/v1/scheduler");
      logger.info(content.toString());
      try
      {
         Response res = target.request()
                  .buildPost(Entity.entity(content, MediaType.APPLICATION_JSON))
                  .invoke();
         System.out.println("code: " + res.getStatus() + " - status: "
                  + Status.fromStatusCode(res.getStatus()) + " - family: "
                  + Status.Family.familyOf(res.getStatus()));
         if (res.getStatus() == Status.OK.getStatusCode())
         {
            String value = res.readEntity(String.class);
            System.out.println(value);
            Assert.assertNotNull(value);
         }
         res.close();

      }
      catch (Exception e)
      {
         System.out.println("Exception : " + e.getMessage());
         e.printStackTrace();
      }
      try
      {
         BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
         System.out.println("Premi un tasto per cancellare i crud creati per il test?");
         br.readLine();
         // Thread.sleep(60000);
      }
      catch (Exception e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }

   @Test
   public void checkTest()
   {
      try
      {
         Content content = makeContent();
         Process process = new Process(content.get(AppConstants.PROCESS));

         for (int i = 1; i <= process.steps.size(); i++)
         {
            for (Step step : process.get(i))
            {
               boolean result = StepChecker.checkSatisfied(step.condition, process, content);
               System.out.println(step.stepId + " " + WEBHOOK_URL + " = "
                        + content.getStepValuesIn(step.stepId).get(WEBHOOK_URL));
               System.out.println(step.stepId + " can run? " + result);
               if (result)
               {
                  step.routed = true;
                  content.getStepValuesOut(step.stepId).put(AppConstants.EXECUTED, Boolean.TRUE.toString());
               }
            }
         }

         content.getStepValuesOut(process.get(1).get(0).stepId).put(AppConstants.OPERATION, "test");
         content.getStepValuesOut(process.get(3).get(0).stepId).put(AppConstants.EXECUTED, "false");

         for (int i = 1; i <= process.steps.size(); i++)
         {
            for (Step step : process.get(i))
            {
               boolean result = StepChecker.checkSatisfied(step.condition, process, content);
               System.out.println(step.stepId + " " + WEBHOOK_URL + " = "
                        + content.getStepValuesIn(step.stepId).get(WEBHOOK_URL));
               System.out.println(step.stepId + " can run? " + result);
               if (result)
               {
                  step.routed = true;
                  content.getStepValuesOut(step.stepId).put(AppConstants.EXECUTED, Boolean.TRUE.toString());
               }
            }
         }
      }
      catch (Exception e)
      {
         Assert.fail(e.getClass().getCanonicalName() + ", message = " + e.getMessage());
      }
   }

   private Content makeContent() throws Exception
   {

      Process process = new Process();

      Step step1 = new Step(UUID.randomUUID().toString(), pluginConfiguration.getId(),
               ChannelType.WEBHOOK.name(), account.getId());

      Step step2 = new Step(UUID.randomUUID().toString(), pluginConfiguration.getId(),
               ChannelType.WEBHOOK.name(), account.getId());

      Step step3 = new Step(UUID.randomUUID().toString(), pluginConfiguration.getId(),
               ChannelType.WEBHOOK.name(), account.getId());

      step2.condition = " "
               // proof of concept of how results of step n-1 can become input for step n
               + "     content.getStepValuesIn('" + step2.stepId + "').put('" + WEBHOOK_URL
               + "', content.getStepValuesOut('" + step1.stepId + "').get('" + AppConstants.OPERATION + "') );\n"
               + ""
               // precondition: execute if step1 is routed and step 2 not
               // postcondition: let execution go on if step2 is routed and step 3 not
               // here in OR since we don't have separate condition statements for the two situations
               + "( process.get('" + step1.stepId + "').routed && !process.get('" + step2.stepId + "').routed ) || "
               + "( process.get('" + step2.stepId + "').routed && !process.get('" + step3.stepId + "').routed ) ;";

      step3.condition = ""
               // precondition: if step3 has not yet executed, execute step3 only if step1 was executed right
               // postcondition: if step3 has executed right, dont let the execution go on
               + " ( content.getStepValuesOut('" + step3.stepId + "').get('" + AppConstants.EXECUTED + "') == null ) " +
               "? " +
               "content.getStepValuesOut('" + step1.stepId + "').get('" + AppConstants.EXECUTED + "')" +
               ":" +
               "!content.getStepValuesOut('" + step3.stepId + "').get('" + AppConstants.EXECUTED + "')";

      process.add(step1).add(step2).add(step3);

      String americaNYtimezone = "America/New_York";

      Calendar calendar = Calendar.getInstance();
      calendar.add(Calendar.SECOND, 15);
      String americaNYwhen = DateUtils.getStringFromDate(calendar.getTime(), americaNYtimezone);
      System.out.println("Date and time in NY: " + americaNYwhen);

      Content content = new Content();
      content.put(AppConstants.TIMEZONE, americaNYtimezone);
      content.put(WHEN, americaNYwhen);

      content.put(AppConstants.PROCESS, process.toJsonString());
      content.getGlobal().put(ACCOUNT_ID, account.getId());

      content.getStepValuesIn(step1.stepId).put(WEBHOOK_URL, TestUtils.TARGET_HOST + "/hook");
      content.getStepValuesIn(step1.stepId).put(WEBHOOK_MSG, "step 1");

      // content.getStepValuesIn(step2.stepId).put(WEBHOOK_URL, TestUtils.TARGET_HOST + "/hook");
      content.getStepValuesIn(step2.stepId).put(WEBHOOK_MSG, "step 2 (if step 1 has been routed)");

      content.getStepValuesIn(step3.stepId).put(WEBHOOK_URL, TestUtils.TARGET_HOST + "/hook");
      content.getStepValuesIn(step3.stepId).put(WEBHOOK_MSG, "step 3 (if step 1 has been executed)");

      return content;

   }

}
