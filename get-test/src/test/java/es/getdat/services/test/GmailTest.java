package es.getdat.services.test;

import static es.getdat.api.management.AppConstants.WHEN;
import static es.getdat.core.management.AppConstants.ACCOUNTS_PATH;
import static es.getdat.core.management.AppConstants.ACCOUNT_ID;
import static es.getdat.core.management.AppConstants.ACTIVATIONS_PATH;
import static es.getdat.core.management.AppConstants.API_PATH;
import static es.getdat.core.management.AppConstants.PLUGIN_CONFIGURATIONS_PATH;
import static es.getdat.core.management.AppConstants.PLUGIN_DEFINITIONS_PATH;
import static es.getdat.mail.management.AppConstants.MAIL_BODY;
import static es.getdat.mail.management.AppConstants.MAIL_FROM;
import static es.getdat.mail.management.AppConstants.MAIL_SUBJECT;
import static es.getdat.mail.management.AppConstants.MAIL_TOS;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.UUID;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.logging.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import es.getdat.accounting.model.Account;
import es.getdat.accounting.model.Activation;
import es.getdat.all.test.CrudTests;
import es.getdat.api.model.Content;
import es.getdat.api.model.Process;
import es.getdat.api.model.Step;
import es.getdat.api.model.enums.ChannelType;
import es.getdat.core.management.AppConstants;
import es.getdat.plugin.model.PluginConfiguration;
import es.getdat.plugin.model.PluginDefinition;
import es.getdat.plugins.test.TestUtils;
import es.getdat.util.DateUtils;

/*
 * create account 
 * 
 * create plugin definition 
 * 
 * create plugin configuration 
 * 
 * create attivation  
 * 
 * schedule event with plivo channel types (buzz)
 */
public class GmailTest
{

   static Logger logger = Logger.getLogger(GmailTest.class);

   // static final String TestUtils.TARGET_HOST = "http://get-dates.rhcloud.com";
   // static final String TestUtils.TARGET_HOST = "http://localhost:8080";

   private static Account account;
   private static PluginDefinition pluginDefinition;
   private static PluginConfiguration pluginConfiguration;
   private static Activation activation;
   private static CrudTests crudTests;

   @BeforeClass
   public static void beforeTest()
   {
      crudTests = new CrudTests();
      // create account
      account = crudTests.create(TestUtils.TARGET_HOST, API_PATH + ACCOUNTS_PATH,
               TestUtils.newAccount());
      // create plugin definition
      pluginDefinition = crudTests
               .create(TestUtils.TARGET_HOST, API_PATH + PLUGIN_DEFINITIONS_PATH,
                        TestUtils.newPluginDefinitionEmail());
      // create plugin configuration
      pluginConfiguration = crudTests
               .create(TestUtils.TARGET_HOST, API_PATH + PLUGIN_CONFIGURATIONS_PATH,
                        TestUtils.newPluginConfigurationEmail(pluginDefinition
                                 .getId()));
      // create attivation
      activation = crudTests.create(
               TestUtils.TARGET_HOST,
               API_PATH + ACTIVATIONS_PATH,
               TestUtils.newActivation(account.getId(),
                        pluginConfiguration.getId(), ChannelType.EMAIL, true));
   }

   @AfterClass
   public static void afterTest()
   {
      // destroy attivation
      crudTests.delete(TestUtils.TARGET_HOST, API_PATH + ACTIVATIONS_PATH,
               activation.getId());
      // destroy plugin configuration
      crudTests.delete(TestUtils.TARGET_HOST, API_PATH + PLUGIN_CONFIGURATIONS_PATH,
               pluginConfiguration.getId());
      // destroy plugin definition
      crudTests.delete(TestUtils.TARGET_HOST, API_PATH + PLUGIN_DEFINITIONS_PATH,
               pluginDefinition.getId());
      // destroy account
      crudTests
               .delete(TestUtils.TARGET_HOST, API_PATH + ACCOUNTS_PATH, account.getId());
   }

   @Test
   public void multiMail() throws Exception
   {
      for (int i = 0; i < 1; i++)
      {
         gmailTest(i);
         System.out.println(i);
      }
      try
      {
         Thread.sleep(90000);
      }
      catch (InterruptedException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }

   public void gmailTest(int i) throws Exception
   {

      Content content = makeContent(i);

      WebTarget target = CrudTests
               .getTarget(TestUtils.TARGET_HOST, "/api/v1/scheduler");
      logger.info(content.toString());

      try
      {
         Response res = target.request()
                  .buildPost(Entity.entity(content, MediaType.APPLICATION_JSON))
                  .invoke();
         System.out.println("code: " + res.getStatus() + " - status: "
                  + Status.fromStatusCode(res.getStatus()) + " - family: "
                  + Status.Family.familyOf(res.getStatus()));
         if (res.getStatus() == Status.OK.getStatusCode())
         {
            String value = res.readEntity(String.class);
            System.out.println(value);
            Assert.assertNotNull(value);
         }
         res.close();

      }
      catch (Exception e)
      {
         System.out.println("Exception : " + e.getMessage());
         e.printStackTrace();
      }

      try
      {
         BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
         System.out.println("Premi un tasto per cancellare i crud creati per il test?");
         br.readLine();
         // Thread.sleep(60000);
      }
      catch (Exception e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }

   private Content makeContent(int i) throws Exception
   {
      Calendar calendar = Calendar.getInstance();
      calendar.add(Calendar.SECOND, 2);

      Process process = new Process();
      Step step = new Step(UUID.randomUUID().toString(), pluginConfiguration.getId(),
               ChannelType.EMAIL.name(), account.getId());
      process.add(step);

      Content content = new Content();
      content.getGlobal().put(WHEN, DateUtils.getStringFromDate(calendar.getTime()));
      content.getGlobal().put(ACCOUNT_ID, account.getId());
      // content.getGlobal().put(AppConstants.TIMEZONE, "Europe/Rome");
      content.getGlobal().put(AppConstants.PROCESS, process.toJsonString());

      content.getStepValuesIn(step.stepId).put(MAIL_FROM, "fiorenzo.pizza@ict-group.it");
      content.getStepValuesIn(step.stepId).put(MAIL_TOS, "fiorenzo.pizza@gmail.com");
      content.getStepValuesIn(step.stepId).put(MAIL_SUBJECT, "soggetto di prova n:" + i);
      content.getStepValuesIn(step.stepId).put(MAIL_BODY, "tutto bene? si bene! test" + i);

      return content;
   }
}
