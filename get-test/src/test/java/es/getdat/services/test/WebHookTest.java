package es.getdat.services.test;

import static es.getdat.api.management.AppConstants.ACCOUNT_ID;
import static es.getdat.api.management.AppConstants.WHEN;
import static es.getdat.core.management.AppConstants.ACCOUNTS_PATH;
import static es.getdat.core.management.AppConstants.ACTIVATIONS_PATH;
import static es.getdat.core.management.AppConstants.API_PATH;
import static es.getdat.core.management.AppConstants.PLUGIN_CONFIGURATIONS_PATH;
import static es.getdat.core.management.AppConstants.PLUGIN_DEFINITIONS_PATH;
import static es.getdat.webhook.management.AppConstants.WEBHOOK_MSG;
import static es.getdat.webhook.management.AppConstants.WEBHOOK_URL;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.UUID;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.logging.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import es.getdat.accounting.model.Account;
import es.getdat.accounting.model.Activation;
import es.getdat.all.test.CrudTests;
import es.getdat.api.model.Content;
import es.getdat.api.model.Process;
import es.getdat.api.model.Step;
import es.getdat.api.model.enums.ChannelType;
import es.getdat.core.management.AppConstants;
import es.getdat.plugin.model.PluginConfiguration;
import es.getdat.plugin.model.PluginDefinition;
import es.getdat.plugins.test.TestUtils;
import es.getdat.util.DateUtils;

/*
 * create account 
 * 
 * create plugin definition 
 * 
 * create plugin configuration 
 * 
 * create attivation  
 * 
 * schedule event with plivo channel types (buzz)
 */
public class WebHookTest
{

   static Logger logger = Logger.getLogger(WebHookTest.class);

   private static Account account;
   private static PluginDefinition pluginDefinition;
   private static PluginConfiguration pluginConfiguration;
   private static Activation activation;
   private static CrudTests crudTests;

   @BeforeClass
   public static void beforeTest()
   {
      crudTests = new CrudTests();
      // create account
      account = crudTests.create(TestUtils.TARGET_HOST, API_PATH + ACCOUNTS_PATH,
               TestUtils.newAccount());
      // create plugin definition
      pluginDefinition = crudTests.create(TestUtils.TARGET_HOST, API_PATH
               + PLUGIN_DEFINITIONS_PATH,
               TestUtils.newPluginDefinitionWebHook());
      // create plugin configuration
      pluginConfiguration = crudTests.create(TestUtils.TARGET_HOST, API_PATH
               + PLUGIN_CONFIGURATIONS_PATH, TestUtils
               .newPluginConfigurationWebhook(pluginDefinition.getId()));
      // create attivation
      activation = crudTests
               .create(TestUtils.TARGET_HOST, API_PATH + ACTIVATIONS_PATH, TestUtils
                        .newActivation(account.getId(),
                                 pluginConfiguration.getId(),
                                 ChannelType.WEBHOOK, true));
   }

   @AfterClass
   public static void afterTest()
   {
      // destroy attivation
      crudTests.delete(TestUtils.TARGET_HOST, API_PATH + ACTIVATIONS_PATH,
               activation.getId());
      // destroy plugin configuration
      crudTests.delete(TestUtils.TARGET_HOST, API_PATH + PLUGIN_CONFIGURATIONS_PATH,
               pluginConfiguration.getId());
      // destroy plugin definition
      crudTests.delete(TestUtils.TARGET_HOST, API_PATH + PLUGIN_DEFINITIONS_PATH,
               pluginDefinition.getId());
      // destroy account
      crudTests
               .delete(TestUtils.TARGET_HOST, API_PATH + ACCOUNTS_PATH, account.getId());
   }

   @Test
   public void webHookTest() throws Exception
   {
      Calendar calendar = Calendar.getInstance();
      calendar.add(Calendar.SECOND, 10);

      Process process = new Process();
      Step step = new Step(UUID.randomUUID().toString(), pluginConfiguration.getId(),
               ChannelType.WEBHOOK.name(), account.getId());
      process.add(step);

      Content content = new Content();
      // content.put(AppConstants.TIMEZONE, "Europe/Rome");
      content.put(WHEN, DateUtils.getStringFromDate(calendar.getTime()));
      content.put(AppConstants.PROCESS, process.toJsonString());
      content.getGlobal().put(ACCOUNT_ID, account.getId());

      content.getStepValuesIn(step.stepId).put(WEBHOOK_URL, TestUtils.TARGET_HOST + "/hook/legacy");
      content.getStepValuesIn(step.stepId).put(WEBHOOK_MSG, "ciao ciao");

      WebTarget target = CrudTests
               .getTarget(TestUtils.TARGET_HOST, "/api/v1/scheduler");
      logger.info(content.toString());
      try
      {
         Response res = target.request()
                  .buildPost(Entity.entity(content, MediaType.APPLICATION_JSON))
                  .invoke();
         System.out.println("code: " + res.getStatus() + " - status: "
                  + Status.fromStatusCode(res.getStatus()) + " - family: "
                  + Status.Family.familyOf(res.getStatus()));
         if (res.getStatus() == Status.OK.getStatusCode())
         {
            String value = res.readEntity(String.class);
            System.out.println(value);
            Assert.assertNotNull(value);
         }
         res.close();

      }
      catch (Exception e)
      {
         System.out.println("Exception : " + e.getMessage());
         e.printStackTrace();
      }
      try
      {
         BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
         System.out.println("Premi un tasto per cancellare i crud creati per il test?");
         br.readLine();
         // Thread.sleep(60000);
      }
      catch (Exception e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }

   @Test
   public void processtest()
   {
      Process process = new Process();
      Step step = new Step();
      step.accountId = "ciao";
      step.channelType = ChannelType.WEBHOOK.name();
      step.order = 1;
      step.stepId = UUID.randomUUID().toString();
      step.pluginConfigurationId = "123";
      process.add(step);
      System.out.println(process.toJsonString());

   }
}
