package es.getdat.services.test;

import static es.getdat.api.management.AppConstants.ACCOUNT_ID;
import static es.getdat.api.management.AppConstants.WHEN;
import static es.getdat.core.management.AppConstants.ACCOUNTS_PATH;
import static es.getdat.core.management.AppConstants.ACTIVATIONS_PATH;
import static es.getdat.core.management.AppConstants.API_PATH;
import static es.getdat.core.management.AppConstants.PLUGIN_CONFIGURATIONS_PATH;
import static es.getdat.core.management.AppConstants.PLUGIN_DEFINITIONS_PATH;
import static es.getdat.plivo.management.AppConstants.PLIVO_SMS_DST;
import static es.getdat.plivo.management.AppConstants.PLIVO_SMS_TEXT;

import java.util.Calendar;
import java.util.TimeZone;
import java.util.UUID;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.logging.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.plivo.ee.helper.api.MessageApi;
import org.plivo.ee.helper.api.configuration.GlobalConstant;
import org.plivo.ee.helper.api.pojo.ApiResponse;
import org.plivo.ee.helper.api.util.Authenticator;

import com.sun.mail.handlers.message_rfc822;

import es.getdat.accounting.model.Account;
import es.getdat.accounting.model.Activation;
import es.getdat.all.test.CrudTests;
import es.getdat.api.model.Content;
import es.getdat.api.model.Process;
import es.getdat.api.model.Step;
import es.getdat.api.model.enums.ChannelType;
import es.getdat.core.management.AppConstants;
import es.getdat.plugin.model.PluginConfiguration;
import es.getdat.plugin.model.PluginDefinition;
import es.getdat.plugins.test.TestUtils;
import es.getdat.util.DateUtils;

/*
 * create account 
 * 
 * create plugin definition 
 * 
 * create plugin configuration 
 * 
 * create attivation  
 * 
 * schedule event with plivo channel types (buzz)
 */
public class PlivoSmsTest
{

   static Logger logger = Logger.getLogger(PlivoSmsTest.class);

   private static Account account;
   private static PluginDefinition pluginDefinition;
   private static PluginConfiguration pluginConfiguration;
   private static Activation activation;
   private static CrudTests crudTests;

   @BeforeClass
   public static void beforeTest()
   {
      crudTests = new CrudTests();
      // create account
      account = crudTests.create(TestUtils.TARGET_HOST, API_PATH + ACCOUNTS_PATH,
               TestUtils.newAccount());
      // create plugin definition
      pluginDefinition = crudTests.create(TestUtils.TARGET_HOST, API_PATH
               + PLUGIN_DEFINITIONS_PATH,
               TestUtils.newPluginDefinitionPlivoSms());
      // create plugin configuration
      pluginConfiguration = crudTests.create(TestUtils.TARGET_HOST, API_PATH
               + PLUGIN_CONFIGURATIONS_PATH, TestUtils
               .newPluginConfigurationPlivoSms(pluginDefinition.getId()));
      // create attivation
      activation = crudTests.create(
               TestUtils.TARGET_HOST,
               API_PATH + ACTIVATIONS_PATH,
               TestUtils.newActivation(account.getId(),
                        pluginConfiguration.getId(), ChannelType.SMS, true));
   }

   @AfterClass
   public static void afterTest()
   {
      // destroy attivation
      crudTests.delete(TestUtils.TARGET_HOST, API_PATH + ACTIVATIONS_PATH,
               activation.getId());
      // destroy plugin configuration
      crudTests.delete(TestUtils.TARGET_HOST, API_PATH + PLUGIN_CONFIGURATIONS_PATH,
               pluginConfiguration.getId());
      // destroy plugin definition
      crudTests.delete(TestUtils.TARGET_HOST, API_PATH + PLUGIN_DEFINITIONS_PATH,
               pluginDefinition.getId());
      // destroy account
      crudTests
               .delete(TestUtils.TARGET_HOST, API_PATH + ACCOUNTS_PATH, account.getId());
   }

   @Test
   public void multiSms() throws Exception
   {
      for (int i = 0; i < 1; i++)
      {
         smsTest(i);
      }
      try
      {
         Thread.sleep(24000);
      }
      catch (InterruptedException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }

   public void smsTest(int i) throws Exception
   {
      Calendar calendar = Calendar.getInstance();
      calendar.add(Calendar.SECOND, 10);
      // calendar.add(Calendar.HOUR, 9);

      Process process = new Process();
      Step step = new Step(UUID.randomUUID().toString(), pluginConfiguration.getId(),
               ChannelType.SMS.name(), account.getId());
      process.add(step);

      Content content = new Content();
      content.getGlobal().put(WHEN, DateUtils.getStringFromDate(calendar.getTime()));
      // content.getGlobal().put(AppConstants.TIMEZONE, "Europe/Rome");
      // content.getGlobal().put(AppConstants.TIMEZONE, TimeZone.getTimeZone("PST").getDisplayName());
      content.getGlobal().put(AppConstants.PROCESS, process.toJsonString());
      content.getGlobal().put(ACCOUNT_ID, account.getId());

      content.getStepValuesIn(step.stepId)
               .put(PLIVO_SMS_DST, "+393922274929");
      content.getStepValuesIn(step.stepId)
               .put(PLIVO_SMS_TEXT, "tanti tanti saluti - msg n." + (i + 1) + "m");

      WebTarget target = CrudTests
               .getTarget(TestUtils.TARGET_HOST, "/api/v1/scheduler");
      try
      {
         Response res = target.request()
                  .buildPost(Entity.entity(content, MediaType.APPLICATION_JSON))
                  .invoke();
         System.out.println("code: " + res.getStatus() + " - status: "
                  + Status.fromStatusCode(res.getStatus()) + " - family: "
                  + Status.Family.familyOf(res.getStatus()));
         if (res.getStatus() == Status.OK.getStatusCode())
         {
            String value = res.readEntity(String.class);
            System.out.println(value);
            Assert.assertNotNull(value);
         }
         res.close();

      }
      catch (Exception e)
      {
         System.out.println("Exception : " + e.getMessage());
         e.printStackTrace();
      }

   }

   public static void main3(String[] args) throws Exception
   {
      String dst = "+393922274929";
      String text = "ciao ciao";

      /*
       * plivoAuthId":"MAOWE0YJUZYMQZNDBLNW",
       * "plivoNumber":"3905221520065","serviceUrl":"https://forgetters-dates.rhcloud .
       * com/api/v1/plivo/sms","plivoAuthToken
       * ":"YmUyNTU1YjhmZWQ1NmQxNGVjODkwMzBhNWUzOGYw","plivoSmsUrl":"https://forgetters
       * -dates.rhcloud.com/api/v1/plivo/sms/url/","plivoAccountName":"landing_page",}
       */

      String number = "3905221520065";
      String authId = "MAOWE0YJUZYMQZNDBLNW";
      String authToken = "YmUyNTU1YjhmZWQ1NmQxNGVjODkwMzBhNWUzOGYw";
      String accountName = "landing_page";
      String url = "https://forgetters-dates.rhcloud.com/api/v1/plivo/sms/url/";
      Client client = ClientBuilder.newClient().register(new Authenticator(authId, authToken));
      MessageApi messageApi = new MessageApi(client);
      messageApi
               .put(GlobalConstant.AUTH_ID, authId)
               .put(GlobalConstant.SRC, number)
               .put(GlobalConstant.DST, dst)
               .put(GlobalConstant.TEXT, text)
               .put(GlobalConstant.TYPE, "sms")
               .put(GlobalConstant.URL,
                        url
                                 + "?eventId=8bfb69f0-f45f-4543-ad4e-374295203af3&stepId=84d09367-797e-4b77-8347-83e4ced48139")
               .put(GlobalConstant.METHOD, "POST")
               .put(GlobalConstant.LOG, "true");
      ApiResponse result = messageApi.sendMessage();
      System.out.println(result);
   }

}
