package es.getdat.services.test;

import static es.getdat.api.management.AppConstants.ACCOUNT_ID;
import static es.getdat.core.management.AppConstants.*;
import static es.getdat.plivo.management.AppConstants.PLIVO_CALL_TO;

import java.util.Calendar;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.logging.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import es.getdat.accounting.model.Account;
import es.getdat.accounting.model.Activation;
import es.getdat.all.test.CrudTests;
import es.getdat.api.model.Content;
import es.getdat.api.model.enums.ChannelType;
import es.getdat.plivo.management.AppConstants;
import es.getdat.plugin.model.PluginConfiguration;
import es.getdat.plugin.model.PluginDefinition;
import es.getdat.plugins.test.TestUtils;
import es.getdat.util.DateUtils;

/*
 * create account 
 * 
 * create plugin definition 
 * 
 * create plugin configuration 
 * 
 * create attivation  
 * 
 * schedule event with plivo channel types (buzz)
 */
public class TwilioCallTest
{

   static Logger logger = Logger.getLogger(TwilioCallTest.class);

   // static final String TARGET_HOST = "http://localhost:8080";
   static final String TARGET_HOST = "https://we.getdat.es";

   private static Account account;
   private static PluginDefinition pluginDefinition;
   private static PluginConfiguration pluginConfiguration;
   private static Activation activation;
   private static CrudTests crudTests;

   @BeforeClass
   public static void beforeTest()
   {
      crudTests = new CrudTests();
      // create account
      account = crudTests.create(TARGET_HOST, API_PATH + ACCOUNTS_PATH,
               TestUtils.newAccount());
      // create plugin definition
      pluginDefinition = crudTests.create(TARGET_HOST, API_PATH
               + PLUGIN_DEFINITIONS_PATH,
               TestUtils.newPluginDefinitionPlivoCall());
      // create plugin configuration
      pluginConfiguration = crudTests.create(TARGET_HOST, API_PATH
               + PLUGIN_CONFIGURATIONS_PATH, TestUtils
               .newPluginConfigurationPlivoCall(pluginDefinition.getId()));
      // create attivation
      activation = crudTests.create(
               TARGET_HOST,
               API_PATH + ACTIVATIONS_PATH,
               TestUtils.newActivation(account.getId(),
                        pluginConfiguration.getId(), ChannelType.CALL, true));
   }

   @AfterClass
   public static void afterTest()
   {
      // destroy attivation
      crudTests.delete(TARGET_HOST, API_PATH + ACTIVATIONS_PATH,
               activation.getId());
      // destroy plugin configuration
      crudTests.delete(TARGET_HOST, API_PATH + PLUGIN_CONFIGURATIONS_PATH,
               pluginConfiguration.getId());
      // destroy plugin definition
      crudTests.delete(TARGET_HOST, API_PATH + PLUGIN_DEFINITIONS_PATH,
               pluginDefinition.getId());
      // destroy account
      crudTests
               .delete(TARGET_HOST, API_PATH + ACCOUNTS_PATH, account.getId());
   }

   @Test
   public void allTest() throws Exception
   {
      callMp3Test();
      // try
      // {
      // Thread.sleep(20000);
      // }
      // catch (InterruptedException e)
      // {
      // // TODO Auto-generated catch block
      // e.printStackTrace();
      // }
      // callMsgTest();
      // try
      // {
      // Thread.sleep(60000);
      // }
      // catch (InterruptedException e)
      // {
      // // TODO Auto-generated catch block
      // e.printStackTrace();
      // }
   }

   public void callMp3Test() throws Exception
   {
      Calendar calendar = Calendar.getInstance();
      calendar.add(Calendar.SECOND, 30);
      Content c = new Content();
      c.getGlobal().put(ACCOUNT_ID, account.getId());

      c.put(WHEN, DateUtils.getStringFromDate(calendar.getTime()))
               .put(TIMEZONE, "Europe/Rome")
               .put(ACCOUNT_ID, account.getId())
               .put(PLIVO_CALL_TO, "+393922274929")
               .put(CHANNEL_TYPE, ChannelType.CALL.name())
               .put(AppConstants.PLIVO_CALL_FILE,
                        "https://s3.amazonaws.com/plivocloud/Trumpet.mp3");
      // file = operation.get(AppConstants.PLIVO_CALL_FILE);;
      Client client = ClientBuilder.newClient();
      WebTarget target = CrudTests
               .getTarget(TARGET_HOST, "/api/v1/scheduler");

      try
      {
         Response res = target.request()
                  .buildPost(Entity.entity(c, MediaType.APPLICATION_JSON))
                  .invoke();
         System.out.println("code: " + res.getStatus() + " - status: "
                  + Status.fromStatusCode(res.getStatus()) + " - family: "
                  + Status.Family.familyOf(res.getStatus()));
         if (res.getStatus() == Status.OK.getStatusCode())
         {
            String value = res.readEntity(String.class);
            System.out.println(value);
            Assert.assertNotNull(value);
         }
         res.close();

      }
      catch (Exception e)
      {
         System.out.println("Exception : " + e.getMessage());
         e.printStackTrace();
      }

   }

   // @Test
   public void callMsgTest() throws Exception
   {
      Calendar calendar = Calendar.getInstance();
      calendar.add(Calendar.SECOND, 30);
      Content c = new Content();
      c.put(WHEN, DateUtils.getStringFromDate(calendar.getTime()))
               .put(TIMEZONE, "Europe/Rome")
               .put(ACCOUNT_ID, account.getId())
               .put(PLIVO_CALL_TO, "+393922274929")
               .put(CHANNEL_TYPE, ChannelType.CALL.name())
               .put(AppConstants.PLIVO_CALL_MSG,
                        "Ciao Michela, come ti senti sotto il sole? Ti aspetto al bar DOLCEZZA.");
      // file = operation.get(AppConstants.PLIVO_CALL_FILE);;
      Client client = ClientBuilder.newClient();
      WebTarget target = CrudTests
               .getTarget(TARGET_HOST, "/api/v1/scheduler");

      try
      {
         Response res = target.request()
                  .buildPost(Entity.entity(c, MediaType.APPLICATION_JSON))
                  .invoke();
         System.out.println("code: " + res.getStatus() + " - status: "
                  + Status.fromStatusCode(res.getStatus()) + " - family: "
                  + Status.Family.familyOf(res.getStatus()));
         if (res.getStatus() == Status.OK.getStatusCode())
         {
            String value = res.readEntity(String.class);
            System.out.println(value);
            Assert.assertNotNull(value);
         }
         res.close();

      }
      catch (Exception e)
      {
         System.out.println("Exception : " + e.getMessage());
         e.printStackTrace();
      }

   }
}
