package es.getdat.all.test;

import static es.getdat.api.management.AppConstants.CHANNEL_TYPE;
import static es.getdat.api.management.AppConstants.SERVICE_URL;
import static es.getdat.api.management.AppConstants.WHEN;
import static es.getdat.webhook.management.AppConstants.WEBHOOK_URL;

import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.junit.Assert;
import org.junit.Test;

import es.getdat.accounting.model.Account;
import es.getdat.accounting.model.Activation;
import es.getdat.api.model.Content;
import es.getdat.api.model.PaginatedListWrapper;
import es.getdat.api.model.enums.ChannelType;
import es.getdat.core.management.AppConstants;
import es.getdat.plugin.model.PluginConfiguration;
import es.getdat.plugin.model.PluginDefinition;
import es.getdat.plugins.test.AccountingTest;
import es.getdat.plugins.test.RouterTestClient;
import es.getdat.plugins.test.TestUtils;
import es.getdat.templates.model.ProcessTemplate;
import es.getdat.templates.model.pojo.ProcessTemplateSearch;

public class AllTests
{

   static final String TARGET_HOST = "http://localhost:8080/";

   // static final String TARGET_HOST = "https://we.getdat.es/";

   static
   {
      try
      {
         TrustManager[] trustAllCerts = { new X509TrustManager()
         {
            public X509Certificate[] getAcceptedIssuers()
            {
               return null;
            }

            public void checkClientTrusted(X509Certificate[] certs,
                     String authType)
            {
            }

            public void checkServerTrusted(X509Certificate[] certs,
                     String authType)
            {
            }
         } };
         SSLContext sc = SSLContext.getInstance("SSL");

         HostnameVerifier hv = new HostnameVerifier()
         {
            public boolean verify(String arg0, SSLSession arg1)
            {
               return true;
            }
         };
         sc.init(null, trustAllCerts, new SecureRandom());

         HttpsURLConnection
                  .setDefaultSSLSocketFactory(sc.getSocketFactory());
         HttpsURLConnection.setDefaultHostnameVerifier(hv);
      }
      catch (Exception localException)
      {
      }
   }

   @Test
   public void account()
   {
      try
      {
         Account newAccount = TestUtils.newAccount();

         CrudTests crudTests = new CrudTests();
         newAccount = crudTests.create(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.ACCOUNTS_PATH, newAccount);
         Assert.assertNotNull(newAccount);
         Assert.assertNotNull(newAccount.getId());
         String accountId = newAccount.getId();
         crudTests.exists(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.ACCOUNTS_PATH, accountId);
         GenericType<PaginatedListWrapper<Account>> genericType = new GenericType<PaginatedListWrapper<Account>>()
         {
         };
         List<Account> accountList = crudTests.wrappedList(TARGET_HOST,
                  AppConstants.API_PATH + AppConstants.ACCOUNTS_PATH,
                  "username asc", genericType);
         if (accountList == null)
         {
            Assert.fail("empty list");
            return;
         }
         boolean found = false;
         for (Account account : accountList)
         {
            if (account.getId().equals(accountId))
            {
               found = true;
               break;
            }
         }
         if (!found)
         {
            Assert.fail("not in list");
         }
         Account account = crudTests.get(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.ACCOUNTS_PATH, accountId, Account.class);
         Assert.assertNotNull(account);
         Assert.assertEquals(accountId, account.getId());

         String newFirstName = "Nuovo Nome";
         account.setFirstName(newFirstName);
         crudTests.update(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.ACCOUNTS_PATH, account);

         account = crudTests.get(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.ACCOUNTS_PATH, accountId, Account.class);
         Assert.assertEquals(newFirstName, account.getFirstName());

         crudTests.delete(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.ACCOUNTS_PATH, accountId);

         crudTests.notExists(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.ACCOUNTS_PATH, accountId);

      }
      catch (Exception ex)
      {
         System.err.println(ex);
         Assert.fail(ex.getClass().getCanonicalName());
      }
   }

   @Test
   public void pluginDefinition()
   {
      try
      {
         PluginDefinition newPluginDefinition = TestUtils
                  .newPluginDefinitionEmail();

         CrudTests crudTests = new CrudTests();
         newPluginDefinition = crudTests.create(TARGET_HOST,
                  AppConstants.API_PATH
                           + AppConstants.PLUGIN_DEFINITIONS_PATH,
                  newPluginDefinition);
         Assert.assertNotNull(newPluginDefinition);
         Assert.assertNotNull(newPluginDefinition.getId());
         String pluginDefinitionId = newPluginDefinition.getId();
         crudTests.exists(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.PLUGIN_DEFINITIONS_PATH, pluginDefinitionId);
         GenericType<PaginatedListWrapper<PluginDefinition>> genericType = new GenericType<PaginatedListWrapper<PluginDefinition>>()
         {
         };
         List<PluginDefinition> pluginDefinitionList = crudTests.wrappedList(
                  TARGET_HOST, AppConstants.API_PATH
                           + AppConstants.PLUGIN_DEFINITIONS_PATH, null,
                  genericType);
         if (pluginDefinitionList == null)
         {
            Assert.fail("empty list");
            return;
         }
         boolean found = false;
         for (PluginDefinition pluginDefinition : pluginDefinitionList)
         {
            if (pluginDefinition.getId().equals(pluginDefinitionId))
            {
               found = true;
               break;
            }
         }
         if (!found)
         {
            Assert.fail("not in list");
         }
         PluginDefinition pluginDefinition = crudTests.get(TARGET_HOST,
                  AppConstants.API_PATH
                           + AppConstants.PLUGIN_DEFINITIONS_PATH,
                  pluginDefinitionId, PluginDefinition.class);
         Assert.assertNotNull(pluginDefinition);
         Assert.assertEquals(pluginDefinitionId, pluginDefinition.getId());

         String newName = "Nuovo Nome";
         pluginDefinition.setName(newName);
         crudTests.update(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.PLUGIN_DEFINITIONS_PATH, pluginDefinition);

         pluginDefinition = crudTests.get(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.PLUGIN_DEFINITIONS_PATH, pluginDefinitionId,
                  PluginDefinition.class);
         Assert.assertEquals(newName, pluginDefinition.getName());

         crudTests.delete(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.PLUGIN_DEFINITIONS_PATH, pluginDefinitionId);

         crudTests.notExists(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.PLUGIN_DEFINITIONS_PATH, pluginDefinitionId);

      }
      catch (Exception ex)
      {
         System.err.println(ex);
         Assert.fail(ex.getClass().getCanonicalName());
      }
   }

   @Test
   public void pluginConfiguration()
   {
      try
      {
         CrudTests crudTests = new CrudTests();

         PluginDefinition newPluginDefinition = crudTests.create(
                  TARGET_HOST, AppConstants.API_PATH
                           + AppConstants.PLUGIN_DEFINITIONS_PATH,
                  TestUtils.newPluginDefinitionEmail());

         PluginConfiguration newPluginConfiguration = TestUtils
                  .newPluginConfigurationEmail(newPluginDefinition.getId());
         newPluginConfiguration = crudTests.create(TARGET_HOST,
                  AppConstants.API_PATH
                           + AppConstants.PLUGIN_CONFIGURATIONS_PATH,
                  newPluginConfiguration);
         Assert.assertNotNull(newPluginConfiguration);
         Assert.assertNotNull(newPluginConfiguration.getId());
         String pluginConfigurationId = newPluginConfiguration.getId();
         crudTests.exists(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.PLUGIN_CONFIGURATIONS_PATH,
                  pluginConfigurationId);
         GenericType<PaginatedListWrapper<PluginConfiguration>> genericType = new GenericType<PaginatedListWrapper<PluginConfiguration>>()
         {
         };
         List<PluginConfiguration> pluginConfigurationList = crudTests.wrappedList(
                  TARGET_HOST, AppConstants.API_PATH
                           + AppConstants.PLUGIN_CONFIGURATIONS_PATH, null,
                  genericType);
         if (pluginConfigurationList == null)
         {
            Assert.fail("empty list");
            return;
         }
         boolean found = false;
         for (PluginConfiguration pluginConfiguration : pluginConfigurationList)
         {
            if (pluginConfiguration.getId().equals(pluginConfigurationId))
            {
               found = true;
               break;
            }
         }
         if (!found)
         {
            Assert.fail("not in list");
         }
         PluginConfiguration pluginConfiguration = crudTests.get(
                  TARGET_HOST, AppConstants.API_PATH
                           + AppConstants.PLUGIN_CONFIGURATIONS_PATH,
                  pluginConfigurationId, PluginConfiguration.class);
         Assert.assertNotNull(pluginConfiguration);
         Assert.assertEquals(pluginConfigurationId,
                  pluginConfiguration.getId());

         String newName = "Nuovo Nome";
         pluginConfiguration.setName(newName);
         crudTests.update(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.PLUGIN_CONFIGURATIONS_PATH,
                  pluginConfiguration);

         pluginConfiguration = crudTests.get(TARGET_HOST,
                  AppConstants.API_PATH
                           + AppConstants.PLUGIN_CONFIGURATIONS_PATH,
                  pluginConfigurationId, PluginConfiguration.class);
         Assert.assertEquals(newName, pluginConfiguration.getName());

         crudTests.delete(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.PLUGIN_CONFIGURATIONS_PATH,
                  pluginConfigurationId);

         crudTests.notExists(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.PLUGIN_CONFIGURATIONS_PATH,
                  pluginConfigurationId);

         crudTests.delete(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.PLUGIN_DEFINITIONS_PATH,
                  newPluginDefinition.getId());

         crudTests.notExists(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.PLUGIN_DEFINITIONS_PATH,
                  newPluginDefinition.getId());

      }
      catch (Exception ex)
      {
         System.err.println(ex);
         Assert.fail(ex.getClass().getCanonicalName());
      }
   }

   @Test
   public void activation()
   {
      try
      {
         CrudTests crudTests = new CrudTests();

         Account newAccount = crudTests.create(TARGET_HOST,
                  AppConstants.API_PATH + AppConstants.ACCOUNTS_PATH,
                  TestUtils.newAccount());

         PluginDefinition newPluginDefinition = crudTests.create(
                  TARGET_HOST, AppConstants.API_PATH
                           + AppConstants.PLUGIN_DEFINITIONS_PATH,
                  TestUtils.newPluginDefinitionEmail());

         PluginConfiguration newPluginConfiguration = crudTests.create(
                  TARGET_HOST, AppConstants.API_PATH
                           + AppConstants.PLUGIN_CONFIGURATIONS_PATH,
                  TestUtils.newPluginConfigurationEmail(newPluginDefinition
                           .getId()));

         Activation newActivation = TestUtils.newActivation(
                  newAccount.getId(), newPluginConfiguration.getId(),
                  ChannelType.EMAIL, true);

         newActivation = crudTests.create(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.ACTIVATIONS_PATH, newActivation);
         Assert.assertNotNull(newActivation);
         Assert.assertNotNull(newActivation.getId());
         String activationId = newActivation.getId();
         crudTests.exists(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.ACTIVATIONS_PATH, activationId);
         GenericType<PaginatedListWrapper<Activation>> genericType = new GenericType<PaginatedListWrapper<Activation>>()
         {
         };
         List<Activation> activationList = crudTests.wrappedList(TARGET_HOST,
                  AppConstants.API_PATH + AppConstants.ACTIVATIONS_PATH,
                  null, genericType);
         if (activationList == null)
         {
            Assert.fail("empty list");
            return;
         }
         boolean found = false;
         for (Activation activation : activationList)
         {
            if (activation.getId().equals(activationId))
            {
               found = true;
               break;
            }
         }
         if (!found)
         {
            Assert.fail("not in list");
         }
         Activation activation = crudTests.get(TARGET_HOST,
                  AppConstants.API_PATH + AppConstants.ACTIVATIONS_PATH,
                  activationId, Activation.class);
         Assert.assertNotNull(activation);
         Assert.assertEquals(activationId, activation.getId());

         Date newEnd = new Date();
         activation.setEnd(newEnd);
         crudTests.update(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.ACTIVATIONS_PATH, activation);

         activation = crudTests.get(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.ACTIVATIONS_PATH, activationId,
                  Activation.class);
         Assert.assertNotNull(activation.getEnd());

         crudTests.delete(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.ACTIVATIONS_PATH, activationId);

         crudTests.notExists(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.ACTIVATIONS_PATH, activationId);

         crudTests.delete(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.PLUGIN_CONFIGURATIONS_PATH,
                  newPluginConfiguration.getId());

         crudTests.notExists(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.PLUGIN_CONFIGURATIONS_PATH,
                  newPluginConfiguration.getId());

         crudTests.delete(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.PLUGIN_DEFINITIONS_PATH,
                  newPluginDefinition.getId());

         crudTests.notExists(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.PLUGIN_DEFINITIONS_PATH,
                  newPluginDefinition.getId());

         crudTests.delete(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.ACCOUNTS_PATH, newAccount.getId());

         crudTests.notExists(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.ACCOUNTS_PATH, newAccount.getId());

      }
      catch (Exception ex)
      {
         System.err.println(ex);
         Assert.fail(ex.getClass().getCanonicalName());
      }
   }

   @Test
   public void accounting()
   {
      try
      {
         CrudTests crudTests = new CrudTests();

         Account newAccount = crudTests.create(TARGET_HOST,
                  AppConstants.API_PATH + AppConstants.ACCOUNTS_PATH,
                  TestUtils.newAccount());

         PluginDefinition newPluginDefinition = crudTests.create(
                  TARGET_HOST, AppConstants.API_PATH
                           + AppConstants.PLUGIN_DEFINITIONS_PATH,
                  TestUtils.newPluginDefinitionEmail());

         PluginConfiguration newPluginConfiguration = crudTests.create(
                  TARGET_HOST, AppConstants.API_PATH
                           + AppConstants.PLUGIN_CONFIGURATIONS_PATH,
                  TestUtils.newPluginConfigurationEmail(newPluginDefinition
                           .getId()));

         Activation newActivation = crudTests.create(TARGET_HOST,
                  AppConstants.API_PATH + AppConstants.ACTIVATIONS_PATH,
                  TestUtils.newActivation(newAccount.getId(),
                           newPluginConfiguration.getId(), ChannelType.EMAIL,
                           true));

         AccountingTest accountingTest = new AccountingTest();
         String pluginConfigurationId = accountingTest.getPluginConfigurationId(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.ACCOUNTING_PATH, newAccount.getId(),
                  newPluginConfiguration.getChannelType().name());

         Assert.assertEquals(pluginConfigurationId, newPluginConfiguration.getId());

         String checkedPluginConfigurationId = accountingTest.checkPluginConfigurationId(TARGET_HOST,
                  AppConstants.API_PATH
                           + AppConstants.ACCOUNTING_PATH, newAccount.getId(),
                  newPluginConfiguration.getChannelType().name(), pluginConfigurationId);

         Assert.assertEquals(pluginConfigurationId, checkedPluginConfigurationId);

         Content content = accountingTest.getPluginConfigurationContent(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.ACCOUNTING_PATH, newAccount.getId(), newPluginConfiguration.getChannelType().name());
         Assert.assertNotNull(content);

         crudTests.delete(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.ACTIVATIONS_PATH, newActivation.getId());

         crudTests.notExists(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.ACTIVATIONS_PATH, newActivation.getId());

         crudTests.delete(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.PLUGIN_CONFIGURATIONS_PATH,
                  newPluginConfiguration.getId());

         crudTests.notExists(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.PLUGIN_CONFIGURATIONS_PATH,
                  newPluginConfiguration.getId());

         crudTests.delete(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.PLUGIN_DEFINITIONS_PATH,
                  newPluginDefinition.getId());

         crudTests.notExists(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.PLUGIN_DEFINITIONS_PATH,
                  newPluginDefinition.getId());

         crudTests.delete(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.ACCOUNTS_PATH, newAccount.getId());

         crudTests.notExists(TARGET_HOST, AppConstants.API_PATH
                  + AppConstants.ACCOUNTS_PATH, newAccount.getId());
      }
      catch (Exception ex)
      {
         System.err.println(ex);
         Assert.fail(ex.getClass().getCanonicalName());
      }
   }

   @Test
   public void router()
   {
      String desiredTimeZone = "Europe/Rome";
      DateFormat desiredDateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
      desiredDateFormatter.setTimeZone(TimeZone.getTimeZone(desiredTimeZone));
      Calendar desiredCalendar = Calendar.getInstance();
      desiredCalendar.add(Calendar.SECOND, 10);
      Content c = new Content();
      c.put(WHEN, desiredDateFormatter.format(desiredCalendar.getTime()))
               .put(SERVICE_URL,
                        "http://localhost:8080/api/v1/webhook")
               .put(WEBHOOK_URL, "http://localhost:8080/hook")
               .put(CHANNEL_TYPE, ChannelType.WEBHOOK.name());
      String result = new RouterTestClient().route(TARGET_HOST, AppConstants.API_PATH + AppConstants.ROUTER_PATH, c);
      Assert.assertNotNull(result);
   }

   @SuppressWarnings("unchecked")
   @Test
   public void templates()
   {
      try
      {
         CrudTests crudTests = new CrudTests();

         Account newAccount = crudTests.create(TARGET_HOST,
                  AppConstants.API_PATH + AppConstants.ACCOUNTS_PATH,
                  TestUtils.newAccount());

         PluginDefinition newPluginDefinition = crudTests.create(
                  TARGET_HOST, AppConstants.API_PATH
                           + AppConstants.PLUGIN_DEFINITIONS_PATH,
                  TestUtils.newPluginDefinitionWebHook());

         PluginConfiguration newPluginConfiguration = crudTests.create(
                  TARGET_HOST, AppConstants.API_PATH
                           + AppConstants.PLUGIN_CONFIGURATIONS_PATH,
                  TestUtils.newPluginConfigurationWebhookNew(newPluginDefinition
                           .getId()));

         @SuppressWarnings("unused")
         Activation newActivation = crudTests.create(TARGET_HOST,
                  AppConstants.API_PATH + AppConstants.ACTIVATIONS_PATH,
                  TestUtils.newActivation(newAccount.getId(),
                           newPluginConfiguration.getId(), ChannelType.WEBHOOK,
                           true));

         @SuppressWarnings("unused")
         ProcessTemplate newProcessTemplate = crudTests.create(TARGET_HOST, AppConstants.API_PATH
                  + es.getdat.templates.management.AppConstants.TEMPLATES_PATH,
                  TestUtils.newProcessTemplate(newPluginDefinition.getId()));

         ProcessTemplateSearch pts = null;
         PaginatedListWrapper<ProcessTemplate> plw = null;

         // test no allowed channel and no plugin specified
         pts = new ProcessTemplateSearch();
         pts.getAllowedChannelTypes().add(ChannelType.EMAIL.name());
         plw = CrudTests
                  .getTarget(
                           TARGET_HOST,
                           AppConstants.API_PATH + es.getdat.templates.management.AppConstants.TEMPLATES_PATH
                                    + "/allowed")
                  .request(MediaType.APPLICATION_JSON)
                  .buildPost(Entity.json(pts)).invoke(PaginatedListWrapper.class);
         Assert.assertNotNull("no allowed channel, no plugin specified - non null answer", plw);
         Assert.assertEquals("no allowed channel, no plugin specified - empty list", new Integer(0), plw.getListSize());

         // test no allowed channel but valid plugin specified
         pts = new ProcessTemplateSearch();
         pts.getAllowedChannelTypes().add(ChannelType.EMAIL.name());
         pts.getAllowedPluginDefinitionIds().add(newPluginDefinition.getId());
         plw = CrudTests
                  .getTarget(
                           TARGET_HOST,
                           AppConstants.API_PATH + es.getdat.templates.management.AppConstants.TEMPLATES_PATH
                                    + "/allowed")
                  .request(MediaType.APPLICATION_JSON)
                  .buildPost(Entity.json(pts)).invoke(PaginatedListWrapper.class);
         Assert.assertNotNull("no allowed channel, allowed plugin - non null answer", plw);
         Assert.assertEquals("no allowed channel, allowed plugin - empty list", new Integer(0), plw.getListSize());

         // test no allowed plugin and no channel specified
         pts = new ProcessTemplateSearch();
         pts.getAllowedPluginDefinitionIds().add("xxx");
         plw = CrudTests
                  .getTarget(
                           TARGET_HOST,
                           AppConstants.API_PATH + es.getdat.templates.management.AppConstants.TEMPLATES_PATH
                                    + "/allowed")
                  .request(MediaType.APPLICATION_JSON)
                  .buildPost(Entity.json(pts)).invoke(PaginatedListWrapper.class);
         Assert.assertNotNull("no allowed plugin, no channel specified - non null answer", plw);
         Assert.assertEquals("no allowed plugin, no channel specified - empty list", new Integer(0), plw.getListSize());

         // test no allowed plugin but valid channel specified
         pts = new ProcessTemplateSearch();
         pts.getAllowedChannelTypes().add(ChannelType.WEBHOOK.name());
         pts.getAllowedPluginDefinitionIds().add("xxx");
         plw = CrudTests
                  .getTarget(
                           TARGET_HOST,
                           AppConstants.API_PATH + es.getdat.templates.management.AppConstants.TEMPLATES_PATH
                                    + "/allowed")
                  .request(MediaType.APPLICATION_JSON)
                  .buildPost(Entity.json(pts)).invoke(PaginatedListWrapper.class);
         Assert.assertNotNull("no allowed plugin, allowed channel - non null answer", plw);
         Assert.assertEquals("no allowed plugin, allowed channel - empty list", new Integer(0), plw.getListSize());

         // test allowed plugin, no channel specificied
         pts = new ProcessTemplateSearch();
         pts.getAllowedPluginDefinitionIds().add(newPluginDefinition.getId());
         plw = CrudTests
                  .getTarget(
                           TARGET_HOST,
                           AppConstants.API_PATH + es.getdat.templates.management.AppConstants.TEMPLATES_PATH
                                    + "/allowed")
                  .request(MediaType.APPLICATION_JSON)
                  .buildPost(Entity.json(pts)).invoke(PaginatedListWrapper.class);
         Assert.assertEquals("allowed plugin, no channel specified", new Integer(1), plw.getListSize());

         // test allowed channel, no plugin specificied
         pts = new ProcessTemplateSearch();
         pts.getAllowedChannelTypes().add(ChannelType.WEBHOOK.name());
         plw = CrudTests
                  .getTarget(
                           TARGET_HOST,
                           AppConstants.API_PATH + es.getdat.templates.management.AppConstants.TEMPLATES_PATH
                                    + "/allowed")
                  .request(MediaType.APPLICATION_JSON)
                  .buildPost(Entity.json(pts)).invoke(PaginatedListWrapper.class);
         Assert.assertEquals("allowed channel, no plugin specified", new Integer(1), plw.getListSize());

         // test allowed channel, allowed plugin
         pts = new ProcessTemplateSearch();
         pts.getAllowedPluginDefinitionIds().add(newPluginDefinition.getId());
         pts.getAllowedChannelTypes().add(ChannelType.WEBHOOK.name());
         plw = CrudTests
                  .getTarget(
                           TARGET_HOST,
                           AppConstants.API_PATH + es.getdat.templates.management.AppConstants.TEMPLATES_PATH
                                    + "/allowed")
                  .request(MediaType.APPLICATION_JSON)
                  .buildPost(Entity.json(pts)).invoke(PaginatedListWrapper.class);
         Assert.assertEquals("allowed channel, allowed plugin", new Integer(1), plw.getListSize());

         // test allowed channels, allowed plugins
         pts = new ProcessTemplateSearch();
         pts.getAllowedPluginDefinitionIds().add(newPluginDefinition.getId());
         pts.getAllowedPluginDefinitionIds().add("xxx");
         pts.getAllowedChannelTypes().add(ChannelType.WEBHOOK.name());
         pts.getAllowedChannelTypes().add(ChannelType.EMAIL.name());
         plw = CrudTests
                  .getTarget(
                           TARGET_HOST,
                           AppConstants.API_PATH + es.getdat.templates.management.AppConstants.TEMPLATES_PATH
                                    + "/allowed")
                  .request(MediaType.APPLICATION_JSON)
                  .buildPost(Entity.json(pts)).invoke(PaginatedListWrapper.class);
         Assert.assertEquals("allowed channel, allowed plugin", new Integer(1), plw.getListSize());

         // from process template to process by substituting all referred stepIds
      }
      catch (Exception ex)
      {
         System.err.println(ex);
         ex.printStackTrace();
         Assert.fail(ex.getClass().getCanonicalName());
      }
   }
}
