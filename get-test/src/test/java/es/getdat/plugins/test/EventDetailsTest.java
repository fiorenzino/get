package es.getdat.plugins.test;

import javax.ws.rs.client.WebTarget;

import org.junit.Test;

import es.getdat.all.test.CrudTests;
import es.getdat.api.model.Content;
import es.getdat.plivo.management.AppConstants;
import es.getdat.util.CallUtils;

public class EventDetailsTest
{

   @Test
   public void contentTest() throws Exception
   {
      WebTarget target = CrudTests
               .getTarget(TestUtils.TARGET_HOST, AppConstants.API_PATH + AppConstants.EVENTDETAILS_PATH
                        + "/events/{idEvent}/steps/{idStep}/content");
//      idEvent: 4ac695db-28c1-462c-b352-ec71408a9560
      // idStep: f9894d93-dfce-4c7a-9a87-f6a7c57226c4
      String eventId = "4ac695db-28c1-462c-b352-ec71408a9560";

      String stepId = "f9894d93-dfce-4c7a-9a87-f6a7c57226c4";

      Content content = CallUtils.content(target, eventId, stepId);

      System.out.println(content);
   }
}
