package es.getdat.plugins.test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.logging.Logger;
import org.junit.Assert;
import org.junit.Test;

import es.getdat.api.model.Content;
import es.getdat.api.model.enums.ChannelType;
import es.getdat.core.management.AppConstants;

public class AccountingTest
{

   static Logger logger = Logger.getLogger(AccountingTest.class);

   static String TARGET_HOST = "http://localhost:8080";

   @Test
   public void getPluginConfigurationId()
   {
      getPluginConfigurationId(TARGET_HOST, AppConstants.API_PATH + AppConstants.ACCOUNTING_PATH, "01d68957-c500-4ee6-b21c-98329e180a12",
               ChannelType.EMAIL.name());
   }

   @Test
   public void checkPluginConfigurationId()
   {
      checkPluginConfigurationId(TARGET_HOST, AppConstants.API_PATH + AppConstants.ACCOUNTING_PATH, "01d68957-c500-4ee6-b21c-98329e180a12",
               ChannelType.EMAIL.name(), "....");
   }

   public String getPluginConfigurationId(String targetHost, String targetPath, String accountId,
            String channelType)
   {
      try
      {

         Client client = ClientBuilder.newClient();
         WebTarget target = client.target(targetHost+targetPath);

         Response res = target
                  .path("/plugins/{accountId}/pluginconfigurations")
                  .resolveTemplate("accountId", accountId)
                  .queryParam("channeltype", channelType)
                  .request().get();
         System.out.println("code: " + res.getStatus() + " - status: "
                  + Status.fromStatusCode(res.getStatus()) + " - family: "
                  + Status.Family.familyOf(res.getStatus()));
         if (res.getStatus() == Status.OK.getStatusCode())
         {
            String pluginConfigurationId = res.readEntity(String.class);
            System.out.println(pluginConfigurationId);
            Assert.assertNotNull(pluginConfigurationId);
            return pluginConfigurationId;
         }
         else
         {
            Assert.fail("wrong status");
            return null;
         }
      }
      catch (Exception e)
      {
         e.printStackTrace();
         logger.error(e.getMessage());

         return null;
      }
   }

   public String checkPluginConfigurationId(String targetHost, String targetPath, String accountId,
            String channelType, String pluginConfigurationId)
   {
      try
      {

         Client client = ClientBuilder.newClient();
         WebTarget target = client.target(targetHost+targetPath);

         Response res = target
                  .path("/plugins/{accountId}/pluginconfigurations/{pluginConfigurationId}")
                  .resolveTemplate("accountId", accountId)
                  .resolveTemplate("pluginConfigurationId", pluginConfigurationId)
                  .queryParam("channeltype", channelType)
                  .request().get();
         System.out.println("code: " + res.getStatus() + " - status: "
                  + Status.fromStatusCode(res.getStatus()) + " - family: "
                  + Status.Family.familyOf(res.getStatus()));
         if (res.getStatus() == Status.OK.getStatusCode())
         {
            String resultPluginConfigurationId = res.readEntity(String.class);
            System.out.println(resultPluginConfigurationId);
            Assert.assertNotNull(resultPluginConfigurationId);
            Assert.assertEquals(resultPluginConfigurationId, pluginConfigurationId);
            return resultPluginConfigurationId;
         }
         else
         {
            Assert.fail("wrong status");
            return null;
         }
      }
      catch (Exception e)
      {
         e.printStackTrace();
         logger.error(e.getMessage());

         return null;
      }
   }

   @Test
   public void getConcurrentPluginConfigurationContentTest()
   {
      for (int i = 0; i < 50; i++)
      {
         final int index = i;
         new Thread()
         {
            @Override
            public void run()
            {
               System.out.println("Running #" + index);
               getPluginConfigurationContent();
               System.out.println("#" + index + " done.");
            }
         }.start();

         // try
         // {
         // Thread.sleep(300);
         // }
         // catch (Exception e)
         // {
         //
         // }
      }
      try
      {
         Thread.sleep(50000);
      }
      catch (Exception e)
      {

      }
   }

   @Test
   public void getPluginConfigurationContent()
   {
      getPluginConfigurationContent(TARGET_HOST, AppConstants.API_PATH + AppConstants.ACCOUNTING_PATH, "01d68957-c500-4ee6-b21c-98329e180a12",
               ChannelType.EMAIL.name());
   }

   public Content getPluginConfigurationContent(String targetHost, String targetPath, String accountId,
            String channelType)
   {
      try
      {

         Client client = ClientBuilder.newClient();
         WebTarget target = client.target(targetHost + targetPath);

         Response res = target
                  .path("/plugins/{accountId}/pluginconfigurations/content")
                  .resolveTemplate("accountId",
                           "01d68957-c500-4ee6-b21c-98329e180a12")
                  .queryParam("channelType", ChannelType.EMAIL.name())
                  .request().get();
         System.out.println("code: " + res.getStatus() + " - status: "
                  + Status.fromStatusCode(res.getStatus()) + " - family: "
                  + Status.Family.familyOf(res.getStatus()));
         if (res.getStatus() == Status.OK.getStatusCode())
         {
            Content content = res.readEntity(Content.class);
            System.out.println(content);
            Assert.assertNotNull(content);
            return content;
         }
         else
         {
            Assert.fail("wrong status");
            return null;
         }
      }
      catch (Exception e)
      {
         e.printStackTrace();
         logger.error(e.getMessage());
         return null;
      }
   }
}
