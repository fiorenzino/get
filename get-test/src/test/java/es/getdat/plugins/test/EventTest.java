package es.getdat.plugins.test;

import javax.ws.rs.client.WebTarget;

import org.junit.Test;

import es.getdat.all.test.CrudTests;

public class EventTest
{

   static final String TARGET_HOST = "https://we.getdat.es";

   @Test
   public void updateEventByServiceNameAndExternalUidTest() throws Exception
   {
      WebTarget target = CrudTests
               .getTarget(TARGET_HOST, "/api/v1/events/services/{serviceName}/externalUid/{externalUid}/executed");
      String serviceName = "PLIVO_BUZZ";
      String externalUid = "a04f3323-1451-485b-9d10-e16e0848d380";
      // updateEventByServiceNameAndExternalUid - externalUid: a04f3323-1451-485b-9d10-e16e0848d380 - serviceName:
      // PLIVO_BUZZ
      // CallUtils.updateEventByServiceNameAndExternalUid(target, externalUid, serviceName);
   }

   @Test
   public void getEventProperties() throws Exception
   {
      WebTarget target = CrudTests
               .getTarget(TARGET_HOST, "/api/v1/events/content");
      String serviceName = "PLIVO_CALL";
      String externalUid = "98a0240b-d026-4bdc-a382-b3802deb843d";
      // updateEventByServiceNameAndExternalUid - externalUid: a04f3323-1451-485b-9d10-e16e0848d380 - serviceName:
      // PLIVO_BUZZ
      // Content content = CallUtils.getEventProperties(externalUid, serviceName, target);
      // System.out.println(content);
   }
}
