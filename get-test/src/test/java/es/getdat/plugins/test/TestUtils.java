package es.getdat.plugins.test;

import static es.getdat.api.management.AppConstants.SERVICE_URL;
import static es.getdat.mail.management.AppConstants.MAIL_ADDRESS;
import static es.getdat.mail.management.AppConstants.MAIL_AUTH;
import static es.getdat.mail.management.AppConstants.MAIL_DEBUG;
import static es.getdat.mail.management.AppConstants.MAIL_PORT;
import static es.getdat.mail.management.AppConstants.MAIL_PWD;
import static es.getdat.mail.management.AppConstants.MAIL_SSL;
import static es.getdat.mail.management.AppConstants.MAIL_TLS;
import static es.getdat.mail.management.AppConstants.MAIL_USER;
import static es.getdat.plivo.management.AppConstants.PLIVO_ACCOUNTNAME;
import static es.getdat.plivo.management.AppConstants.PLIVO_AUTHID;
import static es.getdat.plivo.management.AppConstants.PLIVO_AUTHTOKEN;
import static es.getdat.plivo.management.AppConstants.PLIVO_CALL_ANSWER_URL;
import static es.getdat.plivo.management.AppConstants.PLIVO_NUMBER;
import static es.getdat.plivo.management.AppConstants.PLIVO_SMS_URL;
import static es.getdat.twilio.management.AppConstants.TWILIO_ACCOUNTNAME;
import static es.getdat.twilio.management.AppConstants.TWILIO_CALL_ANSWER_URL;
import static es.getdat.twilio.management.AppConstants.TWILIO_CALL_FALLBACK_URL;
import static es.getdat.twilio.management.AppConstants.TWILIO_CALL_HANGUP_URL;
import static es.getdat.twilio.management.AppConstants.TWILIO_MSG_URL;
import static es.getdat.twilio.management.AppConstants.TWILIO_NUMBER;
import static es.getdat.twilio.management.AppConstants.TWILIO_SID;
import static es.getdat.twilio.management.AppConstants.TWILIO_TOKEN;
import static es.getdat.webhook.management.AppConstants.WEBHOOK_METHOD;
import static es.getdat.webhook.management.AppConstants.WEBHOOK_MSG;
import static es.getdat.webhook.management.AppConstants.WEBHOOK_URL;

import java.util.Date;
import java.util.UUID;

import es.getdat.accounting.model.Account;
import es.getdat.accounting.model.Activation;
import es.getdat.api.model.enums.ChannelType;
import es.getdat.plivo.management.AppConstants;
import es.getdat.plugin.model.PluginConfiguration;
import es.getdat.plugin.model.PluginDefinition;
import es.getdat.templates.model.ProcessTemplate;
import es.getdat.templates.model.StepTemplate;

public class TestUtils
{

//   public static final String TARGET_HOST = "https://we.getdat.es";
   public static final String TARGET_HOST = "http://localhost:8080";

   // public static final String TARGET_HOST = "https://forgetters-dates.rhcloud.com";

   public static Account newAccount()
   {
      Account newAccount = new Account();
      newAccount.setFirstName("" + System.currentTimeMillis());
      newAccount.setUsername("" + System.currentTimeMillis());
      newAccount.setPassword(UUID.randomUUID().toString());
      return newAccount;
   }

   public static PluginDefinition newPluginDefinitionEmail()
   {
      PluginDefinition newPluginDefinition = new PluginDefinition();
      newPluginDefinition.setName("" + System.currentTimeMillis());
      newPluginDefinition.setChannelType(ChannelType.EMAIL);
      newPluginDefinition.add(SERVICE_URL).add(MAIL_ADDRESS).add(MAIL_USER)
               .add(MAIL_PWD).add(MAIL_AUTH).add(MAIL_TLS).add(MAIL_SSL)
               .add(MAIL_PORT).add(MAIL_DEBUG);
      return newPluginDefinition;
   }

   public static PluginDefinition newPluginDefinitionMailGun()
   {
      PluginDefinition newPluginDefinition = new PluginDefinition();
      newPluginDefinition.setName("" + System.currentTimeMillis());
      newPluginDefinition.setChannelType(ChannelType.EMAIL);
      newPluginDefinition.add(SERVICE_URL).add(MAIL_USER).add(MAIL_ADDRESS)
               .add(MAIL_PWD);
      return newPluginDefinition;
   }

   public static PluginDefinition newPluginDefinitionWebHook()
   {
      PluginDefinition newPluginDefinition = new PluginDefinition();
      newPluginDefinition.setName("" + System.currentTimeMillis());
      newPluginDefinition.setChannelType(ChannelType.WEBHOOK);
      newPluginDefinition.add(SERVICE_URL).add(WEBHOOK_METHOD)
               .add(WEBHOOK_MSG).add(WEBHOOK_URL);
      return newPluginDefinition;
   }

   public static PluginDefinition newPluginDefinitionPlivoBuzz()
   {
      PluginDefinition newPluginDefinition = new PluginDefinition();
      newPluginDefinition.setName("" + System.currentTimeMillis());
      newPluginDefinition.setChannelType(ChannelType.BUZZ);
      newPluginDefinition.add(SERVICE_URL).add(PLIVO_ACCOUNTNAME)
               .add(PLIVO_AUTHID).add(PLIVO_AUTHTOKEN).add(PLIVO_NUMBER)
               .add(PLIVO_CALL_ANSWER_URL);
      return newPluginDefinition;
   }

   public static PluginDefinition newPluginDefinitionPlivoSms()
   {
      PluginDefinition newPluginDefinition = new PluginDefinition();
      newPluginDefinition.setName("" + System.currentTimeMillis());
      newPluginDefinition.setChannelType(ChannelType.SMS);
      newPluginDefinition.add(SERVICE_URL).add(PLIVO_ACCOUNTNAME)
               .add(PLIVO_AUTHID).add(PLIVO_AUTHTOKEN).add(PLIVO_NUMBER)
               .add(PLIVO_SMS_URL);
      return newPluginDefinition;
   }

   public static PluginDefinition newPluginDefinitionPlivoCall()
   {
      PluginDefinition newPluginDefinition = new PluginDefinition();
      newPluginDefinition.setName("" + System.currentTimeMillis());
      newPluginDefinition.setChannelType(ChannelType.CALL);
      newPluginDefinition.add(SERVICE_URL).add(PLIVO_ACCOUNTNAME)
               .add(PLIVO_AUTHID).add(PLIVO_AUTHTOKEN).add(PLIVO_NUMBER)
               .add(AppConstants.PLIVO_CALL_ANSWER_URL)
               .add(AppConstants.PLIVO_CALL_FALLBACK_URL)
               .add(AppConstants.PLIVO_CALL_HANGUP_URL);
      return newPluginDefinition;
   }

   public static PluginDefinition newPluginDefinitionTwilioBuzz()
   {
      PluginDefinition newPluginDefinition = new PluginDefinition();
      newPluginDefinition.setName("" + System.currentTimeMillis());
      newPluginDefinition.setChannelType(ChannelType.BUZZ);
      newPluginDefinition.add(SERVICE_URL).add(TWILIO_ACCOUNTNAME)
               .add(TWILIO_NUMBER).add(TWILIO_SID).add(TWILIO_NUMBER)
               .add(TWILIO_TOKEN).add(TWILIO_CALL_ANSWER_URL)
               .add(TWILIO_CALL_FALLBACK_URL).add(TWILIO_CALL_HANGUP_URL);
      return newPluginDefinition;
   }

   public static PluginDefinition newPluginDefinitionTwilioSms()
   {
      PluginDefinition newPluginDefinition = new PluginDefinition();
      newPluginDefinition.setName("" + System.currentTimeMillis());
      newPluginDefinition.setChannelType(ChannelType.SMS);
      newPluginDefinition.add(SERVICE_URL).add(TWILIO_ACCOUNTNAME)
               .add(TWILIO_SID).add(TWILIO_TOKEN).add(TWILIO_NUMBER)
               .add(TWILIO_MSG_URL);
      return newPluginDefinition;
   }

   public static PluginDefinition newPluginDefinitionTwilioCall()
   {
      PluginDefinition newPluginDefinition = new PluginDefinition();
      newPluginDefinition.setName("" + System.currentTimeMillis());
      newPluginDefinition.setChannelType(ChannelType.CALL);
      newPluginDefinition.add(SERVICE_URL).add(TWILIO_ACCOUNTNAME)
               .add(TWILIO_SID).add(TWILIO_TOKEN).add(TWILIO_NUMBER)
               .add(TWILIO_CALL_ANSWER_URL).add(TWILIO_CALL_FALLBACK_URL)
               .add(TWILIO_CALL_HANGUP_URL);
      return newPluginDefinition;
   }

   public static PluginConfiguration newPluginConfigurationEmail(
            String pluginDefinitionId)
   {
      PluginConfiguration newPluginConfiguration = new PluginConfiguration();
      newPluginConfiguration.setPluginDefinitionId(pluginDefinitionId);
      newPluginConfiguration.setChannelType(ChannelType.EMAIL);
      newPluginConfiguration.setName("" + System.currentTimeMillis());
      newPluginConfiguration.setSystemDefault(true);
      newPluginConfiguration.put(SERVICE_URL, TARGET_HOST + "/api/v1/mail")
               .put(MAIL_ADDRESS, "smtp.gmail.com")
               .put(MAIL_USER, "test.getdat.es@gmail.com")
               .put(MAIL_PWD, "g3sucr1st0").put(MAIL_AUTH, "true")
               .put(MAIL_TLS, "true").put(MAIL_SSL, "false")
               .put(MAIL_PORT, "587").put(MAIL_DEBUG, "false");
      return newPluginConfiguration;
   }

   public static PluginConfiguration newPluginConfigurationMailGun(
            String pluginDefinitionId)
   {
      PluginConfiguration newPluginConfiguration = new PluginConfiguration();
      newPluginConfiguration.setPluginDefinitionId(pluginDefinitionId);
      newPluginConfiguration.setChannelType(ChannelType.EMAIL);
      newPluginConfiguration.setName("" + System.currentTimeMillis());
      newPluginConfiguration.setSystemDefault(true);
      newPluginConfiguration.put(SERVICE_URL, TARGET_HOST + "/api/v1/mailgun")
               .put(MAIL_ADDRESS, "https://api.mailgun.net/v2/we.getdat.es/messages")
               .put(MAIL_USER, "api")
               .put(MAIL_PWD, "key-10d888b2898bad513ed4ed28352b6375");

      return newPluginConfiguration;
   }

   public static PluginConfiguration newPluginConfigurationWebhook(
            String pluginDefinitionId)
   {
      PluginConfiguration newPluginConfiguration = new PluginConfiguration();
      newPluginConfiguration.setPluginDefinitionId(pluginDefinitionId);
      newPluginConfiguration.setChannelType(ChannelType.WEBHOOK);
      newPluginConfiguration.setName("local webhook");
      newPluginConfiguration.setSystemDefault(true);
      newPluginConfiguration
               .put(SERVICE_URL, TARGET_HOST + "/api/v1/webhook/legacy");
      return newPluginConfiguration;
   }

   public static PluginConfiguration newPluginConfigurationWebhookNew(
            String pluginDefinitionId)
   {
      PluginConfiguration newPluginConfiguration = new PluginConfiguration();
      newPluginConfiguration.setPluginDefinitionId(pluginDefinitionId);
      newPluginConfiguration.setChannelType(ChannelType.WEBHOOK);
      newPluginConfiguration.setName("local webhook");
      newPluginConfiguration.setSystemDefault(true);
      newPluginConfiguration
               .put(SERVICE_URL, TARGET_HOST + "/api/v1/webhook");
      return newPluginConfiguration;
   }

   public static PluginConfiguration newPluginConfigurationPlivoBuzz(
            String pluginDefinitionId)
   {
      PluginConfiguration newPluginConfiguration = new PluginConfiguration();
      newPluginConfiguration.setPluginDefinitionId(pluginDefinitionId);
      newPluginConfiguration.setChannelType(ChannelType.BUZZ);
      newPluginConfiguration.setName("plivo buzz");
      newPluginConfiguration.setSystemDefault(true);
      newPluginConfiguration
               .put(SERVICE_URL, TARGET_HOST + "/api/v1/plivo/buzz")
               .put(AppConstants.PLIVO_NUMBER, "3905221520065")
               .put(AppConstants.PLIVO_AUTHID, "MAOWE0YJUZYMQZNDBLNW")
               .put(AppConstants.PLIVO_AUTHTOKEN,
                        "YmUyNTU1YjhmZWQ1NmQxNGVjODkwMzBhNWUzOGYw")
               .put(AppConstants.PLIVO_ACCOUNTNAME, "landing_page")
               .put(AppConstants.PLIVO_CALL_ANSWER_URL,
                        TARGET_HOST + "/api/v1/plivo/buzz/answer/")
               .put(AppConstants.PLIVO_CALL_FALLBACK_URL,
                        TARGET_HOST + "/api/v1/plivo/buzz/fallback/")
               .put(AppConstants.PLIVO_CALL_HANGUP_URL,
                        TARGET_HOST + "/api/v1/plivo/buzz/hangup/");
      return newPluginConfiguration;
   }

   public static PluginConfiguration newPluginConfigurationPlivoSms(
            String pluginDefinitionId)
   {
      PluginConfiguration newPluginConfiguration = new PluginConfiguration();
      newPluginConfiguration.setPluginDefinitionId(pluginDefinitionId);
      newPluginConfiguration.setChannelType(ChannelType.SMS);
      newPluginConfiguration.setName("plivo sms");
      newPluginConfiguration.setSystemDefault(true);
      newPluginConfiguration
               .put(SERVICE_URL, TARGET_HOST + "/api/v1/plivo/sms")
               .put(AppConstants.PLIVO_NUMBER, "3905221520065")
               .put(AppConstants.PLIVO_AUTHID, "MAOWE0YJUZYMQZNDBLNW")
               .put(AppConstants.PLIVO_AUTHTOKEN,
                        "YmUyNTU1YjhmZWQ1NmQxNGVjODkwMzBhNWUzOGYw")
               .put(AppConstants.PLIVO_ACCOUNTNAME, "landing_page")
               .put(AppConstants.PLIVO_SMS_URL,
                        TARGET_HOST + "/api/v1/plivo/sms/url/");
      return newPluginConfiguration;
   }

   public static PluginConfiguration newPluginConfigurationPlivoCall(
            String pluginDefinitionId)
   {
      PluginConfiguration newPluginConfiguration = new PluginConfiguration();
      newPluginConfiguration.setPluginDefinitionId(pluginDefinitionId);
      newPluginConfiguration.setChannelType(ChannelType.CALL);
      newPluginConfiguration.setName("plivo call");
      newPluginConfiguration.setSystemDefault(true);
      newPluginConfiguration
               .put(SERVICE_URL, TARGET_HOST + "/api/v1/plivo/call")
               .put(AppConstants.PLIVO_NUMBER, "3905221520065")
               .put(AppConstants.PLIVO_AUTHID, "MAOWE0YJUZYMQZNDBLNW")
               .put(AppConstants.PLIVO_AUTHTOKEN,
                        "YmUyNTU1YjhmZWQ1NmQxNGVjODkwMzBhNWUzOGYw")
               .put(AppConstants.PLIVO_ACCOUNTNAME, "landing_page")
               .put(AppConstants.PLIVO_CALL_ANSWER_URL,
                        TARGET_HOST + "/api/v1/plivo/call/answer/")
               .put(AppConstants.PLIVO_CALL_FALLBACK_URL,
                        TARGET_HOST + "/api/v1/plivo/call/fallback/")
               .put(AppConstants.PLIVO_CALL_HANGUP_URL,
                        TARGET_HOST + "/api/v1/plivo/call/hangup/");
      return newPluginConfiguration;
   }

   public static PluginConfiguration newPluginConfigurationTwilioBuzz(
            String pluginDefinitionId)
   {
      PluginConfiguration newPluginConfiguration = new PluginConfiguration();
      newPluginConfiguration.setPluginDefinitionId(pluginDefinitionId);
      newPluginConfiguration.setChannelType(ChannelType.BUZZ);
      newPluginConfiguration.setName("twilio buzz");
      newPluginConfiguration.setSystemDefault(true);
      newPluginConfiguration
               .put(SERVICE_URL, TARGET_HOST + "/api/v1/twilio/buzz")
               .put(TWILIO_NUMBER, "3905221520065")
               .put(TWILIO_SID, "MAOWE0YJUZYMQZNDBLNW")
               .put(TWILIO_TOKEN, "YmUyNTU1YjhmZWQ1NmQxNGVjODkwMzBhNWUzOGYw")
               .put(TWILIO_ACCOUNTNAME, "landing_page")
               .put(TWILIO_CALL_ANSWER_URL,
                        TARGET_HOST + "/api/v1/twilio/buzz/answer")
               .put(TWILIO_CALL_FALLBACK_URL,
                        TARGET_HOST + "/api/v1/twilio/buzz/fallback")
               .put(TWILIO_CALL_HANGUP_URL,
                        TARGET_HOST + "/api/v1/twilio/buzz/hangup");
      return newPluginConfiguration;
   }

   public static PluginConfiguration newPluginConfigurationTwilioSms(
            String pluginDefinitionId)
   {
      PluginConfiguration newPluginConfiguration = new PluginConfiguration();
      newPluginConfiguration.setPluginDefinitionId(pluginDefinitionId);
      newPluginConfiguration.setChannelType(ChannelType.SMS);
      newPluginConfiguration.setName("twilio sms");
      newPluginConfiguration.setSystemDefault(true);
      newPluginConfiguration
               .put(SERVICE_URL, TARGET_HOST + "/api/v1/twilio/sms")
               .put(AppConstants.PLIVO_NUMBER, "3905221520065")
               .put(AppConstants.PLIVO_AUTHID, "MAOWE0YJUZYMQZNDBLNW")
               .put(AppConstants.PLIVO_AUTHTOKEN,
                        "YmUyNTU1YjhmZWQ1NmQxNGVjODkwMzBhNWUzOGYw")
               .put(AppConstants.PLIVO_ACCOUNTNAME, "landing_page")
               .put(AppConstants.PLIVO_SMS_URL,
                        TARGET_HOST + "/api/v1/twilio/sms/url");
      return newPluginConfiguration;
   }

   public static PluginConfiguration newPluginConfigurationTwilioCall(
            String pluginDefinitionId)
   {
      PluginConfiguration newPluginConfiguration = new PluginConfiguration();
      newPluginConfiguration.setPluginDefinitionId(pluginDefinitionId);
      newPluginConfiguration.setChannelType(ChannelType.CALL);
      newPluginConfiguration.setName("twilio call");
      newPluginConfiguration.setSystemDefault(true);
      newPluginConfiguration
               .put(SERVICE_URL, TARGET_HOST + "/api/v1/twilio/call")
               .put(AppConstants.PLIVO_NUMBER, "3905221520065")
               .put(AppConstants.PLIVO_AUTHID, "MAOWE0YJUZYMQZNDBLNW")
               .put(AppConstants.PLIVO_AUTHTOKEN,
                        "YmUyNTU1YjhmZWQ1NmQxNGVjODkwMzBhNWUzOGYw")
               .put(AppConstants.PLIVO_ACCOUNTNAME, "landing_page")
               .put(AppConstants.PLIVO_CALL_ANSWER_URL,
                        TARGET_HOST + "/api/v1/twilio/call/answer")
               .put(AppConstants.PLIVO_CALL_FALLBACK_URL,
                        TARGET_HOST + "/api/v1/twilio/call/fallback")
               .put(AppConstants.PLIVO_CALL_HANGUP_URL,
                        TARGET_HOST + "/api/v1/twilio/call/hangup");
      return newPluginConfiguration;
   }

   public static Activation newActivation(String accountId,
            String pluginConfigurationId, ChannelType channelType,
            boolean userDefault)
   {
      Activation newActivation = new Activation();
      newActivation.setAccountId(accountId);
      newActivation.setPluginConfigurationId(pluginConfigurationId);
      // newActivation.setChannelType(ChannelType.EMAIL);
      newActivation.setChannelType(channelType);
      newActivation.setInit(new Date());
      newActivation.setEnd(null);
      // newActivation.setUserDefault(true);
      newActivation.setUserDefault(userDefault);
      return newActivation;
   }

   public static ProcessTemplate newProcessTemplate(String webHookPluginDefinitionId)
   {
      ProcessTemplate newProcessTemplate = new ProcessTemplate();
      newProcessTemplate.setName("" + System.currentTimeMillis());

      StepTemplate step1 = new StepTemplate("step1", UUID.randomUUID().toString(), webHookPluginDefinitionId,
               ChannelType.WEBHOOK.name());
      step1.setOrder(1);

      StepTemplate step2 = new StepTemplate("step2", UUID.randomUUID().toString(),
               webHookPluginDefinitionId,
               ChannelType.WEBHOOK.name());
      step2.setOrder(2);

      StepTemplate step3 = new StepTemplate("step3", UUID.randomUUID().toString(), webHookPluginDefinitionId,
               ChannelType.WEBHOOK.name());
      step3.setOrder(3);

      step2.setCondition(" "
               // proof of concept of how results of step n-1 can become input for step n
               + "     content.getStepValuesIn('" + step2.getStepId() + "').put('" + WEBHOOK_URL
               + "', content.getStepValuesOut('"
               + step1.getStepId()
               + "').get('"
               + AppConstants.OPERATION
               + "') );\n"
               + ""
               // precondition: execute if step1 is routed and step 2 not
               // postcondition: let execution go on if step2 is routed and step 3 not
               // here in OR since we don't have separate condition statements for the two situations
               + "( process.get('" + step1.getStepId() + "').routed && !process.get('" + step2.getStepId()
               + "').routed ) || "
               + "( process.get('" + step2.getStepId() + "').routed && !process.get('" + step3.getStepId()
               + "').routed ) ;");
      step2.addReferencedStepId(step1.getStepId()).addReferencedStepId(step2.getStepId());

      step3.setCondition(""
               // precondition: if step3 has not yet executed, execute step3 only if step1 was executed right
               // postcondition: if step3 has executed right, dont let the execution go on
               + " ( content.getStepValuesOut('" + step3.getStepId() + "').get('" + AppConstants.EXECUTED
               + "') == null ) " +
               "? " +
               "content.getStepValuesOut('" + step1.getStepId() + "').get('" + AppConstants.EXECUTED + "')" +
               ":" +
               "!content.getStepValuesOut('" + step3.getStepId() + "').get('" + AppConstants.EXECUTED + "')");
      step3.addReferencedStepId(step1.getStepId()).addReferencedStepId(step3.getStepId());

      newProcessTemplate.add(step1).add(step2).add(step3);

      return newProcessTemplate;
   }

}
