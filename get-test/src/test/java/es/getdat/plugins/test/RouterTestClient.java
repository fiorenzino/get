package es.getdat.plugins.test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;
import org.junit.Test;

import es.getdat.api.model.Content;
import es.getdat.api.model.enums.ChannelType;
import static es.getdat.webhook.management.AppConstants.*;
import static es.getdat.core.management.AppConstants.*;
import es.getdat.core.management.AppConstants;
import es.getdat.util.CallAutocloseableUtils;

public class RouterTestClient
{

   static Logger logger = Logger.getLogger(RouterTestClient.class);

   static String TARGET_HOST = "http://localhost:8080";

   @Test
   public void route()
   {
      Content c = new Content();
      c.put(WHEN, "2014-08-05T18:57:30")
               .put(SERVICE_URL,
                        "http://localhost:8080/api/v1/webhook")
               .put(WEBHOOK_URL, "http://localhost:8080/hook")
               .put(CHANNEL_TYPE, ChannelType.WEBHOOK.name());
      route(TARGET_HOST, AppConstants.API_PATH + AppConstants.ROUTER_PATH, c);
   }

   @Test
   public void routeClosable()
   {
      Content c = new Content();
      c.put(WHEN, "2014-08-05T18:57:30")
               .put(SERVICE_URL,
                        "http://localhost:8080/api/v1/webhook")
               .put(WEBHOOK_URL, "http://localhost:8080/hook")
               .put(CHANNEL_TYPE, ChannelType.WEBHOOK.name());
      try
      {
         CallAutocloseableUtils.call("http://localhost:8080", AppConstants.API_PATH + AppConstants.ROUTER_PATH, c);
      }
      catch (Exception e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      // route(TARGET_HOST, AppConstants.API_PATH + AppConstants.ROUTER_PATH, c);
   }

   public String route(String targetHost, String targetPath, Content content)
   {
      Client client = ClientBuilder.newClient();
      WebTarget target = client.target(targetHost + targetPath);

      try
      {
         Response response = target.request()
                  .buildPost(Entity.entity(content, MediaType.APPLICATION_JSON))
                  .invoke();
         String value = response.readEntity(String.class);
         logger.info(value);
         response.close();
         return value;
      }
      catch (Exception e)
      {
         e.printStackTrace();
         logger.error(e.getMessage());
         return null;
      }
   }
}
