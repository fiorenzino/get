package es.getdat.files.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataOutput;
import org.junit.Assert;
import org.junit.Test;

import es.getdat.files.util.MultipartUtility;

public class FilesTest {

	static String TARGET_URI = "http://localhost:8080/api/v1/files";

	@Test
	public void vanillaJavaTest() {
		String charset = "UTF-8";

		try {
			MultipartUtility multipart = new MultipartUtility(TARGET_URI + "/"
					+ UUID.randomUUID(), charset);
			multipart.addFilePart("filename", "prova.txt", "prova!".getBytes());

			List<String> response = multipart.finish();

			System.out.println("SERVER REPLIED:");

			for (String line : response) {
				System.out.println(line);
			}
		} catch (IOException ex) {
			System.err.println(ex);
			Assert.fail(ex.getClass().getCanonicalName());
		}
	}

	@Test
	public void restEasyTest() {
		try {

			Client client = ClientBuilder.newClient();
			WebTarget target = client.target(TARGET_URI + "/"
					+ UUID.randomUUID());
			MultipartFormDataOutput mdo = new MultipartFormDataOutput();

			mdo.addFormData("filename", new FileInputStream(new File(
					"src/test/java/es/getdat/files/test/"
							+ getClass().getSimpleName() + ".java")),
					MediaType.APPLICATION_OCTET_STREAM_TYPE, getClass()
							.getSimpleName() + ".java");

			// mdo.addPart(new FileInputStream(new
			// File("src/test/java/es/getdat/files/test/"
			// + getClass().getSimpleName() + ".java")),
			// MediaType.APPLICATION_OCTET_STREAM_TYPE, getClass()
			// .getSimpleName() + ".java");

			GenericEntity<MultipartFormDataOutput> entity = new GenericEntity<MultipartFormDataOutput>(
					mdo) {
			};

			// Upload File
			Response res = target.request().post(
					Entity.entity(entity, MediaType.MULTIPART_FORM_DATA_TYPE));

			System.out.println("code: " + res.getStatus() + " - status: "
					+ Status.fromStatusCode(res.getStatus()) + " - family: "
					+ Status.Family.familyOf(res.getStatus()));
			if (res.getStatus() == Status.OK.getStatusCode()) {
				String pluginConfigurationId = res.readEntity(String.class);
				System.out.println(pluginConfigurationId);
				Assert.assertNotNull(pluginConfigurationId);
			}
		} catch (IOException ex) {
			System.err.println(ex);
			Assert.fail(ex.getClass().getCanonicalName());
		}
	}
}
