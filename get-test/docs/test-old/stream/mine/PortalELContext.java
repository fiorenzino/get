package com.sun.el.stream.mine;

import java.lang.reflect.Method;

import javax.el.ELResolver;
import javax.el.ExpressionFactory;
import javax.el.FunctionMapper;
import javax.el.StandardELContext;
import javax.el.ValueExpression;
import javax.el.VariableMapper;

import es.getdat.api.model.Content;

public class PortalELContext extends StandardELContext
{

   private ELResolver streamResolver;
   static Content content;

   public PortalELContext(ExpressionFactory factory, Content c)
   {
      super(factory);
      streamResolver = factory.getStreamELResolver();
      content = new Content();
      c.setGlobal(c.getGlobal());
   }

   private static class CustomFunctionMapper extends FunctionMapper
   {

      @Override
      public Method resolveFunction(String prefix, String localName)
      {
         // TODO Auto-generated method stub
         return null;
      }

   }

   private static class PistonVariableMapper extends VariableMapper
   {

      @Override
      public ValueExpression resolveVariable(String variable)
      {
         System.out.println(variable);
         Object data = null;
         if (PortalELContext.content.get(variable) != null)
         {
            data = PortalELContext.content.get(variable);
         }
         // check in page context
         // PageContext pageContext = PistonContextUtil.getView()
         // .getPageContext();
         // Object data = pageContext.getAttribute(variable);
         //
         // // check in request scope
         // if (null == data) {
         // data = PistonContextUtil.getRequest().getAttribute(variable);
         // }
         //
         // // check in session scope
         // if (null == data) {
         // if (null != PistonContextUtil.getRequest().getSession(false)) {
         // data = PistonContextUtil.getRequest().getSession(false)
         // .getAttribute(variable);
         // }
         // }
         //
         // // check in application scope
         // if (null == data) {
         // data = PistonContextUtil.getApplication()
         // .getAttribute(variable);
         // }

         ExpressionFactory expressionFactory = ExpressionFactory
                  .newInstance();

         return expressionFactory.createValueExpression(data, Object.class);
      }

      @Override
      public ValueExpression setVariable(String variable,
               ValueExpression expression)
      {
         throw new RuntimeException("Unexpected method called.");
      }
   }

   @Override
   public VariableMapper getVariableMapper()
   {
      return new PistonVariableMapper();
   }

   @Override
   public FunctionMapper getFunctionMapper()
   {
      FunctionMapper fm = super.getFunctionMapper();
      return fm;
   }

   @Override
   public ELResolver getELResolver()
   {
      ELResolver elResolver = super.getELResolver();
      return elResolver;
   }

}