package com.sun.el.stream;

import static es.getdat.api.management.AppConstants.CHANNEL_TYPE;
import static es.getdat.api.management.AppConstants.WHEN;
import static es.getdat.webhook.management.AppConstants.WEBHOOK_MSG;
import static es.getdat.webhook.management.AppConstants.WEBHOOK_URL;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Method;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import com.sun.el.stream.mine.PortalELContext;

import es.getdat.api.model.Content;
import es.getdat.api.model.enums.ChannelType;

public class Test2
{

   static Content c = new Content();

   public static Optional stampa(String s)
   {
      System.err.println(s);
      return new Optional(s);
   }

   public static void main(String[] args) throws IOException
   {

      c.put(WHEN, "2014-08-11T17:54:20")
               // .put(ACCOUNT_ID, "01d68957-c500-4ee6-b21c-98329e180a12")
               .put(WEBHOOK_URL, "http://localhost:8080/hook")
               .put(WEBHOOK_MSG, "ciao ciao")
               .put(CHANNEL_TYPE, ChannelType.WEBHOOK.name());

      ExpressionFactory expressionFactory = ExpressionFactory.newInstance();
      ELContext simpleCtx = new PortalELContext(expressionFactory, c);

      if (simpleCtx.getFunctionMapper().resolveFunction("sticazzi", "stampa") == null)
      {
         try
         {
            Method meth = Test2.class.getMethod("stampa", String.class);
            simpleCtx.getFunctionMapper().mapFunction("sticazzi", "stampa", meth);
         }
         catch (NoSuchMethodException nse)
         {
            System.err.println(nse.getClass().getCanonicalName());
         }
      }

      String expression = readFile("docs/hangup.xml");
      ValueExpression valueExp = expressionFactory.createValueExpression(
               simpleCtx, expression, Object.class);

      Object obj = valueExp.getValue(simpleCtx);
      System.out.println(obj);
   }

   private static String readFile(String fileName) throws IOException
   {
      BufferedReader br = new BufferedReader(new FileReader(fileName));
      try
      {
         StringBuilder sb = new StringBuilder();
         String line = br.readLine();

         while (line != null)
         {
            sb.append(line);
            sb.append("\n");
            line = br.readLine();
         }
         return sb.toString();
      }
      finally
      {
         br.close();
      }
   }
}
