package es.getdat.scripting.test;

import java.io.File;
import java.io.FileReader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;
import javax.script.SimpleBindings;

import org.junit.Test;

public class ScriptEngineTest {

	@Test
	public static void test() throws Exception {
		List<ScriptEngineFactory> factories = new ScriptEngineManager()
				.getEngineFactories();
		String languagename = null;
		for (ScriptEngineFactory factory : factories) {
			languagename = factory.getLanguageName();
			break;
		}
		ScriptEngine engine = new ScriptEngineManager()
				.getEngineByName(languagename);
		Writer writer = new StringWriter();
		engine.getContext().setWriter(writer);
		Bindings bindings = new SimpleBindings();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("uno", new BigDecimal(1));
		map.put("due", new BigDecimal(2));
		map.put("NULLO", null);
		List<String> list = new ArrayList<String>();
		list.add("a");
		list.add("b");
		map.put("list", list);
		bindings.put("ctx", map);
		ScriptEngineTest testScript = new ScriptEngineTest();
		bindings.put("testScript", testScript);
		engine.eval("var $ = function(txt) { testScript.stampa(txt); }",
				bindings);
		engine.eval(new FileReader(new File("src/main/resources/", "test.js")),
				bindings);
		System.err.println(writer.toString());
	}

	public String stampa(String s) {
		System.err.println(s);
		return s;
	}

	public void invoca(Method m, Class clazz, String s) throws Exception {
		m.invoke(clazz, s);
	}
}
