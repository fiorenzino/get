package es.getdat.scripting.test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.el.ELProcessor;

import org.jboss.logging.Logger;
import org.junit.Test;

import es.getdat.plugins.test.RouterTestClient;

public class El3Test {
	static Logger logger = Logger.getLogger(RouterTestClient.class);

	static String TARGET_URI = "http://localhost:8080/api/v1/router";

	@Test
	public void testEl() {
		// List list = new ArrayList<>();
		// list.add(Double.valueOf(1.0));
		// list.add(Double.valueOf(2.0));
		// list.add(Double.valueOf(3.0));

		ELProcessor elp = new ELProcessor();
		// elp.defineBean("data", list);
		try {
			String toAnalize = readFile("docs/prova.xml");
			Object message = (Double) elp
			// .eval(toAnalize);
					.eval("mappa = {'one':1, 'two':2, 'three':3}; data = [1, 2, 3]; n = data.stream().count(); s = data.stream().sum(); sq = data.stream().map(i -> i*i).sum(); Math.sqrt(sq/n - Math.pow(s/n, 2))");
			System.out.println(message);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static String readFile(String fileName) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append("\n");
				line = br.readLine();
			}
			return sb.toString();
		} finally {
			br.close();
		}
	}
}
