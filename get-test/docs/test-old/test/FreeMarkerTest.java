package es.getdat.scripting.test;

import static es.getdat.core.management.AppConstants.CHANNEL_TYPE;
import static es.getdat.core.management.AppConstants.WHEN;
import static es.getdat.webhook.management.AppConstants.WEBHOOK_MSG;
import static es.getdat.webhook.management.AppConstants.WEBHOOK_URL;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import org.junit.Test;

import es.getdat.api.model.Content;
import es.getdat.api.model.enums.ChannelType;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class FreeMarkerTest
{

   @Test
   public void test()
   {
      Content content = new Content()
               .put(WHEN, "2014-08-11T17:54:20")
               // .put(ACCOUNT_ID, "01d68957-c500-4ee6-b21c-98329e180a12")
               .put(WEBHOOK_URL, "http://localhost:8080/hook")
               .put(WEBHOOK_MSG, "ciao ciao")
               .put(CHANNEL_TYPE, ChannelType.WEBHOOK.name());
      Configuration cfg = new Configuration();
      try
      {
         // Load template from source folder
         Template template = cfg.getTemplate("docs/hang.xml");

         // Console output
         Writer writer = new StringWriter();
         template.process(content.getGlobal(), writer);
         writer.flush();
         System.out.println(writer.toString());

      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
      catch (TemplateException e)
      {
         e.printStackTrace();
      }
   }
}
