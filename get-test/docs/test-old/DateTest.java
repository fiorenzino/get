import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateTest

{

   public static void main(String[] args) throws Exception
   {

      String dateString = "2014-08-18T08:31:39";
      String desiredTimeZone = "Europe/Rome";

      DateFormat desiredDateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
      desiredDateFormatter.setTimeZone(TimeZone.getTimeZone(desiredTimeZone));

      Date parsedDate = desiredDateFormatter.parse(dateString);
      System.out.println(desiredDateFormatter.format(parsedDate));

      String osTimeZone = "America/New_York";
      DateFormat osDateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
      osDateFormatter.setTimeZone(TimeZone.getTimeZone(osTimeZone));

      System.out.println(osDateFormatter.format(parsedDate));

   }
}
