package es.getdat.jaxb.rest.util.plivo;

public class GlobalConstant
{

   static final String PLIVO_API = "https://api.plivo.com";

   static final String ADDRESS = "address";

   static final String APP_ID = "app_id";
   static final String APP_NAME = "app_name";

   static final String AUTH_ID = "auth_id";

   static final String ANSWER_URL = "answer_url";
   static final String ANSWER_METHOD = "answer_method";

   static final String CALLER_NAME = "caller_name";
   static final String CALL_UUID = "call_uuid";

   static final String CITY = "city";

   static final String DEFAULT_NUMBER_APP = "default_number_app";
   static final String DEFAULT_ENDPOINT_APP = "default_endpoint_ap";

   static final String FALLBACK_ANSWER_URL = "fallback_answer_url";
   static final String FALLBACK_METHOD = "fallback_method";

   static final String FROM = "from";

   static final String HANGUP_URL = "hangup_url";
   static final String HANGUP_METHOD = "hangup_method";
   static final String HANGUP_ON_RING = "hangup_on_ring";

   static final String LIMIT = "limit";

   static final String MACHINE_DETECTION = "machine_detection";
   static final String MACHINE_DETECTION_TIME = "machine_detection_time";
   static final String MACHINE_DETECTION_URL = "machine_detection_url";
   static final String MACHINE_DETECTION_METHOD = "machine_detection_method";

   static final String MESSAGE_URL = "message_url";
   static final String MESSAGE_METHOD = "message_method";

   static final String NAME = "name";

   static final String OFFSET = "offset";

   static final String REQUEST_UUID = "request_uuid";

   static final String RING_URL = "ring_url";
   static final String RING_METHOD = "ring_method";
   static final String RING_TIMEOUT = "ring_timeout";

   static final String SEND_DIGITS = "send_digits";
   static final String SEND_ON_PREANSWER = "send_on_preanswer";

   static final String SIP_HEADERS = "sip_headers";

   static final String SUBACCOUNT = "subaccount";

   static final String TIME_LIMIT = "time_limit";

   static final String TO = "to";

   static final String FILE_FORMAT = "file_format";
   static final String TRANSCRIPTION_TYPE = "transcription_type";
   static final String TRANSCRIPTION_URL = "transcription_url";
   static final String TRANSCRIPTION_METHOD = "transcription_method";
   static final String CALLBACK_URL = "callback_url";
   static final String CALLBACK_METHOD = "callback_method";
   static final String TEXT = "text";
   static final String VOICE = "voice";
   static final String LANGUAGE = "language";
   static final String LEGS = "legs";
   static final String LOOP = "loop";
   static final String MIX = "mix";

   static final String CALL_DIRECTION = "call_direction";
   static final String FROM_NUMBER = "from_number";
   static final String TO_NUMBER = "to_number";
   static final String BILL_DURATION = "bill_duration";
   static final String BILL_DURATION_GT = "bill_duration__gt";
   static final String BILL_DURATION_GTE = "bill_duration__gte";
   static final String BILL_DURATION_LT = "bill_duration__lt";
   static final String BILL_DURATION_LTE = "bill_duration__lte";
   static final String END_TIME = "end_time";
   static final String END_TIME_GT = "end_time__gt";
   static final String END_TIME_GTE = "end_time__gte";
   static final String END_TIME_LT = "end_time__lt";
   static final String END_TIME_LTE = "end_time__lte";

   static final String ALEG_URL = "aleg_url";
   static final String ALEG_METHOD = "aleg_method";
   static final String BLEG_URL = "bleg_url";
   static final String BLEG_METHOD = "bleg_method";

   static final String DIGITS = "digits";
   static final String URLS = "urls";
   static final String LENGHT = "length";

   static final String URL = "URL";

   static final String ENABLED = "enabled";
   static final String CONFERENCE_NAME = "conference_name";
   static final String MEMBER_ID = "member_id";
   static final String SUBAUTH_ID = "subauth_id";
   static final String ENDPOINT_ID = "endpoint_id";

   static final String PASSWORD = "password";

   static final String USERNAME = "username";
   static final String ALIAS = "alias";

   static final String SRC = "src";
   static final String DST = "dst";
   static final String TYPE = "type";
   static final String METHOD = "method";
   static final String LOG = "log";
   static final String MESSGE_UUID = "message_uuid";

   static final String COUNTRY_ISO = "country_iso";
   static final String NUMBER_TYPE = "number_type";
   static final String PREFIX = "prefix";
   static final String REGION = "region";
   static final String SERVICES = "services";

   static final String NUMBER = "number";
   static final String QUANTITY = "quantity";

   static final String NUMBER_STARTSWITH = "number_startswith";
   static final String NUMBERS = "numbers";
   static final String CARRIER = "carrier";

   static final String RECORDING_ID = "recording_id";
   static final String ADD_TIME = "add_time";

}
