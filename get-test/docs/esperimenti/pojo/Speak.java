package es.getdat.jaxb.xml.pojo;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlMixed;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Speak")
public class Speak
{

   public Speak()
   {
   }

   public Speak(String value)
   {
      this.value = value;
   }

   /**
    * The tone to be used for reading out the text.
    * 
    * Accepts: WOMAN, MAN
    * 
    * Defaults to: WOMAN
    */
   public Voice voice;

   /**
    * Language used to read out the text.
    * 
    * Accepts: See Supported voices and languages below.
    * 
    * Defaults to: en-US
    */
   public Language language;

   /**
    * Specifies number of times to speak out the text. If value set to 0, speaks indefinitely
    * 
    * Accepts: integer >= 0 (0 indicates a continuous loop)
    * 
    * Defaults to: 1
    */
   @XmlAttribute
   public Integer loop;

   @XmlMixed
   public String value;

}
