package es.getdat.jaxb.xml.pojo;

public enum TranscriptionType
{

   auto, hybrid;
}
