package es.getdat.jaxb.rest.api;

import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import es.getdat.jaxb.rest.pojo.ApiResponseList;
import es.getdat.jaxb.rest.pojo.Recording;
import es.getdat.jaxb.rest.util.RequestUtils;
import es.getdat.jaxb.rest.util.ResponseException;
import es.getdat.jaxb.rest.util.plivo.RecordingRequest;

public class RecordingApi extends AbstractApi
{

   public RecordingApi(Client client)
   {
      this.client = client;
   }

   public Client getClient()
   {
      return client;
   }

   public void setClient(Client client)
   {
      this.client = client;
   }

   /*
    * List All Recordings
    * 
    * GET https://api.plivo.com/v1/Account/{auth_id}/Recording/
    */
   @SuppressWarnings("unchecked")
   public ApiResponseList<Recording> searchForNewNumbers(Map<String, String> parameters) throws Exception
   {
      return RequestUtils.submit(client,
               RecordingRequest.LIST.getTemplate(),
               parameters, ApiResponseList.class);
   }

   /*
    * List a Specific Recording
    * 
    * GET https://api.plivo.com/v1/Account/{auth_id}/Recording/{recording_id}/
    */

   public Recording getSpecificRecording(Map<String, String> parameters) throws Exception
   {
      return RequestUtils.submit(client,
               RecordingRequest.GET.getTemplate(),
               parameters, Recording.class);
   }

   /*
    * Delete a Specific Recording
    * 
    * DELETE https://api.plivo.com/v1/Account/{auth_id}/Recording/{recording_id}/
    */

   public boolean deleteSpecificRecording(Map<String, String> parameters) throws Exception
   {
      Response response = null;
      try
      {
         response = RequestUtils.submit(client,
                  RecordingRequest.DELETE.getTemplate(),
                  parameters);
         if (response.getStatus() == Status.NO_CONTENT.getStatusCode())
         {
            return true;
         }
         throw new ResponseException(response.getStatus(), response.getStatusInfo() == null ? null : response
                  .getStatusInfo().getReasonPhrase(), new Exception("deleteSpecificRecording error"));

      }
      finally
      {
         if (response != null)
            response.close();
      }
   }

}