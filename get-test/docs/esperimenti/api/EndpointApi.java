package es.getdat.jaxb.rest.api;

import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import es.getdat.jaxb.rest.pojo.ApiResponse;
import es.getdat.jaxb.rest.pojo.ApiResponseList;
import es.getdat.jaxb.rest.pojo.Conference;
import es.getdat.jaxb.rest.util.RequestUtils;
import es.getdat.jaxb.rest.util.ResponseException;
import es.getdat.jaxb.rest.util.plivo.EndpointRequest;

public class EndpointApi extends AbstractApi
{

   public EndpointApi(Client client)
   {
      this.client = client;
   }

   public Client getClient()
   {
      return client;
   }

   public void setClient(Client client)
   {
      this.client = client;
   }

   /*
    * Create an Endpoint
    * 
    * 
    * This API lets you create a new endpoint on Plivo. Check out our tutorial on getting started with SIP endpoints.
    * 
    * POST https://api.plivo.com/v1/Account/{auth_id}/Endpoint/
    */
   public ApiResponse createEndpoint(Map<String, String> parameters) throws Exception
   {
      return RequestUtils.submit(client,
               EndpointRequest.CREATE.getTemplate(),
               parameters, ApiResponse.class);
   }

   /*
    * Get Details of a Single Endpoint
    * 
    * 
    * This API lets you get details of a single endpoint on your account using the endpoint_id.
    * 
    * GET https://api.plivo.com/v1/Account/{auth_id}/Endpoint/{endpoint_id}/
    */
   public Conference getDetailsSingleEndpoint(Map<String, String> parameters) throws Exception
   {
      return RequestUtils.submit(client,
               EndpointRequest.GET.getTemplate(),
               parameters, Conference.class);
   }

   /*
    * Get Details of All Endpoints
    * 
    * 
    * This API lets you get details of all endpoints. This is pretty useful in use-cases where you want statuses of your
    * endpoints and whether they have been registered using a SIP client.
    * 
    * GET https://api.plivo.com/v1/Account/{auth_id}/Endpoint/
    */
   @SuppressWarnings("unchecked")
   public ApiResponseList<Conference> getDetailsAllEndpoints(Map<String, String> parameters) throws Exception
   {
      return RequestUtils.submit(client,
               EndpointRequest.LIST.getTemplate(),
               parameters, ApiResponseList.class);
   }

   /*
    * Modify an Endpoint
    * 
    * 
    * This API lets you modify an endpoint’s password, alias or the application attached to it.
    * 
    * POST https://api.plivo.com/v1/Account/{auth_id}/Endpoint/{endpoint_id}/
    */

   public ApiResponse modifyEndpoint(Map<String, String> parameters) throws Exception
   {
      return RequestUtils.submit(client,
               EndpointRequest.MODIFY.getTemplate(),
               parameters, ApiResponse.class);
   }

   /*
    * Delete an Endpoint
    * 
    * 
    * This API lets you delete an endpoint. This operation cannot be undone.
    * 
    * DELETE https://api.plivo.com/v1/Account/{auth_id}/Endpoint/{endpoint_id}/
    */
   public boolean deleteEndpoint(Map<String, String> parameters) throws Exception
   {
      Response response = null;
      try
      {
         response = RequestUtils.submit(client,
                  EndpointRequest.DELETE.getTemplate(),
                  parameters);
         if (response.getStatus() == Status.NO_CONTENT.getStatusCode())
         {
            return true;
         }
         throw new ResponseException(response.getStatus(), response.getStatusInfo() == null ? null : response
                  .getStatusInfo().getReasonPhrase(), new Exception("deleteEndpoint error"));

      }
      finally
      {
         if (response != null)
            response.close();
      }
   }

}