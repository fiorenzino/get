package es.getdat.webhook.management;

public class AppConstants extends es.getdat.core.management.AppConstants
{

   public static final String WEBHOOK_URL = "webhookUrl";
   public static final String WEBHOOK_METHOD = "webhookMethod";
   public static final String WEBHOOK_MSG = "webhookMsg";

}
