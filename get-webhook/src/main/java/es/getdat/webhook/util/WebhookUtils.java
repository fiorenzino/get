package es.getdat.webhook.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.jboss.logging.Logger;

import es.getdat.api.model.Content;
import es.getdat.core.management.AppProperties;
import es.getdat.util.CallAutocloseableUtils;
import es.getdat.util.ContentUtils;
import es.getdat.util.DateUtils;
import es.getdat.webhook.exception.WebhookServiceException;
import es.getdat.webhook.management.AppConstants;

public class WebhookUtils implements Serializable
{

   private static final long serialVersionUID = 1L;

   static String SERVICE_NAME = "WEBHOOK";

   static Logger logger = Logger.getLogger(WebhookUtils.class.getName());

   public static void executeAndReturnToRouter(Content content) throws Exception
   {
      String eventId = content.get(AppConstants.EVENT_ID);
      String stepId = content.get(AppConstants.STEP_ID);
      String when = DateUtils.getPrecisionStringDate();
      Map<String, String> logs = new HashMap<String, String>();
      try
      {
         // logging
         logger.info(content);

         // check arguments
         String url = content.getStepValuesIn(stepId).get(AppConstants.WEBHOOK_URL);
         logs.put("url", url);
         if (url == null || url.trim().isEmpty())
         {
            throw new WebhookServiceException("no url");
         }

         // execution infos
         String externalId = null;
         String info = null;
         Boolean success = null;

         // biz logic
         if (AppProperties.test.value(false) != null && !AppProperties.test.cast(Boolean.class))
         {
            try
            {
               String result = CallUtils.call(content);
               if (result == null)
               {
                  result = "NO CONTENT";
               }
               externalId = "" + System.currentTimeMillis();
               success = true;
               info = result;
            }
            catch (Exception e)
            {
               logger.error(e.getMessage(), e);
               success = false;
               info = e.getClass().getCanonicalName() + " - " + e.getMessage();
            }
         }
         else
         {
            externalId = UUID.randomUUID().toString();
            success = true;
            info = "test";
         }

         // tanto per produrre un risultato ... n.b. usato nei test
         String thisIsUsedInTheTests = content.getStepValuesIn(stepId).get(AppConstants.WEBHOOK_URL);

         // handle result to return to router
         Map<String, String> outs = new HashMap<>();
         outs.put(AppConstants.OPERATION, thisIsUsedInTheTests);
         ContentUtils.result(eventId, stepId, when, SERVICE_NAME, "executeAndReturnToRouter()", externalId, info,
                  success,
                  content, outs, logs);

         // back to router
         if (success != null)
         {
            CallAutocloseableUtils.call(null, AppConstants.API_PATH + AppConstants.ROUTER_PATH, content);
         }

      }
      catch (Exception e)
      {
         logs.put("exception", e.getClass().getCanonicalName());
         logs.put("message", e.getMessage());
         logger.error(e.getMessage(), e);
      }
      finally
      {
         CallAutocloseableUtils.logs(null, AppConstants.API_PATH
                  + es.getdat.core.management.AppConstants.OPERATIONS_PATH + "/parameters",
                  eventId,
                  stepId, when, SERVICE_NAME, logs);
      }
   }
}
