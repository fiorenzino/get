package es.getdat.webhook.util;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;

import es.getdat.api.model.Content;
import es.getdat.util.WebTargetClosable;
import es.getdat.webhook.management.AppConstants;

public class CallUtils extends es.getdat.util.CallUtils
{
   static Logger logger = Logger.getLogger(CallUtils.class);

   public static String call(Content content) throws Exception
   {
      logger.info(content);
      String stepId = content.getGlobal().get(AppConstants.STEP_ID);
      String webhookMethod = content.getStepValuesIn(stepId).get(AppConstants.WEBHOOK_METHOD);
      String webhookUrl = content.getIn(stepId, es.getdat.webhook.management.AppConstants.WEBHOOK_URL);

      if (webhookMethod != null
               && webhookMethod.toLowerCase().trim().equals("get"))
      {
         return get(content, webhookUrl, stepId);
      }
      else
      {
         return post(content, webhookUrl, stepId);
      }
   }

   private static String post(Content content, String webhookUrl, String stepId) throws Exception
   {
      try (WebTargetClosable webTargetClosable = new WebTargetClosable(webhookUrl))
      {

         Form form = new Form();
         for (String key : content.getStepValuesIn(stepId).keySet())
         {
            form.param(key, content.getStepValuesIn(stepId).get(key));
         }

         webTargetClosable.response = webTargetClosable.webTarget
                  .request()
                  .buildPost(
                           Entity.entity(form,
                                    MediaType.APPLICATION_FORM_URLENCODED_TYPE))
                  .invoke();
         String value = webTargetClosable.response.readEntity(String.class);
         logger.info("result: " + value);
         return value;
      }
   }

   private static String get(Content content, String webhookUrl, String stepId) throws Exception
   {
      try (WebTargetClosable webTargetClosable = new WebTargetClosable(webhookUrl))
      {
         for (String key : content.getStepValuesIn(stepId).keySet())
         {
            webTargetClosable.webTarget = webTargetClosable.webTarget.queryParam(key, content.getStepValuesIn(stepId)
                     .get(key));
         }

         webTargetClosable.response = webTargetClosable.webTarget.request().buildGet().invoke();
         String value = webTargetClosable.response.readEntity(String.class);
         logger.info("result: " + value);
         return value;
      }
   }

}
