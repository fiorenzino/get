package es.getdat.webhook.exception;

public class WebhookServiceException extends Exception
{

   private static final long serialVersionUID = 1L;

   public WebhookServiceException(String message)
   {
      super(message);
   }

}
