package es.getdat.webhook.service.rs;

import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import es.getdat.api.model.Content;
import es.getdat.api.service.GetService;
import es.getdat.webhook.util.WebhookUtils;

@Path("/v1/webhook")
@Stateless
@LocalBean
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class WebhookRest extends GetService
{

   private static final long serialVersionUID = 1L;

   @POST
   @Asynchronous
   public void execute(Content content)
   {
      // asynchronously
      try
      {
         WebhookUtils.executeAndReturnToRouter(content);
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
   }

}
