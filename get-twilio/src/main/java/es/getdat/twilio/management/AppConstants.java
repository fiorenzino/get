package es.getdat.twilio.management;

public class AppConstants {

	public static final String TWILIO_CALL_TO = "twilioCallTo";

	public static final String TWILIO_NUMBER = "twilioCallNumber";
	public static final String TWILIO_SID = "twilioCallSid";
	public static final String TWILIO_TOKEN = "twilioCallToken";
	public static final String TWILIO_ACCOUNTNAME = "twilioAccountName";

	public static final String TWILIO_CALL_ANSWER_URL = "twilioCallAnswerUrl";
	public static final String TWILIO_CALL_FALLBACK_URL = "twilioCallFallbackUrl";
	public static final String TWILIO_CALL_HANGUP_URL = "twilioCallHangupUrl";

	public static final String TWILIO_CALL_FILE = "twilioCallFile";
	public static final String TWILIO_CALL_MSG = "twilioCalMsg";

	public static final String TWILIO_MSG_TO = "twilioMsgTo";
	public static final String TWILIO_MSG_BODY = "twilioMsgBody";
	public static final String TWILIO_MSG_URL = "twilioMsgUrl";

	public static final String TWILIO_CALL_EXTERNAL_UID = "CallSid";
	public static final String TWILIO_MSG_EXTERNAL_UID = "MessageSid";

}