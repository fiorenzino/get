package es.getdat.twilio.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@XmlRootElement
public class TwilioOperation implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;
	private String externalUid;
	private Map<String, String> values;
	private Date start;
	private Date end;

	public TwilioOperation() {
		// TODO Auto-generated constructor stub
	}

	public TwilioOperation(String externalUid) {
		this.externalUid = externalUid;
		this.start = new Date();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ElementCollection(fetch = FetchType.EAGER)
	@MapKeyColumn(name = "name")
	@Column(name = "value")
	@CollectionTable(name = "TwilioOperation_properties", joinColumns = @JoinColumn(name = "TwilioOperation_id"))
	public Map<String, String> getValues() {
		if (values == null)
			this.values = new HashMap<String, String>();
		return values;
	}

	public void setValues(Map<String, String> values) {
		this.values = values;
	}

	@XmlTransient
	public TwilioOperation put(String key, String value) {
		getValues().put(key, value);
		return this;
	}

	@XmlTransient
	@Transient
	public boolean isEmpty() {
		return getValues().isEmpty();
	}

	@XmlTransient
	@Transient
	public String get(String key) {
		return getValues().get(key);
	}

	public String getExternalUid() {
		return externalUid;
	}

	public void setExternalUid(String externalUid) {
		this.externalUid = externalUid;
	}

	@Override
	public String toString() {
		return "Operation ["
				+ (id != null ? "id=" + id + ", " : "")
				+ (externalUid != null ? "extrenalUid=" + externalUid + ", "
						: "")
				+ (values != null ? "values=" + values + ", " : "")
				+ (start != null ? "start=" + start + ", " : "")
				+ (end != null ? "end=" + end : "") + "]";
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

}