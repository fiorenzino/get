package es.getdat.twilio.repository;

import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import es.getdat.api.repository.AbstractRepository;
import es.getdat.api.repository.Search;
import es.getdat.twilio.model.TwilioOperation;

@Stateless
@LocalBean
public class TwilioOperationRepository extends
		AbstractRepository<TwilioOperation> {

	private static final long serialVersionUID = 1L;

	@PersistenceContext
	EntityManager em;

	@Override
	protected EntityManager getEm() {
		return em;
	}

	@Override
	public void setEm(EntityManager em) {
		this.em = em;
	}

	@Override
	protected void applyRestrictions(Search<TwilioOperation> search,
			String alias, String separator, StringBuffer sb,
			Map<String, Object> params) {

	}

	@Override
	protected String getDefaultOrderBy() {
		return "id";
	}

	public TwilioOperation getByExternalUid(String externalUid) {
		Query queryObj = getEm().createQuery(
				"select o from Operation o "
						+ " where o.externalUid = :externalUid ").setParameter(
				"externalUid", externalUid);

		TwilioOperation result = (TwilioOperation) queryObj.getSingleResult();
		return result;
	}

}
