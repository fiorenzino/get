package es.getdat.twilio.util;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.logging.Logger;

import es.getdat.api.model.Content;
import es.getdat.core.management.AppConstants;
import es.getdat.util.DateUtils;

public class CallUtils {

	static Logger logger = Logger.getLogger(CallUtils.class);

	public static void updateEvent(WebTarget target, String eventId,
			String operation) throws Exception {
		String data = DateUtils.getPrecisionStringDate();
		Content content = new Content()
				.put(AppConstants.EVENT_CLOSE, "true")
				.put(AppConstants.EVENT_ID, eventId)
				.put(data + "1", operation)
				.put(data + "2",
						"host: "
								+ System.getProperty(AppConstants.JBOSS_QUALIFIED_HOST_NAME_PROPERTY));
		Response res = target.request()
				.buildPut(Entity.entity(content, MediaType.APPLICATION_JSON))
				.invoke();
		if (res.getStatus() == Status.OK.getStatusCode()) {
			String eventIdResult = res.readEntity(String.class);
			content.put(AppConstants.EVENT_ID, eventIdResult);
		} else {
			logger.error("no event id");
		}
	}
}
