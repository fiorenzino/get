package es.getdat.twilio.util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import org.jboss.logging.Logger;

public class LoggerCallUtils {

	static Logger logger = Logger.getLogger(LoggerCallUtils.class);

	public static Map<String, String> log(UriInfo uriInfo) {
		Map<String, String> map = new HashMap<>();
		MultivaluedMap<String, String> queryParams = uriInfo
				.getQueryParameters();
		MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();
		for (String key : queryParams.keySet()) {
			String value = queryParams.getFirst(key);
			map.put(key, value);
			logger.info(key + ": " + value);
		}
		for (String key : pathParams.keySet()) {
			String value = pathParams.getFirst(key);
			logger.info(key + ": " + value);
			map.put(key, value);
		}
		return map;
	}

	public static Map<String, String> log(ServletRequest servletRequest) {
		Map<String, String> map = new HashMap<>();
		Map<String, String[]> parameters = servletRequest.getParameterMap();
		for (String key : parameters.keySet()) {
			String[] value = parameters.get(key);
			if (value != null && value.length > 0) {
				logger.info(key + ": " + Arrays.toString(value));
				map.put(key, value[0]);
			}
		}
		return map;
	}
}
