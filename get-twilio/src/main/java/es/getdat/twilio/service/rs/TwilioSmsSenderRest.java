package es.getdat.twilio.service.rs;

import java.util.Date;
import java.util.Map;

import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.twiliofaces.cdi.doers.simple.SimpleSender;

import es.getdat.api.model.Content;
import es.getdat.api.service.GetService;
import es.getdat.core.annotation.RsClientTarget;
import es.getdat.twilio.management.AppConstants;
import es.getdat.twilio.model.TwilioOperation;
import es.getdat.twilio.repository.TwilioOperationRepository;
import es.getdat.twilio.util.CallUtils;
import es.getdat.twilio.util.LoggerCallUtils;

@Path("/v1/twilio/sms")
@Stateless
@LocalBean
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TwilioSmsSenderRest extends GetService
{

   private static final long serialVersionUID = 1L;

   @Inject
   @RsClientTarget(es.getdat.core.management.AppConstants.EVENTS_PATH
            + "/parameters")
   Instance<WebTarget> eventTarget;

   @Inject
   TwilioOperationRepository twilioOperationRepository;

   @POST
   @Asynchronous
   public void execute(Content content)
   {
      try
      {
         logger.info(content);
         if (content.get(es.getdat.core.management.AppConstants.EVENT_ID) != null
                  && !content.get(
                           es.getdat.core.management.AppConstants.EVENT_ID)
                           .isEmpty())
         {
            CallUtils.updateEvent(eventTarget.get(), content
                     .get(es.getdat.core.management.AppConstants.EVENT_ID),
                     "twilio sms");
         }
         String to = content.get(AppConstants.TWILIO_MSG_TO);
         String body = content.get(AppConstants.TWILIO_MSG_BODY);

         String accountSid = content.get(AppConstants.TWILIO_SID);
         String authToken = content.get(AppConstants.TWILIO_TOKEN);
         String accountName = content.get(AppConstants.TWILIO_ACCOUNTNAME);
         String number = content.get(AppConstants.TWILIO_NUMBER);
         String url = content.get(AppConstants.TWILIO_MSG_URL);

         SimpleSender simpleSender = new SimpleSender();
         String msgId = simpleSender.simpleSend(number, to, body,
                  accountSid, authToken);
         logger.info("msgId: " + msgId);
         if (msgId == null || msgId.isEmpty())
         {
             Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                     .entity("Failed to send sms to: " + to).build();
         }
         else
         {
            TwilioOperation operation = new TwilioOperation(msgId);
            operation

                     .put(AppConstants.TWILIO_SID, accountSid)
                     .put(AppConstants.TWILIO_TOKEN, authToken)
                     .put(content.get(AppConstants.TWILIO_ACCOUNTNAME),
                              accountName)
                     .put(content.get(AppConstants.TWILIO_NUMBER), number)
                     .put(AppConstants.TWILIO_MSG_TO, to)
                     .put(content.get(AppConstants.TWILIO_MSG_BODY), body)
                     .put(AppConstants.TWILIO_MSG_URL, url);
            twilioOperationRepository.persist(operation);
             Response.status(Response.Status.NO_CONTENT)
                     .entity("Reponse: " + content.toString()).build();
         }
      }
      catch (Exception e)
      {
         logger.error(e.getMessage());
          Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                  .entity("Error executing with content: " + content).build();
      }
   }

   // @GET
   // @Path("/up")
   // public Response up()
   // {
   // return Response.status(Response.Status.OK).entity(true).build();
   // }

   @POST
   @GET
   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
   @Produces(MediaType.APPLICATION_XML)
   @Path("/url")
   public Response buzz(@Context HttpServletRequest httpServletRequest)
   {
      try
      {
         Map<String, String> map = LoggerCallUtils.log(httpServletRequest);
         String externalUid = map.get(AppConstants.TWILIO_MSG_EXTERNAL_UID);
         logger.info(AppConstants.TWILIO_MSG_EXTERNAL_UID + ": "
                  + externalUid);
         TwilioOperation operation = twilioOperationRepository
                  .getByExternalUid(externalUid);
         operation.getValues().putAll(map);
         operation.setEnd(new Date());
         twilioOperationRepository.update(operation);
         // UPDATE EVENT
      }
      catch (Exception e)
      {
         logger.error(e.getMessage());
      }

      return Response
               .ok()
               .entity("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                        + "<Response></Response>").build();
   }
}
