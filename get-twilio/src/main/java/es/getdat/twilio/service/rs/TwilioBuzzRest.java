package es.getdat.twilio.service.rs;

import static es.getdat.twilio.management.AppConstants.TWILIO_CALL_TO;
import static es.getdat.twilio.management.AppConstants.TWILIO_SID;
import static es.getdat.twilio.management.AppConstants.TWILIO_TOKEN;

import java.util.Date;
import java.util.Map;

import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.twiliofaces.cdi.doers.Caller;
import org.twiliofaces.cdi.doers.simple.SimpleCaller;

import es.getdat.api.model.Content;
import es.getdat.api.service.GetService;
import es.getdat.core.annotation.RsClientTarget;
import es.getdat.twilio.management.AppConstants;
import es.getdat.twilio.model.TwilioOperation;
import es.getdat.twilio.repository.TwilioOperationRepository;
import es.getdat.twilio.util.CallUtils;
import es.getdat.twilio.util.LoggerCallUtils;

@Path("/v1/twilio/buzz")
@Stateless
@LocalBean
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TwilioBuzzRest extends GetService
{

   private static final long serialVersionUID = 1L;

   @Inject
   TwilioOperationRepository twilioOperationRepository;

   @Inject
   @RsClientTarget(es.getdat.core.management.AppConstants.EVENTS_PATH
            + "/parameters")
   Instance<WebTarget> eventTarget;

   @Inject
   Caller caller;

   @POST
   @Asynchronous
   public void execute(Content content)
   {

      String twilioCallSid = content.get(TWILIO_SID);
      String twilioCallToken = content.get(TWILIO_TOKEN);
      String twilioAccountName = content.get(AppConstants.TWILIO_ACCOUNTNAME);
      String twilioNumber = content.get(AppConstants.TWILIO_NUMBER);

      String twilioCallTo = content.get(TWILIO_CALL_TO);
      String twilioCallAnswerUrl = content
               .get(AppConstants.TWILIO_CALL_ANSWER_URL);
      String twilioCallFallbackUrl = content
               .get(AppConstants.TWILIO_CALL_FALLBACK_URL);
      String twilioCallHangupUrl = content
               .get(AppConstants.TWILIO_CALL_HANGUP_URL);

      try
      {
         logger.info(content);
         if (content.get(es.getdat.core.management.AppConstants.EVENT_ID) != null
                  && !content.get(
                           es.getdat.core.management.AppConstants.EVENT_ID)
                           .isEmpty())
         {
            CallUtils.updateEvent(eventTarget.get(), content
                     .get(es.getdat.core.management.AppConstants.EVENT_ID),
                     "plivo call");
         }
         SimpleCaller simpleCaller = new SimpleCaller();
         String sid = simpleCaller.simpleCall(twilioNumber, twilioCallTo,
                  twilioCallSid, twilioCallToken, twilioCallAnswerUrl);
         logger.info("sid: " + sid);
         if (sid == null || sid.isEmpty())
         {
            Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                     .entity("Failed to call: " + twilioCallTo).build();
         }
         else
         {
            TwilioOperation operation = new TwilioOperation(sid);
            operation
                     .put(AppConstants.TWILIO_CALL_TO, twilioCallTo)
                     .put(AppConstants.TWILIO_SID, twilioCallSid)
                     .put(AppConstants.TWILIO_TOKEN, twilioCallToken)
                     .put(AppConstants.TWILIO_ACCOUNTNAME, twilioAccountName)
                     .put(AppConstants.TWILIO_NUMBER, twilioNumber)
                     .put(AppConstants.TWILIO_CALL_ANSWER_URL,
                              twilioCallAnswerUrl)
                     .put(AppConstants.TWILIO_CALL_FALLBACK_URL,
                              twilioCallFallbackUrl)
                     .put(AppConstants.TWILIO_CALL_HANGUP_URL,
                              twilioCallHangupUrl);
            twilioOperationRepository.persist(operation);
            Response.status(Response.Status.NO_CONTENT)
                     .entity("Reponse: " + content.toString()).build();
         }
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                  .entity("Error executing with content: " + content).build();
      }
   }

   // @GET
   // @Path("/up")
   // public Response up() {
   // return Response.status(Response.Status.OK).entity(true).build();
   // }

   @POST
   @GET
   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
   @Produces(MediaType.APPLICATION_XML)
   @Path("/answer")
   public Response answer(@Context HttpServletRequest httpServletRequest)
   {
      logger.info("twilio buzz answer executing");
      String externalUid;
      try
      {
         Map<String, String> map = LoggerCallUtils.log(httpServletRequest);
         externalUid = map.get(AppConstants.TWILIO_CALL_EXTERNAL_UID);
         logger.info(AppConstants.TWILIO_CALL_EXTERNAL_UID + ": "
                  + externalUid);
         TwilioOperation operation = twilioOperationRepository
                  .getByExternalUid(externalUid);
         operation.getValues().putAll(map);
         operation.setEnd(new Date());
         twilioOperationRepository.update(operation);
         // UPDATE EVENT
      }
      catch (Exception e)
      {
         logger.error(e.getMessage());
      }
      return Response
               .ok()
               .entity("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                        + "<Response>" + "<Hangup />" + "</Response>").build();
   }

   @POST
   @GET
   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
   @Produces(MediaType.APPLICATION_XML)
   @Path("/fallback")
   public Response buzz(@Context HttpServletRequest httpServletRequest)
   {
      logger.info("twilio buzz fallback executing");
      try
      {
         Map<String, String> map = LoggerCallUtils.log(httpServletRequest);
         String externalUid = map.get(AppConstants.TWILIO_CALL_EXTERNAL_UID);
         TwilioOperation operation = twilioOperationRepository
                  .getByExternalUid(externalUid);
         operation.getValues().putAll(map);
         operation.setEnd(new Date());
         twilioOperationRepository.update(operation);
         // UPDATE EVENT
      }
      catch (Exception e)
      {
         logger.error(e.getMessage());
      }
      return Response
               .ok()
               .entity("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                        + "<Response></Response>").build();
   }

   @POST
   @GET
   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
   @Produces(MediaType.APPLICATION_XML)
   @Path("/hangup")
   public Response hangup(@Context HttpServletRequest httpServletRequest)
   {
      logger.info("twilio buzz hangup executing");
      try
      {
         Map<String, String> map = LoggerCallUtils.log(httpServletRequest);
         String externalUid = map.get(AppConstants.TWILIO_CALL_EXTERNAL_UID);
         TwilioOperation operation = twilioOperationRepository
                  .getByExternalUid(externalUid);
         operation.getValues().putAll(map);
         operation.setEnd(new Date());
         twilioOperationRepository.update(operation);
         // UPDATE EVENT
      }
      catch (Exception e)
      {
         logger.error(e.getMessage());
      }
      return Response
               .ok()
               .entity("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                        + "<Response></Response>").build();
   }

}
