package es.getdat.newrouter.producer;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

import es.getdat.api.model.Content;
import es.getdat.core.annotation.CacheMap;

@Singleton
@LocalBean
public class ProcessCacheProducer implements Serializable
{

   private static final long serialVersionUID = 1L;
   private Map<String, Content> events = new ConcurrentHashMap<>();

   public ProcessCacheProducer()
   {
   }

   @Produces
   @CacheMap
   public Map<String, Content> getMap(InjectionPoint injectionPoint)
   {
      return events;
   }

   public Map<String, Content> getEvents()
   {
      return events;
   }
}
