package es.getdat.newrouter.service;

import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.getdat.api.model.Content;
import es.getdat.util.CallAutocloseableUtils;

@Stateless
@LocalBean
public class Launcher
{

   static Logger logger = Logger.getLogger(Launcher.class);

   @Inject
   CacheService cacheService;

   public boolean synchronousCall(String serviceUrl, Content content)
   {
      boolean routed = false;
      try
      {
         routed = CallAutocloseableUtils.call(serviceUrl, content);
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
      }
      return routed;
   }

   @Asynchronous
   public void asynchronousCall(String serviceUrl, Content content, String eventId, String stepId)
   {
      boolean routed = false;
      try
      {
         routed = CallAutocloseableUtils.call(serviceUrl, content);
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
      }
      if (!routed)
      {
         logger.info("NOT ROUTED: " + content);
         cacheService.unrouted(eventId, stepId);
      }
   }

}