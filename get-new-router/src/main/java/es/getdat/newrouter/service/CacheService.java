package es.getdat.newrouter.service;

import java.io.Serializable;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.getdat.api.model.Content;
import es.getdat.api.model.Process;
import es.getdat.api.model.enums.EventDetailsType;
import es.getdat.core.management.AppConstants;
import es.getdat.core.management.AppProperties;
import es.getdat.events.repository.EventRepository;
import es.getdat.events.service.EventService;
import es.getdat.newrouter.producer.ProcessCacheProducer;

@Stateless
@LocalBean
public class CacheService implements Serializable
{

   private static final long serialVersionUID = 1L;

   Logger logger = Logger.getLogger(getClass());

   @Inject
   EventRepository eventRepository;

   @Inject
   EventService eventDetailService;

   @Inject
   ProcessCacheProducer processCacheProducer;

   public boolean start(String eventId, Content full, String timezone, String dst)
   {
      // DOVREI VERIFICARE CHE LA MAPPA NON LO CONTIENE GIA': SE LO CONTIENE CHE FACCIO???
      // NON SCRIVO I DETTAGLI DI OUT (SIAMO IN CREAZIONE)
      // update remote event repo -
      // CallUtils.details(writeStepContentTarget.get(), full);
      if (getEvents().containsKey(eventId))
      {
         logger.error("----------------------------------------");
         logger.error("----------------------------------------");
         logger.error("----------------------------------------");
         logger.error("we are starting a event yet present in cache:" + eventId);
         logger.error("----------------------------------------");
         logger.error("----------------------------------------");
         logger.error("----------------------------------------");
      }
      try
      {
         logger.info("start: " + eventId);
         getEvents().put(eventId, full);
         eventRepository.start(eventId);

         // DEVO PERSISTERE TUTTO INPUT
         for (String stepId : full.getStepValuesIn().keySet())
         {
            Map<String, String> inMap = full.getStepValuesIn(stepId);
            eventDetailService.persistEventDetailsFromMap(eventId, stepId, timezone, dst, EventDetailsType.IN.name(),
                     inMap);
         }
         return true;
      }
      catch (Throwable t)
      {
         logger.error(t.getMessage(), t);
         return false;
      }

   }

   public boolean end(String eventId, Boolean success)
   {
      try
      {
         logger.info("end: " + eventId);
         eventRepository.endWithSuccess(eventId, success);
         if (AppProperties.test.value(false) != null && !AppProperties.test.cast(Boolean.class))
         {
            getEvents().remove(eventId);
         }
         return true;
      }
      catch (Throwable t)
      {
         logger.error(t.getMessage(), t);
         return false;
      }

   }

   public boolean endWithSuccess(String eventId)
   {
      return end(eventId, true);
   }

   public boolean endWithoutSuccess(String eventId)
   {
      return end(eventId, false);
   }

   public void updateOut(String eventId, String stepId, String timezone, String dst, String eventDetailsType,
            Map<String, String> outMap)
   {
      try
      {
         if (getEvents() != null && getEvents().containsKey(eventId) && getEvents().get(eventId) != null
                  && outMap != null)
         {
            getEvents().get(eventId).addStepValuesOut(stepId, outMap);
            eventDetailService.persistEventDetailsFromMap(eventId, stepId, timezone, dst, eventDetailsType, outMap);
         }
         else
         {
            logger.info("qualcosa non lo trovo - eventId:" + eventId + ", stepId:" + stepId + ",timezone:" + timezone
                     + ", eventDetailsType: " + eventDetailsType);
         }
      }
      catch (Throwable t)
      {
         logger.error(t.getMessage(), t);
      }
   }

   public void out(String eventId, String stepId, String timezone, String dst, String eventDetailsType,
            Map<String, String> outMap)
   {
      try
      {
         eventDetailService.persistEventDetailsFromMap(eventId, stepId, timezone, dst, eventDetailsType, outMap);
      }
      catch (Throwable t)
      {
         logger.error(t.getMessage(), t);
      }
   }

   public Content retrieveFromCacheOrDb(String eventId) throws Exception
   {
      Content full = getEvents().get(eventId);
      if (full != null)
      {
         return full;
      }
      else
      {
         // PROVO A CARICARE DA DB
         logger.info("NON HO EVENT IN CACHE: " + eventId + " E' NORMALE??");
         Content content = eventDetailService.getEventDetailsFromContent(eventId);
         if (content != null)
         {
            getEvents().put(eventId, content);
            return content;
         }
         throw new Exception("non ho il content!!!");
      }
   }

   public boolean executedWithSuccess(String eventId, String stepId)
   {
      try
      {
         Content fullContent = retrieveFromCacheOrDb(eventId);
         Process fullProcess = new Process(fullContent.get(AppConstants.PROCESS));
         fullProcess.execute(stepId);
         fullProcess.success(stepId);
         fullContent.put(AppConstants.PROCESS, fullProcess.toJsonString());
         eventRepository.updateProcess(eventId, fullProcess.toJsonString());
         return true;
      }
      catch (Throwable t)
      {
         logger.error(t.getMessage(), t);
         return false;
      }
   }

   public boolean executedWithoutSuccess(String eventId, String stepId)
   {
      try
      {
         Content fullContent = retrieveFromCacheOrDb(eventId);
         Process fullProcess = new Process(fullContent.get(AppConstants.PROCESS));
         fullProcess.execute(stepId);
         fullContent.put(AppConstants.PROCESS, fullProcess.toJsonString());
         eventRepository.updateProcess(eventId, fullProcess.toJsonString());
         return true;
      }
      catch (Throwable t)
      {
         logger.error(t.getMessage(), t);
         return false;
      }
   }

   public boolean routed(String eventId, String stepId)
   {
      try
      {
         Content fullContent = retrieveFromCacheOrDb(eventId);
         Process fullProcess = new Process(fullContent.get(AppConstants.PROCESS));
         fullProcess.route(stepId);
         fullContent.put(AppConstants.PROCESS, fullProcess.toJsonString());
         eventRepository.updateProcess(eventId, fullProcess.toJsonString());
         return true;
      }
      catch (Throwable t)
      {
         logger.error(t.getMessage(), t);
         return false;
      }
   }

   public boolean unrouted(String eventId, String stepId)
   {
      try
      {
         Content fullContent = retrieveFromCacheOrDb(eventId);
         Process fullProcess = new Process(fullContent.get(AppConstants.PROCESS));
         fullProcess.unroute(stepId);
         fullContent.put(AppConstants.PROCESS, fullProcess.toJsonString());
         eventRepository.updateProcess(eventId, fullProcess.toJsonString());
         return true;
      }
      catch (Throwable t)
      {
         logger.error(t.getMessage(), t);
         return false;
      }
   }

   public Process getProcess(String eventId) throws Exception
   {
      try
      {
         Content content = retrieveFromCacheOrDb(eventId);
         return new Process(content.get(AppConstants.PROCESS));
      }
      catch (Exception e)
      {
         logger.error(e.getMessage());
      }
      return null;
   }

   public Map<String, Content> getEvents()
   {
      return processCacheProducer.getEvents();
   }
}
