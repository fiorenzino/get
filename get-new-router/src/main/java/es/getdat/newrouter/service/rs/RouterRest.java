package es.getdat.newrouter.service.rs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.getdat.accounting.repository.ActivationRepository;
import es.getdat.api.model.Content;
import es.getdat.api.model.Process;
import es.getdat.api.model.Step;
import es.getdat.api.model.enums.EventDetailsType;
import es.getdat.api.service.GetService;
import es.getdat.core.management.AppConstants;
import es.getdat.newrouter.service.CacheService;
import es.getdat.newrouter.service.Launcher;
import es.getdat.newrouter.util.StepChecker;
import es.getdat.newrouter.util.StepValorizer;
import es.getdat.plugin.repository.PluginConfigurationRepository;
import es.getdat.util.ContentUtils;

@Path(AppConstants.ROUTER_PATH)
@Stateless
@LocalBean
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class RouterRest extends GetService
{

   private static final long serialVersionUID = 1L;

   @Inject
   Launcher launcher;

   @Inject
   CacheService cacheService;

   @Inject
   PluginConfigurationRepository pluginConfigurationRepository;

   @Inject
   ActivationRepository activationRepository;

   @Asynchronous
   public void executeAsync(Content content)
   {
      execute(content);
   }

   @POST
   @Asynchronous
   public void execute(Content content)
   {
      String eventId = null;
      String stepId = null;
      String timezone = null;
      String dst = null;
      String executed = null;
      String success = null;
      long routingId = System.currentTimeMillis();
      try
      {
         // CACCIO LE VARIABILI CHE MI SERVONO IN TUTTI I PASSAGGI:
         if (content == null)
         {
            logger.error(routingId + "No content - SKIP");
            return;
         }
         eventId = content.get(AppConstants.EVENT_ID);
         stepId = content.get(AppConstants.STEP_ID);
         timezone = content.get(AppConstants.TIMEZONE);
         dst = content.get(AppConstants.DST);

         // should not happen
         // if (timezone == null)
         // {
         // timezone = TimeZone.getDefault().getDisplayName();
         // }

         if (eventId == null)
         {
            logger.error(routingId + "No eventId for content: " + content);
            return;
         }
         StringBuffer log = new StringBuffer("\n-----------\n");
         log.append(routingId + ": NEW ROUNTING CONTENT eventId:" + eventId);

         // SE stepId E' NULLO => SIAMO AL PRIMO ACCESSO DA SCHEDULER e non stato di esecuzione
         if (stepId != null)
         {
            executed = content.getStepValuesOut(stepId).get(AppConstants.EXECUTED);
            success = content.getStepValuesOut(stepId).get(AppConstants.SUCCESS);
            log.append(",stepId:" + stepId);
            if (executed != null)
            {
               log.append(",executed:" + executed);
            }
            if (success != null)
            {
               log.append(",success:" + success);
            }
            if (eventId.equals(stepId))
            {
               log.append(",ROUTER EVENT!");
            }
         }
         logger.info(log.toString());
         // recognize step
         // SE stepId E' NULLO => SIAMO AL PRIMO ACCESSO DA SCHEDULER
         if (stepId == null)
         {
            // CREO EVENT IN CACHE
            if (!cacheService.start(eventId, content, timezone, dst))
            {
               block("Cache creation error", eventId, stepId, timezone, dst);
               logger.error(routingId + ": Cache creation error");
               return;
            }
         }
         else
         {

            // SE EXECUTED = TRUE => UPDATE THE PROCESS STEP ID
            if (executed != null && Boolean.parseBoolean(executed))
            {
               if (success != null && Boolean.parseBoolean(success))
               {
                  cacheService.executedWithSuccess(eventId, stepId);
               }
               else
               {
                  cacheService.executedWithoutSuccess(eventId, stepId);
               }
            }

            Map<String, String> outMap = ContentUtils.getOutMap(eventId, stepId, content);
            cacheService.updateOut(eventId, stepId, timezone, dst, EventDetailsType.OUT.name(), outMap);

            Map<String, String> forwardmap = new HashMap<String, String>();
            forwardmap.put(AppConstants.OPERATION, "forward from step " + stepId);
            forwardmap.put(AppConstants.SERVICE_NAME, "router");
            forwardmap.put(AppConstants.HOST, System.getProperty(AppConstants.JBOSS_QUALIFIED_HOST_NAME_PROPERTY));
            cacheService.out(eventId, eventId, timezone, dst, EventDetailsType.FORWARD.name(), forwardmap);

         }

         // A QUESTO PUNTO LA CACHE E' AGGIORNATA
         // COMINCIO A GESTIRE IL PROCESS
         // get process definition

         Content full = cacheService.retrieveFromCacheOrDb(eventId);
         Process process = cacheService.getProcess(eventId);

         if (process == null)
         {
            block("Failed to get process definition", eventId, stepId, timezone, dst);
            logger.error(routingId + ": Failed to get process definition");
            return;
         }
         logger.info(routingId + ": process: " + process.toJsonString());

         // get current step
         Step currentStep = null;
         if (stepId != null)
         {
            currentStep = process.get(stepId);
            // no step means a fatal error
            if (currentStep == null)
            {
               block("Step " + stepId + " not found", eventId, stepId, timezone, dst);
               logger.error(routingId + ": Step " + stepId + " not found");
               return;
            }
            logger.info(routingId + ": currentStep: " + currentStep.toJsonString());
            // step not routed means we are here too soon? thus, not going to block. only a warning
            if (!currentStep.routed)
            {
               logger.warn(routingId + ": Step " + stepId + " ROUTED routed");
            }
            if (!currentStep.executed)
            {
               logger.info(routingId + ":current step " + stepId
                        + " have not executed yet. EXIT!");
               return;
            }

            List<Step> concurrentSteps = process.concurrent(currentStep.stepId, currentStep.order);
            if (concurrentSteps != null && concurrentSteps.size() > 0)
            {
               // VALUTO SE TUTTI GLI STEP SONO STATI ESEGUITI (SKIPPANDO GLI ASINCRONI)
               boolean allSynchronusStepsInParallelHaveEnded = true;
               // VALUTO PER CIASCUNO SE LA CONDIZIONE E' OK
               boolean atLeastOneUnsatisfiedStep = false;
               for (Step concurrentStep : concurrentSteps)
               {
                  if (concurrentStep.async)
                  {
                     continue;
                  }
                  if (!concurrentStep.executed)
                  {
                     // IT'S SUFFICIENT ONE STEP NOT EXECUTED TO EXIT FROM THE CYCLE
                     allSynchronusStepsInParallelHaveEnded = false;
                     break;
                  }
                  else
                  {
                     if (!StepChecker.checkSatisfied(concurrentStep.condition, process, full))
                     {
                        atLeastOneUnsatisfiedStep = true;
                     }
                  }
               }
               // VALUTO LO STATO COMPLESSIVO DI ESECUZIONE
               // POSSO CONTINUARE O USCIRE

               if (!allSynchronusStepsInParallelHaveEnded)
               {
                  logger.info(routingId + ": Synchronous step(s) running in parallel to step " + stepId
                           + " have not executed yet. EXIT!");
                  return;
               }
               else
               {
                  logger.info(routingId + ": ALL Synchronous step(s) running in parallel to step " + stepId
                           + " HAVE EXECUTED");

                  if (atLeastOneUnsatisfiedStep)
                  {
                     // DEVO SOLO AGGIORNARE EVENTO COMPLESSIVO PER DATA END
                     logger.info(routingId + ":StepChecker.checkSatisfied: END AND EXIT => process:"
                              + process.toJsonString());
                     // VALUTO LA CONDIZIONE DEL PROCESSO PER ATTRIBUIRE UNO STATO ALL'EVENTO
                     if (process.successCondition != null && !process.successCondition.isEmpty())
                     {
                        // VALUTO LA PROCESS CONDITION PER ATTRIBUIRE SUCCESS ALL'EVENTO
                        if (StepChecker.checkSatisfied(process.successCondition, process, full))
                        {
                           logger.info(routingId + ": SUCCESS CONDITION TRUE - EVENT ENDED WITH SUCCESS");
                           cacheService.endWithSuccess(eventId);
                        }
                        else
                        {
                           logger.info(routingId + ": SUCCESS CONDITION FALSE - EVENT ENDED WITH NO SUCCESS");
                           cacheService.endWithoutSuccess(eventId);
                        }
                     }
                     else
                     {
                        logger.info(routingId + ": NO SUCCESS CONDITION - EVENT WITHOUT SUCCESS FIELD");
                        cacheService.end(eventId, null);
                     }

                     return;
                  }
                  else
                  {
                     // VALUTO I SUCCESSIVI
                     logger.info(routingId + ": StepChecker.checkSatisfied: NO END => process:"
                              + process.toJsonString());
                  }

               }
            }
            else
            {
               logger.info(routingId + ": NO CONCURRENT STEP to step " + stepId);

               // VERIFICO LA CONDIZIONE DELLO STEP UNICO:
               // - SE NON ESISTE => VADO AVANTI.
               // - SE ESISTE ED E' FALSA => ESCO
               // - SE VERA => VADO AVANTI.
               if (!StepChecker.checkSatisfied(currentStep.condition, process, full))
               {
                  // DEVO SOLO AGGIORNARE EVENTO COMPLESSIVO PER DATA END
                  logger.info(routingId + ":StepChecker.checkSatisfied: END AND EXIT => process:"
                           + process.toJsonString());
                  // VALUTO LA CONDIZIONE DLE PROCESSO PER ATTRIBUIRE UNO STATO ALL'EVENTO
                  if (process.successCondition != null && !process.successCondition.isEmpty())
                  {
                     // VALUTO LA PROCESS CONDITION PER ATTRIBUIRE SUCCESS ALL'EVENTO
                     if (StepChecker.checkSatisfied(process.successCondition, process, full))
                     {
                        logger.info(routingId + ": SUCCESS CONDITION TRUE - EVENT ENDED WITH SUCCESS");
                        cacheService.endWithSuccess(eventId);
                     }
                     else
                     {
                        logger.info(routingId + ": SUCCESS CONDITION FALSE - EVENT ENDED WITH NO SUCCESS");
                        cacheService.endWithoutSuccess(eventId);
                     }
                  }
                  else
                  {
                     logger.info(routingId + ": NO SUCCESS CONDITION - EVENT WITHOUT SUCCESS FIELD");
                     cacheService.end(eventId, null);
                  }

                  return;
               }
               else
               {
                  logger.info(routingId + ": StepChecker.checkSatisfied: NO END => process:" + process.toJsonString());
               }
            }

         }

         // IN QUESTA FASE TUTTI GLI STEP SONO STATI ESEGUITI:
         // SE NON CI SONO ALTRI STEP ESCO E CHIUDO

         // what steps are next?
         List<Step> nextSteps = (currentStep == null) ? process.begin() : process.next(currentStep);

         // check if this is the last process stage
         if (nextSteps.isEmpty())
         {
            logger.info(routingId + ": nextSteps EMPTY =>  EXIT WITH END => process");
            if (process.successCondition != null && !process.successCondition.isEmpty())
            {
               // VALUTO LA PROCESS CONDITION PER ATTRIBUIRE SUCCESS ALL'EVENTO
               if (StepChecker.checkSatisfied(process.successCondition, process, full))
               {
                  logger.info(routingId + ": SUCCESS CONDITION TRUE - EVENT ENDED WITH SUCCESS");
                  cacheService.endWithSuccess(eventId);
               }
               else
               {
                  logger.info(routingId + ": SUCCESS CONDITION FALSE - EVENT ENDED WITH NO SUCCESS");
                  cacheService.endWithoutSuccess(eventId);
               }
            }
            else
            {
               logger.info(routingId + ": NO SUCCESS CONDITION - EVENT WITHOUT SUCCESS FIELD");
               cacheService.end(eventId, null);
            }
            return;
         }

         // check if all steps can be routed
         List<String> blockedStepIds = new ArrayList<String>();

         for (Step step : nextSteps)
         {
            // step.accountId, step.pluginConfigurationId, ChannelType.valueOf(step.channelType),

            boolean canBeRouted = StepValorizer
                     .valorizeSingle(full, step, activationRepository,
                              pluginConfigurationRepository);

            // let's abort everything in this case
            if (!canBeRouted)
            {
               blockedStepIds.add(stepId);
            }
         }
         if (blockedStepIds.size() > 0)
         {
            block("no plugin configuration available for step(s): " + blockedStepIds, eventId, stepId, timezone, dst);
            logger.info(routingId + ": NO plugin configuration available for step(s): " + blockedStepIds);
            return;
         }

         for (Step step : nextSteps)
         {
            String serviceUrl = content.getIn(step.stepId, AppConstants.SERVICE_URL);

            // RUOTO IL PROCESSO E LO LANCIO
            cacheService.routed(eventId, step.stepId);
            logger.info(routingId + ": launch:" + step.stepId + ", serviceUrl:" + serviceUrl);
            // PERCHE FARE UN SECONDO CHECK???
            // if (StepChecker.checkSatisfied(step.condition, process, content))
            // {
            if (step.legacy)
            {
               boolean routed = fireAndUpdate(serviceUrl, full, eventId, step.stepId, timezone, dst);
               // SE ANDATO MALE, LO SRUOTO!
               if (!routed)
                  cacheService.unrouted(eventId, step.stepId);
            }
            else
            {
               // ESSENDO ASINCRONO, SI SRUTTA DA SOLO
               fire(serviceUrl, full, eventId, step.stepId, timezone, dst);
            }
         }
         logger.info(routingId + ": end routing content");

      }
      catch (Exception e)
      {
         logger.error(routingId + ":" + e.getMessage(), e);
         try
         {
            block("Exception = " + e.getClass().getCanonicalName() + ", message = "
                     + e.getMessage()
                     + "\nwhile executing with content: " + content, eventId, stepId, timezone, dst);
         }
         catch (Exception be)
         {
            logger.error(routingId + ":" + be.getMessage(), be);
            return;
         }
      }
   }

   private boolean fireAndUpdate(String serviceUrl, Content content, String eventId, String stepId, String timezone,
            String dst)
   {
      try
      {
         boolean success = launcher.synchronousCall(serviceUrl, content);
         if (success)
         {
            Map<String, String> forwardmap = new HashMap<String, String>();
            forwardmap.put(AppConstants.OPERATION, "routed step: " + stepId + ", serviceUrl:" + serviceUrl);
            forwardmap.put(AppConstants.SERVICE_NAME, "router");
            forwardmap.put(AppConstants.HOST, System.getProperty(AppConstants.JBOSS_QUALIFIED_HOST_NAME_PROPERTY));
            cacheService.out(eventId, eventId, timezone, dst, EventDetailsType.FORWARD.name(), forwardmap);

            // CREO DETTAGLIO PER SEGNALARE ROUTED LO STEP CORRENTE
            // executed = content.getStepValuesOut(stepId).get(AppConstants.EXECUTED);
            Map<String, String> outMap = new HashMap<String, String>();
            outMap.put(AppConstants.ROUTED, "true");
            outMap.put(AppConstants.SERVICE_NAME, "router");
            outMap.put(AppConstants.HOST, System.getProperty(AppConstants.JBOSS_QUALIFIED_HOST_NAME_PROPERTY));
            cacheService.out(eventId, stepId, timezone, dst, EventDetailsType.OUT.name(), outMap);

            return true;
         }
         else
         {
            // contro RUTTO
            Map<String, String> forwardmap = new HashMap<String, String>();
            forwardmap.put(AppConstants.OPERATION, "error in routed step: " + stepId + ", serviceUrl:" + serviceUrl);
            forwardmap.put(AppConstants.SERVICE_NAME, "router");
            forwardmap.put(AppConstants.HOST, System.getProperty(AppConstants.JBOSS_QUALIFIED_HOST_NAME_PROPERTY));
            cacheService.out(eventId, eventId, timezone, dst, EventDetailsType.FORWARD.name(), forwardmap);

            return false;
         }
      }
      catch (Exception e)
      {
         String info = "Exception = " + e.getClass().getCanonicalName() + ", message = " + e.getMessage()
                  + "\nwhile executing with content: " + content;
         logger.error(info, e);

         Map<String, String> forwardmap = new HashMap<String, String>();
         forwardmap.put(AppConstants.OPERATION, "error in routed step: " + stepId + ", serviceUrl:" + serviceUrl);
         forwardmap.put(AppConstants.SERVICE_NAME, "router");
         forwardmap.put(AppConstants.HOST, System.getProperty(AppConstants.JBOSS_QUALIFIED_HOST_NAME_PROPERTY));
         cacheService.out(eventId, stepId, timezone, dst, EventDetailsType.FORWARD.name(), forwardmap);
         return false;
      }
   }

   public void fire(String serviceUrl, Content content, String eventId, String stepId, String timezone, String dst)
            throws Exception
   {
      try
      {
         Map<String, String> outMap = new HashMap<String, String>();
         outMap.put(AppConstants.ROUTED, "true");
         outMap.put(AppConstants.SERVICE_NAME, "router");
         outMap.put(AppConstants.HOST, System.getProperty(AppConstants.JBOSS_QUALIFIED_HOST_NAME_PROPERTY));
         cacheService.out(eventId, stepId, timezone, dst, EventDetailsType.OUT.name(), outMap);
         launcher.asynchronousCall(serviceUrl, content, eventId, stepId);
      }
      catch (Exception e)
      {
         String info = "Exception = " + e.getClass().getCanonicalName() + ", message = " + e.getMessage()
                  + "\nwhile executing with content: " + content;
         logger.error(info, e);
      }
   }

   private void block(String errorMessage, String eventId, String stepId, String timezone, String dst) throws Exception
   {
      logger.error(errorMessage);
      Map<String, String> forwardmap = new HashMap<String, String>();
      forwardmap.put(AppConstants.OPERATION, "error in forwarding step: " + stepId);
      forwardmap.put(AppConstants.SERVICE_NAME, "router");
      forwardmap.put(AppConstants.HOST, System.getProperty(AppConstants.JBOSS_QUALIFIED_HOST_NAME_PROPERTY));
      cacheService.out(eventId, stepId, timezone, dst, EventDetailsType.FORWARD.name(), forwardmap);
   }

   @GET
   @Path("/up")
   public Response up()
   {
      return Response.status(Response.Status.OK).entity(true).build();
   }
}
