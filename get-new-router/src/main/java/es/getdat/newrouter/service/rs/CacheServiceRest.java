package es.getdat.newrouter.service.rs;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.websocket.server.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import es.getdat.api.model.Content;
import es.getdat.api.model.ContentValue;
import es.getdat.api.model.enums.ContentValueType;
import es.getdat.api.util.ContentUtils;
import es.getdat.core.management.AppConstants;
import es.getdat.newrouter.service.CacheService;

@Path(AppConstants.CACHE_PATH)
@Stateless
@LocalBean
@Consumes(MediaType.TEXT_PLAIN)
@Produces(MediaType.APPLICATION_JSON)
public class CacheServiceRest
{

   @Inject
   CacheService cacheService;

   @GET
   public List<String> getEventsFromCache()
   {
      if (cacheService.getEvents() != null)
         return new ArrayList<String>(cacheService.getEvents().keySet());
      return null;
   }

   @GET
   @Path("/reset")
   public boolean reset()
   {
      cacheService.getEvents().clear();
      return true;
   }

   @GET
   @Path("/events/{eventId}")
   public List<ContentValue> getValues(@PathParam("eventId") String eventId)
   {
      Content content = cacheService.getEvents().get(eventId);
      List<ContentValue> ret = ContentUtils.toContentValue(content, eventId, null, null);
      return ret;
   }

   @GET
   @Path("/events/{eventId}/{stepId}")
   public List<ContentValue> getValues(@PathParam("eventId") String eventId, @PathParam("stepId") String stepId,
            @QueryParam("contentValueType") String contentValueType)
   {
      List<ContentValue> ret = null;
      if (contentValueType != null && !contentValueType.isEmpty())
      {
         Content content = cacheService.getEvents().get(eventId);
         ret = ContentUtils.toContentValue(content, eventId, stepId, null);
      }
      else
      {
         ContentValueType contentValueType2 = ContentValueType.valueOf(contentValueType);
         Content content = cacheService.getEvents().get(eventId);
         ret = ContentUtils.toContentValue(content, eventId, stepId,
                  contentValueType2);
      }
      return ret;
   }
}
