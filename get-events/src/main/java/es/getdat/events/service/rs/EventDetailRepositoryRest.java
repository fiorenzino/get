package es.getdat.events.service.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.getdat.api.model.Content;
import es.getdat.api.model.PaginatedListWrapper;
import es.getdat.api.repository.Search;
import es.getdat.api.service.GetRepositoryService;
import es.getdat.core.management.AppConstants;
import es.getdat.events.model.EventDetail;
import es.getdat.events.repository.EventDetailRepository;
import es.getdat.events.util.EventDetailUtils;

@Path(AppConstants.EVENTDETAILS_PATH)
@Stateless
@LocalBean
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class EventDetailRepositoryRest extends GetRepositoryService<EventDetail>
{

   private static final long serialVersionUID = 1L;

   @Inject
   EventDetailRepository eventDetailRepository;

   public EventDetailRepositoryRest()
   {
   }

   @Inject
   public EventDetailRepositoryRest(EventDetailRepository eventDetailRepository)
   {
      super(eventDetailRepository);
   }

   @Override
   protected void prePersist(EventDetail eventDetail) throws Exception
   {
      if (eventDetail == null)
      {
         throw new Exception("event detail null");
      }
      if (eventDetail.getExecutionDate() == null)
      {
         throw new Exception("event detail timestamp null");
      }
   }

   @POST
   @Path("/parameters")
   public Response createFromContent(Content content) throws Exception
   {
      logger.info("@POST /parameters => createFromContent");
      String idEvent = content.get(AppConstants.EVENT_ID);
      if (idEvent == null || idEvent.isEmpty())
      {
         return Response
                  .status(Response.Status.NOT_FOUND)
                  .entity("Failed to update resource: event id not present "
                           + content).build();
      }
      String idStep = content.get(AppConstants.STEP_ID);
      if (idStep == null || idStep.isEmpty())
      {
         content.put(AppConstants.STEP_ID, idEvent);
         // return Response
         // .status(Response.Status.NOT_FOUND)
         // .entity("Failed to update resource: step id not present "
         // + content).build();
      }
      List<EventDetail> eventDetails = null;
      try
      {
         eventDetails = EventDetailUtils.fromContent(content);
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                  .entity("Error extracting content : " + content).build();
      }

      List<EventDetail> badRequests = new ArrayList<>();
      List<EventDetail> internalErrors = new ArrayList<>();
      List<Object> uuids = new ArrayList<>();
      for (EventDetail eventDetail : eventDetails)
      {
         try
         {
            prePersist(eventDetail);
         }
         catch (Exception e)
         {
            badRequests.add(eventDetail);
            continue;
         }
         EventDetail persisted = null;
         try
         {
            persisted = getRepository().persist(eventDetail);
         }
         catch (Exception e)
         {
            logger.error(e.getMessage(), e);
            continue;
         }
         finally
         {
            if (persisted == null || getId(persisted) == null)
            {
               internalErrors.add(eventDetail);
            }
            else
            {
               uuids.add(getId(persisted));
               try
               {
                  postPersist(persisted);
               }
               catch (Exception e)
               {
                  logger.error(e.getMessage(), e);
               }
            }
         }
      }
      if (badRequests.size() > 0)
      {
         return Response.status(Response.Status.BAD_REQUEST)
                  .entity("Error before creating one or more event details: " + badRequests.toString()).build();
      }
      if (internalErrors.size() > 0)
      {
         return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                  .entity("Failed to create one or more event details: " + internalErrors.toString()).build();
      }
      return Response.status(Response.Status.OK)
               .entity("Created resources: " + uuids.toString()).build();

   }

   @POST
   @Path("/events/{idEvent}/steps/{idStep}/executed")
   public Response executed(@PathParam("idEvent") String idEvent, @PathParam("idStep") String idStep) throws Exception
   {
      try
      {
         EventDetail eventDetail = new EventDetail();
         eventDetail.setIdEvent(idEvent);
         eventDetail.setIdStep(idStep);
         eventDetail.setExecutionDate(new Date());
         eventDetail.setName(AppConstants.EXECUTED);
         eventDetail.setValue(Boolean.TRUE.toString());
         prePersist(eventDetail);
         getRepository().persist(eventDetail);
         if (getId(eventDetail) == null)
         {
            throw new Exception("failed to persist eventDetail: " + eventDetail);
         }
         postPersist(eventDetail);
         return Response.status(Response.Status.OK)
                  .entity(eventDetail).build();
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                  .entity("Error while marking idEvent " + idEvent + " idStep " + idStep + " executed").build();
      }
   }

   @GET
   @Path("/events/{eventId}/steps/{stepId}")
   public Response list(@PathParam("eventId") String eventId,
            @PathParam("stepId") String stepId) throws Exception
   {
      logger.info("@GET /events/{eventId}/steps/{stepId} => list");
      try
      {
         Search<EventDetail> sed = new Search<>(EventDetail.class);
         sed.getObj().setIdEvent(eventId);
         sed.getObj().setIdStep(stepId);
         List<EventDetail> eventDetails = eventDetailRepository.getList(sed, 0, 0);
         if (eventDetails == null || eventDetails.size() == 0)
         {
            return Response
                     .status(Response.Status.NOT_FOUND)
                     .entity("Failed no event details found for eventId = " + eventId + " stepId = "
                              + stepId).build();
         }
         PaginatedListWrapper<EventDetail> wrapper = new PaginatedListWrapper<>();
         wrapper.setList(eventDetails);
         wrapper.setListSize(eventDetails.size());
         wrapper.setPageSize(eventDetails.size());
         wrapper.setStartRow(0);
         return Response.status(Response.Status.OK)
                  .entity(eventDetails).build();
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return Response
                  .status(Response.Status.INTERNAL_SERVER_ERROR)
                  .entity("Error while getting event details for eventId = " + eventId + " stepId = "
                           + stepId).build();
      }
   }

   @GET
   @Path("/events/{eventId}")
   public Response list(@PathParam("eventId") String eventId) throws Exception
   {
      logger.info("@GET /events/{eventId}/ => list ");
      try
      {
         Search<EventDetail> sed = new Search<>(EventDetail.class);
         sed.getObj().setIdEvent(eventId);
         List<EventDetail> eventDetails = eventDetailRepository.getList(sed, 0, 0);
         if (eventDetails == null || eventDetails.size() == 0)
         {
            return Response
                     .status(Response.Status.NOT_FOUND)
                     .entity("Failed no event details found for eventId = " + eventId).build();
         }
         PaginatedListWrapper<EventDetail> wrapper = new PaginatedListWrapper<>();
         wrapper.setList(eventDetails);
         wrapper.setListSize(eventDetails.size());
         wrapper.setPageSize(eventDetails.size());
         wrapper.setStartRow(0);
         return Response.status(Response.Status.OK)
                  .entity(eventDetails).build();
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return Response
                  .status(Response.Status.INTERNAL_SERVER_ERROR)
                  .entity("Error while getting event details for eventId = " + eventId).build();
      }
   }

}
