package es.getdat.events.service.rs;

import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.getdat.api.model.Content;
import es.getdat.api.service.GetRepositoryService;
import es.getdat.core.management.AppConstants;
import es.getdat.events.model.Event;
import es.getdat.events.repository.EventRepository;
import es.getdat.events.util.EventUtils;

@Path(AppConstants.EVENTS_PATH)
@Stateless
@LocalBean
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class EventRepositoryRest extends GetRepositoryService<Event>
{

   private static final long serialVersionUID = 1L;

   @Inject
   EventRepository eventRepository;

   public EventRepositoryRest()
   {
   }

   @Inject
   public EventRepositoryRest(EventRepository eventRepository)
   {
      super(eventRepository);
   }

   @Override
   protected void prePersist(Event event) throws Exception
   {
      if (event == null)
      {
         throw new Exception("event null");
      }
      if (event.getExecutionDate() == null)
      {
         throw new Exception("event execution date null");
      }
   }

   @Override
   protected void preUpdate(Event event) throws Exception
   {
   }

   @POST
   @Path("/parameters")
   public Response createFromContent(Content content) throws Exception
   {
      logger.info("@POST /parameters => createFromContent");
      Event event = EventUtils.fromContent(content);
      try
      {
         prePersist(event);
      }
      catch (Exception e)
      {
         return Response.status(Response.Status.BAD_REQUEST)
                  .entity("Error before creating resource: " + event).build();
      }
      try
      {
         Event persisted = getRepository().persist(event);
         if (persisted == null || getId(persisted) == null)
         {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                     .entity("Failed to create resource: " + event).build();
         }
         else
         {
            return Response.status(Response.Status.OK)
                     .entity(persisted.getId()).build();
         }
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                  .entity("Error creating resource: " + event).build();
      }
      finally
      {
         try
         {
            postPersist(event);
         }
         catch (Exception e)
         {
            logger.error(e.getMessage(), e);
         }
      }

   }

   @PUT
   @Path("/{idEvent}/start")
   @Asynchronous
   public void start(@PathParam("idEvent") String idEvent) throws Exception
   {
      try
      {
         logger.info("@PUT /start");
         if (!eventRepository.start(idEvent))
         {
            throw new Exception();
         }
         logger.info("@PUT /start: OK");
         // return Response.status(Response.Status.NO_CONTENT).build();
      }
      catch (Exception e)
      {
         logger.info("@PUT /start: KO");
         logger.error(e.getMessage(), e);
         // return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
         // .entity("Error updating resource: " + idEvent).build();
      }
   }

   @PUT
   @Path("/{idEvent}/end")
   @Asynchronous
   public void end(@PathParam("idEvent") String idEvent) throws Exception
   {
      try
      {
         logger.info("@PUT /end");
         if (!eventRepository.endWithSuccess(idEvent, null))
         {
            throw new Exception();
         }
         logger.info("@PUT /end: OK");
         // return Response.status(Response.Status.NO_CONTENT).build();
      }
      catch (Exception e)
      {
         logger.info("@PUT /end: KO");
         logger.error(e.getMessage(), e);
         // return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
         // .entity("Error updating resource: " + idEvent).build();
      }
   }
}
