package es.getdat.events.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import es.getdat.api.model.Content;
import es.getdat.api.repository.Search;
import es.getdat.events.model.Event;
import es.getdat.events.model.EventDetail;
import es.getdat.events.repository.EventDetailRepository;
import es.getdat.events.repository.EventRepository;
import es.getdat.events.util.EventDetailUtils;
import es.getdat.events.util.EventUtils;

@Stateless
public class EventService implements Serializable
{
   private static final long serialVersionUID = 1L;

   Logger logger = Logger.getLogger(getClass());

   @Inject
   EventDetailRepository eventDetailRepository;

   @Inject
   EventRepository eventRepository;

   public int persistEventDetailsFromMap(String eventId, String stepId, String timezone, String dst,
            String eventDetailsType,
            Map<String, String> outMap)
   {
      try
      {
         List<EventDetail> eventDetails = EventDetailUtils.fromMap(eventId, stepId, timezone, dst, eventDetailsType,
                  outMap);
         if (eventDetails != null && !eventDetails.isEmpty())
         {
            for (EventDetail eventDetail : eventDetails)
            {
               eventDetailRepository.persist(eventDetail);
            }
         }
         return eventDetails.size();
      }
      catch (Exception e)
      {
         logger.error("eccezione", e);
      }
      return 0;
   }

   public Content getEventDetailsFromContent(String eventId) throws Exception
   {
      logger.info("ricostruisco content dell'evento ");
      try
      {
         Event event = eventRepository.find(eventId);
         Content content = EventUtils.toContent(event);
         Search<EventDetail> sed = new Search<>(EventDetail.class);
         sed.getObj().setIdEvent(eventId);
         List<EventDetail> eventDetails = eventDetailRepository.getList(sed, 0, 0);
         if (eventDetails == null || eventDetails.size() == 0)
         {
            return null;
         }
         return EventDetailUtils.toContent(eventDetails, content);
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return null;
      }
   }

   public Event persistEventFromContent(Content content) throws Exception
   {
      logger.info("Event => createFromContent");
      Event event = EventUtils.fromContent(content);
      try
      {
         if (event.getExecutionDate() == null)
            return null;
         event = eventRepository.persist(event);
         if (event != null && event.getId() != null)
            return event;
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
      }
      return null;
   }

}
