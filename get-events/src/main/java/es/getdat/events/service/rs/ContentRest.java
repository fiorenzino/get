package es.getdat.events.service.rs;

import java.io.Serializable;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;

import es.getdat.api.repository.Search;
import es.getdat.core.management.AppConstants;
import es.getdat.events.model.EventDetail;
import es.getdat.events.repository.EventDetailRepository;
import es.getdat.events.repository.EventRepository;
import es.getdat.events.util.EventDetailUtils;

@Path(AppConstants.EVENTDETAILS_PATH)
@Stateless
@LocalBean
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ContentRest implements Serializable
{

   Logger logger = Logger.getLogger(getClass());

   private static final long serialVersionUID = 1L;

   @Inject
   EventDetailRepository eventDetailRepository;
   @Inject
   EventRepository eventRepository;

   public ContentRest()
   {
   }

   @GET
   @Path("/events/{idEvent}/steps/{idStep}/content")
   public Response list(@PathParam("idEvent") String eventId,
            @PathParam("idStep") String stepId) throws Exception
   {
      logger.info("@GET events/{idEvent}/steps/{idStep}/content => content");
      try
      {
         Search<EventDetail> sed = new Search<>(EventDetail.class);
         sed.getObj().setIdEvent(eventId);
         sed.getObj().setIdStep(stepId);
         List<EventDetail> eventDetails = eventDetailRepository.getList(sed, 0, 0);
         if (eventDetails == null || eventDetails.size() == 0)
         {
            return Response
                     .status(Response.Status.NOT_FOUND)
                     .entity("Failed no event details found for eventId = " + eventId + " stepId = "
                              + stepId).build();
         }

         return Response.status(Response.Status.OK)
                  .entity(EventDetailUtils.toContent(eventDetails)).build();
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return Response
                  .status(Response.Status.INTERNAL_SERVER_ERROR)
                  .entity("Error while getting event details for eventId = " + eventId + " stepId = "
                           + stepId).build();
      }
   }

   @GET
   @Path("/events/{eventId}/content")
   public Response list(@PathParam("eventId") String eventId) throws Exception
   {
      logger.info("@GET /events/{eventId}/ => list ");
      try
      {
         Search<EventDetail> sed = new Search<>(EventDetail.class);
         sed.getObj().setIdEvent(eventId);
         List<EventDetail> eventDetails = eventDetailRepository.getList(sed, 0, 0);
         if (eventDetails == null || eventDetails.size() == 0)
         {
            return Response
                     .status(Response.Status.NOT_FOUND)
                     .entity("Failed no event details found for eventId = " + eventId).build();
         }
         return Response.status(Response.Status.OK)
                  .entity(EventDetailUtils.toContent(eventDetails)).build();
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return Response
                  .status(Response.Status.INTERNAL_SERVER_ERROR)
                  .entity("Error while getting event details for eventId = " + eventId).build();
      }
   }

}
