package es.getdat.events.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jboss.logging.Logger;

import es.getdat.api.model.Content;
import es.getdat.api.model.enums.EventDetailsType;
import es.getdat.core.management.AppConstants;
import es.getdat.events.model.EventDetail;
import es.getdat.util.DateUtils;

public class EventDetailUtils
{

   static final Logger logger = Logger.getLogger(EventDetailUtils.class.getName());

   public static List<EventDetail> fromContent(Content content) throws Exception
   {
      String eventId = content.get(AppConstants.EVENT_ID);
      String stepId = content.get(AppConstants.STEP_ID);
      // if (stepId.equals(eventId))
      // {
      // logger.info("ROUTER EVENT");
      // }
      String when = content.getIn(stepId, AppConstants.WHEN);
      Date whenDate = null;
      if (when == null || when.trim().isEmpty())
      {
         whenDate = new Date();
      }
      else
      {
         whenDate = DateUtils.getDateFromString(when);
      }
      String timezone = content.get(AppConstants.TIMEZONE);
      EventDetailsType eventDetailsType = null;
      try
      {
         eventDetailsType = EventDetailsType.valueOf(content.get(AppConstants.EVENT_DETAILS_TYPE));
      }
      catch (Exception e)
      {
      }
      String dst = content.get(AppConstants.DST);
      List<EventDetail> eventDetails = new ArrayList<>();
      if (eventDetailsType == null)
      {
         for (String key : content.getStepValuesOut(stepId).keySet())
         {
            EventDetail eventDetail = new EventDetail();
            eventDetail.setIdEvent(eventId);
            eventDetail.setIdStep(stepId);
            eventDetail.setName(key);
            eventDetail.setValue(content.getStepValuesOut(stepId).get(key));
            eventDetail.setTimezone(timezone);
            eventDetail.setDst(dst);
            eventDetail.setEventDetailsType(EventDetailsType.OUT);
            eventDetail.setExecutionDate(whenDate);
            eventDetails.add(eventDetail);
         }
         for (String key : content.getStepValuesIn(stepId).keySet())
         {
            EventDetail eventDetail = new EventDetail();
            eventDetail.setIdEvent(eventId);
            eventDetail.setIdStep(stepId);
            eventDetail.setName(key);
            eventDetail.setValue(content.getStepValuesIn(stepId).get(key));
            eventDetail.setTimezone(timezone);
            eventDetail.setDst(dst);
            eventDetail.setEventDetailsType(EventDetailsType.IN);
            eventDetail.setExecutionDate(whenDate);
            eventDetails.add(eventDetail);
         }
      }
      else if (eventDetailsType.equals(EventDetailsType.IN))
      {
         for (String key : content.getStepValuesIn(stepId).keySet())
         {
            EventDetail eventDetail = new EventDetail();
            eventDetail.setIdEvent(eventId);
            eventDetail.setIdStep(stepId);
            eventDetail.setName(key);
            eventDetail.setValue(content.getStepValuesIn(stepId).get(key));
            eventDetail.setTimezone(timezone);
            eventDetail.setDst(dst);
            eventDetail.setEventDetailsType(eventDetailsType);
            eventDetail.setExecutionDate(whenDate);
            eventDetails.add(eventDetail);
         }
      }
      else if (eventDetailsType.equals(EventDetailsType.OUT))
      {
         for (String key : content.getStepValuesOut(stepId).keySet())
         {
            EventDetail eventDetail = new EventDetail();
            eventDetail.setIdEvent(eventId);
            eventDetail.setIdStep(stepId);
            eventDetail.setName(key);
            eventDetail.setValue(content.getStepValuesOut(stepId).get(key));
            eventDetail.setTimezone(timezone);
            eventDetail.setDst(dst);
            eventDetail.setEventDetailsType(eventDetailsType);
            eventDetail.setExecutionDate(whenDate);
            eventDetails.add(eventDetail);
         }
      }
      else
      {
         for (String key : content.getStepValuesOut(stepId).keySet())
         {
            EventDetail eventDetail = new EventDetail();
            eventDetail.setIdEvent(eventId);
            eventDetail.setIdStep(stepId);
            eventDetail.setName(key);
            eventDetail.setValue(content.getStepValuesOut(stepId).get(key));
            eventDetail.setTimezone(timezone);
            eventDetail.setDst(dst);
            eventDetail.setEventDetailsType(eventDetailsType);
            eventDetail.setExecutionDate(whenDate);
            eventDetails.add(eventDetail);
         }
         for (String key : content.getStepValuesIn(stepId).keySet())
         {
            EventDetail eventDetail = new EventDetail();
            eventDetail.setIdEvent(eventId);
            eventDetail.setIdStep(stepId);
            eventDetail.setName(key);
            eventDetail.setValue(content.getStepValuesIn(stepId).get(key));
            eventDetail.setTimezone(timezone);
            eventDetail.setDst(dst);
            eventDetail.setEventDetailsType(eventDetailsType);
            eventDetail.setExecutionDate(whenDate);
            eventDetails.add(eventDetail);
         }
      }
      return eventDetails;
   }

   public static Content toContent(List<EventDetail> eventDetails, Content content) throws Exception
   {
      for (EventDetail ed : eventDetails)
      {
         if (ed.getIdStep().equals(ed.getIdEvent()))
         {
            content.getGlobal().put(ed.getName(), ed.getValue());
            content.put(AppConstants.STEP_ID, ed.getIdStep());
            content.put(AppConstants.TIMEZONE, ed.getTimezone());
            content.put(AppConstants.DST, ed.getDst());
            content.put(AppConstants.EVENT_ID, ed.getIdEvent());
            content.put(AppConstants.WHEN, DateUtils.getStringFromDate(ed.getExecutionDate()));
         }
         else
         {
            content.getStepValuesIn(ed.getIdStep()).put(ed.getName(), ed.getValue());
            content.getStepValuesIn(ed.getIdStep()).put(AppConstants.WHEN,
                     DateUtils.getStringFromDate(ed.getExecutionDate()));
         }
      }
      return content;
   }

   public static Content toContent(List<EventDetail> eventDetails) throws Exception
   {
      Content content = new Content();
      return toContent(eventDetails, content);
   }

   public static List<EventDetail> fromMap(String eventId, String stepId, String timezone, String dst,
            String eventDetailsType,
            Map<String, String> outMap)
            throws Exception
   {
      List<EventDetail> eventDetails = new ArrayList<>();
      // if (stepId.equals(eventId))
      // {
      // logger.info("ROUTER EVENT");
      // }
      for (String key : outMap.keySet())
      {
         EventDetail eventDetail = new EventDetail();
         eventDetail.setIdEvent(eventId);
         eventDetail.setIdStep(stepId);
         eventDetail.setName(key);
         eventDetail.setValue(outMap.get(key));
         eventDetail.setTimezone(timezone);
         eventDetail.setDst(dst);
         if (eventDetailsType != null && !eventDetailsType.trim().isEmpty())
         {
            eventDetail.setEventDetailsType(EventDetailsType.valueOf(eventDetailsType));
         }
         eventDetail.setExecutionDate(new Date());
         eventDetails.add(eventDetail);
      }

      return eventDetails;
   }
}
