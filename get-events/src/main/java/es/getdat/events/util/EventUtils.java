package es.getdat.events.util;

import es.getdat.api.model.Content;
import es.getdat.core.management.AppConstants;
import es.getdat.events.model.Event;
import es.getdat.util.DateUtils;

public class EventUtils
{

   public static Event fromContent(Content content) throws Exception
   {
      Event event = new Event();
      event.setExecutionDate(
               DateUtils.getDateFromString(content.get(AppConstants.WHEN)
                        ));
      event.setProcess(content.get(AppConstants.PROCESS));
      event.setTimezone(content.get(AppConstants.TIMEZONE));
      event.setDst(content.get(AppConstants.DST));
      event.setAccountId(content.get(AppConstants.ACCOUNT_ID));
      return event;
   }

   public static Content toContent(Event event) throws Exception
   {
      Content content = new Content();
      content.put(AppConstants.WHEN, DateUtils.getStringFromDate(event.getExecutionDate()));
      content.put(AppConstants.PROCESS, event.getProcess());
      content.put(AppConstants.TIMEZONE, event.getTimezone());
      content.put(AppConstants.DST, event.getDst());
      content.put(AppConstants.ACCOUNT_ID, event.getAccountId());
      return content;
   }
}
