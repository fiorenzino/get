package es.getdat.events.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.GenericGenerator;

@Entity
@XmlRootElement
public class Event implements Serializable
{

   private static final long serialVersionUID = 1L;

   private String id;
   private String process;
   private String timezone;
   private String dst;
   private Date executionDate;
   private Date start;
   private Date end;
   private Boolean success;
   private String accountId;

   @Id
   @GeneratedValue(generator = "uuid")
   @GenericGenerator(name = "uuid", strategy = "uuid2")
   @Column(name = "uuid", unique = true)
   public String getId()
   {
      return id;
   }

   public void setId(String id)
   {
      this.id = id;
   }

   @Column(length = 2048)
   public String getProcess()
   {
      return process;
   }

   public void setProcess(String process)
   {
      this.process = process;
   }

   @Temporal(TemporalType.TIMESTAMP)
   public Date getStart()
   {
      return start;
   }

   public void setStart(Date start)
   {
      this.start = start;
   }

   @Temporal(TemporalType.TIMESTAMP)
   public Date getEnd()
   {
      return end;
   }

   public void setEnd(Date end)
   {
      this.end = end;
   }

   @Temporal(TemporalType.TIMESTAMP)
   public Date getExecutionDate()
   {
      return executionDate;
   }

   public void setExecutionDate(Date executionDate)
   {
      this.executionDate = executionDate;
   }

   public String getTimezone()
   {
      return timezone;
   }

   public void setTimezone(String timezone)
   {
      this.timezone = timezone;
   }

   public Boolean getSuccess()
   {
      return success;
   }

   public void setSuccess(Boolean success)
   {
      this.success = success;
   }

   public String getAccountId()
   {
      return accountId;
   }

   public void setAccountId(String accountId)
   {
      this.accountId = accountId;
   }

   public String getDst()
   {
      return dst;
   }

   public void setDst(String dst)
   {
      this.dst = dst;
   }

   @Override
   public String toString()
   {
      return "Event [" + (id != null ? "id=" + id + ", " : "") + (process != null ? "process=" + process + ", " : "")
               + (timezone != null ? "timezone=" + timezone + ", " : "")
               + (dst != null ? "dst=" + dst + ", " : "")
               + (executionDate != null ? "executionDate=" + executionDate + ", " : "")
               + (start != null ? "start=" + start + ", " : "") + (end != null ? "end=" + end + ", " : "")
               + (success != null ? "success=" + success + ", " : "")
               + (accountId != null ? "accountId=" + accountId : "") + "]";
   }

}
