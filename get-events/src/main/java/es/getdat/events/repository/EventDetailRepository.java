package es.getdat.events.repository;

import java.util.List;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import es.getdat.api.repository.AbstractRepository;
import es.getdat.api.repository.Search;
import es.getdat.core.management.AppConstants;
import es.getdat.events.model.EventDetail;

@Stateless
@LocalBean
public class EventDetailRepository extends AbstractRepository<EventDetail>
{

   private static final long serialVersionUID = 1L;

   @PersistenceContext
   EntityManager em;

   @Override
   protected EntityManager getEm()
   {
      return em;
   }

   @Override
   public void setEm(EntityManager em)
   {
      this.em = em;
   }

   @Override
   protected void applyRestrictions(Search<EventDetail> search, String alias,
            String separator, StringBuffer sb, Map<String, Object> params)
   {
      // eventId
      if (search.getObj().getIdEvent() != null && !search.getObj().getIdEvent().trim().isEmpty())
      {
         sb.append(separator).append(alias)
                  .append(".idEvent = :idEvent ");
         params.put("idEvent", search.getObj().getIdEvent());
         separator = " and ";
      }

      // stepId
      if (search.getObj().getIdStep() != null && !search.getObj().getIdStep().trim().isEmpty())
      {
         sb.append(separator).append(alias)
                  .append(".idStep = :idStep ");
         params.put("idStep", search.getObj().getIdStep());
         separator = " and ";
      }
      // from
      if (search.getFrom() != null && search.getFrom().getExecutionDate() != null)
      {
         sb.append(separator).append(alias).append(".executionDate >= :fromExecutionDate ");
         params.put("fromExecutionDate", search.getFrom().getExecutionDate());
         separator = " and ";
      }
      // to
      if (search.getTo() != null && search.getTo().getExecutionDate() != null)
      {
         sb.append(separator).append(alias).append(".executionDate <= :fromExecutionDate ");
         params.put("fromExecutionDate", search.getTo().getExecutionDate());
         separator = " and ";
      }
      // name
      if (search.getObj().getName() != null && !search.getObj().getName().trim().isEmpty())
      {
         sb.append(separator).append(" upper ( ").append(alias)
                  .append(".name ) like :name ");
         params.put("name", likeParam(search.getObj().getName().trim().toUpperCase()));
         separator = " and ";
      }
      // value
      if (search.getObj().getValue() != null && !search.getObj().getValue().trim().isEmpty())
      {
         sb.append(separator).append(" upper ( ").append(alias)
                  .append(".value ) like :value ");
         params.put("value", likeParam(search.getObj().getValue().trim().toUpperCase()));
         separator = " and ";
      }
      // type
      if (search.getObj().getEventDetailsType() != null)
      {
         sb.append(separator).append(alias)
                  .append(".eventDetailsType = :type ");
         params.put("type", search.getObj().getEventDetailsType());
         separator = " and ";
      }
   }

   @Override
   protected String getDefaultOrderBy()
   {
      return "id desc";
   }

   public EventDetail getByExternalUidAndServiceName(String externalUid,
            String serviceName)
   {
      Query queryObj = getEm()
               .createQuery(
                        "select ed from "
                                 + EventDetail.class.getSimpleName()
                                 + " ed "
                                 + " where ed.name = :externalUidName and ed.value = :externalUid and ed.name = :serviceNameName and ed.value = :serviceName",
                        EventDetail.class)
               .setParameter("externalUidName", AppConstants.EXTERNAL_ID)
               .setParameter("serviceNameName", AppConstants.SERVICE_NAME)
               .setParameter("externalUid", externalUid)
               .setParameter("serviceName", serviceName);

      EventDetail result = (EventDetail) queryObj.getSingleResult();
      return result;
   }

   public boolean deleteByEventIdStepIdAndNames(String idEvent, String idStep, List<String> detailNames)
   {
      try
      {
         Query queryObj = getEm()
                  .createQuery(
                           "delete from " + EventDetail.class.getSimpleName()
                                    + " ed where ed.idEvent = :idEvent, ed.idStep = :idStep and ed.name in ( :names ) ")
                  .setParameter("idEvent", idEvent)
                  .setParameter("idStep", idStep)
                  .setParameter("names", detailNames);

         queryObj.executeUpdate();
         return true;
      }
      catch (Exception e)
      {
         logger.error(e.getMessage(), e);
         return false;
      }
   }

}
