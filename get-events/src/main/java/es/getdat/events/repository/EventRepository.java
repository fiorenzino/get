package es.getdat.events.repository;

import java.util.Date;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.getdat.api.repository.AbstractRepository;
import es.getdat.api.repository.Search;
import es.getdat.events.model.Event;

@Stateless
@LocalBean
public class EventRepository extends AbstractRepository<Event>
{

   private static final long serialVersionUID = 1L;

   @PersistenceContext
   EntityManager em;

   @Override
   protected EntityManager getEm()
   {
      return em;
   }

   @Override
   public void setEm(EntityManager em)
   {
      this.em = em;
   }

   @Override
   protected void applyRestrictions(Search<Event> search, String alias,
            String separator, StringBuffer sb, Map<String, Object> params)
   {
      // id
      if (search.getObj().getId() != null && search.getObj().getId().trim().length() > 0)
      {
         sb.append(separator).append(alias).append(".id = :id ");
         params.put("id", search.getObj().getId());
         separator = " and ";
      }

      // from
      if (search.getFrom() != null && search.getFrom().getExecutionDate() != null)
      {
         sb.append(separator).append(alias).append(".executionDate >= :fromExecutionDate ");
         params.put("fromExecutionDate", search.getFrom().getExecutionDate());
         separator = " and ";
      }
      // to
      if (search.getTo() != null && search.getTo().getExecutionDate() != null)
      {
         sb.append(separator).append(alias).append(".executionDate <= :fromExecutionDate ");
         params.put("fromExecutionDate", search.getTo().getExecutionDate());
         separator = " and ";
      }

      // accountId
      if (search.getObj().getAccountId() != null && !search.getObj().getAccountId().trim().isEmpty())
      {
         sb.append(separator).append(alias).append(".accountId <= :accountId ");
         params.put("accountId", search.getObj().getAccountId());
         separator = " and ";
      }

   }

   @Override
   protected String getDefaultOrderBy()
   {
      return "executionDate desc";
   }

   public boolean start(String idEvent)
   {
      int result = 0;
      try
      {
         result = getEm()
                  .createNativeQuery("update " + Event.class.getSimpleName() + " set start = :now where uuid = :id ")
                  .setParameter("now", new Date()).setParameter("id", idEvent).executeUpdate();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      return result > 0;
   }

   public boolean endWithSuccess(String idEvent, Boolean success)
   {
      int result = 0;
      try
      {
         result = getEm()
                  .createNativeQuery(
                           "update " + Event.class.getSimpleName()
                                    + " set end = :now, success = :success where uuid = :id ")
                  .setParameter("now", new Date()).setParameter("success", success).setParameter("id", idEvent)
                  .executeUpdate();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      return result > 0;
   }

   public boolean updateProcess(String idEvent, String process)
   {
      int result = 0;
      try
      {
         result = getEm()
                  .createNativeQuery(
                           "update " + Event.class.getSimpleName() + " set process = :process where uuid = :id ")
                  .setParameter("process", process).setParameter("id", idEvent).executeUpdate();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      return result > 0;
   }

}
