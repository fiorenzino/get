package es.getdat.mailgun.service.rs;

import java.util.Map;

import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import es.getdat.api.model.Content;
import es.getdat.api.service.GetService;
import es.getdat.mailgun.util.LoggerCallUtils;
import es.getdat.mailgun.util.MailgunUtils;
import es.getdat.util.DateUtils;

@Path("/v1/mailgun")
@Stateless
@LocalBean
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class MailgunRest extends GetService
{
   private static final long serialVersionUID = 1L;

   static String SERVICE_NAME = "MAILGUN";

   @POST
   @Asynchronous
   public void execute(Content content)
   {
      // asynchronously
      try
      {
         MailgunUtils.executeAndReturnToRouter(content);
      }
      catch (Throwable e)
      {
         logger.error(e.getMessage(), e);
      }
   }

   @POST
   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
   @Path("/trackingOpens")
   @Asynchronous
   public void trackingOpens(@Context HttpServletRequest httpServletRequest)
   {
      log(httpServletRequest, "mailgun trackingOpens");
   }

   @POST
   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
   @Path("/trackingClicks")
   @Asynchronous
   public void trackingClicks(@Context HttpServletRequest httpServletRequest)
   {
      log(httpServletRequest, "mailgun trackingClicks");
   }

   @POST
   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
   @Path("/trackingUnsubscribes")
   @Asynchronous
   public void trackingUnsubscribes(@Context HttpServletRequest httpServletRequest)
   {
      log(httpServletRequest, "mailgun trackingUnsubscribes");
   }

   @POST
   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
   @Path("/trackingSpamComplaints")
   @Asynchronous
   public void trackingSpamComplaints(@Context HttpServletRequest httpServletRequest)
   {
      log(httpServletRequest, "mailgun trackingSpamComplaints");
   }

   @POST
   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
   @Path("/trackingDeliveries")
   @Asynchronous
   public void trackingDeliveries(@Context HttpServletRequest httpServletRequest)
   {
      log(httpServletRequest, "mailgun trackingDeliveries");
      // return Response.status(Response.Status.OK).entity(true).build();
   }

   @POST
   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
   @Path("/trackingFailures")
   @Asynchronous
   public void trackingFailures(@Context HttpServletRequest httpServletRequest)
   {
      log(httpServletRequest, "mailgun trackingFailures");
      // return Response.status(Response.Status.OK).entity(true).build();
   }

   @POST
   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
   @Path("/trackingBounces")
   @Asynchronous
   public void trackingBounces(@Context HttpServletRequest httpServletRequest)
   {
      log(httpServletRequest, "mailgun trackingBounces");
   }

   private void log(HttpServletRequest httpServletRequest, String operation)
   {
      logger.info(operation);
      try
      {
         Map<String, String> map = LoggerCallUtils.log(httpServletRequest);

         Content operationContent = new Content();
         operationContent.getGlobal().putAll(map);
         operationContent.put(es.getdat.core.management.AppConstants.WHEN, DateUtils.getPrecisionStringDate())
                  .put(es.getdat.core.management.AppConstants.SERVICE_NAME, SERVICE_NAME);
         // CallUtils.logs(operationTarget.get(), eventId, stepId, when, SERVICE_NAME, logs);
      }
      catch (Exception e)
      {
         logger.error(e.getMessage());
      }
   }
}
