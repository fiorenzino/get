package es.getdat.mailgun.service.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ApiResponse
{
   public String message;
   public String id;

   public ApiResponse()
   {
   }

   @Override
   public String toString()
   {
      return "ApiResponse [" + (message != null ? "message=" + message + ", " : "") + (id != null ? "id=" + id : "")
               + "]";
   }

}
