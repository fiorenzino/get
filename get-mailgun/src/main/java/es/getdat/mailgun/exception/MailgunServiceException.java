package es.getdat.mailgun.exception;

public class MailgunServiceException extends Exception
{

   private static final long serialVersionUID = 1L;

   public MailgunServiceException(String message)
   {
      super(message);
   }

}
