package es.getdat.mailgun.util;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.ResponseProcessingException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;

import es.getdat.api.model.Content;
import es.getdat.core.management.AppProperties;
import es.getdat.mailgun.exception.MailgunServiceException;
import es.getdat.mailgun.management.AppConstants;
import es.getdat.mailgun.service.model.ApiResponse;
import es.getdat.util.CallAutocloseableUtils;
import es.getdat.util.CallUtils;
import es.getdat.util.ContentUtils;
import es.getdat.util.DateUtils;

public class MailgunUtils
{

   static String SERVICE_NAME = "MAILGUN";

   static Logger logger = Logger.getLogger(MailgunUtils.class.getName());

   public static void executeAndReturnToRouter(Content content
            // ,
            // Instance<WebTarget> routerTarget,
            // Instance<WebTarget> operationTarget
            ) throws Exception
   {
      String eventId = content.get(AppConstants.EVENT_ID);
      String stepId = content.get(AppConstants.STEP_ID);
      String when = DateUtils.getPrecisionStringDate();
      Map<String, String> logs = new HashMap<String, String>();

      try
      {
         // logging
         logger.info(content);

         // check arguments
         String mailUser = content.getIn(stepId, AppConstants.MAIL_USER);
         String mailPwd = content.getIn(stepId, AppConstants.MAIL_PWD);
         String from = content.getIn(stepId, AppConstants.MAIL_FROM);
         String tos = content.getIn(stepId, AppConstants.MAIL_TOS);
         String subject = content.getIn(stepId, AppConstants.MAIL_SUBJECT);
         String body = content.getIn(stepId, AppConstants.MAIL_BODY);
         String address = content.getIn(stepId, AppConstants.MAIL_ADDRESS);
         if (tos == null || tos.trim().isEmpty())
         {
            throw new MailgunServiceException("impossible to send an email without from or to field");
         }

         // execution infos
         String externalId = null;
         String info = null;
         Boolean success = null;
         Client client = null;
         Response response = null;
         // biz logic
         if (AppProperties.test.value(false) != null && !AppProperties.test.cast(Boolean.class))
         {
            try
            {
               client = ClientBuilder.newClient().register(new Authenticator(mailUser, mailPwd));
               WebTarget webTarget = client.target(address);
               Map<String, String> map = new HashMap<String, String>();
               map.put("from", from);
               String[] to = tos.split(";");
               for (String singleTo : to)
               {
                  map.put("to", singleTo);
               }
               map.put("subject", subject);
               map.put("text", body);
               map.put("v:my-custom-data", "event_id: " + eventId + "}");
               response = webTarget.request()
                        .post(Entity.form(CallUtils.getForm(map)));
               if (response == null)
               {
                  throw new ResponseProcessingException(null, "no response");
               }
               if (response.getStatus() < 200 || response.getStatus() >= 300)
               {
                  logger.info(response.getStatusInfo() == null ? "no status info"
                           : response.getStatusInfo().getReasonPhrase());
                  throw new ClientErrorException(response);
               }
               ApiResponse apiResponse = response.readEntity(ApiResponse.class);
               externalId = apiResponse.id;
               success = externalId != null && externalId.trim().length() > 0;
               info = "real";
               logs.put("apiResponse", apiResponse.toString());
            }
            catch (Exception e)
            {
               success = false;
               info = e.getClass().getCanonicalName() + ": " + e.getMessage();
            }
            finally
            {
               if (response != null)
               {
                  response.close();
               }
               if (client != null)
               {
                  client.close();
               }
            }
         }
         else
         {
            externalId = UUID.randomUUID().toString();
            success = true;
            info = "test";
         }

         // handle result to return to router
         Map<String, String> outs = new HashMap<>();
         ContentUtils.result(eventId, stepId, when, SERVICE_NAME, "executeAndReturnToRouter()", externalId, info,
                  success,
                  content, outs, logs);

         // back to router
         if (success != null)
         {
            CallAutocloseableUtils.call(null, AppConstants.API_PATH + AppConstants.ROUTER_PATH, content);
         }
      }

      catch (Exception e)
      {
         logs.put("exception", e.getClass().getCanonicalName());
         logs.put("message", e.getMessage());
         logger.error(e.getMessage(), e);
      }
      finally
      {
         CallAutocloseableUtils.logs(null, AppConstants.API_PATH
                  + es.getdat.core.management.AppConstants.OPERATIONS_PATH + "/parameters",
                  eventId,
                  stepId, when, SERVICE_NAME, logs);
      }

   }
}
