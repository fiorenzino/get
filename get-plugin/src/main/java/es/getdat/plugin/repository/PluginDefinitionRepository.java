package es.getdat.plugin.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import es.getdat.api.repository.AbstractRepository;
import es.getdat.api.repository.Search;
import es.getdat.plugin.model.PluginDefinition;

@Stateless
@LocalBean
public class PluginDefinitionRepository extends
         AbstractRepository<PluginDefinition>
{

   private static final long serialVersionUID = 1L;

   @PersistenceContext
   EntityManager em;

   @Override
   protected EntityManager getEm()
   {
      return em;
   }

   @Override
   public void setEm(EntityManager em)
   {
      this.em = em;
   }

   @Override
   protected void applyRestrictions(Search<PluginDefinition> search,
            String alias, String separator, StringBuffer sb,
            Map<String, Object> params)
   {

      // id
      if (search.getObj().getId() != null && search.getObj().getId().trim().length() > 0)
      {
         sb.append(separator).append(alias).append(".id = :id ");
         params.put("id", search.getObj().getId());
         separator = " and ";
      }

      // name
      if (search.getObj().getName() != null
               && !search.getObj().getName().trim().isEmpty())
      {
         sb.append(separator).append(alias).append(".name = :name ");
         params.put("name", search.getObj().getName());
         separator = " and ";
      }

      // description
      if (search.getObj().getDescription() != null
               && !search.getObj().getDescription().trim().isEmpty())
      {
         sb.append(separator).append(" upper ( ").append(alias).append(".description ) like :description ");
         params.put("description", likeParam(search.getObj().getDescription().trim()));
         separator = " and ";
      }

      // tags
      if (search.getObj().getTags() != null
               && !search.getObj().getTags().trim().isEmpty())
      {
         List<String> tags = new ArrayList<String>();
         for (String tag : search.getObj().getTags().trim().split(" "))
         {
            if (tag != null && tag.trim().length() > 0)
            {
               tags.add(tag.trim());
            }
         }
         if (tags.size() > 0)
         {
            sb.append(separator).append(" ( ");
            for (int i = 0; i < tags.size(); i++)
            {
               if (i > 0)
               {
                  sb.append(" or ");
               }
               sb.append(" upper ( ").append(alias).append(".tags ) like :tag" + i + " ");
               params.put("tag" + i, likeParam(tags.get(i).trim().toUpperCase()));
            }
            sb.append(" ) ");
            separator = " and ";
         }
      }

      // channel type
      if (search.getObj().getChannelType() != null)
      {
         sb.append(separator).append(alias).append(".channelType = :channelType ");
         params.put("channelType", search.getObj().getChannelType());
         separator = " and ";
      }

   }

   @Override
   protected String getDefaultOrderBy()
   {
      return "id";
   }

   public Long countByName(String name, String id)
   {
      StringBuffer query = new StringBuffer(
               "select count(a) from " + PluginDefinition.class.getSimpleName() + " a where a.name = :name ");
      if (id != null && !id.trim().isEmpty())
      {
         query.append(" and id != :id");
      }
      Query queryObj = getEm().createQuery(query.toString());
      queryObj.setParameter("name", name);
      if (id != null && !id.trim().isEmpty())
      {
         queryObj.setParameter("id", id);
      }
      Long result = (Long) queryObj.getSingleResult();
      logger.info("count: " + result);
      return result;
   }

}
