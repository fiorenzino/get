package es.getdat.plugin.repository;

import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.getdat.api.repository.AbstractRepository;
import es.getdat.api.repository.Search;
import es.getdat.plugin.model.PluginLocation;

@Stateless
@LocalBean
public class PluginLocationRepository extends
		AbstractRepository<PluginLocation> {

	private static final long serialVersionUID = 1L;

	@PersistenceContext
	EntityManager em;

	@Override
	protected EntityManager getEm() {
		return em;
	}

	@Override
	public void setEm(EntityManager em) {
		this.em = em;
	}

	@Override
	protected void applyRestrictions(Search<PluginLocation> search,
			String alias, String separator, StringBuffer sb,
			Map<String, Object> params) {

	}

	@Override
	protected String getDefaultOrderBy() {
		return "id";
	}

}
