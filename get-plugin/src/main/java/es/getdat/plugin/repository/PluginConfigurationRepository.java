package es.getdat.plugin.repository;

import java.util.List;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import es.getdat.api.model.enums.ChannelType;
import es.getdat.api.repository.AbstractRepository;
import es.getdat.api.repository.Search;
import es.getdat.plugin.model.PluginConfiguration;

@Stateless
@LocalBean
public class PluginConfigurationRepository extends
         AbstractRepository<PluginConfiguration>
{

   private static final long serialVersionUID = 1L;

   @PersistenceContext
   EntityManager em;

   @Override
   protected EntityManager getEm()
   {
      return em;
   }

   @Override
   public void setEm(EntityManager em)
   {
      this.em = em;
   }

   @Override
   protected void applyRestrictions(Search<PluginConfiguration> search,
            String alias, String separator, StringBuffer sb,
            Map<String, Object> params)
   {
      // id
      if (search.getObj().getId() != null && search.getObj().getId().trim().length() > 0)
      {
         sb.append(separator).append(alias).append(".id = :id ");
         params.put("id", search.getObj().getId());
         separator = " and ";
      }

      // name
      if (search.getObj().getName() != null
               && !search.getObj().getName().trim().isEmpty())
      {
         sb.append(separator).append(alias).append(".name = :name ");
         params.put("name", search.getObj().getName());
         separator = " and ";
      }

      // description
      if (search.getObj().getDescription() != null
               && !search.getObj().getDescription().trim().isEmpty())
      {
         sb.append(separator).append(" upper ( ").append(alias).append(".description ) like :description ");
         params.put("description", likeParam(search.getObj().getDescription().trim()));
         separator = " and ";
      }

      // pluginDefinitionId
      if (search.getObj().getPluginDefinitionId() != null)
      {
         sb.append(separator).append(alias).append(".pluginDefinitionId = :pluginDefinitionId ");
         params.put("pluginDefinitionId", search.getObj().getPluginDefinitionId());
         separator = " and ";
      }

      // channelType;
      if (search.getObj().getChannelType() != null)
      {
         sb.append(separator).append(alias)
                  .append(".channelType = :channelType ");
         params.put("channelType", search.getObj().getChannelType());
         separator = " and ";
      }
      // userDefault
      if (search.getObj().isSystemDefault())
      {
         sb.append(separator).append(alias)
                  .append(".systemDefault = :systemDefault ");
         params.put("systemDefault", search.getObj().isSystemDefault());
         separator = " and ";
      }
   }

   @Override
   protected String getDefaultOrderBy()
   {
      return "id";
   }

   public String getSystemDefaultPluginConfigurationId(ChannelType channelType)
            throws Exception
   {
      Search<PluginConfiguration> search = new Search<PluginConfiguration>(
               PluginConfiguration.class);
      search.getObj().setChannelType(channelType);
      search.getObj().setSystemDefault(true);
      List<PluginConfiguration> list = getList(search, 0, 1);
      if (list != null && list.size() > 0)
      {
         return list.get(0).getId();
      }
      return null;
   }

   public List<PluginConfiguration> getSystemDefaultPluginConfigurationList()
            throws Exception
   {
      Search<PluginConfiguration> search = new Search<PluginConfiguration>(
               PluginConfiguration.class);
      search.getObj().setSystemDefault(true);
      List<PluginConfiguration> list = getList(search, 0, 0);
      if (list != null && list.size() > 0)
      {
         return list;
      }
      return null;
   }

   public void deleteByPluginDefinitionId(String id) throws Exception
   {
      int deleted = getEm().createNativeQuery(
               "delete from " + PluginConfiguration.class.getSimpleName() + " where pluginDefinitionId = :id ")
               .setParameter("id", id).executeUpdate();
      logger.info(deleted + " " + PluginConfiguration.class.getSimpleName() + " depending on pluginDefinitionId " + id);
   }

}
