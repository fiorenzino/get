package es.getdat.plugin.util;

import es.getdat.api.model.Content;
import es.getdat.plugin.model.PluginConfiguration;

public class PluginConfigurationUtils {

	public static Content getContent(PluginConfiguration pluginConfiguration) {
		Content content = new Content();

		for (String key : pluginConfiguration.getProperties().keySet()) {
			String value = pluginConfiguration.get(key);
			content.put(key, value);
		}
		return content;
	}
}
