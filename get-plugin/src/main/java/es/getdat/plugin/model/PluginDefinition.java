package es.getdat.plugin.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.GenericGenerator;

import es.getdat.api.model.enums.ChannelType;

@Entity
@XmlRootElement
public class PluginDefinition implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;
	private String name;
	private String description;
	private String tags;
	private ChannelType channelType;
	private List<String> properties;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(name = "uuid", unique = true)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Enumerated(EnumType.STRING)
	public ChannelType getChannelType() {
		return channelType;
	}

	public void setChannelType(ChannelType channelType) {
		this.channelType = channelType;
	}

	@ElementCollection(fetch = FetchType.EAGER)
	@Column(name = "name")
	@CollectionTable(name = "PluginDefinition_properties", joinColumns = @JoinColumn(name = "PluginDefinition_id"))
	public List<String> getProperties() {
		if (properties == null)
			this.properties = new ArrayList<>();
		return properties;
	}

	public void setProperties(List<String> properties) {
		this.properties = properties;
	}

	@XmlTransient
	public PluginDefinition add(String property) {
		getProperties().add(property);
		return this;
	}

	@Override
	public String toString() {
		return "PluginDefinition ["
				+ (id != null ? "id=" + id + ", " : "")
				+ (name != null ? "name=" + name + ", " : "")
				+ ", "
				+ (channelType != null ? "channelType=" + channelType + ", "
						: "") + "]";
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

}
