package es.getdat.plugin.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.GenericGenerator;

import es.getdat.api.model.enums.ChannelType;

@Entity
@XmlRootElement
public class PluginConfiguration implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;
	private String name;
	private String description;
	private String pluginDefinitionId;
	private ChannelType channelType;
	private boolean systemDefault;
	private Map<String, String> properties;

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(name = "uuid", unique = true)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPluginDefinitionId() {
		return pluginDefinitionId;
	}

	public void setPluginDefinitionId(String pluginDefinitionId) {
		this.pluginDefinitionId = pluginDefinitionId;
	}

	@Enumerated(EnumType.STRING)
	public ChannelType getChannelType() {
		return channelType;
	}

	public void setChannelType(ChannelType channelType) {
		this.channelType = channelType;
	}

	@ElementCollection(fetch = FetchType.EAGER)
	@MapKeyColumn(name = "name")
	@Column(name = "value")
	@CollectionTable(name = "PluginConfiguration_properties", joinColumns = @JoinColumn(name = "PluginConfiguration_id"))
	public Map<String, String> getProperties() {
		if (properties == null)
			this.properties = new HashMap<String, String>();
		return properties;
	}

	public void setProperties(Map<String, String> properties) {
		this.properties = properties;
	}

	@XmlTransient
	public PluginConfiguration put(String key, String value) {
		getProperties().put(key, value);
		return this;
	}

	@XmlTransient
	public String get(String key) {
		return getProperties().get(key);
	}

	@Override
	public String toString() {
		return "PluginConfiguration ["
				+ (id != null ? "id=" + id + ", " : "")
				+ (name != null ? "name=" + name + ", " : "")
				+ (description != null ? "description=" + description + ", "
						: "")
				+ (pluginDefinitionId != null ? "pluginDefinitionId="
						+ pluginDefinitionId + ", " : "")
				+ (channelType != null ? "channelType=" + channelType + ", "
						: "") + "systemDefault=" + systemDefault + ", "
				+ (properties != null ? "properties=" + properties : "") + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isSystemDefault() {
		return systemDefault;
	}

	public void setSystemDefault(boolean systemDefault) {
		this.systemDefault = systemDefault;
	}

}
