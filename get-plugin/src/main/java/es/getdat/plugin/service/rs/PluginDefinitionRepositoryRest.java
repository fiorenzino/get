package es.getdat.plugin.service.rs;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import es.getdat.api.service.GetRepositoryService;
import es.getdat.core.management.AppConstants;
import es.getdat.plugin.model.PluginDefinition;
import es.getdat.plugin.repository.PluginDefinitionRepository;

@Path(AppConstants.PLUGIN_DEFINITIONS_PATH)
@Stateless
@LocalBean
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PluginDefinitionRepositoryRest extends
         GetRepositoryService<PluginDefinition>
{

   private static final long serialVersionUID = 1L;

   @Inject
   PluginDefinitionRepository pluginDefinitionRepository;

   public PluginDefinitionRepositoryRest()
   {
   }

   @Inject
   public PluginDefinitionRepositoryRest(
            PluginDefinitionRepository pluginDefinitionRepository)
   {
      super(pluginDefinitionRepository);
   }

   @Override
   protected void prePersist(PluginDefinition pluginDefinition)
            throws Exception
   {
      if (pluginDefinition == null)
      {
         throw new Exception("pluginDefinition null");
      }
      if (pluginDefinition.getName() == null
               || pluginDefinition.getName().trim().isEmpty())
      {
         throw new Exception("pluginDefinition name null or empty");
      }
      if (pluginDefinitionRepository.countByName(pluginDefinition.getName(),
               null) > 0)
      {
         throw new Exception("pluginDefinition name already used");
      }
   }

   @Override
   protected void preUpdate(PluginDefinition pluginDefinition)
            throws Exception
   {
      if (pluginDefinition == null)
      {
         throw new Exception("pluginDefinition null");
      }
      if (pluginDefinition.getName() == null
               || pluginDefinition.getName().trim().isEmpty())
      {
         throw new Exception("pluginDefinition name null or empty");
      }
      if (pluginDefinitionRepository.countByName(pluginDefinition.getName(),
               pluginDefinition.getId()) > 0)
      {
         throw new Exception("pluginDefinition name already used");
      }
   }

}
