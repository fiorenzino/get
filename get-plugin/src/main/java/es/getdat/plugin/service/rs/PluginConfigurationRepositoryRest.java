package es.getdat.plugin.service.rs;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import es.getdat.api.model.Content;
import es.getdat.api.service.GetRepositoryService;
import es.getdat.core.management.AppConstants;
import es.getdat.plugin.model.PluginConfiguration;
import es.getdat.plugin.repository.PluginConfigurationRepository;
import es.getdat.plugin.repository.PluginDefinitionRepository;

@Path(AppConstants.PLUGIN_CONFIGURATIONS_PATH)
@Stateless
@LocalBean
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PluginConfigurationRepositoryRest extends
         GetRepositoryService<PluginConfiguration>
{

   private static final long serialVersionUID = 1L;

   @Inject
   PluginConfigurationRepository pluginConfigurationRepository;

   @Inject
   PluginDefinitionRepository pluginDefinitionRepository;

   public PluginConfigurationRepositoryRest()
   {
   }

   @Inject
   public PluginConfigurationRepositoryRest(
            PluginConfigurationRepository pluginConfigurationRepository)
   {
      super(pluginConfigurationRepository);
   }

   @Override
   protected void prePersist(PluginConfiguration pluginConfiguration)
            throws Exception
   {
      if (pluginConfiguration == null)
      {
         throw new Exception("pluginConfiguration null");
      }
      if (pluginConfiguration.getPluginDefinitionId() == null
               || pluginConfiguration.getPluginDefinitionId().trim().isEmpty())
      {
         throw new Exception("pluginConfiguration id null or empty");
      }
      if (pluginDefinitionRepository.find(pluginConfiguration
               .getPluginDefinitionId()) == null)
      {
         throw new Exception("pluginDefinition doesn't exist");
      }
      // TODO: make the override of system configuration for same CHANNEL TYPE:
      // first option: reset the flag of the old system configuration plugin and accept the default plugin
      // secondo option: not accept the new default plugin, untill you don't change the old
   }

   @Override
   protected void preUpdate(PluginConfiguration pluginConfiguration)
            throws Exception
   {
      prePersist(pluginConfiguration);
   }

   @GET
   @Path("/{id}/content")
   public Content get(@PathParam("id") String id) throws Exception
   {
      PluginConfiguration pluginConfiguration = pluginConfigurationRepository
               .find(id);
      if (pluginConfiguration != null)
      {
         return new Content(pluginConfiguration.getProperties());
         // content.put(AppConstants.PLUGINCONFIGURATION_ID, id);
         // for (String key : pluginConfiguration.getProperties().keySet())
         // {
         // String value = pluginConfiguration.getProperties().get(key);
         // content.put(key, value);
         // }
      }
      return null;
   }
}
